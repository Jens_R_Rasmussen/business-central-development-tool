# EV Business Central Development Tool

Tool to manage containers, new environments, update of add-ins, dependecies and fonts.


## Requirements

* Docker Engine

## Commands

### Git Util
* `Git Cleanup Local Branches`: Cleanup local branches to match remote branches.
* `Git Fetch (Prune) And Checkout`: Cleanup local branches to match remote branches & checkout a specific branch.

### BC/Docker Util
* `Add Container Test Users`: Adds default test users (`Essential`, `Basic`, etc)
* `Add Fonts to Container`: Copies fonts from `./fonts` to the container
* `Add New User to container`: Creates a new user to the BC environment
* `Backup App.json`: Backups the current `app.json` into vscode's context. Enables the user to restore `app.json` later.
* `Backup Container Database`: Backups the container to a database (`.bak`-file)
* `Build & Generate Xliff Files`: Builds the solution using AL-language extension and then generates Xliff-files using comments. Read [XLIFF Functionality](#xliff-functionality) for more information.
* `Edit NST Settings in Container`: Opens a gridview for changing NST settings.
* `Edit Web Client Settings in Container`: Opens a gridview for changing web client settings.
* `Get Container Information`: Prints the container information in terminal.
* `Install NST Server Add-ins in Container`: Installs control add-ins in the container (.NET client add-ins).
* `Install or Update BCContainerHelper PowerShell Module`: Installs or updates the bccontainerhelper powershell module
* `Install Test Toolkit in Container`: Installs the Test Toolkit 
* `Open Client`: Opens the webclient of the current workspace container.
* `Open Container Event Log`: Opens the Event Viewer pointing to the container.
* `Open remote Git repository in browser`: Opens the git repository in the users browser.
* `Open Terminal in Container`: Opens powershell instance inside the container.
* `Open Terminal With EV PowerShell Module`: Imports the EV PowerShell module into the terminal.
* `Remove Container`: Removes the container
* `Restart Server Instance in Container`:  Restarts the service tier in the container
* `Restore App.json`: Restores the `app.json` to the state at the point the command `Backup App.json` was run.
* `Restore Container Database`: Reads the backupfolder and allows the user to select a backup to restore the database to.
* `Select Global Settings`: 
* `Select License File to update`: Picks a new license for the container to run with.
* `Show Container Backup Folder`: Shows the folder containing backups (created through `Backup Container Database`).
* `Show Container List`: Show a list of containers present on the current machine.
* `Start Container`: Starts the container of the current workspace.
* `Stop Container`: Stops the container of the current workspace.
* `Update Container Dependencies`: Updates dependencies from the `./dependencies`-folder to the container.
* `Update Development License`: Updates the license with a developer license from your Azure Storage account.

### New Development Environment
All of these end up with a [New Container View](#new-container-view).
* `Clone development Environment from BitBucket`: Clones a repository from bitbucket and opens the request page (if `azure-pipeline.yaml` is available in the repository).
* `Create Container from settings`: Creates a container from the current workspace settings. Requires workspace settings to be available.
* `Create New Development Environment`: Pre-populates Request page with values from the database settings.
* `New Business Centrail Container`: Creates a BC container from the request page.

### Azure Storage
* `Install All Dependency from azure blob storage`: Searches and installs all the dependencies from `app.json` from Azure Blob Storage.
* `Select App to install from Azure Blob Storage`: Gives the user a list of the apps in Azure Blob Storage. The user can then search & pick a dependency to install.
* `Upload App to Azure Blob Storage`: Opens a filepicker to which the user can pick a file an upload to Azure Blob Storage

### EV Compiler Settings
After picking one of these commands, the user will be presented with a picker to choose the EV Compiler Setting to use.
* `Add new app to Fixeddependencies on EVCompilerSettings`: Utilizes the Azure Storage-feature to pick an app to install and add to dependencies of the EV Compiler Settings.
* `Debug without publishing with compiler settings`: Updates `app.json` with the compiler settings picked and runs Debug without publishing.
* `Download Symbols From Compiler Settings`: Updates `app.json` with the compiler settings picked and runs Download Symbols.
* `Init Compiler Settings`: Initialises the `EVCompilerSettings.json` from the current workspace.
* `Package with compiler settings`: Updates `app.json` with the compiler settings picked and runs Package.
* `Publish and open in the designer with compiler settings`: Updates `app.json` with the compiler settings picked and runs Publish and open in the designer.
* `Publish with debugging with compiler settings`: Updates `app.json` with the compiler settings picked and runs Publish with debugging.
* `Publish without debugging with compiler settings`: Updates `app.json` with the compiler settings picked and runs Publish without debugging.
* `Rapid Application Publish with debugging with compiler settings`: Updates `app.json` with the compiler settings picked and runs Rapid Application Publish with debugging.
* `Rapid Application Publish without debugging with compiler settings`: Updates `app.json` with the compiler settings picked and runs Rapid Application Publish without debugging.
* `Show Compiler Settings`: Shows the [Compiler Settings-view](#compiler-settings)
* `Update App.json from compiler settings`: Updates the `app.json` from the compiler settings picked.

### Misc
* `Clear Bitbucket Credentials`: Removes bitbucket credentials from storage - useful for changing user.

## Functionality
The extension includes some functionality which may require more context or a usecase.
Below are some of the functionalities
- [XLIFF functionality](#xliff-functionality)
- [Backup/Restore functionality](#backuprestore-functionality)
- [Differentiated Licensed depending on the container name](#differentiated-licensed-depending-on-the-container-name)
- [Azure Storage integration](#azure-storage-integration)
- [EV Compiler Settings](#ev-compiler-settings-1)

### XLIFF functionality
Generates translations based on the comments in the scope.
In the following example, the caption is English, with a comment specifying the danish translation and in some cases a spanish translation.  
Each of the translations follows the pattern of `<captionml_language_code>="(.*?)"`, where the `captionml_language_code` is the one from Dynamics NAV (e.g. `DAN`, `ENU`, `ESP`).  
More information on the available scopes to use this can be found on [Microsoft Docs - Caption Property](https://docs.microsoft.com/en-us/dynamics365/business-central/dev-itpro/developer/properties/devenv-caption-property).  
![Showcase of XLIFF translation](https://bitbucket.org/Jens_R_Rasmussen/business-central-development-tool/raw/master/Images/xliff.png)

### Backup/restore functionality
Useful for creating a backup of the container databasebase and for restoring it. A usecase could be to perform manual testing. Steps could look as follows:
1. Setup the test database
2. Run the command `Backup Container Database`.
3. Perform the test
4. Run the command `Restore Container Database`, selecting the backup you just made.

### Differentiated Licensed depending on the container version
By setting the `LicensePath`-setting, the user can specify different license depending on the container version. In the example below, version 13, 14, 15, 16, 17, 18, 19 will use a `.flf`-file, while the default is a `.bclicense`-file
![Showcase of LicensePath setting](https://bitbucket.org/Jens_R_Rasmussen/business-central-development-tool/raw/master/Images/LicensePath.png)

### Azure Storage integration
This feature enables the user to hold dependencies in an Azure Storage-account. The feature is controlled by the setting `dependenciesAzureStorage` which specifices the storage account.

### EV Compiler Settings
EV Compiler Settings is the solution to the problem of having to compile an app to multiple different versions.
EV Compiler Settings makes it possible easily switch between multiple `launch.json` and `app.json` files in the same repository. It furthermore enables the user to set variables for their pipeline (If the pipeline is set up for it).
As can be seen below, the file `EVCompilerSettings.json` is an array consisting of objects. These objects have properties `name`, `launch`, `app` and `pipeline`.
* `name` is the name of the compilersettings. It is meant for the user to be easily able to differentiate settings and to be defined in their pipeline.
* `launch` is the properties of `launch.json` to replace when selecting the given EV Compiler Setting.
* `app` is the properties of `app.json` to replace when selecting the given EV Compiler Setting.
* `pipeline` is the properties to use in the pipeline. This could be which version to use when compiling in the pipeline.
There exists a view of the EV Compiler Settings files, which can be found in [Compiler Settings View](#compiler-settings)
![Showcase of `EVCompilerSettings.json`](Images/https://bitbucket.org/Jens_R_Rasmussen/business-central-development-tool/raw/master/EVCompilerSettings.json.png)

## Views
The extension provides multiple views [New Container View](#new-container-view), [Container list View](#container-list-view), [Extension Manager](#extension-manager) and [Compiler Settings](#compiler-settings).

### New Container View
The page for creating a new docker container.  
![View for creating a new container](https://bitbucket.org/Jens_R_Rasmussen/business-central-development-tool/raw/master/Images/NewContainerView.png)

### Container List View
The list of containers currently installed on the machine.  
![Listing of all installed containers on the machine](https://bitbucket.org/Jens_R_Rasmussen/business-central-development-tool/raw/master/Images/ContainerView.png)

### Extension Manager
The list of extensions installed in the container. Can be founder by going to Container List and thrugh the `Action`, clicking `Extension Management`.  
![Extension manager for a specific container](https://bitbucket.org/Jens_R_Rasmussen/business-central-development-tool/raw/master/Images/ExtensionManagement.png)

### Compiler settings
A view to show the different compiler settings. From the bar on top, you can see the different projects's `EVCompilerSettings.json` (Eg. in `App/EVCompilerSettings.json`, `Test/EVCompilerSettings.json`, `Debug/EVCompilerSettings` etc.).
In the left side, you see the different names of EV Compiler Settings of the selected EVCompilerSettings-file.
Once you have selected a EV Compiler Setting, you can right-click and update your `launch.json`, `app.json` or both. You can also copy the setting or edit the setting.
![EV Compiler Setting-view to show the settings](https://bitbucket.org/Jens_R_Rasmussen/business-central-development-tool/raw/master/Images/CompilerSettingsView.png)

## Special Settings which require more context

* `Clone development Environment from BitBucket` uses the settings `bitbucketWorkspaceId`, `bitbucketApiQuerystring` to determine the Bitbucket workspace ID and filter string.

* `Create New Development Environment` uses the settings `useEnvironmentDatabaseServer`, `environmentDatabaseServer`, `environmentDatabaseName`, `environmentOdbcDriver` to determine versions.

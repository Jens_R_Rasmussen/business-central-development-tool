# Change Log

## Version 1.0.33 - 04-11-2022

- Bugfix: Webview not loading resources after VS Code update to version 1.73.0 

## Version 1.0.23 - 08-08-2022

- Bugfix: EV: Git Fetch (Prune) and Checkout and EV: Cleanup Local Branches error loading git repositories in workspace
- It is now possible to define full version on EV: New Business Central Container

## Version 1.0.22 - 13-01-2022

- Clone from Bitbucket load container settings from workspace file
## Version 1.0.21 - 13-01-2022

- Bitbucket Authorization is changed from Basic to OAuth 2.0
- BcContainerHelper will download when needed so you don't need to install PS Module
- tenant is auto set to default on launch file when pulling new development environment
- Bugfix: Installing extension sometimes not loading PS cmd-let
## Version 1.0.20 - 04-11-2021

- Bugfix: Unpublish app from Container List always remove data
- Update container dependecies NULL Array

## Version 1.0.19 - 07-10-2021

- Bugfix: Install App on BC19
- Bugfix: Xliff creation on multiple language, copies from previous language if not exist in comments
- New Snippet for EV Cloud License Codeunit 

## Version 1.0.18 - 21-09-2021

- Bugfix: Container lookup crashes if launch configurations contains cloud settings

## Version 1.0.17 - 06-09-2021

- New Commands: 'Add Container Test Users', 'Create Container from settings'
- New Settings: 'Custom New Bc Container Parameter' to setup custom parameter there will be pasted to New-BcContainer Cmd-Let on creation of an new container can be edit on new container WebView
- New Settings: Store Container Settings: will store container settings on workspace or folder settings and can be used to create a new container
- WebView 'New Container' under include test tool kit new option 'Test Libraries Only'. under Advanced Options new field to New-BcContainer Parameter

## Version 1.0.16 - 06-07-2021

- New Commands: 'Backup Container Database', 'Restore Container Database' and 'Show Container Backup Folder'
- New Settings: 'Use Multitenant' to set default parameter on create container WebView (default value is set to true)
- WebView 'Show Container List' added 'Backup Container Database' and 'Restore Container Database'

## Version 1.0.15 - 02-06-2021

- New Commands: 'Open Client', 'Restart Server Instance in Container' and 'Install Test Toolkit in Container'
- Settings property 'Cleanup Local Branches Confirm Dialog' default set to true
- New Settings 'Configuring Business Central Server' and 'Configuring Business Central WebServer Instances' where you can define settings there will be updatet on creation of container
- If settings 'Cleanup Local Branches Confirm Dialog' is set to true then branch is deleted with Force
- Bugfix: Install license file
- Bugfix: 'Cleanup Local Branches' command now force delete if confirm delete
- Bugfix: 'Git Fetch (Prune) And Checkout' only needs to be in a Git Repository, no open file needed 

## Version 1.0.14 - 28/02/2021

- Bugfix "EV: Install NST Server Add-ins in Container" sometimes create folder as a file in container
- Bugfix Cmd-let Add-EVDependenciesToAppFile fails if no dependencies already in app.json
- "EV: Cleanup Local Branches" & "EV: Git Fetch (Prune) And Checkout" commands are rewriten and added to activation events
- ev-tools.setMandatortStaging default value changed from true to false
- Update Dependencies Added to WebView default value false can be set in settings ev-tools.updateDependenciesOnNewDevelopmentEnvironment

## Version 1.0.13 - 29/12/2020

- Bugfix sync dependencies on BC 13
- BakFile added to New Business Central Container

## Version 1.0.12 - 23/11/2020

- Bugfix Stop container command runs Start-EVContainer
- Bugfix Upgrade extension uninstall all extensions there is dependent on the extension 
- WebClientPort added to New Business Central Container 

## Version 1.0.11 - 17/11/2020

- Bugfix loading command ev-cleanup-local-branches  

## Version 1.0.10 - 11/11/2020

- It is now possible to setup license file paths to major versions
- Create New Development Environment now uses the a WebView as dialog where you can change container settings before continue  
- New feature Create Development Environment From BitBucket
- Install/Update Extensions from Extension management rewriten 
- Bugfix update Add-ins with BcContainerHelper
- Bugfix Transparency on Extension management is removed from Container List Webview  
- Default ODBC Driver set to 'SQL Server' on new development environment server connection
- If you dont use Environment Database Server the dialog is now changed to a WebView

## Version 1.0.9 - 01/09/2020

- Bugfix could not load on vscode version 1.49 node_module msnodesqlv8 is replaced by node_module odbc

## Version 1.0.8 - 01/09/2020

- Improved WebViev Container List
- Added WebView New Business Central Container
- Changed from using NavContainerHelper module to BcContainerHelper Module

## Version 1.0.7 - 27/05/2020

- Improved WebViev Container List
- Added Extension Management from Container List
- Added Install NST Server Add-ins from Container List
- Added Install Fonts from Container List

## Version 1.0.6 - 06/03/2020

- WebView Local NAV/Business Central Container with start, stop, open Client, Update License, Install App and Delete functions

## Version 1.0.3 - 31/01/2020

- Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process in EV SCM terminal

## Version 1.0.2 - 31/01/2020

- Open terminal in container
- Open EV SCM terminal

## Version 1.0.1 - 28/01/2020

- EVBusinessCentralDevelopmentHelper PowerShell Module included 

## Version 1.0.0 - 26/01/2020

- First Version
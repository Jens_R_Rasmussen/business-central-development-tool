import { commands, ExtensionContext, window, workspace } from 'vscode';
import { connect, NodeOdbcError, Connection, Result } from 'odbc';
import { NewContainer } from './webViews/newContainerWebView';
import { getLicensePath } from './getLicenseFile';
import { ContainerParameter, getDefaultContainerParameter, convertToBoolean } from './dockerLibrary';
import * as constants from './constants';

export class NewDevEnvironment {
    context: ExtensionContext;
    constructor(context: ExtensionContext) {
        this.context = context;
        this.registreCommands();
    }
    registreCommands() {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(commands.registerCommand(constants.newDevelopmentEnvironmentCommand, () => this.newDevEnvironment()));
    }
    newDevEnvironment() {
        let config = workspace.getConfiguration(constants.extensionConfig);
        let useServer = config.get(constants.useEnvironmentDatabaseServerSettings);
        if (useServer) {
            this.executeNewDevEnvironmentCommandFromServer();
        }
        else {
            this.executeNewDevEnvironmentCommandFromUserInput();
        }
    }
    executeNewDevEnvironmentCommandFromUserInput() {
        let newEnvironment = new NewContainer(this.context, constants.newDevelopmentEnvironmentPanelTitle);
        newEnvironment.show(true);
    }
    executeNewDevEnvironmentCommandFromServer() {
        let config = workspace.getConfiguration(constants.extensionConfig);
        // https://www.microsoft.com/en-us/download/details.aspx?id=50402
        let connectionString = `Server=${config.get(constants.environmentDatabaseServerSettings)};Database=${config.get(constants.environmentDatabaseNameSettings)};Trusted_Connection=Yes;Driver={${config.get(constants.environmentOdbcDriverSettings)}}`;
        let query = "SELECT EnvironmentName, JiraID, CustomerName, BCVersion, CUVersion, SQLServer, DBName, RepositoryURL, Country, YearMonthVersion FROM DevelopmentEnvironments";
        let command = undefined;
        connect(connectionString).then((connection: Connection) => {
            connection.query(query).then((result: Result<any>) => {
                let quickPick = window.createQuickPick();
                quickPick.items = result.map((repo: any) => ({ label: `${repo.CustomerName}-${repo.EnvironmentName}`, detail: `Version: ${repo.BCVersion}-${repo.CUVersion}-${repo.Country}`.toLowerCase(), description: repo.RepositoryURL, row: repo }));
                quickPick.onDidChangeSelection(([item]: any) => {
                    let row = item.row;
                    if (row.CUVersion.toUpperCase() === 'RTM') {
                        row.CUVersion = '0';
                    } else {
                        row.CUVersion = row.CUVersion.match(/[0-9]{1,3}/);
                    }
                    let artifactVersion = `${row.BCVersion}.${row.CUVersion}`;
                    let licenseFile = getLicensePath(row.BCVersion);
                    let containerParameter: ContainerParameter = getDefaultContainerParameter();
                    containerParameter.name = row.EnvironmentName;
                    containerParameter.version = artifactVersion;
                    containerParameter.country = row.Country.toLowerCase();
                    containerParameter.licensefile = licenseFile;
                    containerParameter.repository = row.RepositoryURL;
                    containerParameter.updateDependencies = convertToBoolean(config.get(constants.updateDependenciesOnNewDevelopmentEnvironmentSettings));

                    let newEnvironment = new NewContainer(this.context, `${constants.continueCreationOfContainerToRepositoryPanalTitle}: ${containerParameter.name}`);
                    newEnvironment.showDevelopmentContainer(containerParameter, '');
                    quickPick.dispose();
                });
                quickPick.onDidHide(() => quickPick.dispose());
                quickPick.show();
            }).catch((error: NodeOdbcError) => {
                let errMessage = error.odbcErrors[0].message;
                window.showErrorMessage(`New Development Environment: ${error.message}\n\n${errMessage}`);
            });
        }).catch((error: NodeOdbcError) => {
            let errMessage = error.odbcErrors[0].message;
            window.showErrorMessage(`New Development Environment: ${error.message}\n\n${errMessage}`);
        });
    }
    dispose() {
    };
}
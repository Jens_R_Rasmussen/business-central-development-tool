import * as vscode from 'vscode';
import { OutputChannel } from './outputChannel';
import { parse, j2xParser, X2jOptions, J2xOptions } from 'fast-xml-parser';
import * as fs from 'fs';
import * as path from 'path';
import { decode } from 'he';
import { getAppManifest, getCurrentWorkspaceFolder } from './workspaceFileAndFolderHelper';
import * as constants from './constants';

const oldLanguageCodes = ["SVE", "RUS", "NLD", "NLB", "NOR", "ITA", "ITS", "ISL", "FRA", "FRS", "FRC", "FRB", "FIN", "ESM", "ESP", "ENU", "ENZ", "ENG", "ENC", "ENA", "DEU", "DES", "DEA", "DAN", "CSY"];
const newLanguageCodes = ["sv-SE", "ru-RU", "nl-NL", "nl-BE", "nb-NO", "it-IT", "it-CH", "is-IS", "fr-FR", "fr-CH", "fr-CA", "fr-BE", "fi-FI", "es-MX", "es-ES", "en-US", "en-NZ", "en-GB", "en-CA", "en-AU", "de-DE", "de-CH", "de-AT", "da-DK", "cs-CZ"];
const regexEscape = /[&<>`]/g;
const escapeMap: { [char: string]: string } = {
    '&': "&amp;",
    '<': "&lt;",
    '>': "&gt;",
    '`': "&#x60;"
};
const jsonOptions: X2jOptions = {
    attributeNamePrefix: "",
    attrNodeName: "attribute",
    textNodeName: "#text",
    ignoreAttributes: false,
    ignoreNameSpace: false,
    allowBooleanAttributes: false,
    parseNodeValue: true,
    parseAttributeValue: false,
    trimValues: false,
    cdataTagName: "__cdata",
    cdataPositionChar: "\\c",
    parseTrueNumberOnly: false,
    attrValueProcessor: (val: any, tagName: any) => decode(val, { isAttributeValue: true }),
    tagValueProcessor: (val: any, attrName: any) => trimIfNewLine(attrName, decode(val)),
    stopNodes: ["parse-me-as-string"],
    arrayMode: false,
    numParseOptions: {
        hex: false,
        leadingZeros: false
    },
    alwaysCreateTextNode: false
};

const xmlOptions: J2xOptions = {
    attributeNamePrefix: "",
    attrNodeName: "attribute",
    textNodeName: "#text",
    ignoreAttributes: true,
    cdataTagName: "__cdata",
    cdataPositionChar: "\\c",
    format: true,
    indentBy: "  ",
    supressEmptyNode: true,
    tagValueProcessor: (a: any) => escape(a),
    attrValueProcessor: (a: any) => escape(a)
};

export class Xliff {
    context: vscode.ExtensionContext;
    constructor(context: vscode.ExtensionContext) {
        this.context = context;
        this.registerCommands();
    }
    registerCommands() {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(vscode.commands.registerCommand(constants.generateXliffFilesCommand, () => this.createXliffFilesFromComments()));
    }
    createXliffFilesFromComments() {
        let service = this.getMicrosoftAlExtensionService('BuildService');
        if (service === undefined) {
            vscode.window.showErrorMessage('BuildService could not be loaded from AL Language Extension');
        }
        service.packageContainer(false).then((result: any) => {
            if (result) {
                this.createXliffFiles();
            }
        }).catch(() => {
            vscode.window.showErrorMessage('The package could not be created. No Xliff files created');
        });
    }

    async createXliffFiles() {
        OutputChannel.createChannal('Xliff');
        OutputChannel.appendLine('Xliff', `${getTimeStamp()} Start generating Xliff files`);
        let appfilePath = getCurrentWorkspaceFolder();
        let app = getAppManifest();
        OutputChannel.appendLine('Xliff', `Load file: '${app.name}.g.xlf'`);
        let xliffFile = await findTranslationFiles(appfilePath, `**/${app.name}`);
        if (xliffFile === undefined) {
            return;
        }
        let translationPath = path.dirname(xliffFile);
        for (let i = 0; i < newLanguageCodes.length; i++) {
            let code = newLanguageCodes[i];
            await vscode.workspace.findFiles(new vscode.RelativePattern(translationPath, `*${code}*`)).then((file) => {
                if (file.length >= 1) {
                    fs.unlinkSync(file[0].fsPath);
                    console.log(`Deleted: ${file[0].fsPath}`);
                }
            });
        };
        fs.readFile(xliffFile, "utf-8", function (err, data) {
            if (err) {
                OutputChannel.appendLine('Xliff', err.message);
                vscode.window.showErrorMessage(err.message);
                return;
            }
            OutputChannel.appendLine('Xliff', 'File Loaded');
            let origTranslation = parse(data.toString(), jsonOptions);
            let newTranslation = JSON.parse(JSON.stringify(origTranslation));
            for (let i = 0; i < newLanguageCodes.length; i++) {
                if (data.toString().includes(`${oldLanguageCodes[i]}=`)) {
                    newTranslation['xliff']['file']['body']['group']['trans-unit'] = [];
                    OutputChannel.appendLine('Xliff', `\n${getTimeStamp()} Generate Language File: ${newLanguageCodes[i]}`);
                    newTranslation['xliff']['file']['attribute']['target-language'] = newLanguageCodes[i];
                    origTranslation['xliff']['file']['body']['group']['trans-unit'].forEach((element: any) => {
                        let nodetext: string | undefined;
                        element['note'].forEach((note: any) => {
                            if (note['attribute']['from'] === 'Developer') {
                                nodetext = note['#text'];
                            }
                        });
                        let expresion = new RegExp(`${oldLanguageCodes[i]}="(.*?)"`);
                        if (nodetext !== undefined) {
                            let group = nodetext.match(expresion);
                            if (group !== null) {
                                element.target = group[1];
                                newTranslation['xliff']['file']['body']['group']['trans-unit'].push(element);
                            }
                            else {
                                expresion = new RegExp(`${oldLanguageCodes[i]}=(.+)|${oldLanguageCodes[i]} = (.+)`);
                                let group = nodetext.match(expresion);
                                if (group !== null) {
                                    if (group[1] !== undefined) {
                                        element.target = group[1];
                                    } else {
                                        element.target = group[2];
                                    }
                                    newTranslation['xliff']['file']['body']['group']['trans-unit'].push(element);
                                }
                            }
                            if (element.target !== undefined) {
                                OutputChannel.appendLine('Xliff', `Processing: '${nodetext}' result: '${element.target}'`);
                            }
                        }
                    });
                    let fastJsonParser = new j2xParser(xmlOptions);
                    try {
                        var xml = fastJsonParser.parse(newTranslation);
                    } catch (err: any) {
                        vscode.window.showErrorMessage(err);
                        return undefined;
                    }
                    let header = '<?xml version="1.0" encoding="utf-8"?>\n';
                    header += '<!-- Generated By Elbek & Vejrup Business Central Dev Tool -->\n';
                    header += xml;
                    let outputFileName = `${translationPath}\\${app.name}.${newLanguageCodes[i]}.g.xlf`;
                    fs.writeFile(outputFileName, header, function (err) { });
                    console.log(`Created: ${outputFileName}`);
                }
            }
            OutputChannel.appendLine('Xliff', `${getTimeStamp()} Done!`);
        });
    }
    getMicrosoftAlExtensionService(serviceName: string) {
        let alLanguageExt = vscode.extensions.getExtension('ms-dynamics-smb.al');
        if (alLanguageExt === undefined) {
            return undefined;
        }
        // return alLanguageExt.exports.services.find((element: any) => (element.constructor.name === serviceName));
        let extensionService = alLanguageExt.exports.services.find((element: any) => (element[serviceName] !== undefined));
        return extensionService[serviceName];
    }
    dispose() {

    }
}

async function findTranslationFiles(filePath: any, searchFileNameContent: string) {
    let files = await (findFiles(filePath, `${searchFileNameContent}.g.xlf`));
    let file = files.find((file: { scheme: string; }) => file.scheme === 'file');
    if (file !== undefined) {
        return file.path.substr(1, 1024);
    }
    return undefined;
}

function findFiles(workspaceFolder: any, searchFilter: string) {
    let relativePattern = new vscode.RelativePattern(workspaceFolder, searchFilter);
    return vscode.workspace.findFiles(relativePattern).then((files) => files.sort((a, b) => {
        if (a.fsPath.length !== b.fsPath.length) {
            return a.fsPath.length - b.fsPath.length;
        }
        return a.fsPath.localeCompare(b.fsPath);
    }));
}

function trimIfNewLine(name: any, value: string) {
    if (value.includes('\n')) {
        return value.trim();
    }
    return value;
}

function escape(string: string) {
    try {
        return string.replace(regexEscape, ($0) => escapeMap[$0]);
    } catch (err) {
        return `${string}`;
    }

}
function getTimeStamp() {
    let date = new Date();
    return `[${appendLeadingZero(date.getDate())}-${appendLeadingZero(date.getMonth() + 1)}-${date.getFullYear()} ${appendLeadingZero(date.getHours())}:${appendLeadingZero(date.getMinutes())}:${appendLeadingZero(date.getSeconds())}.${appendLeadingZeroes(date.getMilliseconds())}]`;
    function appendLeadingZero(n: number) {
        if (n <= 9) {
            return "0" + n;
        }
        return n;
    }
    function appendLeadingZeroes(n: number) {
        if (n <= 9) {
            return "00" + n;
        }
        if (n <= 99) {
            return "0" + n;
        }
        return n;
    }
}
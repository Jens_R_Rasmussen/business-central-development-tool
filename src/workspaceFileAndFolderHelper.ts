import { TextDocument, Uri, window, workspace, WorkspaceFolder } from 'vscode';
import { existsSync, readFileSync, writeFileSync } from 'fs';
import * as constants from './constants';
import { CompilerSettings, DependencyTarget, InstallSourceType } from './CompilerSettings/compilerSettings';

export function getCurrentWorkspaceFolder(): WorkspaceFolder | undefined {
    if (!Array.isArray(workspace.workspaceFolders) || workspace.workspaceFolders.length === 0) {
        return undefined;
    }
    if (workspace.workspaceFolders.length === 1) {
        return workspace.workspaceFolders[0];
    }
    const activeEditor = window.activeTextEditor;
    if (activeEditor) {
        if (activeEditor.document && isPossibleProjectFile(activeEditor.document)) {
            return getWorkspaceFolderFromPath(activeEditor.document.fileName);
        }
    }
    const visibleTextEditors = window.visibleTextEditors;
    if (visibleTextEditors && visibleTextEditors.length > 0) {
        for (const e of visibleTextEditors) {
            if (isPossibleProjectFile(e.document)) {
                return getWorkspaceFolderFromPath(e.document.fileName);
            }
        }
    }
    return undefined;
}
function getAppSourceCopFilePath(file: string | undefined): string {
    return `${getWorkspaceUri(file)?.fsPath}\\${constants.appSourceCopFileName}`;
}
export function getAppSourceCop(file: string | undefined = undefined): any {
    let path = getAppSourceCopFilePath(file);
    if (!existsSync(path)) {
        return undefined;
    }
    let data = readFileSync(path);
    return JSON.parse(data.toString("utf-8"));
}
export function setAppSourceCop(appSourceCop: any, file: string | undefined = undefined) {
    let uri = getWorkspaceUri(file);
    writeFileSync(`${uri?.fsPath}\\${constants.appSourceCopFileName}`, JSON.stringify(appSourceCop, undefined, 4));
}
export function getAppManifest(file: string = ''): any {
    let appfilePath: any;
    if (file !== '') {
        appfilePath = getWorkspaceFolderFromPath(file);
    } else {
        appfilePath = getCurrentWorkspaceFolder();
    }
    if (appfilePath === undefined) {
        return undefined;
    }
    let data = readFileSync(`${appfilePath.uri.fsPath}\\app.json`);
    return JSON.parse(data.toString("utf-8"));
}
export function getWorkspaceUri(file: string | undefined): Uri | undefined {
    if (file !== undefined) {
        return getWorkspaceFolderFromPath(file)?.uri;
    }
    return getCurrentWorkspaceFolder()?.uri;
}
export function setAppManifest(app: any, file: string = '') {
    let appfilePath: any;
    if (file !== '') {
        appfilePath = getWorkspaceFolderFromPath(file);
    } else {
        appfilePath = getCurrentWorkspaceFolder();
    }
    writeFileSync(`${appfilePath.uri.fsPath}\\app.json`, JSON.stringify(app, undefined, 4));
}
function isPossibleProjectFile(document: TextDocument) {
    return (document.languageId === "al" || document.languageId === "json" || document.languageId === "jsonc" ||
        document.uri.scheme === "file") && existsSync(document.fileName);
}
export function getWorkspaceFolderFromPath(path: string) {
    const uri = Uri.file(path);
    return workspace.getWorkspaceFolder(uri);
}
export function setEVCompilerSettings(filePath: string, compilerSettingsList: CompilerSettings[]) {
    writeFileSync(getCompilerFilePath(filePath), JSON.stringify(compilerSettingsList, replacer, 4));
}

export function getEVCompilerSettings(filePath: string): CompilerSettings[] {
    let compilerFilePath = getCompilerFilePath(filePath);
    if (!existsSync(compilerFilePath)) {
        let compilerSettingsList: CompilerSettings[] = [];
        return compilerSettingsList;
    }
    let data = readFileSync(compilerFilePath);
    let compilerSettingsList: CompilerSettings[] = JSON.parse(data.toString("utf-8"));
    return compilerSettingsList;
}

export function getCompilerFilePath(filePath: string) {
    let appfilePath = getWorkspaceFolderFromPath(filePath);
    if (!appfilePath) {
        return constants.compilerFileName;
    }
    return `${appfilePath.uri.fsPath}\\${constants.compilerFileName}`;
}
function replacer(key: any, value: any): any {
    if (typeof value === 'number') {
        if (key === 'dependencyTarget') {
            return DependencyTarget[value];
        }
        if (key === 'type') {
            return InstallSourceType[value];
        }
    }
    return value;
}
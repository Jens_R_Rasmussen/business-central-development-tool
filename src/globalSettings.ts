import * as constants from './constants';
import { extensions, commands, ExtensionContext, window, workspace, ConfigurationTarget, Uri } from "vscode";
import { existsSync, readFileSync, statSync } from 'fs';
import { OutputChannel } from './outputChannel';

export class GlobalSettings {
    context: ExtensionContext;
    channelName: string = 'Global Settings';
    globalStateName: string = 'GlobalSettingsModifiedTime';
    isActive: boolean;
    requestUpdateAllowedUNCHosts: boolean = false;
    constructor(context: ExtensionContext) {
        this.context = context;
        this.registerCommands();
        this.isActive = false;
        this.activateGlobalSettings();
    }
    autoUpdate() {
        if (!this.isActive) {
            return;
        }
        let modifiedTime = statSync(<string>this.getPath()).mtime.toISOString();
        let lastMotifiedTime = this.context.globalState.get(this.globalStateName);
        if (modifiedTime !== lastMotifiedTime) {
            let properties = <string[]>workspace.getConfiguration(constants.extensionConfig).get(constants.globalSettingsAutoUpdatePropertiesSettings);
            if (properties.length !== 0) {
                OutputChannel.createChannal(this.channelName);
                let data = JSON.parse(readFileSync(<string>this.getPath()).toString("utf-8"));
                properties.forEach((element: string) => {
                    let configName = element.split('.')[0];
                    let property = element.split('.')[1];
                    let value = data[element];
                    if (value) {
                        this.updateProperty(configName, property, value, element);
                    }
                });
            }
            this.context.globalState.update(this.globalStateName, modifiedTime);
        }
    }
    private updateProperty(configName: string, property: string, value: any, element: string) {
        let config = workspace.getConfiguration(configName);
        if (config.has(property)) {
            config.update(property, value, ConfigurationTarget.Global);
            OutputChannel.appendLine(this.channelName, `Copy property: '${element}' from global settings`);
            OutputChannel.appendLine(this.channelName, `Value: ${JSON.stringify(value, undefined, 2)}`);
            OutputChannel.appendLine(this.channelName, '');
        } else {
            OutputChannel.appendLine(this.channelName, `Property: '${element}' is not valid in you setup`);
            OutputChannel.appendLine(this.channelName, '');
        }
    }

    registerCommands() {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(commands.registerCommand(constants.selectGlobalSettingsCommand, () => this.selectGlobalSettings()));
        subscriptions.push(window.onDidChangeActiveTextEditor((e: any) => this.activateGlobalSettings()));
    }
    async selectGlobalSettings() {
        let path = this.getPath();
        if (path === undefined) {
            return;
        }
        let data = readFileSync(path);
        let list: any = [];
        Object.entries(JSON.parse(data.toString("utf-8"))).forEach(([key, value]) => {
            console.log(`${key}: ${value}`);
            let config = key.split('.')[0];
            let property = key.split('.')[1];
            list.push({ prop: key, value: value, config: config, property: property });
        });
        if (list.length === 0) {
            window.showInformationMessage('Empty List');
        }
        let result = await window.showQuickPick(list.map((element: any) => ({ label: element.prop, data: element })), { placeHolder: 'Select Properties to copy', canPickMany: true });
        if (result?.length !== 0) {
            OutputChannel.createChannal(this.channelName);
        }
        result?.forEach((element: any) => {
            this.updateProperty(element.data.config, element.data.property, element.data.value, element.label);
        });
    }
    activateGlobalSettings() {
        let path: string | undefined = this.getPath();
        if (path !== undefined) {
            if (!this.requestUpdateAllowedUNCHosts) {
                if (path.startsWith('\\\\')) {
                    let host = path.split('\\')[2];
                    let allowedUNCHosts: string[] | any = workspace.getConfiguration('security').get('allowedUNCHosts');
                    if (!allowedUNCHosts.includes(host)) {
                        this.requestUpdateAllowedUNCHosts = true;
                        window.showInformationMessage(`Global settings needs access to '${host}' do you want to add this to allowed UNC Hosts?`, { modal: false }, 'OK').then((result) => {
                            if (result === 'OK') {
                                allowedUNCHosts.push(host);
                                workspace.getConfiguration('security').update('allowedUNCHosts', allowedUNCHosts, ConfigurationTarget.Global);
                            }
                        });
                    }
                }
            }
            this.isActive = existsSync(path);
        }
        commands.executeCommand('setContext', constants.eVGlobalSettingsExist, this.isActive);
    }
    getPath(): string | undefined {
        return workspace.getConfiguration(constants.extensionConfig).get(constants.globalSettingsPathSettings);
    }
    dispose() {

    }
}

// function selectGlobalSettings(): any {
//     window.showInformationMessage('Function not implemented.');

//     let data = readFileSync(`${appfilePath.uri.fsPath}\\app.json`);
//     return JSON.parse(data.toString("utf-8"));


//     Object.entries(compilerSettings.launch).forEach(([key, value]) => {
//         console.log(`${key}: ${value}`);
//         containerConfigs[key] = value;
    // });
// }


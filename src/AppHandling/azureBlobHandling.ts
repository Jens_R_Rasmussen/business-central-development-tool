import { commands, ExtensionContext, workspace, window, Uri, OpenDialogOptions } from 'vscode';
import * as constants from '../constants';
import * as fetch from "node-fetch";
import { parse } from 'fast-xml-parser';
import * as crypto from 'crypto';
import { decode } from 'he';
import { pipeline } from "stream";
import { promisify } from "util";
import { copyFile, createWriteStream, readFileSync } from "fs";
import { tmpdir } from "os";
import { DockerContainer } from "../dockerLibrary";
import { Terminal } from "../executeLibrary";
import { AppInfo, BlobInfo, getAppInfoFromFile } from "./AppInfo";
import { getAppManifest, getAppSourceCop, getCurrentWorkspaceFolder, setAppManifest, setAppSourceCop } from "../workspaceFileAndFolderHelper";
import { FixedDependenciesInstall, InstallSourceType } from "../CompilerSettings/compilerSettings";
import { Compiler } from "../CompilerSettings/compiler";
import { OutputChannel } from "../outputChannel";
import path from 'path';

export class AzureBlobHandling {
    context: ExtensionContext;
    private baseAzureUrl = 'blob.core.windows.net';
    private dockerContainer: DockerContainer;
    constructor(context: ExtensionContext) {
        this.context = context;
        this.registreCommands();
        this.dockerContainer = new DockerContainer(context);
    }
    private registreCommands(): void {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(commands.registerCommand(constants.installDependencyFromAzureStorageCommand, () => this.installDependencyFromAzureStorage()));
        subscriptions.push(commands.registerCommand(constants.installAllDependencyFromAzureStorageCommand, () => this.installAllDependencyFromAzureStorage()));
        subscriptions.push(commands.registerCommand(constants.uploadAppToAzureStorageCommand, () => this.uploadAppToAzureStorage()));
        subscriptions.push(commands.registerCommand(constants.addAppToFixeddependenciesOnEVCompilersettingsCommand, () => this.addAppToFixeddependenciesOnEVCompilerSettings()));
        subscriptions.push(commands.registerCommand(constants.eVAddLatestVersionToAppSourceCopCommand, () => this.addLatestVersionToAppSourceCop()));
    }
    async addLatestVersionToAppSourceCop() {
        let appInfo = getAppManifest();
        if (appInfo === undefined) {
            return;
        }
        let account = await this.selectAccount();
        if (account === undefined) {
            return;
        }
        let where = `&where=id %3D '${appInfo.id}' AND searchVersion >%3D '${this.getSearchVersion(appInfo.version)}' AND microsoftApplicationVersion <%3D '${createSearchVersion(appInfo.application)}'`;
        let query = `restype=container&comp=blobs${where}&maxresults=10`;
        let req = this.buildRequset(account, 'GET', query);
        fetch.default(req.url, req.request).then(async (response) => {
            let responseStatus = { code: response.status, text: response.statusText };
            if (responseStatus.code !== 200) {
                window.showErrorMessage(responseStatus.text);
                return;
            }
            let body = await response.text();
            let content = parse(body, {
                tagValueProcessor: ((tagValue, tagName) => {
                    return decode(tagValue);
                })
            });
            try {
                var list = content.EnumerationResults.Blobs.Blob.map((value: any) => (this.getAppInfoFromBlobList(value)));
            } catch (e) {
                console.log(e);
            }
            while (content.EnumerationResults.NextMarker !== '') {
                try {
                    content = await this.getNextMarker(account, req.url, content.EnumerationResults.NextMarker);
                } catch (e: any) {
                    throw new Error(e);
                }
                try {
                    list.push(...content.EnumerationResults.Blobs.Blob.map((value: any) => (this.getAppInfoFromBlobList(value))));
                } catch {
                    let value = content.EnumerationResults.Blobs.Blob;
                    list.push(this.getAppInfoFromBlobList(value));
                }
            }

            let newList: Array<any> = list.sort(this.compareSemanticVersions('version'));
            let blobInfo = newList.reverse()[0];
            this.downloadBlob(blobInfo.blob, account).then((filepath) => {
                this.updateAppSourceCop(filepath);
            });
        });


    }
    updateAppSourceCop(filepath: string) {
        getAppInfoFromFile(filepath).then((appInfo) => {
            let appSourceCop = getAppSourceCop();
            appSourceCop.name = appInfo.name;
            appSourceCop.publisher = appInfo.publisher;
            appSourceCop.version = appInfo.version;
            let folder: string = getCurrentWorkspaceFolder() ? <string>getCurrentWorkspaceFolder()?.uri.fsPath : '';
            let newpath = path.join(folder, appSourceCop.baselinePackageCachePath, `${appInfo.name} ${appInfo.version}.app`);
            copyFile(appInfo.file, newpath, () => { });
            setAppSourceCop(appSourceCop);
        });
    }
    getSearchVersion(version: any) {
        let majorVersion = Number.parseInt(version.split('.')[0]) - 1;
        return createSearchVersion(`${majorVersion}.0.0.0`);
    }
    private compareSemanticVersions(key?: string) {
        return (a: any, b: any) => {
            // 1. Split the strings into their parts.
            let a1;
            let b1;

            if (key) {
                a1 = a[key].split('.');
                b1 = b[key].split('.');
            } else {
                a1 = a.split('.');
                b1 = b.split('.');
            }
            // 2. Contingency in case there's a 4th or 5th version
            const len = Math.min(a1.length, b1.length);
            // 3. Look through each version number and compare.
            for (let i = 0; i < len; i++) {
                const a2 = +a1[i] || 0;
                const b2 = +b1[i] || 0;

                if (a2 !== b2) {
                    return a2 > b2 ? 1 : -1;
                }
            }

            // 4. We hit this if the all checked versions so far are equal
            return b1.length - a1.length;
        };
    }
    private getAppInfoFromBlobList(value: any) {
        let tags = value.Tags.TagSet.Tag;
        let id = tags.findLast((element: any) => element.Key === 'id').Value;
        let version = tags.findLast((element: any) => element.Key === 'searchVersion').Value;
        return { id: id, version: version, blob: value.Name };
    }

    getNextMarker(account: AzureStorageAccount, url: string, marker: any): any {
        return new Promise<any>((resolve, reject) => {
            let query = `${url.split('?')[1]}&marker=${marker}`;
            let req = this.buildRequset(account, 'GET', query);
            fetch.default(req.url, req.request).then((response) => {
                let responseStatus = { code: response.status, text: response.statusText };
                if (responseStatus.code !== 200) {
                    reject(responseStatus.text);
                }
                else {
                    response.text().then((value) => {
                        resolve(parse(decode(value)));
                    });
                }
            });
        });
    }
    addBlobToFixeddependencies(account: AzureStorageAccount) {
        this.selectBlob(account).then((blob) => {
            this.downloadBlob(blob, account).then((filepath) => {
                this.addAppFileToEVCompilerSettings(filepath, blob, account);
            }).catch((reason) => {
                window.showErrorMessage(reason);
            });
        }).catch((reason) => {
            window.showErrorMessage(reason);
        });
    }
    uploadBlob(account: AzureStorageAccount, blobInfo: BlobInfo): void {
        let fileContent = readFileSync(blobInfo.fullpath);
        let blob = `${blobInfo.publisher}/${blobInfo.name}/${blobInfo.version}/${blobInfo.filename}`;
        let { request, url }: { request: fetch.RequestInit; url: string; } = this.buildRequset(account, 'PUT', '', blob, fileContent.byteLength.toString(), blobInfo);
        request.body = fileContent;
        console.log(url, request);
        fetch.default(url, request).then((response) => {
            let responseStatus = { code: response.status, text: response.statusText };
            if (responseStatus.code !== 201) {
                let errorMessage = `Uploading Blob to blob storage failed with the following error: Errorcode: ${responseStatus.code}, Errormessage: ${responseStatus.text}`;
                OutputChannel.appendLine(account.name, errorMessage);
                window.showErrorMessage(errorMessage);
                return;
            }
            let infoMessage = `App: ${blobInfo.name} Version: ${blobInfo.version} is uploaded sussesfull to storeage account: ${account.account}`;
            OutputChannel.appendLine(account.name, infoMessage);
            window.showInformationMessage(infoMessage);
        }).catch(err => {
            let errorMessage = `Uploading Blob to blob storage failed with the following error: Errorcode: ${err.code}, Errormessage: ${err.message}`;
            OutputChannel.appendLine(account.name, errorMessage);
            window.showErrorMessage(errorMessage);
            console.log(err);
        });
    }
    uploadAppToBlob(account: AzureStorageAccount) {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        let dialogOptions: OpenDialogOptions = { canSelectFiles: true, canSelectFolders: false, canSelectMany: true, filters: { 'AppFiles': ['app'] }, title: 'Select App Files to upload' };
        window.showOpenDialog(dialogOptions).then((result) => {
            if (result !== undefined) {
                OutputChannel.createChannal(account.name);
            }
            result?.forEach((element: Uri) => {
                this.uploadApp(account, element);
            });
        });
    }
    private async uploadApp(account: AzureStorageAccount, element: Uri) {
        getAppInfoFromFile(element.fsPath).then((appInfo) => {
            let fileName = element.path.split('/').pop();
            let blobInfo: BlobInfo = {
                id: appInfo.id,
                name: appInfo.name.replace('/', ' '),
                publisher: appInfo.publisher.replace('/', ' '),
                version: appInfo.version,
                application: appInfo.application,
                filename: fileName === undefined ? '' : fileName,
                fullpath: appInfo.file
            };
            let infoMessage = `Uploading App: ${appInfo.name} Version: ${appInfo.version}`;
            OutputChannel.appendLine(account.name, infoMessage);
            let blob = encodeURIComponent(`${blobInfo.publisher}/${blobInfo.name}/${blobInfo.version}/${blobInfo.filename}`);
            console.log(appInfo, fileName, account);
            let { request, url }: { request: fetch.RequestInit; url: string; } = this.buildRequset(account, 'GET', `restype=container&comp=list&prefix=${blob}`);
            console.log(request);
            fetch.default(url, request).then(response => {
                let responseStatus = { code: response.status, text: response.statusText };
                response.text().then(res => {
                    if (responseStatus.code === 200) {
                        let list = parse(decode(res));
                        console.log(list);
                        if (list.EnumerationResults.Blobs.Blob) {
                            let infoMessage = `App: ${appInfo.name} Version: ${appInfo.version} already exist on blob storage do you want to overwrite?`;
                            window.showInformationMessage(infoMessage, { modal: true }, 'Yes').then((result) => {
                                if (result === 'Yes') {
                                    OutputChannel.appendLine(account.name, `${infoMessage} - ${result}`);
                                    this.uploadBlob(account, blobInfo);
                                } else {
                                    OutputChannel.appendLine(account.name, `${infoMessage} - Cancel`);
                                }
                                return;
                            });
                        } else {
                            this.uploadBlob(account, blobInfo);
                        }
                    } else {
                        console.log(res);
                        window.showErrorMessage(`Uploading app to blob storage failed with the following error: Errorcode: ${responseStatus.code}, Errormessage: ${responseStatus.text}`);
                    }
                }).catch(err => {
                    window.showErrorMessage(err.message);
                    console.log(err);
                });
            }).catch(err => {
                window.showErrorMessage(`Uploading app to blob storage failed with the following error: Errorcode: ${err.code}, Errormessage: ${err.message}`);
                console.log(err);
            });
        }).catch((reason) => {
            OutputChannel.appendLine(account.name, reason);
        });
    }

    private buildRequset(account: AzureStorageAccount, method: string, query: string, blob: string = '', contentLength: string = '', blobInfo: BlobInfo | any = undefined): { request: fetch.RequestInit; url: string; } {
        let url = this.constructUri(account);
        let uri = Uri.parse(url);
        if (query) {
            if (uri.query !== '') {
                url += `&${query}`;
            } else {
                url += `?${query}`;
            }
        }
        url = this.addBlobToUrl(url, blob);
        let headers: fetch.Headers = setHeaders(method, account, url, blob, contentLength, blobInfo);
        let request: fetch.RequestInit = {
            method: method,
            headers: headers
        };
        return { request, url };
    }
    addBlobToUrl(url: string, blob: string): string {
        if (!blob) {
            return url;
        }
        let result = '';
        let counter = 0;
        url.split('?').forEach((part) => {
            if (counter === 1) {
                result += '?';
            }
            result += part;
            if (counter === 0) {
                result += `/${blob}`;
            }
            counter += 1;
        });
        return result;
    }

    async addAppFileToEVCompilerSettings(filepath: string, blob: string, account: AzureStorageAccount) {
        let appInfo: AppInfo = await getAppInfoFromFile(filepath);
        let fixedDependencies: FixedDependenciesInstall = { id: appInfo.id, type: InstallSourceType.blob };
        if (account.sharedKey) {
            fixedDependencies.sharedKey = account.sharedKey;
        }
        fixedDependencies.url = this.addBlobToUrl(this.constructUri(account), blob);
        console.log(fixedDependencies);
        let compiler: Compiler = <Compiler>this.context.subscriptions.find(element =>
            element.constructor.name === 'Compiler'
        );
        if (compiler !== undefined) {
            compiler.addFixeddependenciesToEVCompilerSettings(appInfo, fixedDependencies);
        }
    }

    async installAppFile(filepath: string, account: AzureStorageAccount) {
        let configContainerName = await this.dockerContainer.getContainerNameFromLaunchConfiguration(true);
        if (configContainerName) {
            let appInfo: AppInfo = await getAppInfoFromFile(filepath);
            console.log(appInfo);
            // TODO change install process
            // let container = this.dockerContainer.getContainers(configContainerName);
            // let containerName = container[0].Names;
            let containerName = this.dockerContainer.getContainerName(configContainerName);
            if (!containerName) {
                window.showErrorMessage(`Container: '${configContainerName}' does not exist!`, { modal: true });
                return;
            }
            window.showInformationMessage(`Do you want to add: '${appInfo.name}' to app.json`, { modal: true }, 'Ok').then((result) => {
                if (result === 'Ok') {
                    let app = getAppManifest();
                    let dependency = {
                        id: appInfo.id,
                        name: appInfo.name,
                        publisher: appInfo.publisher,
                        version: appInfo.version
                    };
                    let dependencyIndex = app.dependencies.findIndex((value: any) => {
                        return (value.id === appInfo.id);
                    });
                    if ((dependencyIndex !== undefined) && (dependencyIndex !== -1)) {
                        app.dependencies[dependencyIndex] = dependency;
                    } else {
                        app.dependencies.push(dependency);
                    }
                    setAppManifest(app);
                }
            });
            if (appInfo.dependencies !== undefined && appInfo.dependencies.length > 0) {
                await window.showInformationMessage(`Do you want to install dependencies?`, { modal: true }, 'Ok').then((result) => {
                    if (result !== 'Ok') {
                        Terminal.executeCmdLet(`Install-EVAppFileInContainer -containerName ${containerName} -appFile '${appInfo.file}'`);
                    }
                    else {
                        this.dockerContainer.installNewAppInContainerWithDependencies(appInfo.file, account, containerName);
                    }
                });
            } else {
                this.dockerContainer.installNewAppInContainerWithDependencies(appInfo.file, account, containerName);
            }
        } else {
            if (configContainerName === null) {
                let appInfo: AppInfo = await getAppInfoFromFile(filepath);
                window.showErrorMessage(`No running container exist. Can't install '${appInfo.name}'`, { modal: true });
            }
        }
    }
    downloadBlob(blob: string, account: AzureStorageAccount) {
        return new Promise<string>((resolve, reject) => {
            console.log(account);
            let { request, url }: { request: fetch.RequestInit; url: string; } = this.buildRequset(account, 'GET', '', blob);
            fetch.default(url, request).then((response) => {
                let responseStatus = { code: response.status, text: response.statusText };
                if (responseStatus.code !== 200) {
                    reject(`Getting Blob from blob storage failed with the following error: Errorcode: ${responseStatus.code}, Errormessage: ${responseStatus.text}`);
                    return;
                }
                let streamPipeline = promisify(pipeline);
                let filename = blob.split('/')[blob.split('/').length - 1];
                let filepath = `${tmpdir()}\\${filename}`;
                streamPipeline(response.body, createWriteStream(filepath)).then(() => {
                    resolve(filepath);
                }).catch((err) => {
                    reject(`Getting Blob from blob storage failed with the following error: Errorcode: ${err.code}, Errormessage: ${err.message}`);
                });
            }).catch(err => {
                reject(`Getting Blob from blob storage failed with the following error: Errorcode: ${err.code}, Errormessage: ${err.message}`);
            });
        });
    }
    async addMarkerToBlobs(blobs: any, account: AzureStorageAccount, marker: string) {
        let { request, url }: { request: fetch.RequestInit; url: string; } = this.buildRequset(account, 'GET', `restype=container&comp=list&marker=${marker}&include=metadata`);
        await fetch.default(url, request).then(async response => {
            let responseStatus = { code: response.status, text: response.statusText };
            await response.text().then(async res => {
                if (responseStatus.code === 200) {
                    let list = parse(decode(res));
                    blobs.push(...list.EnumerationResults.Blobs.Blob);
                    let marker = list.EnumerationResults.NextMarker;
                    if (marker) {
                        await this.addMarkerToBlobs(blobs, account, marker);
                    }
                } else {
                    console.log(res);
                    window.showErrorMessage(`Getting apps from blob storage failed with the following error: Errorcode: ${responseStatus.code}, Errormessage: ${responseStatus.text}`);
                }
            }).catch(err => {
                window.showErrorMessage(err.message);
                console.log(err);
            });
        }).catch(err => {
            window.showErrorMessage(`Getting apps from blob storage failed with the following error: Errorcode: ${err.code}, Errormessage: ${err.message}`);
            console.log(err);
        });

    }

    selectBlob(account: AzureStorageAccount) {
        return new Promise<any>((resolve, reject) => {
            let { request, url }: { request: fetch.RequestInit; url: string; } = this.buildRequset(account, 'GET', `restype=container&comp=list&include=metadata`);
            fetch.default(url, request).then(response => {
                let responseStatus = { code: response.status, text: response.statusText };
                response.text().then(async res => {
                    if (responseStatus.code === 200) {
                        let list = parse(decode(res));
                        let blobs = list.EnumerationResults.Blobs.Blob;
                        let marker = list.EnumerationResults.NextMarker;
                        if (marker) {
                            try {
                                await this.addMarkerToBlobs(blobs, account, marker);
                            } catch (err) {
                                reject(err);
                            }
                        }
                        console.log(blobs);
                        let quickPick = window.createQuickPick();
                        quickPick.items = blobs.map((blob: any) => ({
                            label: blob.Metadata !== '' ? `${blob.Metadata.name} ${blob.Metadata.version}` : `${blob.Name.split('/')[1]} ${blob.Name.split('/')[2]}`,
                            description: blob.Metadata !== '' ? blob.Metadata.filename : blob.Name.split('/')[3],
                            detail: blob.Metadata !== '' ? blob.Metadata.publisher : blob.Name.split('/')[0],
                            blob: blob.Name
                        }));
                        quickPick.onDidChangeSelection(([item]) => {
                            quickPick.dispose();
                            let selectedItem: any = item;
                            resolve(selectedItem.blob);
                        });
                        quickPick.onDidHide(() => quickPick.dispose());
                        quickPick.show();
                    } else {
                        reject(`Getting apps from blob storage failed with the following error: Errorcode: ${responseStatus.code}, Errormessage: ${responseStatus.text}`);
                    }
                }).catch(err => {
                    reject(err.message);
                });
            }).catch(err => {
                reject(`Getting apps from blob storage failed with the following error: Errorcode: ${err.code}, Errormessage: ${err.message}`);
            });
        });
    }
    constructUri(item: AzureStorageAccount): string {
        let url = '';
        if (item.url !== undefined) {
            if (item.url.toUpperCase().startsWith('HTTP')) {
                url = item.url;
            }
        }
        if (url === '') {
            url = `https://${item.account}.${this.baseAzureUrl}/${item.container}`;
            if (item.url !== undefined) {
                url = `https://${item.account}.${item.url}/${item.container}`;
            }
            if (item.sharedKey === undefined) {
                if (item.sasToken !== undefined) {
                    if (item.sasToken.startsWith('?')) {
                        url += item.sasToken;
                    } else {
                        url += `?${item.sasToken}`;
                    }
                }
            }
        }
        return url;
    }
    addAppToFixeddependenciesOnEVCompilerSettings() {
        this.selectAccount().then((account) => {
            this.addBlobToFixeddependencies(account);
        });
    }
    installAllDependencyFromAzureStorage(): any {
        this.selectAccount().then((account) => {
            this.dockerContainer.installAllDependecies(account);
        });
    }
    uploadAppToAzureStorage(): any {
        this.selectAccount().then((account) => {
            this.uploadAppToBlob(account);
        });
    }
    installDependencyFromAzureStorage() {
        this.selectAccount().then((account) => {
            this.selectBlob(account).then((blob) => {
                this.downloadBlob(blob, account).then((filepath) => {
                    this.installAppFile(filepath, account);
                }).catch((reason) => {
                    window.showErrorMessage(reason);
                });
            }).catch((reason) => {
                window.showErrorMessage(reason);
            });
        });
    }
    private selectAccount() {
        return new Promise<AzureStorageAccount>((resolve, reject) => {
            let accounts: AzureStorageAccount[] | undefined = workspace.getConfiguration(constants.extensionConfig).get(constants.dependenciesAzureStorageSettings);
            console.log(accounts);
            if (accounts === undefined) {
                reject();
                return;
            }
            if (accounts.length === 1) {
                resolve(accounts[0]);
                return;
            }
            let quickPick = window.createQuickPick();
            quickPick.items = accounts.map((account: any) => ({ label: account.name, account: account }));
            quickPick.onDidChangeSelection(([item]) => {
                quickPick.dispose();
                let selectedItem: any = item;
                resolve(selectedItem.account);
            });
            quickPick.onDidHide(() => {
                quickPick.dispose();
            });
            quickPick.show();
        });
    }
    dispose() {
    }
}
export interface AzureStorageAccount {
    name: string;
    account: string;
    container?: string;
    url?: string;
    sasToken?: string;
    sharedKey?: string;
}

function setHeaders(method: string, account: AzureStorageAccount, url: string, blob: string = '', contentLength: string = '', blobInfo: BlobInfo | any = undefined): fetch.Headers {
    let headers = new fetch.Headers;
    headers.append('User-Agent', constants.extensionShortName);
    if (method === 'PUT') {
        let searchVersion = createSearchVersion(blobInfo.version);
        let microsoftApplicationVersion = createSearchVersion(blobInfo.application);
        headers.append('x-ms-blob-type', 'BlockBlob');
        headers.append('x-ms-meta-id', blobInfo.id);
        headers.append('x-ms-meta-name', blobInfo.name);
        headers.append('x-ms-meta-publisher', blobInfo.publisher);
        headers.append('x-ms-meta-version', blobInfo.version);
        headers.append('x-ms-meta-filename', blobInfo.filename);
        headers.append('x-ms-tags', `id=${blobInfo.id}&version=${blobInfo.version}&searchVersion=${searchVersion}&microsoftApplicationVersion=${microsoftApplicationVersion}`);
    }
    if ((account.sharedKey !== undefined) && (account.sharedKey !== '')) {
        let date = new Date().toUTCString();
        let version = '2019-12-12';
        headers.append("x-ms-date", date);
        headers.append("x-ms-version", version);
        let canonicalizedHeaders: string = getCanonicalizedHeaders(headers);
        let authString: string;
        if (!blob) {
            authString = `${method}\n\n\n\n\n\n\n\n\n\n\n\n${canonicalizedHeaders}/${account.account}/${account.container}${addUrlQuery(url)}`;
        } else {
            authString = `${method}\n\n\n${contentLength}\n\n\n\n\n\n\n\n\n${canonicalizedHeaders}/${account.account}/${account.container}/${encodeURI(blob)}${addUrlQuery(url)}`;
        }
        console.log(authString);
        let key = Buffer.from(account.sharedKey, 'base64');
        let hash = crypto.createHmac('SHA256', key).update(authString).digest('base64');
        headers.append('Authorization', `SharedKey ${account.account}:${hash}`);
    }
    return headers;
}
function createSearchVersion(version: string): string {
    return version.split('.').map((part) => {
        return part.padStart(10, '0');
    }).join('.');
}
function getCanonicalizedHeaders(headers: fetch.Headers): string {
    let result: string = '';
    headers.forEach((value: String, name: String) => {
        if (name.startsWith('x-ms-')) {
            result += `${name}:${value}\n`;
        }
    });
    return result;
}
function addUrlQuery(url: string): string {
    if (!url.includes('?')) {
        return '';
    }
    let result: string = '';
    let query = url.split('?')[1];
    let queryelements = query.split('&');
    let queryProperties = new Array();
    queryelements.forEach((element) => {
        queryProperties.push({ name: element.split('=')[0], value: element.split('=')[1] });
    });
    queryProperties.sort((a: any, b: any) => {
        if (a.name < b.name) {
            return -1;
        }
        if (a.name > b.name) {
            return 1;
        }
        if (a.value < b.value) {
            return -1;
        }
        if (a.value > b.value) {
            return 1;
        }
        return 0;
    });
    queryProperties.forEach((property) => {
        if (result.includes(`${property.name}:`)) {
            result += `,${decodeURIComponent(property.value)}`;
        } else {
            result += `\n${property.name}:${decodeURIComponent(property.value)}`;
        }
    });
    return result;
}
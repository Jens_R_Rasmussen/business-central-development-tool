import constants = require("../constants");
import { commands, ExtensionContext, window } from "vscode";
import { getAppManifest, setAppManifest } from "../workspaceFileAndFolderHelper";
import { BCAppInfo } from './BCAppInfo';
import { resolve } from "path";

export class AppHandling {
    context: ExtensionContext;
    constructor(context: ExtensionContext) {
        this.context = context;
        this.registreCommands();
        // commands.executeCommand('setContext', constants.containerSettingsExist, containerSettingsExist());
    }
    registreCommands() {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(commands.registerCommand(constants.backupManifestCommand, () => this.updateManifest()));
        subscriptions.push(commands.registerCommand(constants.restoreManifestCommand, () => this.restoreManifest()));
    }
    loadManifest(): any {
        let manifest: any = getAppManifest();
        if (manifest === undefined) {
            return manifest;
        }
        manifest = this.context.globalState.get(manifest.id);
        if (manifest === undefined) {
            manifest = this.updateManifest();
        }
        return manifest;
    }
    updateManifest(): any {
        let manifest = getAppManifest();
        if (manifest === undefined) {
            return manifest;
        }
        this.context.globalState.update(manifest.id, manifest);
        return manifest;
    }
    restoreManifest() {
        setAppManifest(this.loadManifest());
    }
    dispose() {

    }
}

export function getAppInfoFromFile(file: string): Promise<AppInfo> {
    return new Promise<AppInfo>((resolve, reject) => {
        new BCAppInfo().getBCAppInfo(file).then((value) => {
            resolve({
                id: value.id,
                name: value.name,
                publisher: value.publisher,
                version: value.version,
                application: value.applicationVersion,
                dependencies: value.dependencies,
                file: file
            });
        }).catch((reason) => {
            reject(reason);
        });
    });
}
export interface AppInfo {
    id: string;
    name: string;
    publisher: string;
    version: string;
    application: string;
    file: string;
    dependencies?: any[];
}
export interface BlobInfo {
    id: string;
    name: string;
    publisher: string;
    version: string;
    application: string;
    filename: string;
    fullpath: string;
}
import * as vscode from 'vscode';
import * as execute from './executeLibrary';
import { Xliff } from './generateXliff';
import { WebViewService } from './webViewService';
import { NewDevEnvironment } from './NewDevelopmentEnvironment';
import { registreCommands as gitRegistreCommands, setGitCommitConfiguration } from './git';
import { DockerContainer } from './dockerLibrary';
import { registerCommands as powerShellModuleregisterCommands } from './powerShellModule';
import { BitBucket } from './bitbucket/bitbucket';
import * as constants from './constants';
import { Compiler } from './CompilerSettings/compiler';
import { AzureBlobHandling } from './AppHandling/azureBlobHandling';
import { AppHandling } from './AppHandling/AppInfo';
import { GlobalSettings } from './globalSettings';
import { EVGo } from './EV.Go/evGo';
import { BitBucketAuthorization } from './bitbucket/bitBucketAuthorization';

export async function activate(context: vscode.ExtensionContext) {
	console.log('Extension "business-central-development-tool" is now active!');
	execute.Terminal.downloadBCContainerHelper();
	vscode.commands.executeCommand('setContext', constants.extensionContextName, true);
	vscode.window.onDidCloseTerminal(terminal => execute.Terminal.onDidCloseTerminal(terminal));
	let subscriptions = context.subscriptions;
	let compiler = new Compiler(context);
	compiler.registerCommands();
	subscriptions.push(compiler);
	let azureBlobHandling = new AzureBlobHandling(context);
	subscriptions.push(azureBlobHandling);
	let xliff = new Xliff(context);
	subscriptions.push(xliff);
	let webViewService = new WebViewService(context);
	subscriptions.push(webViewService);
	let newDevEnvironment = new NewDevEnvironment(context);
	subscriptions.push(newDevEnvironment);
	let bitBucketAuthorization = new BitBucketAuthorization(context);
	let bitBucket = new BitBucket(context, bitBucketAuthorization);
	subscriptions.push(bitBucket);
	let dockerContainer = new DockerContainer(context);
	dockerContainer.registreCommands();
	subscriptions.push(dockerContainer);
	let appHandling = new AppHandling(context);
	subscriptions.push(appHandling);
	gitRegistreCommands(context);
	powerShellModuleregisterCommands(context);
	setGitCommitConfiguration();
	let globalSettings = new GlobalSettings(context);
	subscriptions.push(globalSettings);
	subscriptions.push(new EVGo(context, bitBucketAuthorization));
	globalSettings.autoUpdate();
}
export function deactivate() {
	execute.Terminal.dispose();
}
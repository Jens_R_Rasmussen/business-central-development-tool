import { workspace } from "vscode";
import { BitBucketAuthorization } from "../bitbucket/bitBucketAuthorization";
import { existsSync, readFile, rm } from "fs";
import path from "path";
import { gitAddRemote, gitInit } from "../git";
import { BitBucketRepository } from "../bitbucket/bitBucketRepository";
import { OutputChannel } from "../outputChannel";
import * as constants from "../constants";

export class InitWorkSpace {
    private bitBucketAuthorization: BitBucketAuthorization;
    constructor(bitBucketAuthorization: BitBucketAuthorization) {
        this.bitBucketAuthorization = bitBucketAuthorization;
        // this.initWorkspace();
    }
    private initWorkspace() {
        let workspaceFolder = workspace.workspaceFolders?.find(element => (element.name === 'Project'));
        if (workspaceFolder === undefined) {
            return;
        }
        let filePath = path.join(workspaceFolder.uri.fsPath, 'EVInit.json');
        if (!existsSync(filePath)) {
            return;
        }
        readFile(filePath, (err, data) => {
            if (err) {
                console.log(err);
                return;
            }
            let init = JSON.parse(data.toString());
            rm(filePath, (err) => {
                console.log(err);
            });
            this.initGitRepository(init);
        });
    }
    initGitRepository(init: any) {
        OutputChannel.appendLine(constants.evGoOutputChannel, `Git Init: '${init.gitRepositoryPath}'`);
        OutputChannel.appendLine(constants.evGoOutputChannel, `Stage all files and create Init commit`);
        gitInit(init.gitRepositoryPath)
            .then((value) => {
                if (value === true) {
                    if (init.createRepository) {
                        OutputChannel.appendLine(constants.evGoOutputChannel, `Creating BitBucket Repository: '${init.name}'`);
                        new BitBucketRepository(this.bitBucketAuthorization).createRepository(init.name, init.gitProject)
                            .then((repository: any) => {
                                console.log(repository);
                                if (repository.links.clone) {
                                    let repositoryClonePath = repository.links.clone.find((element: any) => (element.name === 'https'));
                                    if (repositoryClonePath) {
                                        OutputChannel.appendLine(constants.evGoOutputChannel, `Push master branch to: '${repositoryClonePath.href}'`);
                                        gitAddRemote(init.gitRepositoryPath, repositoryClonePath.href);
                                    }
                                }
                            })
                            .catch((reason) => {
                                console.log(reason);
                            });
                    }
                }
            }
            ).catch((reason) => {
                console.log(reason);
            });
    }
}

// name: data.projectName,
//             gitRepositoryPath: workspacePath,
//             createRepository: data.createBitBucketRepository,
//             gitProject: data.gitProject,
//             containerName: data.containerName,
import { extensions } from "vscode";

export class ManifestHandler {
    private chooseTargetPlatform: Function;
    private generateAppJson: Function;
    constructor() {
        this.chooseTargetPlatform = this.getMicrosoftAlExtensionObject('chooseTargetPlatform');
        this.generateAppJson = this.getMicrosoftAlExtensionObject('generateAppJson');
    }
    async createManifest(path: string, target: string) {
        return await this.generateAppJson(path, target);
    }
    async selectTarget() {
        let target = await this.chooseTargetPlatform('');
        return target[1];
    }
    private getMicrosoftAlExtensionObject(objectName: string) {
        let alLanguageExt = extensions.getExtension('ms-dynamics-smb.al');
        if (alLanguageExt === undefined) {
            return undefined;
        }
        let extensionObject = alLanguageExt.exports.services.find((element: any) => (element[objectName] !== undefined));
        return extensionObject[objectName];
    }
    dispose() {
    }
}
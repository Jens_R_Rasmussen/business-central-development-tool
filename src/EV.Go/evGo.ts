import { ExtensionContext, commands } from "vscode";
import * as constants from '../constants';
import { WorkspaceCreator } from "./workspaceCreator";
import { BitBucketAuthorization } from "../bitbucket/bitBucketAuthorization";
import { userInfo } from "os";
export class EVGo {
    context: ExtensionContext;
    workspaceCreator: WorkspaceCreator;
    constructor(context: ExtensionContext, bitBucketAuthorization: BitBucketAuthorization) {
        this.context = context;
        this.workspaceCreator = new WorkspaceCreator(context, bitBucketAuthorization);
        this.registerCommands();
        this.active();
    }
    active() {
        commands.executeCommand('setContext', constants.evGoActive, userInfo().username === 'JRR');
    }
    private registerCommands() {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(commands.registerCommand(constants.eVGoCommand, () => this.workspaceCreator.createFromdialog()));
    }
    dispose() {
        this.workspaceCreator.dispose();
    }
}
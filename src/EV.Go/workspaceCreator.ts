import { ExtensionContext, WorkspaceConfiguration, window, workspace } from 'vscode';
import * as constants from '../constants';
import * as path from 'path';
import * as os from 'os';
import { existsSync, mkdirSync, readFileSync, writeFileSync, rmSync, writeFile, readFile, rm } from 'fs';
import { ManifestHandler } from './mainfestHandler';
import { randomUUID } from 'crypto';
import { EVGoSetup } from '../webViews/evGoSetupView';
import { Process } from '../executeLibrary';
import fetch from 'node-fetch';
import { copyFile } from 'fs/promises';
import { BitBucketAuthorization } from '../bitbucket/bitBucketAuthorization';
import decompress, { } from 'decompress';
import { InitWorkSpace } from './initWorkspace';
import { OutputChannel } from '../outputChannel';

export class WorkspaceCreator {
    manifestHandler: ManifestHandler;
    testDependencies: any = [];
    defaultTestDependencies: Dependency[] = [];
    context: ExtensionContext;
    bitBucketAuthorization: BitBucketAuthorization;
    constructor(context: ExtensionContext, bitBucketAuthorization: BitBucketAuthorization) {
        this.manifestHandler = new ManifestHandler();
        this.context = context;
        this.bitBucketAuthorization = bitBucketAuthorization;
    }
    async createFromdialog() {
        let projectName = await window.showInputBox({ placeHolder: 'App Name', title: 'Enter Main App Name' });
        if (projectName === undefined) {
            return;
        }
        projectName = projectName.trim();
        let folderName = projectName.replace(/\\|\/|:|\*|\?|"|<|>|\|/gm, '');
        let target = await this.manifestHandler.selectTarget();
        let data: WorkspaceCreationData = {
            mainfolder: folderName,
            subFolders: this.defaultApps(projectName),
            target: target,
            projectName: projectName,
            containerName: '',
            files: this.getProjectFiles()
        };
        data.path = this.createMainfolder(data);
        new EVGoSetup(this.context, this.bitBucketAuthorization, constants.evGoSetupPanelTitle, data).show();
    }
    initNewAppDialog(data: WorkspaceCreationData) {
        return new Promise<any>((resolve) => {
            window.showInputBox({ placeHolder: 'Enter New App Name', title: 'App Name' }).then((name) => {
                if (name !== undefined) {
                    window.showQuickPick(['App', 'Test App'], { title: 'Select Type', canPickMany: false }).then((type) => {
                        if (type !== undefined) {
                            switch (type) {
                                case 'App':
                                    let app = this.initApp(workspace.getConfiguration('ev-go'), name, '');
                                    app.name = name;
                                    app.path = name.replace(/[^A-Za-z0-9\-]/gm, '-').toLowerCase();
                                    resolve(app);
                                    break;
                                default:
                                    let testApp = this.initTestApp(workspace.getConfiguration('ev-go'), name);
                                    testApp.name = name;
                                    testApp.path = name.replace(/[^A-Za-z0-9\-]/gm, '-').toLowerCase();
                                    resolve(testApp);
                                    break;
                            }
                            return;
                        }
                        else {
                            resolve(undefined);
                        }
                    });
                } else {
                    resolve(undefined);
                }
            });
        });
    }
    getProjectFiles(): string[] | undefined {
        let config = workspace.getConfiguration('ev-go');
        return config.get(constants.defaultProjectFilesSettings);
    }
    defaultApps(projectName: string): AppFolder[] {
        let config = workspace.getConfiguration('ev-go');
        let initTestApp = config.get(constants.initTestAppSettings);
        if (initTestApp) {
            return [this.initApp(config, projectName), this.initTestApp(config, projectName)];
        }
        return [this.initApp(config, projectName, '')];
    }
    initApp(config: WorkspaceConfiguration, name: string, testAppName = 'Test'): AppFolder {
        let idRange = <IdRange>config.get(constants.initAppIdRangesSettings);
        return {
            name: "App",
            path: "app",
            testApp: testAppName,
            includeSourceFolder: <boolean>config.get(constants.initIncludeSourceFolderSettings),
            appType: AppType.appSource,
            appJsonInformation: {
                name: name,
                publisher: <string>config.get(constants.initPublisherSettings),
                version: '1.0.0.0',
                brief: name,
                description: name,
                privacyStatement: <string>config.get(constants.initPrivacyStatementSettings),
                eula: <string>config.get(constants.initEULASettings),
                help: <string>config.get(constants.initHelpSettings),
                url: <string>config.get(constants.initUrlSettings),
                contextSensitiveHelpUrl: <string>config.get(constants.initContextSensitiveHelpUrlSettings),
                logo: <string>config.get(constants.initLogoSettings),
                idRange: [
                    idRange.from,
                    idRange.to
                ],
                features: <string[]>config.get(constants.initFeaturesSettings),
                applicationInsightsKey: "00000000-0000-0000-0000-000000000000",
                target: <string>config.get(constants.initTargetSettings)
            }
        };
    }
    initTestApp(config: WorkspaceConfiguration, name: string): AppFolder {
        let idRange = <IdRange>config.get(constants.initTestAppIdRangesSettings);
        return {
            name: "Test",
            path: "test",
            includeSourceFolder: <boolean>config.get(constants.initIncludeSourceFolderSettings),
            includeTestDependecies: <boolean>config.get(constants.initIncludeTestDependenciesSettings),
            includeTestInstallCodeunit: <boolean>config.get(constants.initIncludeTestInstallCodeunitSettings),
            appType: AppType.test,
            appJsonInformation: {
                name: `${name} Test`,
                publisher: <string>config.get(constants.initPublisherSettings),
                version: '1.0.0.0',
                idRange: [
                    idRange.from,
                    idRange.to
                ],
                features: [],
                applicationInsightsKey: "00000000-0000-0000-0000-000000000000",
                target: <string>config.get(constants.initTargetSettings)
            }
        };
    }
    async create(data: WorkspaceCreationData) {
        if (data.target === '') {
            data.target = await this.manifestHandler.selectTarget();
        }
        this.updateDataBeforeProcess(data);
        this.createFolderStructureAndContent(data);
    }
    private updateDataBeforeProcess(data: WorkspaceCreationData) {
        if (data.projectName === undefined) {
            data.projectName = data.mainfolder;
        }
    }
    private async createFolderStructureAndContent(data: WorkspaceCreationData) {
        let config = workspace.getConfiguration('ev-go');
        let projectFiles = <string[]>config.get(constants.defaultProjectFilesSettings);
        let workspacePath: string;
        if (data.path !== undefined) {
            workspacePath = data.path.trim();
        } else {
            workspacePath = this.createMainfolder(data);
        }
        OutputChannel.createChannal(constants.evGoOutputChannel);
        OutputChannel.appendLine(constants.evGoOutputChannel, `Creating new Workspace for project: ${data.projectName}`);
        this.defaultTestDependencies = await this.addTestDependencies();
        this.testPathAndCreateIfMissing(workspacePath);
        await Promise.all(projectFiles.map(async (file) => {
            await this.insertFile(file, workspacePath);
        }));

        let mainManifest = await this.manifestHandler.createManifest(workspacePath, data.target);
        let mainManifestObject = JSON.parse(readFileSync(mainManifest, 'utf-8'));
        rmSync(mainManifest);
        let workspaceFolders = await this.createSubFolders(workspacePath, data, mainManifestObject);
        let workspaceFile = path.join(workspacePath, `${data.mainfolder}.code-workspace`);
        OutputChannel.appendLine(constants.evGoOutputChannel, `Creating workspacefile: '${data.mainfolder}.code-workspace'`);
        writeFileSync(workspaceFile, JSON.stringify({ folders: workspaceFolders }, null, 4));
        OutputChannel.appendLine(constants.evGoOutputChannel, `Creating ReadMe.md`);
        writeFileSync(path.join(workspacePath, `ReadMe.md`), this.createReadMeContent(data));
        new InitWorkSpace(this.bitBucketAuthorization).initGitRepository(this.createEVInit(workspacePath, data));
        Process.exec(`powershell -Command "&{ code -n '${workspaceFile}'; }"`);
    }
    createEVInit(workspacePath: string, data: WorkspaceCreationData): any {
        return {
            name: data.projectName,
            gitRepositoryPath: workspacePath,
            createRepository: data.createBitBucketRepository,
            gitProject: data.gitProject,
            containerName: data.containerName,
        };
    }
    createReadMeContent(data: WorkspaceCreationData): string {
        let content = `# ${data.projectName}\n`;
        content += '\n';
        content += '### Object Range ###\n';
        content += '|App|Id Range|\n|-|-|\n';
        data.subFolders.forEach((folder) => {
            let from = 0;
            let to = 0;
            if (folder.appJsonInformation !== undefined) {
                if (folder.appJsonInformation.idRange !== undefined) {
                    from = folder.appJsonInformation.idRange[0];
                    to = folder.appJsonInformation.idRange[1];
                }
            }
            if (from !== 0) {
                content += `|${folder.name}|${from} - ${to}|\n`;
            }
        });
        return content;
    }
    private createMainfolder(data: WorkspaceCreationData) {
        let workspacePath: string = this.getBaseAlPath();
        workspacePath = path.join(workspacePath, data.mainfolder);
        return workspacePath.trim();

    }
    private getBaseAlPath() {
        let baseAlPath: string = <string>workspace.getConfiguration('al').get('algoSuggestedFolder');
        if (baseAlPath === '') {
            baseAlPath = path.join(os.homedir(), "Documents", "AL");
        }
        return baseAlPath;
    }
    private testPathAndCreateIfMissing(folderPath: string) {
        if (!existsSync(folderPath)) {
            try {
                mkdirSync(folderPath);
                OutputChannel.appendLine(constants.evGoOutputChannel, `Creating new folder: ${folderPath.split('\\').pop()}`);
            } catch (e) {
                let newPath = this.getBaseAlPath();
                let missingPathElements = folderPath.replace(newPath, '').split('\\');
                missingPathElements.forEach((element) => {
                    newPath = path.join(newPath, element);
                    if (!existsSync(newPath)) {
                        OutputChannel.appendLine(constants.evGoOutputChannel, `Creating new folder: ${newPath.split('\\').pop()}`);
                        mkdirSync(newPath);
                    }
                });
            }
        }
    }

    private async createSubFolders(workspacePath: string, data: WorkspaceCreationData, mainManifestObject: any) {
        let workspaceFolders: any[] = [];
        let appsWithTestApp = data.subFolders.filter((element) => element.testApp !== '' && element.testApp !== undefined);
        let appsWithOutTestApp = data.subFolders.filter((element) => element.testApp === '' || element.testApp === undefined);
        if (appsWithTestApp !== undefined) {
            await Promise.all(appsWithTestApp.map(async (element) => {
                await this.createSubFolder(workspaceFolders, element, workspacePath, data, mainManifestObject);
            }));
        }
        if (appsWithOutTestApp !== undefined) {
            await Promise.all(appsWithOutTestApp.map(async (element) => {
                await this.createSubFolder(workspaceFolders, element, workspacePath, data, mainManifestObject);
            }));
        }
        workspaceFolders.push({ path: ".", name: "Project" });
        return workspaceFolders;
    }
    private async createSubFolder(workspaceFolders: any[], element: AppFolder, workspacePath: string, data: WorkspaceCreationData, mainManifestObject: any) {
        OutputChannel.appendLine(constants.evGoOutputChannel, `Creating App: '${element.appJsonInformation?.name}'`);
        workspaceFolders.push({ path: element.path, name: element.name });
        let folderPath = path.join(workspacePath, element.path);
        this.testPathAndCreateIfMissing(folderPath);
        await this.insertAppFiles(element.appType, folderPath);
        if (element.includeSourceFolder) {
            let sourceFolder = path.join(folderPath, 'source');
            this.testPathAndCreateIfMissing(sourceFolder);
            if (element.includeTestInstallCodeunit) {
                let installFolder = path.join(sourceFolder, 'install');
                this.testPathAndCreateIfMissing(installFolder);
                let fromId = 50000;
                if (element.appJsonInformation !== undefined) {
                    if (element.appJsonInformation.idRange !== undefined) {
                        fromId = element.appJsonInformation.idRange[0];
                    }
                    OutputChannel.appendLine(constants.evGoOutputChannel, `Creating Install Test Suite Codeunit for app: '${element.appJsonInformation?.name}'`);
                    writeFileSync(path.join(installFolder, 'InstallTestSuite.Codeunit.al'), this.installCodeunit(fromId, data.projectName ? data.projectName : data.mainfolder));
                }
            }
        }
        if (element.appJsonInformation !== undefined) {
            let newMainManifestObject = Object.assign({}, mainManifestObject, { dependencies: [] });
            let testDependency = this.testDependencies.find((dependencyElement: any) => (dependencyElement.name === element.name));
            if (testDependency !== undefined) {
                OutputChannel.appendLine(constants.evGoOutputChannel, `Add Main app dependency to app: '${element.appJsonInformation?.name}'`);
                newMainManifestObject.dependencies.push(testDependency.dependency);
            }
            if (element.includeTestDependecies) {
                if (this.defaultTestDependencies !== undefined) {
                    OutputChannel.appendLine(constants.evGoOutputChannel, `Add Test dependencies to app: '${element.appJsonInformation?.name}'`);
                    newMainManifestObject.dependencies.push(...this.defaultTestDependencies);
                }
            }
            await this.createManifestFile(folderPath, newMainManifestObject, element.appJsonInformation);
            if (element.testApp !== undefined) {
                this.testDependencies.push({
                    name: element.testApp,
                    dependency: {
                        id: newMainManifestObject.id,
                        name: newMainManifestObject.name,
                        publisher: newMainManifestObject.publisher,
                        version: newMainManifestObject.version
                    }
                });
            }
        }
    }

    async insertAppFiles(appType: AppType, folderPath: string) {
        let files: DefaultFiles[] = getDefaultAppFiles(appType);
        await Promise.all(files.map(async (file: DefaultFiles) => {
            let destinationPath = path.join(folderPath, file.destination);
            this.testPathAndCreateIfMissing(destinationPath);
            await this.insertFile(file.path, destinationPath);
        }));
    }

    private async addTestDependencies(): Promise<Dependency[]> {
        let config = workspace.getConfiguration('ev-go');
        let path = <string>config.get(constants.testDependenciesObjectPathSettings);
        if (!path) {
            return new Promise((resolve) => {
                resolve([]);
            });
        }
        if (path.toLowerCase().startsWith('http')) {
            return new Promise((resolve, rejects) => {
                fetch(<string>path).then((response) => {
                    response.text().then((result) => {
                        resolve(JSON.parse(result));
                    }).catch((reason) => {
                        rejects(reason);
                    });
                }).catch((reason) => {
                    rejects(reason);
                });
            });
        } else {
            return new Promise((resolve, rejects) => {
                readFile(path, (err, data) => {
                    if (err) {
                        rejects(err);
                    } else {
                        resolve(JSON.parse(data.toString()));
                    }
                });
            });
        }
    }
    private async createManifestFile(folderPath: string, mainManifestObject: any, appJsonInformation: AppJsonInformation) {
        mainManifestObject['id'] = randomUUID();
        OutputChannel.appendLine(constants.evGoOutputChannel, `Creating App.json for app: '${appJsonInformation.name}'`);
        await Promise.all(Object.entries(appJsonInformation).map(async ([key, value]) => {
            if (key === 'logo') {
                if (value) {
                    let logoPath = path.join(folderPath, 'logo');
                    this.testPathAndCreateIfMissing(logoPath);
                    await this.insertFile(value, logoPath);
                    value = path.join('logo', this.getFilenameFromPath(value));
                }
            }
            if (key === 'eula') {
                key = 'EULA';
            }
            if (key === 'idRange') {
                key = 'idRanges';
                value = [{ from: value[0], to: value[1] }];
            }
            if (key === 'applicationInsightsKey') {
                let runtimeVersion: number = mainManifestObject.runtime.split('.')[0];
                if (runtimeVersion > 7) {
                    key = 'applicationInsightsConnectionString';
                    value = `InstrumentationKey=${value};IngestionEndpoint=https://westeurope-4.in.applicationinsights.azure.com/;LiveEndpoint=https://westeurope.livediagnostics.monitor.azure.com/`;
                }
            }
            mainManifestObject[key] = value;
        }));
        writeFileSync(path.join(folderPath, 'app.json'), JSON.stringify(mainManifestObject, null, 4));
    }
    dispose() {
        this.manifestHandler.dispose();
    }
    testData(): WorkspaceCreationData {
        return {
            mainfolder: "test workspace creation",
            target: "",
            subFolders: [{
                name: "App",
                path: "app",
                testApp: "Test",
                includeSourceFolder: true,
                appType: AppType.none,
                appJsonInformation: {
                    name: "Test Workspace Creation",
                    publisher: "Elbek & Vejrup",
                    idRange: [
                        50100,
                        50149
                    ],
                    features: ["NoImplicitWith", "TranslationFile"],
                    applicationInsightsKey: "00000000-0000-0000-0000-000000000000",
                    target: "Cloud"
                }
            }, {
                name: "Test",
                path: "test",
                includeSourceFolder: true,
                includeTestDependecies: true,
                includeTestInstallCodeunit: true,
                appType: AppType.none,
                appJsonInformation: {
                    name: "Test Workspace Creation Test",
                    publisher: "Elbek & Vejrup",
                    idRange: [
                        80100,
                        80149
                    ]
                }
            }]
        };
    }
    private insertFile(source: string, destinationPath: string) {
        return new Promise<void>((resolve, reject) => {
            if (source.toLowerCase().startsWith('http')) {
                this.downloadFile(source, destinationPath, resolve, reject);
            } else {
                let filename = path.join(destinationPath, this.getFilenameFromPath(source));
                OutputChannel.appendLine(constants.evGoOutputChannel, `Copy: '${this.getFilenameFromPath(filename)}'`);
                copyFile(source, filename).then(() => {
                    if (filename.toLowerCase().endsWith('.zip')) {
                        let dest = path.dirname(filename);
                        decompress(filename, dest).then((value) => {
                            console.log(`unzip: '${filename}'`);
                            OutputChannel.appendLine(constants.evGoOutputChannel, `Unzip: '${this.getFilenameFromPath(filename)}'`);
                            value.forEach((element) => {
                                OutputChannel.appendLine(constants.evGoOutputChannel, `Zip output: '${element.path}'`);
                            });
                            rm(filename, (err) => {
                                console.log(err);
                            });
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                });
            }
        });
    }

    private downloadFile(source: string, destinationPath: string, resolve: (value: void | PromiseLike<void>) => void, reject: (reason?: any) => void) {
        fetch(source).then(async (response) => {
            let sourcePath = source.split('?')[0];
            let filename = path.join(destinationPath, this.getFilenameFromPath(sourcePath));
            OutputChannel.appendLine(constants.evGoOutputChannel, `Downloading: '${this.getFilenameFromPath(filename)}'`);
            writeFile(filename, await response.buffer(), (err) => {
                if (err) {
                    console.log(err);
                    reject();
                    return;
                }
                if (filename.toLowerCase().endsWith('.zip')) {
                    // let dest = filename.substring(0, filename.length - 4);
                    let dest = path.dirname(filename);
                    decompress(filename, dest).then((value) => {
                        console.log(`unzip: '${filename}'`);
                        OutputChannel.appendLine(constants.evGoOutputChannel, `Unzip: '${this.getFilenameFromPath(filename)}'`);
                        value.forEach((element) => {
                            OutputChannel.appendLine(constants.evGoOutputChannel, `Zip output: '${element.path}'`);
                        });
                        rm(filename, (err) => {
                            console.log(err);
                        });
                        resolve();
                    });
                } else {
                    resolve();
                }
            });
        });
    }
    private getFilenameFromPath(path: string) {
        let part: string[] = [];
        if (path.includes('\\')) {
            part = path.split('\\');
        } else {
            path = path.split('?')[0];
            part = path.split('/');
        }
        let filenameIndex = part.length - 1;
        return part[filenameIndex];
    }
    private installCodeunit(fromId: number, projectName: string) {
        return `// TODO: This file is only an example on how to install a Test Suite
        codeunit ${fromId} "Install Test Suite"
        {
            Subtype = Install;
        
            trigger OnInstallAppPerCompany()
            begin
                SetupTestSuite();
            end;
        
            procedure SetupTestSuite()
            var
                ALTestSuite: Record "AL Test Suite";
                TestSuiteMgt: Codeunit "Test Suite Mgt.";
                SuiteName: Code[10];
            begin
                SuiteName := 'EV-TEST'; // TODO: Must match the setup in your /azure-pipelines.yml file
                
                if ALTestSuite.Get(SuiteName) then
                    ALTestSuite.Delete(true);
        
                TestSuiteMgt.CreateTestSuite(SuiteName);
                Commit();
                ALTestSuite.Get(SuiteName);
                ALTestSuite.Description := copystr('Test Suite for ${projectName}',1,maxstrlen(ALTestSuite.Description));
                ALTestSuite.Modify();
        
                TestSuiteMgt.SelectTestMethodsByRange(ALTestSuite, '${fromId + 1}|${fromId + 2}'); // TODO: Must be aligned with the Objects used for Unit Tests
            end;
        }`;
    }
}

export function getDefaultAppFiles(appType: AppType) {
    let config = workspace.getConfiguration('ev-go');
    let files: DefaultFiles[];
    switch (appType) {
        case AppType.appSource:
            files = (<DefaultFiles[]>config.get(constants.defaultAppFilesSettings)).filter((element: any) => (element.appType === "AppSource"));
            break;
        case AppType.pTE:
            files = (<DefaultFiles[]>config.get(constants.defaultAppFilesSettings)).filter((element: any) => (element.appType === "PTE"));
            break;
        case AppType.test:
            files = (<DefaultFiles[]>config.get(constants.defaultAppFilesSettings)).filter((element: any) => (element.appType === "Test"));
            break;
        default:
            files = (<DefaultFiles[]>config.get(constants.defaultAppFilesSettings)).filter((element: any) => (element.appType === "None"));
            break;
    }
    return files;
}

export type DefaultFiles = {
    destination: string;
    path: string;
};

interface IdRange {
    from: number,
    to: number
}
interface Dependency {
    id: string,
    name: string,
    publisher: string,
    version: string
}
export interface WorkspaceCreationData {
    path?: string;
    mainfolder: string;
    projectName?: string;
    subFolders: AppFolder[];
    target: string;
    settings?: string;
    files?: string[];
    gitProject?: string;
    createBitBucketRepository?: boolean;
    containerName?: string;
}
export interface AppFolder {
    name: string;
    path: string;
    testApp?: string;
    includeTestDependecies?: boolean;
    includeTestInstallCodeunit?: boolean;
    includeSourceFolder?: boolean;
    appJsonInformation?: AppJsonInformation;
    settings?: string;
    files?: string[];
    appType: AppType;
}
export interface AppJsonInformation {
    name: string;
    publisher: string;
    version?: string;
    brief?: string;
    description?: string;
    privacyStatement?: string;
    eula?: string;
    help?: string;
    url?: string;
    contextSensitiveHelpUrl?: string;
    logo?: string;
    applicationInsightsKey?: string;
    idRange?: number[]
    features?: string[];
    target?: string;
}
enum AppType {
    none,
    appSource,
    pTE,
    test
}
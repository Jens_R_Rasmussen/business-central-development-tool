import { window, workspace, commands, ExtensionContext, Uri, OpenDialogOptions, extensions, comments } from 'vscode';
import * as path from 'path';
import { Terminal, Process } from './executeLibrary';
import { URL } from 'url';
import { executeEVToolsPropertyWithContainerNameParameter } from './powerShellModule';
import * as constants from './constants';
import * as fs from 'fs';
import open from 'open';
import { NewContainer } from './webViews/newContainerWebView';
import { OutputChannel } from './outputChannel';
import { getLicensePath } from './getLicenseFile';
import { Compiler, getCompilerFilePath, getEVCompilerSettingsList } from './CompilerSettings/compiler';
import { AzureBlobHandling, AzureStorageAccount } from './AppHandling/azureBlobHandling';
import { getCurrentWorkspaceFolder } from './workspaceFileAndFolderHelper';
import * as ChildProcess from 'child_process';
import { AppInfo, getAppInfoFromFile } from './AppHandling/AppInfo';
export class DockerContainer {
    newContainerFromExistingContainer(containerName: string) {
        let config = this.getContainerConfigValues(containerName);
        let container = getDefaultContainerParameter();
        container.name = containerName;
        container.type = config.isSandBox ? 'Sandbox' : 'OnPrem';
        container.country = config.country;
        container.isolation = config.isolation ? config.isolation : 'process';
        container.authentication = config.auth === 'NavUserPassword' ? 'UserPassword' : 'Windows';
        container.licensefile = getLicensePath(parseInt(config.version.split('.')[0]));
        container.usessl = config.useSSL;
        container.ismultitenant = config.isMultitenant;
        container.useexternaldatabase = config.databaseServer !== '' ? true : false;
        container.databaseserver = config.databaseServer;
        container.databaseinstance = config.databaseInstance;
        container.databasename = config.databaseName;
        container.sqlusername = config.databaseUsername;
        container.version = config.version;
        container.webclientport = config.webClientPort.toString();
        let newContainer = new NewContainer(this.context, `Create New Container From: ${container.name}`);
        newContainer.dontLoadDefaultValues();
        newContainer.showDevelopmentContainer(container, '');
    }
    async installAllDependecies(account: AzureStorageAccount) {
        let compilerSettings = getEVCompilerSettingsList();
        let appfilePath = getCurrentWorkspaceFolder();
        let mainfestFile = `${appfilePath?.uri.fsPath}\\app.json`;
        let compilerSettingsFile = getCompilerFilePath();
        let result = undefined;
        if (compilerSettings.length === 1) {
            result = { label: compilerSettings[0].name, description: `Version: ${compilerSettings[0].app.version}`, detail: `Server: ${compilerSettings[0].launch.server}`, item: compilerSettings[0] };
        } else {
            result = await window.showQuickPick(compilerSettings.map((item: any) => ({ label: item.name, description: `Version: ${item.app !== undefined ? item.app.version : undefined}`, detail: `Server: ${item.launch !== undefined ? item.launch.server : undefined}`, item: item })), { placeHolder: 'Select Compiler Settings', canPickMany: false });
        }
        if (result !== undefined) {
            let containerName = new URL(result.item.launch.server).hostname;
            let container = this.getContainers(containerName);
            let versionElements = container[0].Version.split('.');
            containerName = container[0].Names;
            let version = `${versionElements[0]}.${versionElements[1]}`;
            let path = extensions.getExtension('ElbekVejrup.ev-business-central-dev-tool')?.extensionPath;
            let command = `Push-Location (join-path '${path}' 'out\\PowerShell Module\\DependenciesHelper'); `;
            command += 'Import-Module ".\\DependencyLibrary.ps1"; ';
            if (account.sharedKey) {
                command += `Install-Dependencies -appManifest "${mainfestFile}" -appCompilerSettings "${compilerSettingsFile}" -compilerSettingsName "${result.label}" -containerName ${containerName} -containerUrl "https://${account.account}.blob.core.windows.net/${account.container}" -sharedKey "${account.sharedKey}" -Version "${version}"`;
            }
            if (account.sasToken) {
                command += `Install-Dependencies -appManifest "${mainfestFile}" -appCompilerSettings "${compilerSettingsFile}" -compilerSettingsName "${result.label}" -containerName ${containerName} -containerUrl "https://${account.account}.blob.core.windows.net/${account.container}" -sasToken "${account.sasToken}" -Version "${version}"`;
            }
            Terminal.executeCmdLet(command);
            Terminal.executeCmdLet('', '', false);
        }
    }
    installNewAppInContainerWithDependencies(filepath: string, account: AzureStorageAccount, containerName: string) {
        let path = extensions.getExtension('ElbekVejrup.ev-business-central-dev-tool')?.extensionPath;
        let command = `Push-Location (join-path '${path}' 'out\\PowerShell Module\\DependenciesHelper'); `;
        command += 'Import-Module ".\\DependencyLibrary.ps1"; ';
        if (account.sharedKey) {
            command += `Install-AppWithDependencies -file "${filepath}" -containerName ${containerName} -containerUrl "https://${account.account}.blob.core.windows.net/${account.container}" -sharedKey "${account.sharedKey}"`;
        }
        if (account.sasToken) {
            command += `Install-AppWithDependencies -file "${filepath}" -containerName ${containerName} -containerUrl "https://${account.account}.blob.core.windows.net/${account.container}" -sasToken "${account.sasToken}"`;
        }
        Terminal.executeCmdLet(command);
        Terminal.executeCmdLet('', '', false);
    }
    context: ExtensionContext;
    constructor(context: ExtensionContext) {
        this.context = context;
        commands.executeCommand('setContext', constants.containerSettingsExist, containerSettingsExist());
    }
    registreCommands() {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(commands.registerCommand(constants.openTerminalInContainerCommand, () => this.openTerminalInContainer()));
        subscriptions.push(commands.registerCommand(constants.updateContainerDependenciesCommand, () => this.updateContainerDependencies(constants.updateContainerDependenciesSettings)));
        subscriptions.push(commands.registerCommand(constants.startContainerCommand, () => this.startContainer(constants.startContainerSettings)));
        subscriptions.push(commands.registerCommand(constants.stopContainercommand, () => this.stopContainer(constants.stopContainerSettings)));
        subscriptions.push(commands.registerCommand(constants.installTestToolkitCommand, () => this.installTestToolkit()));
        subscriptions.push(commands.registerCommand(constants.openClientCommand, () => this.runClientCommand()));
        subscriptions.push(commands.registerCommand(constants.restartServerInstanceCommand, () => this.restartServerInstanceInContainer()));
        subscriptions.push(commands.registerCommand(constants.backupContainerDatabaseCommand, () => this.backupContainerDatabaseCommand()));
        subscriptions.push(commands.registerCommand(constants.restoreContainerDatabaseCommand, () => this.restoreContainerDatabaseCommand()));
        subscriptions.push(commands.registerCommand(constants.showContainerDatabaseBackupFolderCommand, () => this.openBackupFolderCommand()));
        subscriptions.push(commands.registerCommand(constants.createContainerFromSettingsCommand, () => this.createContainerFromSettings(this.context)));
        subscriptions.push(commands.registerCommand(constants.addContainerTestUsersCommand, () => this.addContainerTestUsersCommand()));
        subscriptions.push(commands.registerCommand(constants.updateDevLicenseCommand, () => this.updateDevLicense()));
        subscriptions.push(commands.registerCommand(constants.updateLicenseDialogCommand, () => this.importContainerLicense(undefined)));
        subscriptions.push(commands.registerCommand(constants.addNewUserToContainerCommand, () => this.addNewContainerUserCommand()));

        // subscriptions.push(commands.registerCommand(constants.addContainerTestUsersCommand, () => this.getContainerSettings()));
    }
    async updateDevLicense() {
        let containerName = await this.getContainerNameFromLaunchConfiguration(false);
        if (!containerName) {
            return;
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't Update Development License in container. Container '${containerName} does not exist'`);
            return;
        }
        let containerInfo = this.getContainers(correctContainerName)[0];
        if (containerInfo.State !== 'running') {
            window.showErrorMessage(`Container '${containerName} is not running!'`);
            return;
        }
        this.updateLicenseFromMajorVersion(containerInfo.MajorVersion, correctContainerName);
    }

    updateLicenseFromMajorVersion(majorVersion: string, correctContainerName: string) {
        let licensePath = getLicensePath(parseInt(majorVersion));
        this.importLicense(correctContainerName, licensePath);
    }

    async addContainerTestUsersCommand(containerName: any = undefined) {
        if (containerName === undefined) {
            containerName = await this.getContainerNameFromLaunchConfiguration(true);
            if (!containerName) {
                return;
            }
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't add test users to container. Container '${containerName} does not exist'`);
            return;
        }
        let command = `$containerName = '${correctContainerName}';$testUserPassword = ConvertTo-SecureString -String "Logon2me" -AsPlainText -Force;$Dev = Get-BcContainerBcUser -containerName $containerName -tenant default | Where-Object UserName -EQ 'DEV';if ($Dev) {$password = ConvertTo-SecureString -String "Logon2me" -AsPlainText -Force;$credential = New-Object System.Management.Automation.PSCredential ("dev", $password);Setup-BcContainerTestUsers -containerName $containerName -tenant default -Password $testUserPassword -credential $credential;} else {Setup-BcContainerTestUsers -containerName $containerName -tenant default -Password $testUserPassword;}`;
        Terminal.executeCmdLet(command, `Add Test Users to: ${correctContainerName}`);
    }
    async addNewContainerUserCommand(containerName: any = undefined) {
        if (containerName === undefined) {
            containerName = await this.getContainerNameFromLaunchConfiguration(true);
            if (!containerName) {
                return;
            }
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't add user to container. Container '${containerName} does not exist'`);
            return;
        }
        let username = await window.showInputBox({ placeHolder: 'Username' });
        if (username !== undefined) {
            let addPremium = await window.showInformationMessage('Assign Premium Plan to user?', { modal: true }, 'OK');
            let commandMessage = `Add User: ${username} to: ${correctContainerName}`;
            let command = `$containerName = '${correctContainerName}'; $password = ConvertTo-SecureString -String "Logon2me" -AsPlainText -Force; $credential = New-Object System.Management.Automation.PSCredential ('${username}', $password); New-BcContainerBcUser -ChangePasswordAtNextLogOn $false -containerName $containerName -tenant default -PermissionSetId SUPER -Credential $Credential`;
            if (addPremium) {
                command += ' -assignPremiumPlan;';
            }
            OutputChannel.createChannal(containerName);
            OutputChannel.appendLine(containerName, `Execute command: ${commandMessage}`);
            let result: ChildProcess.ChildProcess | any = Process.execPowershellCommand(`${command}; Clear-Host; Write-Host 'response:${containerName}';`);
            result.stdout.on('data', (data: string) => {
                data = data.trim();
                if (data.startsWith('response:')) {
                    let info = `User: '${username}' created in container: '${data.split(':')[1]}'`;
                    OutputChannel.appendLine(containerName, info);
                    window.showInformationMessage(info);
                } else {
                    if ((!data.startsWith('WARNING:')) && (!data.startsWith('BcContainerHelper')) && (data !== '')) {
                        OutputChannel.appendLine(containerName, data);
                    }
                }
            });
            result.stderr.on('data', (data: string) => {
                if (!data.startsWith('Import-Module')) {
                    window.showErrorMessage(data);
                }
            });
            result.on('close', () => {
                result.kill();
            });

            //      Terminal.executeCmdLet(command, `Add User: ${username} to: ${correctContainerName}`);
        }
    }
    createContainerFromSettings(context: ExtensionContext) {
        let container = getContainerSettings();
        let newContainer = new NewContainer(context, `Create Container '${container.name}' From Settings`);
        newContainer.dontLoadDefaultValues();
        newContainer.showDevelopmentContainer(container, '');
    }
    private async openBackupFolderCommand() {
        let containerName = await this.getContainerNameFromLaunchConfiguration(false);
        if (!containerName) {
            return;
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't backup database in container. Container '${containerName} does not exist'`);
            return;
        }
        this.openBackupFolder(correctContainerName);
    }
    openBackupFolder(containerName: string) {
        let containerInfo: any = this.getContainers(containerName)[0];
        if (containerInfo.CacheFolder !== undefined) {
            let backupPath: fs.PathLike = path.join(containerInfo.CacheFolder, 'backup', containerName);
            fs.stat(backupPath, (err) => {
                if (err === null) {
                    Terminal.executeCmdLet(`start "${backupPath.toString()}"`);
                } else {
                    window.showErrorMessage(`No Backup Exist!`);
                }
            });
        }
    }
    backupContainerDatabase(containerName: string) {
        let containerInfo: any = this.getContainers(containerName)[0];
        if (containerInfo.CacheFolder !== undefined) {
            let date = new Date();
            let datePath = `${date.getFullYear()}${date.getDate().toString().padStart(2, '0')}${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getHours().toString().padStart(2, '0')}${date.getMinutes().toString().padStart(2, '0')}`;
            let backupPath = path.join(containerInfo.CacheFolder, 'backup', containerName, datePath);
            let command = `Backup-BcContainerDatabases -containerName ${containerName} -bakFolder ${backupPath}`;
            Terminal.executeCmdLet(command, command);
        }
    }
    async restoreContainerDatabase(containerName: string) {
        let containerInfo: any = this.getContainers(containerName)[0];
        if (containerInfo.CacheFolder !== undefined) {
            let backupPath: fs.PathLike = path.join(containerInfo.CacheFolder, 'backup', containerName);
            try {
                let directories = new Array<string>();
                directories = fs.readdirSync(backupPath, { withFileTypes: true }).reduce((a: any, c) => {
                    c.isDirectory() && a.push(c.name);
                    return a;
                }, []);
                let result = await window.showQuickPick(directories.map((item: string) => ({ label: item })), { placeHolder: 'Select Backup', canPickMany: false });
                if (result !== undefined) {
                    console.log(result.label);
                    backupPath = path.join(backupPath, result.label);
                }
                else {
                    return;
                }
            } catch (error) {
                window.showErrorMessage(`No Backup Found for container: ${containerName}`, { modal: true }, 'Select Backup Folder').then((item: any) => {
                    if (item === 'Select Backup Folder') {
                        let defaultUri: Uri = Uri.file(containerInfo.CacheFolder);
                        let option: OpenDialogOptions = { canSelectFiles: false, canSelectFolders: true, canSelectMany: false, title: 'Select Backup Folder', defaultUri: defaultUri };
                        window.showOpenDialog(option).then((item: any) => {
                            if (item !== undefined) {
                                let command = `Restore-DatabasesInBcContainer -containerName ${containerName} -bakFolder '${item[0].fsPath}'`;
                                Terminal.executeCmdLet(command, command);
                            }
                        });
                    }
                });
                console.log(error);
                return;
            }
            let command = `Restore-DatabasesInBcContainer -containerName ${containerName} -bakFolder ${backupPath}`;
            Terminal.executeCmdLet(command, command);
        }
    }
    dispose() {

    }
    private async backupContainerDatabaseCommand() {
        let containerName = await this.getContainerNameFromLaunchConfiguration(true);
        if (!containerName) {
            return;
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't backup database in container. Container '${containerName} does not exist'`);
            return;
        }
        this.backupContainerDatabase(correctContainerName);
    }
    private async restoreContainerDatabaseCommand() {
        let containerName = await this.getContainerNameFromLaunchConfiguration(true);
        if (!containerName) {
            return;
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't restore database in container. Container '${containerName} does not exist'`);
            return;
        }
        this.restoreContainerDatabase(correctContainerName);
    }
    private async stopContainer(stopContainerSettings: string) {
        let containerName = await this.getContainerNameFromLaunchConfiguration(true);
        if (!containerName) {
            return;
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't stop container. Container '${containerName} does not exist'`);
            return;
        }
        executeEVToolsPropertyWithContainerNameParameter(stopContainerSettings, correctContainerName);
    }
    private async startContainer(startContainerSettings: string) {
        let containerName = await this.getContainerNameFromLaunchConfiguration(false);
        if (!containerName) {
            return;
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't start container. Container '${containerName} does not exist'`);
            return;
        }
        executeEVToolsPropertyWithContainerNameParameter(startContainerSettings, correctContainerName);
    }
    private async installTestToolkit() {
        let containerName = await this.getContainerNameFromLaunchConfiguration(true);
        if (!containerName) {
            return;
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't Install Test Toolkit. Container '${containerName} does not exist'`);
            return;
        }
        Terminal.executeCmdLet(`Import-TestToolkitToBcContainer -containerName ${correctContainerName}`);
    }
    private async updateContainerDependencies(updateContainerDependenciesSettings: string) {
        let compiler: Compiler = <Compiler>this.context.subscriptions.find(element =>
            element.constructor.name === 'Compiler'
        );
        if (compiler !== undefined) {
            if (compiler.testActiveCompilerSettings(null)) {
                let azureBlobHandling: AzureBlobHandling = <AzureBlobHandling>this.context.subscriptions.find(element =>
                    element.constructor.name === 'AzureBlobHandling');
                if (azureBlobHandling !== undefined) {
                    azureBlobHandling.installAllDependencyFromAzureStorage();
                    return;
                }
            }
        }
        let containerName = await this.getContainerNameFromLaunchConfiguration(true);
        if (!containerName) {
            return;
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't update dependencies\nContainer '${containerName} does not exist'`);
            return;
        }
        executeEVToolsPropertyWithContainerNameParameter(updateContainerDependenciesSettings, correctContainerName);
    }
    private async runClientCommand() {
        let containerName = await this.getContainerNameFromLaunchConfiguration(true);
        if (!containerName) {
            return;
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't Open Client\nContainer '${containerName} does not exist'`);
            return;
        }
        this.runClient(correctContainerName);
    }
    private async restartServerInstanceInContainer() {
        let containerName = await this.getContainerNameFromLaunchConfiguration(true);
        if (!containerName) {
            return;
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't Restart Server Instance\nContainer '${containerName} does not exist'`);
            return;
        }
        let containerInfo = this.getContainers().filter((item: any) => (item.Names === correctContainerName))[0];
        Terminal.executeCmdLet(`Invoke-ScriptInBcContainer -scriptblock { Restart-NAVServerInstance ${containerInfo.Instance} } -containerName ${correctContainerName}`, `Restart-NAVServerInstance ${containerInfo.Instance}`);
    }

    runClient(containerName: string) {
        let containerInfo = this.getContainers().filter((item: any) => (item.Names === containerName))[0];
        let containerConfig = this.getContainerConfigValues(containerName);
        if (this.isContainerExpired(containerConfig)) {
            if (containerConfig.auth === 'NavUserPassword') {
                this.getContainerSettings(containerName);
            }
        }
        let url: string = 'http://';
        if (containerConfig.useSSL) {
            url = 'https://';
        }
        if (containerConfig.webClientPort !== 0) {
            url += `${containerName}:${containerConfig.webClientPort}/${containerInfo.Instance}`;
        } else {
            url += `${containerName}/${containerInfo.Instance}`;
        }
        if (containerConfig.isSandBox || containerConfig.isMultitenant) {
            url += '?tenant=default';
        }
        open(url);
    }
    isContainerExpired(containerConfig: ContainerConfig): boolean {
        let lastYear = new Date();
        lastYear.setFullYear(new Date().getFullYear() - 1);
        return (containerConfig.created.valueOf() <= lastYear.valueOf());
    }

    getContainers(name = '') {
        let containers = new Array();
        Process.execSync('docker ps -a --format "{{ json . }}" --no-trunc').split(/[\r\n]+/g).filter((item) => item).forEach((container) => {
            try {
                let containerObject = JSON.parse(container);
                this.getVersionInfoFromLabels(containerObject);
                this.getCacheFolder(containerObject);
                if (this.containerNameFilter(containerObject, name)) {
                    containers.push(containerObject);
                }
            } catch (error: any) {
                window.showErrorMessage(error);
                return undefined;
            }
        });
        return containers;
    }
    getCacheFolder(containerObject: any) {
        let cacheFolder = undefined;
        try {
            cacheFolder = containerObject.Mounts.split(',').filter((item: string) => {
                return (item.search('bcartifacts.cache') !== -1);
            })[0];
        } catch (error) {
        }
        containerObject.CacheFolder = cacheFolder;
    }
    getVersionInfoFromLabels(containerObject: any) {
        let version = undefined;
        let majorVersion = undefined;
        let instance = undefined;
        try {
            version = containerObject.Labels.split(',').filter((item: string) => {
                return (item.search('version=') === 0);
            })[0];
            version = version.split('=')[1];
            majorVersion = version.split('.')[0];
            if (majorVersion >= 15) {
                instance = 'BC';
            } else {
                instance = 'NAV';
            }
        } catch (error) {
            version = 'Unknown';
            majorVersion = 'Unknown';
            instance = 'Unknown';
        }
        containerObject.Version = version;
        containerObject.MajorVersion = majorVersion;
        containerObject.Instance = instance;
    }
    getValueFromLables(containerObject: any, name: string): string {
        let value = containerObject.Labels.split(',').filter((item: string) => {
            return (item.search(`${name}=`) === 0);
        });
        if (value[0] !== undefined) {
            return value[0].split('=')[1];
        }
        return '';
    }
    getContainerName(name: string): any {
        let containers = this.getContainers(name);
        if (containers.length < 1) {
            return false;
        }
        return containers[0].Names;
    }
    containerNameFilter(item: any, name: string) {
        if (!name) {
            return true;
        }
        return (item.Names.toUpperCase() === name.toUpperCase());
    }
    getContainerConfigValues(name: string): ContainerConfig {
        let result = Process.execSync(`docker inspect ${name}`);
        let obj = JSON.parse(result);
        let env = obj[0].Config.Env;
        let artifactUrl = this.getEnvironmentVariableFromDockerInspect(env, 'artifactUrl', String);
        let version = getVersionFromUrl(artifactUrl);
        return {
            useSSL: this.getEnvironmentVariableFromDockerInspect(env, 'useSSL', Boolean),
            isSandBox: this.getEnvironmentVariableFromDockerInspect(env, 'isBcSandbox', Boolean),
            isMultitenant: this.getEnvironmentVariableFromDockerInspect(env, 'multitenant', Boolean),
            webClientPort: this.getEnvironmentVariableFromDockerInspect(env, 'WebClientPort', Number),
            auth: this.getEnvironmentVariableFromDockerInspect(env, 'auth', String),
            artifactUrl: artifactUrl,
            databaseServer: this.getEnvironmentVariableFromDockerInspect(env, 'databaseServer', String),
            databaseInstance: this.getEnvironmentVariableFromDockerInspect(env, 'databaseInstance', String),
            databaseName: this.getEnvironmentVariableFromDockerInspect(env, 'databaseName', String),
            databaseUsername: this.getEnvironmentVariableFromDockerInspect(env, 'databaseUsername', String),
            isolation: obj[0].HostConfig.Isolation,
            country: obj[0].Config.Labels.country,
            version: version,
            created: new Date(obj[0].Created)

        };
    }
    getEnvironmentVariableFromDockerInspect(env: any, name: string, type: any): any {
        let result = '';
        try {
            result = env.filter((item: string) => {
                return (item.search(`${name}=`) === 0);
            }).pop().replace(`${name}=`, '');
        } catch (e) {

        }
        switch (type.name) {
            case 'Boolean':
                if (result === 'Y') {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'Number':
                if (result === '') {
                    return 0;
                }
                return parseInt(result);
                break;
            default:
                return result;
                break;
        }
    }
    getImages() {
        let images = new Array;
        Process.execSync('docker images --format "{{ json . }}" --no-trunc').split(/[\r\n]+/g).filter((item) => item).forEach((image) => {
            try {
                let imageObject = JSON.parse(image);
                images.push(imageObject);
            } catch (error: any) {
                window.showErrorMessage(error);
                return undefined;
            }
        });
        return images;
    }
    async importContainerLicense(containerName: string | undefined) {
        if (!containerName) {
            containerName = await this.getContainerNameFromLaunchConfiguration(true);
            if (!containerName) {
                return;
            }
            let correctContainerName = this.getContainerName(containerName);
            if (correctContainerName === false) {
                window.showErrorMessage(`Can't Update Development License in container. Container '${containerName} does not exist'`);
                return;
            }
            let containerInfo = this.getContainers(correctContainerName)[0];
            if (containerInfo.State !== 'running') {
                window.showErrorMessage(`Container '${containerName} is not running!'`);
                return;
            }
        }
        // eslint-disable-next-line @typescript-eslint/naming-convention
        window.showOpenDialog({ title: 'Select Licensefile', canSelectFiles: true, filters: { LicenseFile: ['flf', 'bclicense'] }, openLabel: "OK" }).then((file) => {
            if (file === undefined) {
                return;
            }
            if (containerName !== undefined) {
                this.importLicense(containerName, file[0].fsPath);
            }
        });
    }
    private importLicense(containerName: string, path: string) {
        let commandMessage = `Import-BcContainerLicense -containerName ${containerName} -licenseFile ${path}`;
        let command = `Import-BcContainerLicense -containerName ${containerName} -licenseFile $env:licensePath`;
        console.log(command);
        OutputChannel.createChannal(containerName);
        OutputChannel.appendLine(containerName, `Execute command: ${commandMessage}`);
        let result: ChildProcess.ChildProcess | any = Process.exec(`set licensePath=${path.replace(/&/gi, '^&')} & Powershell -Command Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process; Import-Module (Join-Path $env:TEMP 'BcContainerHelper/BcContainerHelper.psm1'); ${command}; Clear-Host; Write-Host 'response:${containerName}';`);
        result.stdout.on('data', (data: string) => {
            data = data.trim();
            if (data.startsWith('response:')) {
                let info = `License file: '${path}' uploaded to container: '${data.split(':')[1]}'`;
                OutputChannel.appendLine(containerName, info);
                window.showInformationMessage(info);
            } else {
                if ((!data.startsWith('WARNING:')) && (!data.startsWith('BcContainerHelper')) && (data !== '')) {
                    OutputChannel.appendLine(containerName, data);
                }
            }
        });
        result.stderr.on('data', (data: string) => {
            if (!data.startsWith('Import-Module')) {
                window.showErrorMessage(data);
            }
        });
        result.on('close', () => {
            result.kill();
            OutputChannel.appendLine(containerName, 'Done!');
        });
    }

    installNewAppInContainer(containerName: string, forceSync: any) {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        window.showOpenDialog({ title: 'Select appfile', canSelectFiles: true, canSelectMany: true, filters: { AppFile: ['app'] }, openLabel: "OK" }).then((file) => {
            if (file === undefined) {
                return;
            }
            if (file.length > 1) {
                this.installAppFiles(file, containerName, forceSync);
                return;
            }
            if (forceSync === undefined) {
                Terminal.executeCmdLet(`Publish-BcContainerApp -containerName ${containerName} -appFile '${file[0].fsPath}' -install -scope Tenant -skipVerification`);
            } else {
                Terminal.executeCmdLet(`Install-EVAppFileInContainer -containerName ${containerName} -appFile '${file[0].fsPath}'`);
            }
        });
    }
    installAppFiles(file: Uri[], containerName: string, forceSync: any) {
        let appInfos: AppInfo[] = [];
        file.forEach(async (element) => {
            let appInfo = await getAppInfoFromFile(element.fsPath);
            appInfos.push(appInfo);
        });
        let result: any[] = [];
        appInfos.forEach((element) => {
            result = addSorting(element, result, appInfos, appInfos.length);
        });
        let apps = result.sort((a: any, b: any) => {
            if (a.order < b.order) {
                return -1;
            }
            if (a.order > b.order) {
                return 1;
            }
            return 0;
        });
        Terminal.executeCmdLet('');
        apps.forEach((element) => {
            if (forceSync === undefined) {
                Terminal.executeCmdLet(`Publish-BcContainerApp -containerName ${containerName} -appFile '${element.file}' -install -scope Tenant -skipVerification`, '', false);
            } else {
                Terminal.executeCmdLet(`Install-EVAppFileInContainer -containerName ${containerName} -appFile '${element.file}'`, '', false);
            }
        });
    }
    installAddIns(containerName: string) {
        window.showOpenDialog({ title: 'Select Add-ins Folder', openLabel: "Select Add-ins Folder", canSelectFiles: false, canSelectFolders: true, canSelectMany: false }).then(item => {
            console.log(item);
            if (item !== undefined) {
                let basepath = path.dirname(item[0].fsPath);
                let currFolder = path.basename(item[0].fsPath);
                console.log(basepath, currFolder);
                Terminal.executeCmdLet(`Update-EVAddins -ContainerName ${containerName} -RepositoryFolder '${basepath}' -DefaultFolder '${currFolder}'`);
            }
        });
    }
    installFonts(containerName: string) {
        window.showOpenDialog({ title: "Select Fonts Folder", openLabel: "Select Fonts Folder", canSelectFiles: false, canSelectFolders: true, canSelectMany: false }).then(item => {
            console.log(item);
            if (item !== undefined) {
                Terminal.executeCmdLet(`Add-FontsToBcContainer -containerName ${containerName} -path '${item[0].fsPath}'`);
            }
        });
    }
    getAppList(containerName: string) {
        try {
            let result = Process.execSyncPowershellCommand(`$json = (ConvertTo-Json -Depth 1 -InputObject (Get-BcContainerAppInfo -containerName ${containerName} -tenantSpecificProperties)); $json.Replace('“', ' ').Replace('”', ' ');`);
            let arrayPos = result.indexOf("[");
            let objPos = result.indexOf("{");
            let pos = arrayPos < objPos ? arrayPos : objPos;
            result = result.substring(pos);
            result = result.replace(/\7/gm, '');
            if (result.startsWith('{')) {
                result = `[${result}]`;
            }
            // TODO fs.writeFileSync('c:\\temp\\result.json', result);
            return JSON.parse(result);
        } catch (e: any) {
            console.log(e.message);
            window.showErrorMessage(e.message);
            return undefined;
        }
    }
    editServiceSettings(containerName: string) {
        Terminal.executeCmdLet(`Edit-EVServiceParameter -containerName ${containerName}`);
    }
    editWebClientSettings(containerName: string) {
        Terminal.executeCmdLet(`Edit-EVWebServerParameter -containerName ${containerName}`);
    }
    openSharedFolder(containerName: string) {
        let command = `$SharedPath = (Join-Path $env:ProgramData '*\\Extensions\\${containerName}\\my\\Shared Folder')\nif (!(Test-Path $SharedPath)) {\n$SharedPath = (Join-Path $env:ProgramData '*\\Extensions\\${containerName}\\my')\n}\nexplorer (Get-Item $SharedPath).FullName`;
        Terminal.executeCmdLet(command);
    }
    async openTerminalInContainer(containerName: string | undefined = undefined) {
        if (!containerName) {
            containerName = await this.getContainerNameFromLaunchConfiguration(true);
            if (!containerName) {
                return;
            }
        }
        let correctContainerName = this.getContainerName(containerName);
        if (correctContainerName === false) {
            window.showErrorMessage(`Can't open Terminal\nContainer '${containerName} does not exist'`);
            return;
        }
        let containerTerminal = window.createTerminal(`PowerShell In ${containerName}`);
        containerTerminal.show();
        containerTerminal.sendText(`docker.exe exec -it ${correctContainerName} powershell -noexit c:\\run\\prompt.ps1`);
    }

    async getContainerNameFromLaunchConfiguration(showOnlyRunning: boolean) {
        let workspaceFolders: any[] | undefined = workspace.workspaceFolders?.map((value: any) => {
            return (value.uri);
        });
        if (workspaceFolders === undefined) {
            workspaceFolders = new Array<any>();
        }
        workspaceFolders.push(null);
        let containerNames = new Array<string>();
        let launch: any;
        workspaceFolders.forEach((uri: any) => {
            launch = workspace.getConfiguration('launch', uri);
            let configurations: any = launch.get('configurations');
            let containerConfigs = configurations.filter((item: any) => { return (item.type.toUpperCase() === 'AL') && (item.serverInstance !== undefined) && (item.server !== undefined) && (item.serverInstance.toUpperCase() === 'BC' || item.serverInstance.toUpperCase() === 'NAV'); });
            containerConfigs.forEach((element: any) => {
                let containerName = new URL(element.server).hostname;
                if (!containerNames.includes(containerName)) {
                    containerNames.push(containerName);
                }
            });
        });
        getEVCompilerSettingsList().forEach((element) => {
            if ((element.launch.server !== undefined) && ((element.launch.serverInstance === 'BC') || (element.launch.serverInstance === 'NAV'))) {
                let containerName = new URL(element.launch.server).hostname;
                if (!containerNames.includes(containerName)) {
                    containerNames.push(containerName);
                }
            }
        });
        if (containerNames.length === 0) {
            return null;
        }
        if (containerNames.length === 1) {
            return containerNames[0];
        }
        else {
            let list: any[] = [];
            let containerInfos = this.getContainers();
            containerNames.forEach((container) => {
                let containerInfo = containerInfos.find(i => container.toUpperCase() === i.Names.toUpperCase());
                let state = containerInfo === undefined ? 'Dont Exist' : containerInfo.State;
                if (((state.toUpperCase() === 'RUNNING') && (showOnlyRunning)) || (!showOnlyRunning)) {
                    if (containerInfo === undefined) {
                        list.push({ name: container, state: state });
                    } else {
                        list.push({ name: containerInfo.Names, state: state });
                    }
                }
            });
            if (list.length === 0) {
                return null;
            }
            let result = await window.showQuickPick(list.map((item: any) => ({ label: item.name, description: item.state })), { placeHolder: 'Select Container', canPickMany: false });
            if (result !== undefined) {
                return result.label;
            }
            return result;
        }
    }
    getContainerSettings(name?: string) {
        let containerParemeters = new Array();
        let containers = this.getContainers(name);
        containers.forEach(element => {
            let urlSettings = this.getContainerConfigValues(element.Names);
            let type = urlSettings.isSandBox ? 'Sandbox' : 'OnPrem';
            let containerParemeter: ContainerParameter & { artifact: string } = {
                name: element.Names,
                type: type,
                country: this.getValueFromLables(element, 'country'),
                isolation: urlSettings.isolation === undefined ? 'process' : urlSettings.isolation,
                authentication: urlSettings.auth === undefined ? 'UserPassword' : urlSettings.auth,
                includetesttoolkit: true,
                usessl: urlSettings.useSSL,
                useexternaldatabase: false,
                version: `${element.Version.split('.')[0]}.${element.Version.split('.')[1]}`,
                username: "Dev",
                password: "Logon2me",
                createshortcut: "None",
                ismultitenant: urlSettings.isMultitenant,
                licensefile: getLicensePath(element.Version.split('.')[0]),
                artifact: urlSettings.artifactUrl === undefined ? '' : urlSettings.artifactUrl
            };
            if (containerParemeter.authentication === 'NavUserPassword') {
                containerParemeter.authentication = 'UserPassword';
            }
            containerParemeters.push(containerParemeter);
        });
        OutputChannel.createChannal('Container Settings');
        OutputChannel.appendLine('Container Settings', JSON.stringify(containerParemeters));
    }
}

export function updateServerConfig(containerName: string) {
    let config = workspace.getConfiguration(constants.extensionConfig);
    let command = '';
    let serverInstanceCommand = `$ServerInstance = (Get-NAVServerInstance).ServerInstance.Split('$') | Select-Object -Last 1; `;
    let restart = false;
    let webServerConfig: object | undefined = config.get(constants.configuringBusinessCentralWebServerSettings);
    if (webServerConfig !== undefined) {
        let entries = Object.entries(webServerConfig);
        entries.forEach(element => {
            command += `Write-Host "Update WebServer Config: Key: '${element[0]}' Value: '${element[1]}'"; Write-Host; `;
            command += `Set-NAVWebServerInstanceConfiguration -WebServerInstance $ServerInstance -KeyName "${element[0]}" -KeyValue "${element[1]}"; `;
        });
    }
    let serverConfig: object | undefined = config.get(constants.configuringBusinessCentralServerSettings);
    if (serverConfig !== undefined) {
        let entries = Object.entries(serverConfig);
        entries.forEach(element => {
            restart = true;
            command += `Write-Host "Update Server Config: Key: '${element[0]}' Value: '${element[1]}'"; Write-Host; `;
            command += `Set-NAVServerConfiguration -ServerInstance $ServerInstance -KeyName "${element[0]}" -KeyValue "${element[1]}"; `;
        });
    }
    if (command !== '') {
        if (restart) {
            command += `Write-Host; Write-Host -ForegroundColor Green "Restarting Service instance $ServerInstance"; `;
            command += `Get-NAVServerInstance | Restart-NAVServerInstance; `;
        }
        Terminal.executeCmdLet('Write-Host -ForegroundColor Green "Update Server Config"', '', false);
        Terminal.executeCmdLet(`Invoke-ScriptInBcContainer -scriptblock { ${serverInstanceCommand}${command} } -containerName ${containerName}`, '', false);
    }
}

export function getDefaultContainerParameter(): ContainerParameter {
    let config = workspace.getConfiguration(constants.extensionConfig);
    let parameter: ContainerParameter = {
        authentication: 'UserPassword',
        country: convertToString(config.get(constants.containerCountrySettings)),
        includetesttoolkit: convertToBoolean(config.get(constants.includeTestToolkitOnNewContainerSettings)),
        testlibrariesonly: convertToBoolean(config.get(constants.includeTestToolkitOnNewContainerSettings)) ? convertToBoolean(config.get(constants.testLibrariesOnlyOnNewContainerSettings)) : false,
        isolation: convertToString(config.get(constants.useIsolationTypeSettings)),
        name: '',
        type: convertToString(config.get(constants.typeOnNewContainerSettings)),
        useexternaldatabase: false,
        usessl: false,
        createshortcut: convertToString(config.get(constants.shortcutLocationSettings)),
        ismultitenant: convertToBoolean(config.get(constants.useMultitenantSettings)),
        customparameter: convertcustomNewBcContainerParameterToString(config.get(constants.customNewBcContainerParameterSettings))
    };
    return parameter;
}
export function convertToBoolean(settings: any): boolean {
    if (settings === undefined) {
        return false;
    }
    return settings;
}
export function convertToString(settings: any): string {
    if (settings === undefined) {
        return '';
    }
    return settings;
}


export interface ContainerParameter {
    name: string;
    type: string;
    version?: string;
    country: string;
    containerurl?: string;
    tag?: string;
    licensefile?: string;
    isolation: string;
    authentication: string;
    username?: string;
    password?: string;
    createshortcut?: string;
    includetesttoolkit: boolean;
    testlibrariesonly?: boolean;
    usessl: boolean;
    webclientport?: string;
    publishedports?: string;
    sqlbackupfile?: string;
    useexternaldatabase: boolean;
    databaseserver?: string;
    databaseinstance?: string;
    databasename?: string;
    sqlusername?: string;
    sqlpassword?: string;
    repository?: string;
    updateDependencies?: boolean;
    ismultitenant?: boolean;
    customparameter?: string;
}
export interface ContainerConfig {
    useSSL: boolean;
    isSandBox: boolean;
    isMultitenant: boolean;
    webClientPort: number;
    auth?: string;
    isolation?: string;
    artifactUrl?: string;
    databaseServer: string;
    databaseInstance: string;
    databaseName: string;
    databaseUsername: string;
    country: string;
    version: string;
    created: Date;
}

function convertcustomNewBcContainerParameterToString(customNewBcContainerParameter: any): string {
    let result = '';
    customNewBcContainerParameter.forEach((element: { parameter: string; value: any; }) => {
        if (typeof element.value === typeof '') {
            result += `${element.parameter}: "${element.value}"\n`;
        } else {
            result += `${element.parameter}: ${element.value}\n`;
        }
    });
    if (result !== '') {
        return result.substr(0, result.length - 1);
    }
    return result;
}
export function saveContainerSettings(container: ContainerParameter, path: string) {
    let workspacePath = workspace.workspaceFolders === undefined ? '' : getPath(workspace.workspaceFolders, path);
    if ((workspacePath === path) || ((workspacePath !== '') && (path === ''))) {
        let config = workspace.getConfiguration(constants.extensionConfig);
        let currentContainerSettings = Object.assign({}, config.get(constants.containerSettings));
        if (shallowEqual(currentContainerSettings, {})) {
            config.update(constants.containerSettings, container, 2);
            return;
        }
        if (!shallowEqual(currentContainerSettings, container)) {
            window.showInformationMessage('Do you want to overwrite container settings?', { modal: true }, 'OK').then((result) => {
                if (result === 'OK') {
                    config.update(constants.containerSettings, container, 2);
                }
            });
        }
    } else {
        var workspaceFiles = fs.readdirSync(path).filter(fn => fn.endsWith('.code-workspace'));
        if (workspaceFiles.length === 1) {
            let content = fs.readFileSync(`${path}\\${workspaceFiles[0]}`);
            try {
                let data = JSON.parse(content.toString());
                if (data.settings === undefined) {
                    data.settings = { "ev-tools.containerSettings": container };
                    fs.writeFileSync(`${path}\\${workspaceFiles[0]}`, JSON.stringify(data, undefined, 4));
                } else {
                    if (data.settings['ev-tools.containerSettings'] === undefined) {
                        data.settings['ev-tools.containerSettings'] = container;
                        fs.writeFileSync(`${path}\\${workspaceFiles[0]}`, JSON.stringify(data, undefined, 4));
                    } else {
                        let currentContainer = data.settings['ev-tools.containerSettings'];
                        if (!shallowEqual(currentContainer, container)) {
                            window.showInformationMessage('Do you want to overwrite container settings?', { modal: true }, 'OK').then((result) => {
                                if (result === 'OK') {
                                    data.settings['ev-tools.containerSettings'] = container;
                                    fs.writeFileSync(`${path}\\${workspaceFiles[0]}`, JSON.stringify(data, undefined, 4));
                                }
                            });
                        }
                    }
                }
            } catch {
                //TODO
            }
        }
    }
}
function shallowEqual(object1: any, object2: any) {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);

    if (keys1.length !== keys2.length) {
        return false;
    }

    for (let key of keys1) {
        if (object1[key] !== object2[key]) {
            return false;
        }
    }

    return true;
}

function containerSettingsExist(): boolean {
    let containerSettings: any;
    try {
        let workspaceFolders = workspace.workspaceFolders;
        if (workspaceFolders === undefined) {
            throw new Error("No Folder Found");
        }
        workspaceFolders.every((uri: any) => {
            let config = workspace.getConfiguration(constants.extensionConfig, uri);
            containerSettings = Object.assign({}, config.get(constants.containerSettings));
            if (!shallowEqual(containerSettings, {})) {
                return false;
            }
            return true;
        });
    } catch (e) {
        let config = workspace.getConfiguration(constants.extensionConfig);
        containerSettings = Object.assign({}, config.get(constants.containerSettings));
    }
    if (shallowEqual(containerSettings, {})) {
        return false;
    }
    return true;
}
function getContainerSettings(): ContainerParameter {
    let containerSettings: any;
    try {
        let workspaceFolders = workspace.workspaceFolders;
        if (workspaceFolders === undefined) {
            throw new Error("No Folder Found");
        }
        workspaceFolders.every((uri: any) => {
            let config = workspace.getConfiguration(constants.extensionConfig, uri);
            containerSettings = Object.assign({}, config.get(constants.containerSettings));
            if (!shallowEqual(containerSettings, {})) {
                return false;
            }
            return true;
        });
    } catch (e) {
        let config = workspace.getConfiguration(constants.extensionConfig);
        containerSettings = Object.assign({}, config.get(constants.containerSettings));
    }
    return containerSettings;
}
function getPath(workspaceFolders: readonly import("vscode").WorkspaceFolder[], path: string): string {
    if (path === '') {
        return workspaceFolders[0].uri.fsPath;
    }
    let result = '';
    workspaceFolders.forEach((folder: import("vscode").WorkspaceFolder) => {
        if (folder.uri.fsPath.toLowerCase() === path.toLowerCase()) {
            result = path;
        }
    });
    return result;
}

function addSorting(app: AppInfo | undefined, dependencyArray: any[], appInfos: AppInfo[], order: number): any[] {
    if (app !== undefined) {
        app.dependencies?.forEach((element) => {
            dependencyArray = addSorting(appInfos.find(i => i.id === element.AppId), dependencyArray, appInfos, order - 1);
        });
        if ((!dependencyArray) || (!dependencyArray.find(i => i.id === app.id))) {
            dependencyArray.push(app);
            dependencyArray.find(i => i.id === app.id).order = order;
        } else {
            if (dependencyArray.find(i => i.id === app.id).order > order) {
                dependencyArray.find(i => i.id === app.id).order = order;
            }
        }
    }
    return dependencyArray;
}

function getVersionFromUrl(artifactUrl: string) {
    let elements = artifactUrl.split('/');
    let fullVersion = elements[elements.length - 2];
    let versionElements = fullVersion.split('.');
    return `${versionElements[0]}.${versionElements[1]}`;
}


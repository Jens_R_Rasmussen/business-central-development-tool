<#
 .Synopsis
  Compile Objects to Nav Container
 .Description
  Create a session to a Nav container and run Compile-NavApplicationObject
 .Parameter containerName
  Name of the container in which you want to compile objects
 .Parameter filter
  Filter specifying the objects you want to compile (default is Compiled=0)
 .Parameter SAPassword
  Password for the SQL admin user (The password used when you created the Container)
 .Parameter SynchronizeSchemaChanges
  Specify Force, Yes or No on whether you want to synchronize schema changes to the database
  .Example
  Invoke-CompileContainerObjects -containerName test2 -SAPassword P@ssword1
 .Example
  Invoke-CompileContainerObjects -containerName test2
#>
function Invoke-EVCompileContainerObjects {
    Param(
        [Parameter(Mandatory=$true)]
        [string]$containerName,
        [string]$filter = "compiled=0",
        [Parameter(Mandatory=$true)]
        [System.Security.SecureString]$SAPassword,
        [ValidateSet('Force','Yes','No')]
        [string]$SynchronizeSchemaChanges = 'Force'
    )
    Invoke-ScriptInNavContainer -containerName $containerName -ScriptBlock { Param($filter, [System.Security.SecureString]$SAPassword, $SynchronizeSchemaChanges)
        $Password = ([System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SAPassword)))
        $customConfigFile = Join-Path (Get-Item "C:\Program Files\Microsoft Dynamics NAV\*\Service").FullName "CustomSettings.config"
        [xml]$customConfig = [System.IO.File]::ReadAllText($customConfigFile)
        $databaseServer = $customConfig.SelectSingleNode("//appSettings/add[@key='DatabaseServer']").Value
        $databaseInstance = $customConfig.SelectSingleNode("//appSettings/add[@key='DatabaseInstance']").Value
        $databaseName = $customConfig.SelectSingleNode("//appSettings/add[@key='DatabaseName']").Value
        $managementServicesPort = $customConfig.SelectSingleNode("//appSettings/add[@key='ManagementServicesPort']").Value
        if ($databaseInstance) { $databaseServer += "\$databaseInstance" }

        if ("$filter" -ne "") {
            Write-Output "Compiling objects with $filter"
        }
        else {
            Write-Output "Compiling all objects"
        }
        $enableSymbolLoadingKey = $customConfig.SelectSingleNode("//appSettings/add[@key='EnableSymbolLoadingAtServerStartup']")
        if ($null -ne $enableSymbolLoadingKey -and $enableSymbolLoadingKey.Value -eq "True") {
            Write-Output "Generating symbols for objects compiled"
            # HACK: Parameter insertion...
            # generatesymbolreference is not supported by Compile-NAVApplicationObject yet
            # insert an extra parameter for the finsql command by splitting the filter property
            if ($filter -eq '') {
                $filter = "ID=1.."
            }
            $filter = '",generatesymbolreference=1,filter="'+$filter
        }

        $params = @{ 'Username' = "sa"; 'Password' = $Password }
        Compile-NAVApplicationObject @params -Filter $filter `
                                     -DatabaseName $databaseName `
                                     -DatabaseServer $databaseServer `
                                     -Recompile `
                                     -SynchronizeSchemaChanges $SynchronizeSchemaChanges `
                                     -NavServerName localhost `
                                     -NavServerInstance $ServerInstance `
                                     -NavServerManagementPort "$managementServicesPort"
    } -ArgumentList $filter, $SAPassword, $SynchronizeSchemaChanges
    Write-Host -ForegroundColor Green "Objects successfully compiled"
}
Export-ModuleMember -Function Invoke-EVCompileContainerObjects

<#
 .Synopsis
  Stop NAV/BC Container
 .Description
  Stop NAV/BC Container
 .Parameter ContainerName
  Name of the container if blank select from launch file
 .Parameter List
  Open list of not running containers to select from
 .Example
  Stop-Container -ContainerName navserver
 .Example
  Stop-Container -List
#>
function Stop-EVContainer {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true)]
        [string]$ContainerName,
        [Switch]$List
    )
    if ($List) {
        $Containers = docker ps --format "{{ json . }}" --no-trunc | ConvertFrom-Json | Where-Object { $_.Mounts.Contains("containerhelper") }
        $SelectedContainer = $Containers | Select-Object Names | Out-GridView -OutputMode Multiple -Title "Select Containers to Stop"
        if ($SelectedContainer) {
            $SelectedContainer | ForEach-Object {
                Write-Host
                Write-Host -ForegroundColor Green "Stop Container: $($_.Names)"
                Stop-BcContainer -containerName $_.Names
            }
        }
    }
    else {
        if (!$ContainerName) {
            try {
                $ContainerName = Get-ContainerNameFromLaunch
            }
            catch {
                Write-Log -LogType Error -Message $_
                Write-Host -ForegroundColor Red "No Container Found"
                Start-Sleep -Seconds 2
                return
            }
        }
        Write-Host
        Write-Host -ForegroundColor Green "Stop Container: $ContainerName"
        $Container = docker ps -a --format "{{ json . }}" --no-trunc | ConvertFrom-Json | Where-Object { $_.Names.ToUpper() -eq $ContainerName.ToUpper() } | Select-Object -First 1
        if (!$Container) {
            Write-Host
            Write-Host -ForegroundColor Red "Container: $ContainerName does not exist"
            Start-Sleep -Seconds 2
            return
        }
        Stop-BcContainer -containerName $Container.Names
    }
}
Export-ModuleMember Stop-EVContainer
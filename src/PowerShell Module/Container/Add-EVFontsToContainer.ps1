<#
.Synopsis
  Copy Fonts to NAV/BC Container
 .Description
  Copy Fonts to NAV or Business Central Container
 .Parameter ContainerName
  Name of the container for which you want to get the EventLog if blank it will select name from launch file
 .Example
  Add-EVFontsToContainer
 .Example
  Add-EVFontsToContainer -ContainerName navserver
 .Example
  Add-EVFontsToContainer -ContainerName navserver -Path C:\Windows\Fonts
#>
function Add-EVFontsToContainer {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $True)]
        [String]$ContainerName,
        [Parameter(Mandatory = $false)]
        $Path = "$(Get-AppFolder)\fonts"
    )
    if (($Path -eq [String]::Empty) -or (!$Path)) {
        Write-Host -ForegroundColor Green "No Path assigned"
        return
    }
    if (!(Test-Path $Path)) {
        Write-Host -ForegroundColor Green "'$Path' does not Exist"
        return
    }
    If (!$ContainerName) {
        try {
            $ContainerName = Get-ContainerNameFromLaunch
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red "No Container Found"
            Start-Sleep -Seconds 2
            return
        }
    }
    Add-FontsToBcContainer -containerName $ContainerName -path $Path
}
Export-ModuleMember -Function Add-EVFontsToContainer
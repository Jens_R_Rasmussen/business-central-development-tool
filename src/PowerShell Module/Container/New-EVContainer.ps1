<#
.Synopsis
  Create New Container Running NAV/BC Container
  Description
  Create New Container Running NAV/BC Container
  Default created with NavUserPassword
  Username: Dev
  Password: Logon2me
 .Parameter ContainerName
  Name of the container
 .Parameter ImageTag
  Business Central version
  Get List from https://mcr.microsoft.com/v2/businesscentral/onprem/tags/list
  Or https://mcr.microsoft.com/v2/dynamicsnav/tags/list
 .Parameter DockerImageRepositoryUrl
  Docker Image Repository default "mcr.microsoft.com/businesscentral/onprem"
  Version 2016 - 2018 "mcr.microsoft.com/dynamicsnav"
 .Parameter LicenseFilePath
  Local path to license file
  if not definded if will use cmdlet Get-LicenseFilePath
 .Parameter Isolation
  Default Process
 .Parameter Auth
  Default NavUserPassword
 .Parameter Credential
  Set your own credentials
 .Example
  New-EVContainer -ContainerName Dev -ImageTag 1904-cu2-dk
#>
Function New-EVContainer {
    [CmdletBinding()]#SupportsShouldProcess)]
    param(
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true)]
        [string]$ContainerName,
        [Parameter(Mandatory = $false,
            ValueFromPipelineByPropertyName = $true)]
        [string]$ArtifactUrl,
        [Parameter(Mandatory = $false)]
        [string]$ImageTag,
        [Parameter(Mandatory = $false)]
        #[ValidateSet("mcr.microsoft.com/businesscentral/onprem", "mcr.microsoft.com/dynamicsnav")]
        [string]$DockerImageRepositoryUrl = "mcr.microsoft.com/businesscentral/onprem",
        [Parameter(Mandatory = $false)]
        [string]$LicenseFilePath,
        [Parameter(Mandatory = $false)]
        [ValidateSet("process", "hyperv")]
        [string]$Isolation = "process",
        [Parameter(Mandatory = $false)]
        [ValidateSet("Windows", "UserPassword")]
        [string]$Auth = "UserPassword",
        [Parameter(Mandatory = $false)]
        [pscredential]$Credential,
        [Parameter(Mandatory = $false)]
        [ValidateSet("Desktop", "MyDocuments", "None")]
        [string]$ShortCutLocation = "Desktop",
        [Parameter(Mandatory = $false)]
        [switch]$IncludeTestToolkit,
        [Parameter(Mandatory = $false)]
        [switch]$IncludeTestLibrariesOnly,
        [Parameter(Mandatory = $false)]
        [PSCustomObject]$ContainerInfo,
        [Parameter(Mandatory = $false)]
        [Int32]$WebClientPort,
        [Parameter(Mandatory = $false)]
        [Int32[]]$PublishPorts,
        [Parameter(Mandatory = $false)]
        [Int32]$MajorVersion,
        [Parameter(Mandatory = $false)]
        [string]$DatabaseServer,
        [Parameter(Mandatory = $false)]
        [string]$DatabaseInstance,
        [Parameter(Mandatory = $false)]
        [string]$DatabaseName,
        [Parameter(Mandatory = $false)]
        [pscredential]$DatabaseCredential,
        [Parameter(Mandatory = $false)]
        [switch]$useSSL,
        [Parameter(Mandatory = $false)]
        [string]$bakFile,
        [Parameter(Mandatory = $false)]
        [string]$CustomParameter,
        [Parameter(Mandatory = $false)]
        [switch]$Multitenant
        
        
    )
    if (!$ArtifactUrl -and !$ImageTag) {
        throw "ArtifactUrl or ImageTag must have a value!!!" 
    }
    Push-Location -StackName "CurrentRunPath"
    try {
        if (!(Get-Variable -Name "StartTimerNewDevelopmentEnvironmen" -ErrorAction Ignore)) {
            $StartTimerNewDevelopmentEnvironmen = [System.DateTime]::Now;
        }
        $ContinueInstall = $true
        if (!$ContainerInfo) {
            $ContainerInfo = Get-EVContainerInfo -ContainerName $ContainerName
            if ($ContainerInfo) {
                Write-Host
                Write-Host -ForegroundColor Red "Container " -NoNewline
                Write-Host -ForegroundColor Green $ContainerName -NoNewline
                Write-Host -ForegroundColor Red " is allready created do you want to overwrite (Y/N) " -NoNewline
                $ContinueInstall = (Read-host).ToUpper() -eq 'Y'
                if (!$ContinueInstall) {
                    if ($ContainerInfo.Status.Substring(0, 2).ToUpper() -ne 'UP') {
                        Write-Host
                        Write-Host -ForegroundColor Green "Start Container: $ContainerName"
                        Start-EVContainer -ContainerName $ContainerName
                    }
                }
            }
        }
        if (!$Credential) {
            if ($Auth -eq "Windows") {
                $Credential = Get-Credential -UserName $env:USERNAME -Message "Enter your $Auth Credentials"
            }
            else {
                $SecurePassword = ConvertTo-SecureString "Logon2me" -AsPlainText -Force
                $Credential = New-Object PSCredential ("Dev", $SecurePassword)
            }
        }
        $Instance = ''
        if (!$MajorVersion) {
            if ($ArtifactUrl) {
                $MajorVersion = ($ArtifactUrl.Split('/') | Select-Object -Last 2 | Select-Object -First 1).Split('.') | Select-Object -First 1
            }
            else {
                $MajorVersion = Get-EVRemoteImageMajorVersion -ImageTag $ImageTag -DockerImageRepositoryUrl $DockerImageRepositoryUrl
            }
        }
        if ($ContinueInstall) {
            # if (!$LicenseFilePath) {
            #     $LicenseFilePath = Get-EVLicenseFilePath -MajorVersion $MajorVersion
            # }
            Write-Host -ForegroundColor Green "Creating Container: $ContainerName"
            $Instance = 'BC'
            $NewContainerArgs = @{ }
            $NewContainerArgs += @{restart = "no" }
            $NewContainerArgs += @{shortcuts = "None" }
            $NewContainerArgs += @{Credential = $Credential }
            $NewContainerArgs += @{isolation = $Isolation }
            $NewContainerArgs += @{auth = $Auth }
            $NewContainerArgs += @{updateHosts = $true }
            $NewContainerArgs += @{accept_outdated = $true }
            $NewContainerArgs += @{containerName = $ContainerName }
            if ($ArtifactUrl) {
                $NewContainerArgs += @{artifactUrl = $ArtifactUrl }
            }
            else {
                $NewContainerArgs += @{imageName = "$($DockerImageRepositoryUrl):$($ImageTag)" }
            }
            $NewContainerArgs += @{accept_eula = $true }
            if ($LicenseFilePath) {
                $NewContainerArgs += @{licenseFile = $LicenseFilePath }
            }
            $NewContainerArgs += @{useBestContainerOS = $true }
            if ($WebClientPort) {
                $NewContainerArgs += @{WebClientPort = $WebClientPort }
            }
            if ($PublishPorts) {
                $NewContainerArgs += @{PublishPorts = $PublishPorts }
            }
            if ($IncludeTestToolkit) {
                $NewContainerArgs += @{includeTestToolkit = $true }
                if ($IncludeTestLibrariesOnly) {
                    $NewContainerArgs += @{includeTestLibrariesOnly = $IncludeTestLibrariesOnly }
                }
            }
            if ($useSSL) {
                $NewContainerArgs += @{useSSL = $true }
            }
            IF ($Multitenant) {
                $NewContainerArgs += @{multitenant = $true }
            }
            if ($MajorVersion -lt 15) {
                $NewContainerArgs += @{enableSymbolLoading = $true }
                $NewContainerArgs += @{doNotExportObjectsToText = $true }
                $NewContainerArgs += @{includeCSide = $true }
                $Instance = 'NAV'
            }
            if ($bakFile) {
                $NewContainerArgs += @{bakFile = $bakFile }
            }
            if ($DatabaseServer) {
                $NewContainerArgs += @{databaseServer = $DatabaseServer }
                if ($DatabaseInstance) {
                    $NewContainerArgs += @{databaseInstance = $DatabaseInstance }
                }
                $NewContainerArgs += @{databaseName = $DatabaseName }
                if ($DatabaseCredential) {
                    $NewContainerArgs += @{databaseCredential = $DatabaseCredential }
                }
                else {
                    [pscredential]$NewCredential = Get-Credential -Message "Database Credentials"
                    if (!$NewCredential) {
                        throw "You need to supply database credentials!"
                    }
                    $NewContainerArgs += @{databaseCredential = $NewCredential }
                }
            }
            if ($CustomParameter) {
                Write-Host $CustomParameter
                $CustomParameterObject = ConvertFrom-Json $CustomParameter
                $CustomParameterObject.psobject.Properties | ForEach-Object { $NewContainerArgs[$_.Name] = $_.Value }
            }
            $global:Credential = $Credential
            New-BcContainer @NewContainerArgs
        }
    }
    catch {
        Write-Log -LogType Error -Message $_
        Pop-Location -StackName "CurrentRunPath"
        throw $_
    }

    if ($DatabaseServer) {
        if ($Auth -eq "Windows") {
            New-NavContainerNavUser -containerName $ContainerName -WindowsAccount $Credential.UserName -PermissionSetId "SUPER"
        }
        else {
            New-NavContainerNavUser -containerName $ContainerName -Credential $Credential -PermissionSetId "SUPER" -ChangePasswordAtNextLogOn $false
        }
    }

    $ContainerHelperPath = Join-Path $env:ProgramData "BcContainerHelper\Extensions\$ContainerName\"
    # if ($MajorVersion -ge 15) {
    #     if (!(Test-Path (Join-Path $ContainerHelperPath 'DLL'))) {
    #         New-Item -Path (Join-Path $ContainerHelperPath 'DLL') -ItemType Directory | Out-Null
    #     }
    #     $AssembliesToCopy = @('DocumentFormat.OpenXml.dll', 'Microsoft.CodeAnalysis.CSharp.dll', 'Microsoft.CodeAnalysis.dll',
    #         'Microsoft.Dynamics.Nav.AL.Common.dll', 'Microsoft.Dynamics.Nav.Apps.dll', 'Microsoft.Dynamics.Nav.Apps.Management.dll',
    #         'Microsoft.Dynamics.Nav.CodeAnalysis.dll', 'Microsoft.Dynamics.Nav.Common.dll', 'Microsoft.Dynamics.Nav.Language.dll',
    #         'Microsoft.Dynamics.Nav.Management.dll', 'Microsoft.Dynamics.Nav.Types.dll', 'Mono.Cecil.dll', 'Newtonsoft.Json.dll',
    #         'System.Collections.Immutable.dll', 'System.Memory.dll', 'System.Reflection.Metadata.dll', 'System.Runtime.CompilerServices.Unsafe.dll')
    #     $AssembliesToCopy | ForEach-Object {
    #         if (!(Test-Path (Join-Path $ContainerHelperPath "DLL\$_"))) {
    #             Copy-FileFromNavContainer -containerName $ContainerName -containerPath "C:\Program Files (x86)\Microsoft Dynamics NAV\*\RoleTailored Client\$_" -localPath (Join-Path $ContainerHelperPath "DLL\$_")
    #         }
    #     }
    # }
    if ($ShortCutLocation -ne 'None') {
        $ShortCutFolder = "$([Environment]::GetFolderPath($ShortCutLocation))\Docker Container Shortcuts\$ContainerName\"
        Write-Host -ForegroundColor Green "Creating shortcuts in '$ShortCutFolder'"
        if ($MajorVersion -lt 15) {
            $WinClientFolder = (Get-Item (Join-Path $ContainerHelperPath "Program Files\*\RoleTailored Client")).FullName
            New-EVShortCut -Name "Windows Client" -TargetPath "$WinClientFolder\Microsoft.Dynamics.Nav.Client.exe" -Arguments "-settings:ClientUserSettings.config" -Folder $ShortCutFolder
            New-EVShortCut -Name "WinClient Debugger" -TargetPath "$WinClientFolder\Microsoft.Dynamics.Nav.Client.exe" -Arguments "-settings:ClientUserSettings.config ""DynamicsNAV:////debug""" -Folder $ShortCutFolder
            Write-Host -ForegroundColor Green "Reading CustomSettings.config from $ContainerName"
            $CustomConfig = Get-NavContainerServerConfiguration -ContainerName $ContainerName
            $DatabaseInstance = $CustomConfig.DatabaseInstance
            $DatabaseName = $CustomConfig.DatabaseName
            $DatabaseServer = $CustomConfig.DatabaseServer
            if ($DatabaseServer -eq "localhost") {
                $DatabaseServer = "$ContainerName"
            }
            if ($Auth -eq "Windows") {
                $NTAuth = "1"
            }
            else {
                $NTAuth = "0"
            }
            if ($DatabaseInstance) {
                $DatabaseServer += "\$DatabaseInstance"
            }
            $CsideParameters = "servername=$DatabaseServer, Database=$DatabaseName, ntauthentication=$NTAuth, ID=$ContainerName"
            if ($MajorVersion -gt 10) {
                $CsideParameters += ", generatesymbolreference=1"
            }
            New-EVShortCut -Name "CSIDE" -TargetPath "$WinClientFolder\finsql.exe" -Arguments "$CsideParameters" -Folder $ShortCutFolder
        }
        if ($Instance -eq '') {
            $Instance = (Get-EVContainerInfo $ContainerName).Instance
        }
        if (!(Test-Path (Join-Path $ContainerHelperPath "my\Shared Folder"))) {
            New-Item -Path (Join-Path $ContainerHelperPath "my\Shared Folder") -ItemType Directory | Out-Null
        }
        New-EVShortCut -Name "My Shared Folder in Container" -Folder $ShortCutFolder -TargetPath (Join-Path $ContainerHelperPath "my\Shared Folder")
        New-EVShortCut -Name "Stop" -Folder $ShortCutFolder -TargetPath "powershell.exe" -Arguments "-ExecutionPolicy Bypass Stop-NavContainer $ContainerName" -IconLocation (Get-ScriptPath 'stop.ico')
        New-EVShortCut -Name "Start" -Folder $ShortCutFolder -TargetPath "powershell.exe" -Arguments "-ExecutionPolicy Bypass Start-NavContainer $ContainerName" -IconLocation (Get-ScriptPath 'play.ico')
        $Url = 'http://'
        if ($useSSL) {
            $Url = 'https://'
        }
        if (($WebClientPort -ne $null) -and ($WebClientPort -ne 0)) {
            $Url += "$($ContainerName):$WebClientPort/$Instance"
        }
        else {
            $Url += "$ContainerName/$Instance"
        }
        if ($Multitenant -or $ArtifactUrl.Contains('sandbox')) {
            $Url += '?tenant=default';
        }
        New-EVShortCut -Name "Web Client" -Folder $ShortCutFolder -TargetPath $Url -IconLocation (Get-ScriptPath 'Webclient.ico')
        if ($IncludeTestToolkit) {
            if ($MajorVersion -lt 15) {
                New-EVShortCut -Name "Test Tool" -Folder $ShortCutFolder -TargetPath "$Url/?page=130401" -IconLocation (Get-ScriptPath 'Webclient.ico')
            }
            else {
                New-EVShortCut -Name "Test Tool" -Folder $ShortCutFolder -TargetPath "$Url/?page=130451" -IconLocation (Get-ScriptPath 'Webclient.ico')
            }
        }
        New-EVShortCut -Name "Command Prompt" -Folder $ShortCutFolder -TargetPath "cmd.exe" -Arguments "/C docker.exe exec -it $ContainerName cmd" -IconLocation (Get-ScriptPath 'docker.ico')
        New-EVShortCut -Name "PowerShell Prompt" -Folder $ShortCutFolder -TargetPath "cmd.exe" -Arguments "/C docker.exe exec -it $ContainerName powershell -noexit c:\run\prompt.ps1" -IconLocation (Get-ScriptPath 'docker.ico')
    }
    [int]$Seconds = ([System.DateTime]::Now).Subtract($StartTimerNewDevelopmentEnvironmen).TotalSeconds
    Write-Host -ForegroundColor Green "Done in $Seconds Seconds"
}
Export-ModuleMember -function New-EVContainer
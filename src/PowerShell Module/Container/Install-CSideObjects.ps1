<#
 .Synopsis
  Import objects to NAV/BC Container
 .Description
  Import objects to NAV/BC Container
  Read the install.json and process the files in that order they are in install.json
  If CompileAllObject = true all objects are compiled not only imported
  If CompileAllObject = false only not compiled objects are compiled
  Example install.json
  {
    "objects": [
        "Object File.fob",
        "Object File.txt"
    ],
    "CompileAllObjects" : false
  }
 .Parameter ContainerName
  Name of the container
 .Parameter AppFolder
  Path of the App folder
 .Parameter Credential
  SQL Credential used for compile container objects
 .Example
  Install-EVCSideObjects -ContainerName navserver -AppFolder Documents\AL\Project\App
#>
function Install-EVCSideObjects {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true)]
        [string]$ContainerName,
        [Parameter(Mandatory = $true)]
        [string]$AppFolder,
        [Parameter(Mandatory = $false)]
        [pscredential]$Credential
    )
    try {
        $Files = $null
        $ObjectsInstalled = $false
        if (Test-Path (Join-Path $AppFolder 'CSide\install.json')) {
            $Files = Get-Content (Join-Path $AppFolder 'CSide\install.json') | ConvertFrom-Json
            $Files.objects | ForEach-Object {
                $ObjectFile = Join-Path (Join-Path $AppFolder 'CSide') $_
                Import-ObjectsToNavContainer -containerName $ContainerName -objectsFile $ObjectFile -ImportAction Overwrite -SynchronizeSchemaChanges No -sqlCredential $Credential
                $ObjectsInstalled = $true
            }
        }
        else {
            $FobFiles = Get-ChildItem -Path $AppFolder -Filter "*.fob" -Recurse
            $FobFiles | ForEach-Object {
                Import-ObjectsToNavContainer -containerName $ContainerName -objectsFile $_.FullName -ImportAction Overwrite -SynchronizeSchemaChanges No -sqlCredential $Credential
                $ObjectsInstalled = $true
            }
            $TxtFiles = Get-ChildItem -Path (Join-Path $AppFolder 'cside') -Filter "*.txt" -Recurse
            $TxtFiles | ForEach-Object {
                Import-ObjectsToNavContainer -containerName $ContainerName -objectsFile $_.FullName -ImportAction Overwrite -SynchronizeSchemaChanges No -sqlCredential $Credential
                $ObjectsInstalled = $true
            }
        }
        if ($ObjectsInstalled) {
            if (($null -eq $Files) -or !$Files.CompileAllObjects) {
                Invoke-EVCompileContainerObjects -containerName $ContainerName -SynchronizeSchemaChanges No -SAPassword $Credential.Password
            }
            else {
                Invoke-EVCompileContainerObjects -containerName $ContainerName -SynchronizeSchemaChanges No -SAPassword $Credential.Password -filter ""
            }

            Invoke-ScriptInBCContainer -containerName $ContainerName -scriptblock {
                Sync-NAVTenant -ServerInstance 'NAV' -Mode ForceSync -Force
                Restart-NAVServerInstance -ServerInstance 'NAV' -Force
            }
        }
    }
    catch {
        Write-Log -LogType Error -Message $_
        throw $_
    }
}
Export-ModuleMember -Function Install-EVCSideObjects
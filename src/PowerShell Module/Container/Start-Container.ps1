<#
 .Synopsis
  Start NAV/BC Container
 .Description
  Start NAV/BC Container
 .Parameter ContainerName
  Name of the container if blank select from launch file
 .Parameter List
  Open list of not running containers to select from
 .Example
  Start-Container -ContainerName navserver
 .Example
  Start-Container -List
#>
function Start-EVContainer {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true)]
        [string]$ContainerName,
        [Switch]$List
    )
    if ($List) {
        $Containers = docker ps -a --format "{{ json . }}" --no-trunc | ConvertFrom-Json | Where-Object { $_.Mounts.Contains("containerhelper") -and $_.Status.Contains("Exited") }
        $SelectedContainer = $Containers | Select-Object Names | Out-GridView -OutputMode Multiple -Title "Select Containers to start"
        if ($SelectedContainer) {
            $SelectedContainer | ForEach-Object {
                Write-Host
                Write-Host -ForegroundColor Green "Start Container: $($_.Names)"
                Start-BcContainer -containerName $_.Names
            }
        }
    }
    else {
        if (!$ContainerName) {
            try {
                try {
                    $ContainerName = Get-ContainerNameFromLaunch
                }
                catch {
                    Write-Log -LogType Error -Message $_
                    Write-Host -ForegroundColor Red "No Container Found"
                    Start-Sleep -Seconds 2
                    return
                }
            }
            catch {
                Write-Log -LogType Error -Message $_
                Write-Host -ForegroundColor Red "No Container Found"
                Start-Sleep -Seconds 2
                return
            }
        }
        $Container = $null
        $Container = docker ps --format "{{ json . }}" --no-trunc | ConvertFrom-Json | Where-Object { $_.Names.ToUpper() -eq $ContainerName.ToUpper() } | Select-Object -First 1
        if ($Container) {
            Write-Host
            Write-Host -ForegroundColor Green "Container: $ContainerName Is already running"
            Start-Sleep -Seconds 2
            return
        }
        Write-Host
        Write-Host -ForegroundColor Green "Start Container: $ContainerName"
        $Container = docker ps -a --format "{{ json . }}" --no-trunc | ConvertFrom-Json | Where-Object { $_.Names.ToUpper() -eq $ContainerName.ToUpper() } | Select-Object -First 1
        if (!$Container) {
            Write-Host
            Write-Host -ForegroundColor Red "Container: $ContainerName does not exist"
            Start-Sleep -Seconds 2
            return
        }
        Start-BcContainer -containerName $Container.Names
    }
}
Export-ModuleMember Start-EVContainer
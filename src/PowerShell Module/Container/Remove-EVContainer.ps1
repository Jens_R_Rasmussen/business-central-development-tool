# TODO
function Remove-EVContainer {
    param (
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true)]
        [string]$ContainerName,
        [Parameter(Mandatory = $false)]
        [Switch]$List
    )
    if ($List) {
        $Containers = docker ps -a --format "{{ json . }}" --no-trunc | ConvertFrom-Json | Where-Object { $_.Mounts.Contains("containerhelper") }
        $SelectedContainer = $Containers | Select-Object Names | Out-GridView -OutputMode Multiple -Title "Select Containers to Remove"
        if ($SelectedContainer) {
            $SelectedContainer | ForEach-Object {
                Write-Host
                Write-Host -ForegroundColor Green "Remove Container: '$($_.Names)'! (Y/N) " -NoNewline
                $Result = Read-Host
                if ($Result.ToUpper() -eq 'Y') {
                    Write-Host
                    Write-Host -ForegroundColor Green "Removing Container: $($_.Names)"
                    Remove-BcContainer -containerName $($_.Names)
                    $ShortCutFolder = "$([Environment]::GetFolderPath("Desktop"))\Docker Container Shortcuts\$($_.Names)\"
                    if (Test-Path $ShortCutFolder -ErrorAction SilentlyContinue) {
                        Remove-Item -Path $ShortCutFolder -Recurse -Force -ErrorAction SilentlyContinue
                    }
                    $ShortCutFolder = "$([Environment]::GetFolderPath("MyDocuments"))\Docker Container Shortcuts\$($_.Names)\"
                    if (Test-Path $ShortCutFolder -ErrorAction SilentlyContinue) {
                        Remove-Item -Path $ShortCutFolder -Recurse -Force -ErrorAction SilentlyContinue
                    }
                }
            }
        }
    }
    if ($ContainerName -and !$List) {
        Write-Host
        Write-Host -ForegroundColor Green "Remove Container: '$ContainerName'! (Y/N) " -NoNewline
        $Result = Read-Host
        if ($Result.ToUpper() -eq 'Y') {
            Write-Host
            Write-Host -ForegroundColor Green "Removing Container: $ContainerName"
            Remove-BcContainer -containerName $ContainerName
            $ShortCutFolder = "$([Environment]::GetFolderPath("Desktop"))\Docker Container Shortcuts\$ContainerName\"
            if (Test-Path $ShortCutFolder -ErrorAction SilentlyContinue) {
                Remove-Item -Path $ShortCutFolder -Recurse -Force -ErrorAction SilentlyContinue
            }
            $ShortCutFolder = "$([Environment]::GetFolderPath("MyDocuments"))\Docker Container Shortcuts\$ContainerName\"
            if (Test-Path $ShortCutFolder -ErrorAction SilentlyContinue) {
                Remove-Item -Path $ShortCutFolder -Recurse -Force -ErrorAction SilentlyContinue
            }
        }
    }
}
Export-ModuleMember -Function Remove-EVContainer
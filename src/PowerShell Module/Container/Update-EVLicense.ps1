# TODO
function Update-EVLicense {
    [CmdletBinding(SupportsShouldProcess = $true,
        ConfirmImpact = 'high'
    )]
    param (
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $True,
            ValueFromPipelineByPropertyName = $true)]
        [string]$LicenseFilePath,
        [Parameter(Mandatory = $false,
            ValueFromPipelineByPropertyName = $true)]
        [String]$ContainerName,
        [switch]$Force,
        [switch]$Dialog
    )
    If (!$ContainerName) {
        try {
            $ContainerName = Get-ContainerNameFromLaunch
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red "No Container Found"
            Start-Sleep -Seconds 2
            return
        }
    }
    if ($Dialog) {
        if (!$ContainerName) {
            $Containers = docker ps --format "{{ json . }}" --no-trunc | ConvertFrom-Json | Where-Object { $_.Mounts.Contains("containerhelper") }
            $ContainerName = ($Containers | Select-Object Names | Out-GridView -OutputMode Single -Title "Select Container").Names
        }
        if (!$LicenseFilePath) {
            try {
                $LicenseFilePath = Select-OpenFileDialog -Title "Select License File" -Filter "License files (*.flf)|*.flf" -Directory "\\ev-filsrv01.ev.dk.local\public\NAV-Licenser\Udvikler\"
            }
            catch {
                Write-Log -LogType Error -Message $_
                Write-Host -ForegroundColor Red $_.Exception.Message
                return
            }
        }
    }
    if (!$ContainerName) {
        throw "You need to specify -ContainerName"
    }
    $ContainerInfo = Get-EVContainerInfo -ContainerName $ContainerName
    if (!$LicenseFilePath) {
        $LicenseFilePath = Get-EVLicenseFilePath -MajorVersion $ContainerInfo.MajorVersion
    }
    if ($Force -or $PSCmdlet.ShouldProcess($ContainerName, "Import License: $LicenseFilePath")) {
        If ($ContainerName -and $LicenseFilePath) {
            Import-BcContainerLicense -licenseFile $LicenseFilePath -containerName $ContainerName
        }
        else {

        }
    }
    else {
        Write-Output "Import-NavContainerLicense -licenseFile $LicenseFilePath -containerName $ContainerName"
    }
}
Export-ModuleMember Update-EVLicense
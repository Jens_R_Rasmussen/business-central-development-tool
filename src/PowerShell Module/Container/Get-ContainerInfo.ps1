﻿<#
 .Synopsis
  Get ContainerID From Running NAV/BC Container
 .Description
  Returns the Container Object of NAV or Business Central Container
 .Parameter ContainerName
  Name of the container for which you want to get the container Id
 .Example
  Get-EVContainerInfo -ContainerName navserver
#>
function Get-EVContainerInfo {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $True,
            ValueFromPipelineByPropertyName = $True)]
        [String]$ContainerName
    )
    If (!$ContainerName) {
        try {
            $ContainerName = Get-ContainerNameFromLaunch
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red "No Container Found"
            Start-Sleep -Seconds 2
            return
        }
    }
    $ContainerInfo = $null
    $ContainerInfo = docker ps -a -f name="$ContainerName" --format "{{ json . }}" --no-trunc | ConvertFrom-Json | Where-Object { $_.Mounts.Contains("containerhelper") -and $_.Names -eq $ContainerName }
    if (!$ContainerInfo) {
        Write-Host "Container: '$ContainerName' does not exist or is not a Business Central Container!"
        return $null
    }
    try {
        $ContainerInfo = $ContainerInfo | Add-Member -NotePropertyMembers @{Version = ($ContainerInfo.Labels.Split(',') | Where-Object { $_ -like 'version=*' }).Split('=')[1] } -PassThru
        $ContainerInfo = $ContainerInfo | Add-Member -NotePropertyMembers @{MajorVersion = $ContainerInfo.Version.Split('.')[0] } -PassThru
    }
    catch {
        Write-Log -LogType Error -Message $_
        $ContainerInfo = $ContainerInfo | Add-Member -NotePropertyMembers @{Version = "0.0.0.0" } -PassThru
        $ContainerInfo = $ContainerInfo | Add-Member -NotePropertyMembers @{MajorVersion = "0" } -PassThru
    }
    try {
        $ContainerInfo = $ContainerInfo | Add-Member -NotePropertyMembers @{ArtifactCacheFolder = ($ContainerInfo.Mounts.Split(',') | Where-Object { $_ -like '*.cache' }) } -PassThru
    }
    catch {
        Write-Log -LogType Error -Message $_
        $ContainerInfo = $ContainerInfo | Add-Member -NotePropertyMembers @{ArtifactCacheFolder = "$($env:HOMEDRIVE)/bcartifacts.cache" } -PassThru
    }

    $Instance = 'BC'
    if ($ContainerInfo.MajorVersion -lt 15) {
        $Instance = 'NAV'
    }
    $ContainerInfo = $ContainerInfo | Add-Member -NotePropertyMembers @{Instance = $Instance } -PassThru
    return $ContainerInfo
}
Export-ModuleMember -Function Get-EVContainerInfo
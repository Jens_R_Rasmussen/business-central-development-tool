function Get-EVRemoteImageMajorVersion {
    [OutputType('System.Int32')]
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true)]
        [string]$ImageTag,
        [Parameter(Mandatory = $false,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateSet("mcr.microsoft.com/businesscentral/onprem", "mcr.microsoft.com/dynamicsnav")]
        [string]$DockerImageRepositoryUrl = "mcr.microsoft.com/businesscentral/onprem"
    )

    try {
        $Uri = "https://"
        $DockerImageRepositoryUrl.Split('/') | ForEach-Object {
            if ($Uri.Length -eq 8) {
                $Uri += "$_/v2/"
            }
            else {
                $Uri += "$_/"
            }
        }
        $Uri += "manifests/$imageTag"

        $i = 0
        do {
            try {
                $Response = Invoke-WebRequest -Uri $Uri -Method Get
                $Object = $OutputEncoding.GetString($Response.Content) | ConvertFrom-Json
                $Image = ($Object.history | Where-Object v1Compatibility -Like '{"architecture*').v1Compatibility | ConvertFrom-Json
                $Version = $Image.config.Labels.version
                $MajorVersion = [int]$Version.Split('.')[0]
                $i++
            }
            catch {
                $i++
                Write-Log -LogType Error -Message $_
                $MajorVersion = $null
            }
        } until ($MajorVersion -or ($i -gt 2))
        if (!$MajorVersion) {
            throw "Can't read Major version from: $Uri"
        }
        return $MajorVersion
    }
    catch {
        throw "Can't read Major version from: $Uri"
    }
}
Export-ModuleMember -Function Get-EVRemoteImageMajorVersion
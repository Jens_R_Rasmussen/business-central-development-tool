<#
 .Synopsis
  Set new password in Running NAV/BC Container
 .Description
  Set new password on windows account
 .Parameter ContainerName
  Name of the container if blank select from launch file
 .Example
  New-WindowsPasswordInContainer -ContainerName navserver
#>
function Set-EVWindowsPasswordInContainer {
    [CmdletBinding(SupportsShouldProcess)]
    param(
        [Parameter(Mandatory = $False,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true)]
        [String]$ContainerName
    )
    if (!$ContainerName) {
        try {
            $ContainerName = Get-ContainerNameFromLaunch
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red "No Container Found"
            Start-Sleep -Seconds 2
            return
        }
    }
    $ContainerInfo = Get-EVContainerInfo -ContainerName $ContainerName
    Start-Process powershell.exe -ArgumentList "& '$(Get-ScriptPath "ChangePasswordInContainer.ps1")'", $ContainerInfo.ID -Wait
}
Export-ModuleMember Set-EVWindowsPasswordInContainer
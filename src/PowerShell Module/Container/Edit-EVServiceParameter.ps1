function Edit-EVServiceParameter {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $True,
            ValueFromPipelineByPropertyName = $true)]
        [string]$ContainerName,
        [string]$KeyName,
        $KeyValue
    )
    if (!$ContainerName) {
        try {
            $ContainerName = Get-ContainerNameFromLaunch
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red "No Container Found"
            Start-Sleep -Seconds 2
            return
        }
    }
    if (!$KeyName -or !$KeyValue) {
        $TempFileName = Join-Path $env:TEMP "$([System.Guid]::NewGuid()).xml"
        Copy-FileFromNavContainer -containerName $ContainerName -containerPath "C:\Program Files\microsoft dynamics nav\*\service\customsettings.config" -localPath $TempFileName | Out-Null
        [xml]$Config = Get-Content -Path $TempFileName
        Remove-Item $TempFileName -Force
        $Node = $Config.appSettings.add | Out-GridView -Title "Select Key to change" -OutputMode Single
        if ($null -eq $Node) {
            Write-Host
            Write-Host -ForegroundColor Red "No Key Selected"
            Start-Sleep -Seconds 2
            return
        }
        [void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')
        $Node.value = [Microsoft.VisualBasic.Interaction]::InputBox("Selected Key: $($Node.key) current Value: $($Node.Value)", "Type New Value")
        if (!$Node.value) {
            return
        }
        $KeyName = $Node.key
        $KeyValue = $Node.value
    }
    $ContainerInfo = Get-EVContainerInfo -ContainerName $ContainerName
    Invoke-ScriptInNavContainer -containerName $ContainerName -ScriptBlock {
        param($KeyName, $KeyValue, $Instance)
        $CurrentServerInstance = Get-NAVServerInstance -ServerInstance $Instance
        Write-Host -ForegroundColor Green "Change Property: '$KeyName' to '$KeyValue'"
        Write-Host
        $CurrentServerInstance | Set-NAVServerConfiguration -KeyName $KeyName -KeyValue $KeyValue
        Write-Host
        Write-Host -ForegroundColor Green "Restart Service: $Instance"
        $CurrentServerInstance | Restart-NAVServerInstance
    } -ArgumentList $KeyName, $KeyValue, $ContainerInfo.Instance
}
Export-ModuleMember -Function Edit-EVServiceParameter
﻿<#
 .Synopsis
  Install App In Running NAV/BC Container
 .Description
  Install or Update App In Running NAV/BC Container
 .Parameter AppFile
  Path of the local appfile
 .Parameter ContainerName
  Name of the container
 .Example
  Install-AppInContainer -AppFile "C:\AppFolder\AppFile.app" -ContainerName navserver
#>
function Install-EVAppFileInContainer {
    [OutputType('System.Boolean')]
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $True,
            ValueFromPipelineByPropertyName = $true)]
        [String]$ContainerName,
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [String]$AppFile
    )
    
    $ContainerInfo = Get-EVContainerInfo -ContainerName $ContainerName
    $SyncMode = 'ForceSync';
    if ($ContainerInfo.MajorVersion -lt 15) {
        $SyncMode = 'Add';
    }
    $ModulePath = Join-Path $ContainerInfo.ArtifactCacheFolder "*\$($ContainerInfo.Version)\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Management\Microsoft.Dynamics.Nav.Apps.Management.dll"
    if (!(Test-Path $ModulePath)) {
        $ModulePath = Join-Path $ContainerInfo.ArtifactCacheFolder "*\$($ContainerInfo.Version)\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll"
    }
    if (!(Test-Path $ModulePath)) {
        $ModulePath = Join-Path 'c:\bcartifacts.cache' "*\*\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Management\Microsoft.Dynamics.Nav.Apps.Management.dll"
    }
    if (!(Test-Path $ModulePath)) {
        $ModulePath = Join-Path 'c:\bcartifacts.cache' "*\*\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll"
    }
    $ModulePath = (Get-Item -Path $ModulePath | Select-Object -Last 1).Fullname  
    Import-Module $ModulePath
    $AppToInstall = Get-NAVAppInfo -Path $AppFile
    $AllPublishedApps = Get-BcContainerAppInfo -containerName $ContainerName -tenantSpecificProperties
    $InstalledApps = $AllPublishedApps | Where-Object { $_.Name -eq $AppToInstall.Name -and $_.Publisher -eq $AppToInstall.Publisher -and $_.AppId -eq $AppToInstall.AppId } 
    if (!$InstalledApps) {
        Write-Output "Install $($AppToInstall.Name)"
        Publish-BcContainerApp -containerName $ContainerName -appFile $AppFile -skipVerification -sync -install -syncMode $SyncMode
        Return $AppToInstall.Name
    }
    if ($InstalledApps | Where-Object Version -eq $AppToInstall.Version) {
        Return "'$($AppToInstall.Name)' Already Installed" 
    }
    if ($InstalledApps | Where-Object Version -GT $AppToInstall.Version) {
        return "'$($AppToInstall.Name)' Newer version already installed"
    }
    try {
        $InstalledApps | ForEach-Object {
            UnInstall-BcContainerApp -containerName $ContainerName -appName $_.Name -appVersion $_.Version -Force
        }
        Write-Output "Update $($AppToInstall.Name)"
        Publish-BcContainerApp -containerName $ContainerName -appFile $AppFile -skipVerification
        Sync-BcContainerApp -containerName $ContainerName -appName $AppToInstall.Name -appVersion $AppToInstall.Version -Mode $SyncMode -Force
        Start-BcContainerAppDataUpgrade -containerName $ContainerName -appName $AppToInstall.Name -appVersion $AppToInstall.Version
        $InstalledApps | ForEach-Object {
            UnPublish-BcContainerApp -containerName $ContainerName -appName $_.Name -version $_.Version -publisher $_.Publisher -Force 
        }

        $AllUninstaleldApps = Get-BcContainerAppInfo -containerName $ContainerName -tenantSpecificProperties | Where-Object IsInstalled -eq $false
        ForEach ($UninstalledApp in $AllUninstaleldApps) {
            $App = $AllPublishedApps | Where-Object { $_.Name -eq $UninstalledApp.Name -and $_.Publisher -eq $UninstalledApp.Publisher -and $_.AppId -eq $UninstalledApp.AppId -and $_.Version -eq $UninstalledApp.Version } | Select-Object -First 1 
            if ($App.IsInstalled -eq $true) {
                Install-BcContainerApp -containerName $ContainerName -appName $App.Name -tenant $App.Tenant -appVersion $App.Version -Force 
            }
        }
        Return $AppToInstall.Name
    }
    catch {
        Return "Error: $_"
    }
}
Export-ModuleMember -Function Install-EVAppFileInContainer
<#
.Synopsis
  Get EventLog From Running NAV/BC Container
 .Description
  Returns the Eventlog of NAV or Business Central Container
 .Parameter ContainerName
  Name of the container for which you want to get the EventLog if blank it will select name from launch file
 .Example
  Get-ContainerEventLog -ContainerName navserver
#>
function Get-EVContainerEventLog {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $True)]
        [String]$ContainerName
    )
    If (!$ContainerName) {
        try {
            $ContainerName = Get-ContainerNameFromLaunch
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red "No Container Found"
            Start-Sleep -Seconds 2
            return
        }
    }
    If ($ContainerName) {
        Get-BCContainerEventLog -containerName $ContainerName
    }
}
Export-ModuleMember Get-EVContainerEventLog
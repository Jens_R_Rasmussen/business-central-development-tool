function Edit-EVWebServerParameter {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $True,
            ValueFromPipelineByPropertyName = $true)]
        [string]$ContainerName,
        [string]$KeyName,
        $KeyValue
    )
    if (!$ContainerName) {
        try {
            $ContainerName = Get-ContainerNameFromLaunch
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red "No Container Found"
            Start-Sleep -Seconds 2
            return
        }
    }
    $ContainerInfo = Get-EVContainerInfo -ContainerName $ContainerName
    if (!$KeyName -or !$KeyValue) {
        $TempFileName = Join-Path $env:TEMP "$([System.Guid]::NewGuid()).xml"
        Copy-FileFromNavContainer -containerName $ContainerName -containerPath "C:\inetpub\wwwroot\$($ContainerInfo.Instance)\navsettings.json" -localPath $TempFileName
        $Config = Get-Content -Path $TempFileName | ConvertFrom-Json
        Remove-Item $TempFileName -Force
        $Keys = @{ }
        $Config.NAVWebSettings.PSObject.Properties | Where-Object { ($_.MemberType -EQ "NoteProperty") -and ($_.Name -notlike "//*") } | ForEach-Object {
            $Keys.Add($_.Name, $_.Value)
        }
        $Key = $Keys | Out-GridView -Title "Select Key to change" -OutputMode Single
        if ($null -eq $Key) {
            Write-Host
            Write-Host -ForegroundColor Red "No Key Selected"
            Start-Sleep -Seconds 2
            return
        }
        [void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')
        $Key.Value = [Microsoft.VisualBasic.Interaction]::InputBox("Selected Key: $($Key.Name) current Value: $($Key.Value)", "Type New Value")
        if (!$Key.Value) {
            return
        }
        $KeyName = $Key.Name
        $KeyValue = $Key.Value
    }
    Invoke-ScriptInNavContainer -containerName $ContainerName -ScriptBlock {
        param($KeyName, $KeyValue, $Instance)
        if ($KeyValue -eq '') {
            $KeyValue = " "
        }
        Write-Host -ForegroundColor Green "Change Property: '$KeyName' to '$KeyValue'"
        Set-NAVWebServerInstanceConfiguration -WebServerInstance $Instance -KeyName $KeyName -KeyValue $KeyValue
    } -ArgumentList $KeyName, $KeyValue , $ContainerInfo.Instance
}
Export-ModuleMember -Function Edit-EVWebServerParameter
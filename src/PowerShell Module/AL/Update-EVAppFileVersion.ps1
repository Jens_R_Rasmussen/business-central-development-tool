# TODO
function Update-EVAppFileVersion {
    [CmdLetBinding(SupportsShouldProcess)]
    param(
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $True,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateSet('Major', 'Minor', 'Build', 'Revision')]
        $UpdateVersion,
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        $AppFileName
    )
    $AppFile = Get-Content $AppFileName | ConvertFrom-Json
    $Version = @()
    $AppFile.version.Split('.') | ForEach-Object {
        $Version += [int]$_
    }
    while ($Version.Count -lt 4) {
        $Version += 0
    }
    $AppFile.version
    switch ($UpdateVersion) {
        'Major' { $Position = 0 }
        'Minor' { $Position = 1 }
        'Build' { $Position = 2 }
        'Revision' { $Position = 3 }
    }
    $Version[$Position] += 1
    $NewVersion = ''
    $Version | ForEach-Object {
        $NewVersion += "$_."
    }
    $AppFile.version = $NewVersion.TrimEnd('.')
    $AppFile.version
    $AppFile | ConvertTo-Json | Set-Content $AppFileName
}
Export-ModuleMember Update-EVAppFileVersion
<#
.Synopsis
  Get a sorted list of dependencies to see in which order you have to install apps
 .Description
  Get a sorted list of dependencies to see in which order you have to install apps
 .Parameter ContainerName
  Name of the container
 .Parameter LicenseFilePath
  Local path to dependencies
 .Example
  Get-EVSortedDependencyTree
 .Example
  New-DevelopmentEnvironment -ContainerName Dev
 .Example
  New-DevelopmentEnvironment -ContainerName Dev -Path Documents\AL\Project\App\Dependencies
#>
function Get-EVSortedDependencyTree {
    param(
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true)]
        [string]$ContainerName,
        [Parameter(Mandatory = $false)]
        [string]$Path = "$(Get-AppFolder)\Dependencies\*"
    )
    if (!$ContainerName) {
        try {
            $ContainerName = Get-ContainerNameFromLaunch
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red "No Container Found"
            Start-Sleep -Seconds 2
            return
        }
    }
    $ContainerInfo = Get-EVContainerInfo -ContainerName $ContainerName
    $ModulePath = Join-Path $ContainerInfo.ArtifactCacheFolder "*\$($ContainerInfo.Version)\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll"
    if (!(Test-Path $ModulePath)) {
        $ModulePath = Join-Path 'c:\bcartifacts.cache' "*\*\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll"
        $ModulePath = (Get-Item -Path $ModulePath | Select-Object -Last 1).Fullname  
    }
    Import-Module $ModulePath


    if (!$Path.EndsWith('*')) {
        $Path = Join-Path $Path '*'
    }
    try {
        $AllAppFiles = Get-ChildItem -Path $Path -Filter "*.app" -Exclude ".gitkeep"
        $AllApps = @()
        $AllAppFiles | ForEach-Object {
            $App = Get-NAVAppInfo -Path $_.FullName
            $AllApps += [PSCustomObject]@{
                AppId        = $App.AppId
                Version      = $App.Version
                Name         = $App.Name
                Publisher    = $App.Publisher
                ProcessOrder = 0
                Dependencies = $App.Dependencies
                Path         = $_.FullName
            }
        }
    }
    catch {
        Write-Log -LogType Error -Message $_
        throw $_
    }
    $FinalResult = @()
    $AllApps | ForEach-Object {
        $FinalResult = Add-ToDependencyTree -App $_ -DependencyArray $FinalResult -AppCollection $AllApps -Order $AllApps.Count
    }

    $FinalResult | Sort-Object ProcessOrder #| Format-Table
}
Export-ModuleMember -Function Get-EVSortedDependencyTree
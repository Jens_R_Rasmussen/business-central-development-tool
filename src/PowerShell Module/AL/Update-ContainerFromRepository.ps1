Function Update-ContainerFromRepository {
    param(
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true)]
        [string]$ContainerName,
        [Parameter(Mandatory = $true)]
        [string]$RepositoryFolder,
        [Parameter(Mandatory = $true)]
        [string]$Auth,
        [Parameter(Mandatory = $false)]
        [pscredential]$Credential,
        [Parameter(Mandatory = $false)]
        [switch]$InstallDependencies
    )

    try {
        $StartTimerNewDevelopmentEnvironmen = [System.DateTime]::Now;
        Push-Location -StackName "CurrentRunPath"
        if (!$Credential) {
            if ($Auth -eq "Windows") {
                $Credential = Get-Credential -UserName $env:USERNAME -Message "Enter your $Auth Credentials"
            }
            else {
                $SecurePassword = ConvertTo-SecureString "Logon2me" -AsPlainText -Force
                $Credential = New-Object PSCredential ("Dev", $SecurePassword)
            }
        }
        $AppFolder = Get-AppFolder -BasePath $RepositoryFolder
        Write-Host -ForegroundColor Green "Install Fonts in Container"
        try {
            Add-EVFontsToContainer -ContainerName $ContainerName -Path (Join-Path $AppFolder "Fonts")
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red $_.Exception.Message -ErrorAction SilentlyContinue
        }
        Write-Host -ForegroundColor Green "Update-Launch"
        try {
            Update-EVLaunch -ContainerName $ContainerName -LaunchFile (Join-Path $AppFolder ".vscode\launch.json") -Force
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red $_.Exception.Message -ErrorAction SilentlyContinue
        }
        Write-Host -ForegroundColor Green "Update-Addins"
        try {
            Update-EVAddins -RepositoryFolder $AppFolder -ContainerName $ContainerName
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red $_.Exception.Message -ErrorAction SilentlyContinue
        }
        Write-Host -ForegroundColor Green "Install-CSideObjects"
        try {
            Install-EVCSideObjects -ContainerName $ContainerName -AppFolder $AppFolder -Credential $Credential
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red $_.Exception.Message -ErrorAction SilentlyContinue
        }
        if ($InstallDependencies) {
            Write-Host -ForegroundColor Green "Update-ContainerDependencies"
            try {
                Update-EVContainerDependencies -ContainerName $ContainerName -DependenciesPath (Join-Path $AppFolder 'Dependencies')
            }
            catch {
                Write-Log -LogType Error -Message $_
                Write-Host -ForegroundColor Red $_.Exception.Message -ErrorAction SilentlyContinue
            }
        }
        Start-EVVSCode -RepositoryFolder $RepositoryFolder
        Pop-Location -StackName "CurrentRunPath"
    }
    catch {
        Write-Log -LogType Error -Message $_
        Write-Host -ForegroundColor Red $_
        Pop-Location -StackName "CurrentRunPath"
    }
    [int]$Seconds = ([System.DateTime]::Now).Subtract($StartTimerNewDevelopmentEnvironmen).TotalSeconds
    Write-Host -ForegroundColor Green "Done in $Seconds Seconds"
}
Export-ModuleMember Update-ContainerFromRepository
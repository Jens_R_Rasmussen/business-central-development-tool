function Invoke-EVCompileALApp {
    param(
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true)]
        [string]$AlCompilerPath
    )
    $Alc = $null
    if ($AlCompilerPath) {
        $Alc = Get-ChildItem -Path $AlCompilerPath -Filter "alc.exe" -Recurse | Sort-Object VersionInfo -Descending | Select-Object -First 1
    }
    $App = Get-Content (Join-Path $(Get-AppFolder) app.json) -ErrorAction SilentlyContinue | ConvertFrom-Json
    if (!$Alc) {
        if ($App) {
            if (!(Get-Member -InputObject $App -Name 'runtime')) {
                $ALc = Get-ChildItem -Path (Join-Path $env:USERPROFILE ".vscode\extensions") -Filter "alc.exe" -Recurse | Sort-Object VersionInfo | Select-Object -First 1
            }
        }
    }
    if (!$ALc) {
        $ALc = Get-ChildItem -Path (Join-Path $env:USERPROFILE ".vscode\extensions") -Filter "alc.exe" -Recurse | Sort-Object VersionInfo -Descending | Select-Object -First 1
    }
    Write-Host -ForegroundColor Green "Start Build '$($App.name)' With Compiler Version: $($ALc.VersionInfo.ProductVersion)"
    $LogFile = Join-Path $env:TEMP "$([Guid]::NewGuid()).log"

    $assemblyprobingpaths = @()
    try {
        $UserSettings = Get-Content -Path (Join-Path $env:APPDATA 'Code\user\settings.json') | ConvertFrom-Json
        $UserSettings."al.assemblyProbingPaths" | ForEach-Object {
            if (Test-Path $_ -PathType Container) {
                if (!$assemblyprobingpaths.Contains($_)) {
                    $assemblyprobingpaths += $_
                }
            }
        }
    }
    catch { }
    try {
        $WorkspaceSettings = Get-Content -Path '.\.vscode\settings.json' -ErrorAction SilentlyContinue | ConvertFrom-Json
        if ($WorkspaceSettings."al.assemblyProbingPaths") {
            $WorkspaceSettings."al.assemblyProbingPaths" | ForEach-Object {
                if (Test-Path $_ -PathType Container) {
                    if (!$assemblyprobingpaths.Contains($_)) {
                        $assemblyprobingpaths += $_
                    }
                }
            }
        }
    }
    catch { }
    try {
        $FolderSettings = Get-Content -Path '.\app\.vscode\settings.json' -ErrorAction SilentlyContinue | ConvertFrom-Json
        if ($FolderSettings."al.assemblyProbingPaths") {
            $FolderSettings."al.assemblyProbingPaths" | ForEach-Object {
                if (Test-Path $_ -PathType Container) {
                    if (!$assemblyprobingpaths.Contains($_)) {
                        $assemblyprobingpaths += $_
                    }
                }
            }
        }
    }
    catch { }
    $assemblyprobingpathsParameter = ''
    if ($assemblyprobingpaths.Count -gt 0) {
        $assemblyprobingpaths | ForEach-Object {
            $assemblyprobingpathsParameter += "$_,"
        }
        $assemblyprobingpathsParameter = $assemblyprobingpathsParameter.TrimEnd(',')

        Start-Process -FilePath $ALc.FullName -ArgumentList "/project:$(Get-AppFolder) /packagecachepath:$(Get-AppFolder)\.alpackages /assemblyprobingpaths:$assemblyprobingpathsParameter" -Wait -NoNewWindow -RedirectStandardOutput $LogFile
    }
    else {
        Start-Process -FilePath $ALc.FullName -ArgumentList "/project:$(Get-AppFolder) /packagecachepath:$(Get-AppFolder)\.alpackages /assemblyprobingpaths:C:/windows/assembly" -Wait -NoNewWindow -RedirectStandardOutput $LogFile
    }
    $LogContent = Get-Content $LogFile
    $LogContent | ForEach-Object {
        if ($_.Contains(": error")) {
            throw "Compile Error: $_"
        }
    }
    Remove-Item -Path $LogFile -Force -ErrorAction SilentlyContinue
    Write-Host -ForegroundColor Green "Build Complete '$($App.name)'"
}
Export-ModuleMember -Function Invoke-EVCompileALApp
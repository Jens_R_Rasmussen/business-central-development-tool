<#
.Synopsis
  Add dependencies to app.json
 .Description
  Open a list of installed app's in container
  Mark the app's you want to add to app.json
 .Example
  Add-EVDependenciesToAppFile
#>
function Add-EVDependenciesToAppFile {
    if (Test-Path (Join-Path $(Get-AppFolder) "app.json")) {
        try {
            $ContainerName = Get-ContainerNameFromLaunch
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red "No Container Found"
            Start-Sleep -Seconds 2
            return
        }
        $Apps = Invoke-ScriptInBCContainer -containerName $ContainerName -scriptblock {
            param(
                [string]$Instance
            )
            Get-NAVAppInfo -ServerInstance $Instance
        } -argumentList (Get-EVContainerInfo $ContainerName).Instance
    }
    else {
        throw "Must be run from AL App or Workspace directory"
    }
    $Dependencies = $Apps | Select-Object AppId, Name, Publisher, Version | Out-GridView -Title "Select App" -OutputMode Multiple
    if ($Dependencies) {
        $AppPath = Join-Path $(Get-AppFolder) "app.json"
        $App = Get-Content -Path $AppPath -ErrorAction SilentlyContinue | ConvertFrom-Json
        if (!($App | Get-Member dependencies)) {
            $App | Add-Member -MemberType NoteProperty -Name dependencies -Value @();
        }
        $Dependencies | ForEach-Object {
            $CurrentDependency = $App.dependencies | Where-Object appId -eq $_.AppId
            $NewApp = @{appId = $_.AppId; name = $_.Name; publisher = $_.Publisher; version = $_.Version }
            if ($CurrentDependency) {
                $App.dependencies[$App.dependencies.IndexOf($CurrentDependency)] = $NewApp
            }
            else {
                $App.dependencies += $NewApp
            }
        }
        $App | ConvertTo-Json | Set-Content -Path $AppPath
    }
}
Export-ModuleMember -Function Add-EVDependenciesToAppFile
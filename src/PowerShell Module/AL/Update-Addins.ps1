<#
 .Synopsis
  Copy Addins to NAV/BC Container
 .Description
  Copy Addins to NAV/BC Container and restart service tier
 .Parameter ContainerName
  Name of the container
 .Parameter RepositoryFolder
  Path of the RepositoryFolder
  Copy all from RepositoryFolder\Add-ins
 .Example
  Update-EVAddins -ContainerName navserver -RepositoryFolder Documents\AL\Project\App\
#>
function Update-EVAddins {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true)]
        [string]$RepositoryFolder,
        [Parameter(Mandatory = $true)]
        [string]$ContainerName,
        [Parameter(Mandatory = $false)]
        [string]$DefaultFolder = '.\Add-ins'
        
    )
    Push-Location -StackName "Default"
    Push-Location $RepositoryFolder
    if (Test-Path $DefaultFolder) {
        $Files = Get-ChildItem -Path $DefaultFolder -Exclude ".gitkeep" -Recurse -ErrorAction SilentlyContinue
        if (!$Files) {
            Pop-Location -StackName "Default"
            return
        }
        Write-Host
        Write-Host -ForegroundColor Green "Copy Add-ins from $(Join-Path $RepositoryFolder $DefaultFolder)"
        $ContainerAddinsFolder = Join-Path $env:ProgramData "BcContainerHelper\Extensions\$ContainerName\My\Copy Add-Ins"
        Copy-Item -Path $DefaultFolder -Destination $ContainerAddinsFolder -Recurse -Force -ErrorAction SilentlyContinue
        Invoke-ScriptInNavContainer -containerName $ContainerName -argumentList $DefaultFolder -scriptblock {
            param (
                [Parameter(Mandatory = $true)]
                $DefaultFolder)
            $DestinationFolder = Get-Item 'C:\Program Files\Microsoft Dynamics NAV\*\Service\Add-ins\'
            if ($DefaultFolder.ToUpper() -ne '.\ADD-INS') {
                $DestinationFolder = Join-Path $DestinationFolder $DefaultFolder
            }
            if (!(Test-Path $DestinationFolder)) {
                New-Item -Path $DestinationFolder -ItemType Directory
            }
            Copy-Item -Path "c:\Run\My\Copy Add-Ins\*" -Destination $DestinationFolder -Recurse -Force
            Import-Module 'C:\Program Files\Microsoft Dynamics NAV\*\Service\NavAdminTool.ps1' | Out-Null
            $CurrentServerInstance = Get-NAVServerInstance -ServerInstance NAV
            if (!$CurrentServerInstance) {
                $CurrentServerInstance = Get-NAVServerInstance -ServerInstance BC
            }
            Write-Output "Restart $($CurrentServerInstance.DisplayName)"
            $CurrentServerInstance | Set-NAVServerInstance -Restart
        }
        Pop-Location -StackName "Default"
        Remove-Item $ContainerAddinsFolder -Force -Recurse -ErrorAction SilentlyContinue
        Write-Host
        Write-Host -ForegroundColor Green "Done"
    }
}
Export-ModuleMember Update-EVAddins
function Get-ContainerNameFromLaunch {
    $LaunchFile = Get-Item (Join-Path $(Get-AppFolder) ".vscode\launch.json") -ErrorAction SilentlyContinue
    if (!$LaunchFile) {
        throw "$(Join-Path $(Get-AppFolder) ".vscode\launch.json") does not exist"
    }
    $Launch = (Get-Content $LaunchFile) -join '' | ConvertFrom-Json -ErrorAction SilentlyContinue
    $List = @()
    $Servers = $Launch.configurations | Where-Object { $_.type -eq "al" -and $_.request -eq "launch" -and ($_.serverinstance -eq "NAV" -or $_.serverinstance -eq "BC") } | Select-Object server
    if (!$Servers) {
        throw "Now Container exist in $($LaunchFile.FullName)"
    }
    $Servers | ForEach-Object {
        $ContainerName = $_.server.Split('/') | Select-Object -Last 1
        $List += @{Name = "$ContainerName" }
    }
    $Containers = ($List | ConvertTo-Json | ConvertFrom-Json)
    $ContainerName = ''
    if ($Containers.GetType().BaseType -ne [System.Array]) {
        $ContainerName = $Containers.Name
    }
    else {
        $Selected = $Containers | Out-GridView -OutputMode Single -Title "Select Container"
        if ($Selected) {
            $ContainerName = $Selected.Name
        }
        else {
            throw "No Container Selected"
        }
    }
    $Container = docker ps -a --format "{{ json . }}" --no-trunc | ConvertFrom-Json | Where-Object { $_.Names.ToUpper() -eq $ContainerName.ToUpper() } | Select-Object -First 1
    return $Container.Names
}
<#
 .Synopsis
  Install or Update Dependencies in NAV/BC Container
 .Description
  Install or Update Dependencies in NAV/BC Container
  Read the install.json and process the files in that order they are in install.json
  Example install.json
  {
    "apps": [
        "Dependent App 1.app",
        "Dependent App 2.app"
    ]
  }
 .Parameter ContainerName
  Name of the container where the Apps will be installed or Updated
 .Parameter DependenciesPath
  Path of the folder where the install.json and apps are placed
  if not definded '.\Dependencies\'
 .Example
  Update-ContainerDependencies -ContainerName navserver
#>
function Update-EVContainerDependencies {
    [CmdletBinding(SupportsShouldProcess, ConfirmImpact = 'Medium')]
    param (
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $True,
            ValueFromPipelineByPropertyName = $true)]
        [String]$ContainerName,
        [Parameter(Mandatory = $false,
            ValueFromPipelineByPropertyName = $true)]
        $DependenciesPath = "$(Get-AppFolder)\Dependencies\"
    )

    If (!$ContainerName) {
        try {
            $ContainerName = Get-ContainerNameFromLaunch
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red "No Container Found"
            Start-Sleep -Seconds 2
            return
        }
    }
    #$ContainerInfo = Get-EVContainerInfo $ContainerName
    if (Test-Path (Join-Path $DependenciesPath 'install.json')) {
        $Dependencies = Get-Content (Join-Path $DependenciesPath 'install.json') | ConvertFrom-Json
        $Dependencies.apps | ForEach-Object {
            $AppFile = Join-Path $DependenciesPath $_
            Install-EVAppFileInContainer -ContainerName $ContainerName -AppFile $AppFile #-Instance $ContainerInfo.Instance
        }
    }
    else {
        (Get-EVSortedDependencyTree -ContainerName $ContainerName -Path $DependenciesPath) | ForEach-Object {
            Install-EVAppFileInContainer -ContainerName $ContainerName -AppFile $_.Path #-Instance $ContainerInfo.Instance
        }
    }
}
Export-ModuleMember -Function Update-EVContainerDependencies
<#
 .Synopsis
  Add or Create NAV/BC Container to Launch.json
 .Description
  Add or Create NAV/BC Container to Launch.json
  Only add config to existing launch file if run with -Forec
 .Parameter ContainerName
  Name of the container
 .Parameter LaunchFile
  Path of the LaunchFile
  Default .\.vscode\launch.json
 .Example
  Update-EVLaunch -ContainerName navserver
 .Example
  Update-EVLaunch -ContainerName navserver -LaunchFile Documents\AL\Project\App\.vscode\launch.json
#>
function Update-EVLaunch {
    [CmdletBinding(SupportsShouldProcess)]
    param(
        [parameter(Mandatory = $true,
            ValueFromPipeline = $true)]
        [string]$ContainerName,
        [Parameter(Mandatory = $false)]
        [string]$LaunchFile = "$(Get-AppFolder)\.vscode\launch.json",
        [Parameter(Mandatory = $false)]
        [Switch]$Force
    )
    $ExistingLaunch = $null
    if (!$LaunchFile.Contains(".vscode\launch.json")) {
        Write-Host -ForegroundColor Red "Wrong launch path: $LaunchFile"
        return
    }
    if (Test-Path $LaunchFile) {
        $ExistingLaunch = (Get-Content -Path $LaunchFile -ErrorAction SilentlyContinue) -join '' | ConvertFrom-Json -ErrorAction SilentlyContinue
        if ($ExistingLaunch -and $Force) {
            $CurrConfig = $ExistingLaunch.configurations | Where-Object { $_.server -eq "http://$ContainerName" }
            if ($CurrConfig) {
                Write-Host -ForegroundColor Green "Launch.json already containes the right configuration"
                return
            }
        }
        if ($ExistingLaunch -and !$Force) {
            Write-host -ForegroundColor Yellow "Launch.json already exist to update use this command: Update-EVLaunch -ContainerName $ContainerName -LaunchFile '$LaunchFile' -Force"
            return
        }
    }
    $ContainerInfo = Get-EVContainerInfo -ContainerName $ContainerName
    $Object = New-Object psobject
    $Configuration = New-Object psobject
    $Object | Add-Member -MemberType NoteProperty -Name version -Value "0.2.0"
    $Object | Add-Member -MemberType NoteProperty -Name configurations -Value @($Configuration)
    $Configuration | Add-Member -MemberType NoteProperty -Name type -Value al
    $Configuration | Add-Member -MemberType NoteProperty -Name request -Value launch
    $Configuration | Add-Member -MemberType NoteProperty -Name name -Value "Dev $ContainerName Container"
    $Configuration | Add-Member -MemberType NoteProperty -Name server -Value "http://$ContainerName"
    $Configuration | Add-Member -MemberType NoteProperty -Name serverInstance -Value $ContainerInfo.Instance
    $Configuration | Add-Member -MemberType NoteProperty -Name tenant -Value 'default'
    $Configuration | Add-Member -MemberType NoteProperty -Name authentication -Value UserPassword
    $Configuration | Add-Member -MemberType NoteProperty -Name startupObjectId -Value 22
    $Configuration | Add-Member -MemberType NoteProperty -Name startupObjectType -Value Page
    $Configuration | Add-Member -MemberType NoteProperty -Name breakOnError -Value $true
    if ($ContainerInfo.Instance -eq 'BC') {
        $Configuration | Add-Member -MemberType NoteProperty -Name launchBrowser -Value $true
        $Configuration | Add-Member -MemberType NoteProperty -Name enableLongRunningSqlStatements  -Value $true
        $Configuration | Add-Member -MemberType NoteProperty -Name enableSqlInformationDebugger  -Value $true
    }
    if ($ExistingLaunch) {
        $ExistingLaunch.configurations | ForEach-Object {
            $Object.configurations += $_
        }
    }
    $Path = Split-Path $LaunchFile
    if (!(Test-Path $Path)) {
        New-Item -Path $Path -ItemType Directory -Force | Out-Null
    }
    $Object | ConvertTo-Json | Set-Content -Path $LaunchFile
    Write-Host -ForegroundColor Green "Launch.json is updated"
}
Export-ModuleMember Update-EVLaunch
<#
.Synopsis
  Create New Development Environment with Running NAV/BC Container
 .Description
  Create New Development Environment from GET repository with Running NAV/BC Container
  Default created with NavUserPassword
  Username: Dev
  Password: Logon2me
 .Parameter ContainerName
  Name of the container
 .Parameter ImageTag
  Business Central version
  Get List from https://mcr.microsoft.com/v2/businesscentral/onprem/tags/list
  Or https://mcr.microsoft.com/v2/dynamicsnav/tags/list
 .Parameter GITRepositoryUrl
  Eks. https://bitbucket.org/Account/MyRepository.git
 .Parameter DockerImageRepositoryUrl
  Docker Image Repository default "mcr.microsoft.com/businesscentral/onprem"
  Version 2016 - 2018 "mcr.microsoft.com/dynamicsnav"
 .Parameter LicenseFilePath
  Local path to license file
  if not definded if will use cmdlet Get-LicenseFilePath
 .Parameter Isolation
  Default Process
 .Parameter Auth
  Default NavUserPassword
 .Parameter Credential
  Set your own credentials
 .Parameter PublishPorts
  Set NAT ports example 7047 -> 7047 you can now access port 7047 from another computer
 .Example
  New-DevelopmentEnvironment -ContainerName Dev -ImageTag 1904-cu2-dk -GITRepositoryUrl https://bitbucket.org/Account/MyRepository.git
#>
Function New-EVDevelopmentEnvironment {
    param(
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true)]
        [string]$ContainerName,
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [string]$GITRepositoryUrl,
        [Parameter(Mandatory = $false,
            ValueFromPipelineByPropertyName = $true)]
        [string]$ArtifactUrl, 
        [Parameter(Mandatory = $false)]
        [string]$ImageTag,
        [Parameter(Mandatory = $false)]
        #[ValidateSet("mcr.microsoft.com/businesscentral/onprem", "mcr.microsoft.com/dynamicsnav")]
        [string]$DockerImageRepositoryUrl = "mcr.microsoft.com/businesscentral/onprem",
        [Parameter(Mandatory = $false)]
        [string]$LicenseFilePath,
        [Parameter(Mandatory = $false)]
        [ValidateSet("process", "hyperv")]
        [string]$Isolation = "process",
        [Parameter(Mandatory = $false)]
        [ValidateSet("Windows", "UserPassword")]
        [string]$Auth = "UserPassword",
        [Parameter(Mandatory = $false)]
        [pscredential]$Credential,
        [Parameter(Mandatory = $false)]
        [ValidateSet("Desktop", "MyDocuments", "None")]
        [string]$ShortCutLocation = "Desktop",
        [Parameter(Mandatory = $false)]
        [switch]$IncludeTestToolkit,
        [Parameter(Mandatory = $false)]
        [switch]$IncludeTestLibrariesOnly,
        [Parameter(Mandatory = $false)]
        [Int32]$WebClientPort,
        [Parameter(Mandatory = $false)]
        [Int32[]]$PublishPorts,
        [Parameter(Mandatory = $false)]
        [Int32]$MajorVersion,
        [Parameter(Mandatory = $false)]
        [string]$DatabaseServer,
        [Parameter(Mandatory = $false)]
        [string]$DatabaseInstance,
        [Parameter(Mandatory = $false)]
        [string]$DatabaseName,
        [Parameter(Mandatory = $false)]
        [pscredential]$DatabaseCredential,
        [Parameter(Mandatory = $false)]
        [string]$bakFile,
        [Parameter(Mandatory = $false)]
        [switch]$InstallDependencies,
        [Parameter(Mandatory = $false)]
        [string]$MainRepositoryFolder,
        [Parameter(Mandatory = $false)]
        [string]$CustomParameter,
        [Parameter(Mandatory = $false)]
        [switch]$Multitenant        
    )

    try {
        $StartTimerNewDevelopmentEnvironmen = [System.DateTime]::Now;
        Push-Location -StackName "CurrentRunPath"
        $ContinueInstall = $true
        # $ContainerInfo = Get-EVContainerInfo -ContainerName $ContainerName
        # if ($ContainerInfo) {
        #     Write-Host
        #     Write-Host -ForegroundColor Red "Container " -NoNewline
        #     Write-Host -ForegroundColor Green $ContainerName -NoNewline
        #     Write-Host -ForegroundColor Red " is allready created do you want to overwrite (Y/N) " -NoNewline
        #     $ContinueInstall = (Read-host).ToUpper() -eq 'Y'
        #     if (!$ContinueInstall) {
        #         if ($ContainerInfo.Status.Substring(0, 2).ToUpper() -ne 'UP') {
        #             Write-Host
        #             Write-Host -ForegroundColor Green "Start Container: $ContainerName"
        #             Start-Container -ContainerName $ContainerName
        #         }
        #     }
        # }
        if (!$Credential) {
            if ($Auth -eq "Windows") {
                $Credential = Get-Credential -UserName $env:USERNAME -Message "Enter your $Auth Credentials"
            }
            else {
                $SecurePassword = ConvertTo-SecureString "Logon2me" -AsPlainText -Force
                $Credential = New-Object PSCredential ("Dev", $SecurePassword)
            }
        }

        if ($ContinueInstall) {
            #Write-Host -ForegroundColor Green "Creating Container: $ContainerName"
            $NewContainerArgs = @{ }
            $NewContainerArgs += @{Credential = $Credential }
            $NewContainerArgs += @{ContainerName = $ContainerName }
            if ($ArtifactUrl) {
                $NewContainerArgs += @{artifactUrl = $ArtifactUrl }
            }
            else {
                $NewContainerArgs += @{ImageTag = $ImageTag }
            }
            $NewContainerArgs += @{Auth = $Auth }
            $NewContainerArgs += @{Isolation = $Isolation }
            $NewContainerArgs += @{DockerImageRepositoryUrl = $DockerImageRepositoryUrl }
            $NewContainerArgs += @{ShortCutLocation = $ShortCutLocation }
            if ($IncludeTestToolkit) {
                $NewContainerArgs += @{IncludeTestToolkit = $IncludeTestToolkit } 
                if ($IncludeTestLibrariesOnly) {
                    $NewContainerArgs += @{includeTestLibrariesOnly = $IncludeTestLibrariesOnly }
                }
            }
            if ($LicenseFilePath) {
                $NewContainerArgs += @{licenseFile = $LicenseFilePath }
            }
            if ($WebClientPort) {
                $NewContainerArgs += @{WebClientPort = $WebClientPort }
            }
            if ($PublishPorts) {
                $NewContainerArgs += @{PublishPorts = $PublishPorts }
            }
            if ($MajorVersion) {
                $NewContainerArgs += @{MajorVersion = $MajorVersion }
            }
            if ($bakFile) {
                $NewContainerArgs += @{bakFile = $bakFile }
            }
            if ($DatabaseServer) {
                $NewContainerArgs += @{databaseServer = $DatabaseServer }
                if ($DatabaseInstance) {
                    $NewContainerArgs += @{databaseInstance = $DatabaseInstance }
                }
                $NewContainerArgs += @{databaseName = $DatabaseName }
                if ($DatabaseCredential) {
                    $NewContainerArgs += @{databaseCredential = $DatabaseCredential }
                }
            }
            IF ($Multitenant) {
                $NewContainerArgs += @{multitenant = $true }
            }
            if ($CustomParameter) {
                $NewContainerArgs += @{CustomParameter = $CustomParameter }
            }
            # if ($ContainerInfo) {
            #     $NewContainerArgs += @{ContainerInfo = $ContainerInfo }
            # }
            New-EVContainer @NewContainerArgs
        }
        $RepositoryFolder = New-EVRepository -GitRepositoryUrl $GITRepositoryUrl -ContainerName $ContainerName -LocalRepositoryFolder $MainRepositoryFolder -ShortCutLocation $ShortCutLocation
        $AppFolder = Get-AppFolder -BasePath $RepositoryFolder
        Write-Host -ForegroundColor Green "Install Fonts in Container"
        try {
            Add-EVFontsToContainer -ContainerName $ContainerName -Path (Join-Path $AppFolder "Fonts")
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red $_.Exception.Message -ErrorAction SilentlyContinue
        }
        Write-Host -ForegroundColor Green "Update-Launch"
        try {
            Update-EVLaunch -ContainerName $ContainerName -LaunchFile (Join-Path $AppFolder ".vscode\launch.json") -Force
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red $_.Exception.Message -ErrorAction SilentlyContinue
        }
        Write-Host -ForegroundColor Green "Update-Addins"
        try {
            Update-EVAddins -RepositoryFolder $AppFolder -ContainerName $ContainerName
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red $_.Exception.Message -ErrorAction SilentlyContinue
        }
        Write-Host -ForegroundColor Green "Install-CSideObjects"
        try {
            Install-EVCSideObjects -ContainerName $ContainerName -AppFolder $AppFolder -Credential $Credential
        }
        catch {
            Write-Log -LogType Error -Message $_
            Write-Host -ForegroundColor Red $_.Exception.Message -ErrorAction SilentlyContinue
        }
        if ($InstallDependencies) {
            Write-Host -ForegroundColor Green "Update-ContainerDependencies"
            try {
                Update-EVContainerDependencies -ContainerName $ContainerName -DependenciesPath (Join-Path $AppFolder 'Dependencies')
            }
            catch {
                Write-Log -LogType Error -Message $_
                Write-Host -ForegroundColor Red $_.Exception.Message -ErrorAction SilentlyContinue
            }
        }
        Start-EVVSCode -RepositoryFolder $RepositoryFolder
        Pop-Location -StackName "CurrentRunPath"
    }
    catch {
        Write-Log -LogType Error -Message $_
        Write-Host -ForegroundColor Red $_
        Pop-Location -StackName "CurrentRunPath"
    }
    [int]$Seconds = ([System.DateTime]::Now).Subtract($StartTimerNewDevelopmentEnvironmen).TotalSeconds
    Write-Host -ForegroundColor Green "Done in $Seconds Seconds"
}
Export-ModuleMember New-EVDevelopmentEnvironment
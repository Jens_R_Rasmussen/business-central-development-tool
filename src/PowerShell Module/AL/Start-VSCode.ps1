<#
 .Synopsis
  Start Visual Studion Code
 .Description
  Start Visual Studion Code
 .Parameter RepositoryFolder
  Folder to start in
  If app folder exist in RepositoryFolder it will start in Appfolder
 .Example
  Start-EVVSCode -RepositoryFolder .
#>
function Start-EVVSCode {
    [CmdletBinding(SupportsShouldProcess)]
    param(
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true)]
        [string]$RepositoryFolder
    )
    Write-Host
    Write-Host -ForegroundColor Green "Open VS Code"
    if (Test-Path (Join-Path $RepositoryFolder "*.code-workspace")) {
        $WorkspaceFile = Get-Item (Join-Path $RepositoryFolder "*.code-workspace") | Select-Object -First 1
        Code $WorkspaceFile.FullName
    }
    else {
        Code (Get-AppFolder -BasePath $RepositoryFolder)
    }
}
Export-ModuleMember Start-EVVSCode
function Add-ToDependencyTree() {
    param(
        [PSObject] $App,
        [PSObject[]] $DependencyArray,
        [PSObject[]] $AppCollection,
        [Int] $Order = 1
    )
    if ($App) {
        foreach ($Dependency in $App.Dependencies) {
            $DependencyArray = Add-ToDependencyTree `
                -App ($AppCollection | Where-Object AppId -eq $Dependency.AppId) `
                -DependencyArray $DependencyArray `
                -AppCollection $AppCollection `
                -Order ($Order - 1)
        }
        if ((!$DependencyArray) -or -not($DependencyArray | Where-Object AppId -eq $App.AppId)) {
            $DependencyArray += $App
            ($DependencyArray | Where-Object AppId -eq $App.AppId).ProcessOrder = $Order
        }
        else {
            if (($DependencyArray | Where-Object AppId -eq $App.AppId).ProcessOrder -gt $Order) {
            ($DependencyArray | Where-Object AppId -eq $App.AppId).ProcessOrder = $Order
            }
        }
    }
    $DependencyArray
}
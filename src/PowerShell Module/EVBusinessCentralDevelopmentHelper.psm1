#Requires -PSEdition Desktop

Set-StrictMode -Version 2.0

$verbosePreference = "SilentlyContinue"
$warningPreference = 'Continue'
$errorActionPreference = 'Stop'

if ([intptr]::Size -eq 4) {
    throw "BusinessCentralDevelopmentHelper cannot run in Windows PowerShell (x86), need 64bit mode"
}
$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
$isAdministrator = $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)

. (Join-Path $PSScriptRoot "Helper\Get-ScriptPath.ps1")
. (Join-Path $PSScriptRoot "Helper\Test-RunAsAdministrator.ps1")
. (Join-Path $PSScriptRoot "Helper\Install-OrUpdateModule.ps1")
. (Join-Path $PSScriptRoot "Helper\Get-LicenseFilePath.ps1")
. (Join-Path $PSScriptRoot "Helper\Select-OpenFileDialog.ps1")
. (Join-Path $PSScriptRoot "Helper\Write-Log.ps1")
. (Join-Path $PSScriptRoot "Helper\Get-AppFolder.ps1")
. (Join-Path $PSScriptRoot "Helper\Update-EVModule.ps1")

. (Join-Path $PSScriptRoot "AL\Get-ContainerNamefromLaunch.ps1")
. (Join-Path $PSScriptRoot "AL\Update-ContainerDependencies.ps1")
. (Join-Path $PSScriptRoot "AL\Update-Addins.ps1")
. (Join-Path $PSScriptRoot "AL\Start-VSCode.ps1")
. (Join-Path $PSScriptRoot "AL\New-DevelopmentEnvironment.ps1")
. (Join-Path $PSScriptRoot "AL\Update-Launch.ps1")
. (Join-Path $PSScriptRoot "AL\Add-DependenciesToAppFile.ps1")
. (Join-Path $PSScriptRoot "AL\Get-SortedDependencyTree.ps1")
. (Join-Path $PSScriptRoot "AL\Add-ToDependencyTree.ps1")
. (Join-Path $PSScriptRoot "AL\Update-EVAppFileVersion.PS1")
. (Join-Path $PSScriptRoot "AL\Invoke-EVGenerateXliffFiles.ps1")
. (Join-Path $PSScriptRoot "AL\Invoke-EVCompileALApp.ps1")
. (Join-Path $PSScriptRoot "AL\Update-ContainerFromRepository.ps1")

. (Join-Path $PSScriptRoot "Container\Install-AppFileInContainer.ps1")
. (Join-Path $PSScriptRoot "Container\Get-ContainerInfo.ps1")
. (Join-Path $PSScriptRoot "Container\Set-WindowsPasswordInContainer.ps1")
. (Join-Path $PSScriptRoot "Container\Start-Container.ps1")
. (Join-Path $PSScriptRoot "Container\Stop-Container.ps1")
. (Join-Path $PSScriptRoot "Container\Invoke-CompileContainerObjects.ps1")
. (Join-Path $PSScriptRoot "Container\Get-ContainerEventLog.ps1")
. (Join-Path $PSScriptRoot "Container\Install-CSideObjects.ps1")
. (Join-Path $PSScriptRoot "Container\New-EVContainer.ps1")
. (Join-Path $PSScriptRoot "Container\Add-EVFontsToContainer.ps1")
. (Join-Path $PSScriptRoot "Container\Update-EVLicense.ps1")
. (Join-Path $PSScriptRoot "Container\Get-EVRemoteImageMajorVersion.ps1")
. (Join-Path $PSScriptRoot "Container\Remove-EVContainer.ps1")
. (Join-Path $PSScriptRoot "Container\Edit-EVServiceParameter.ps1")
. (Join-Path $PSScriptRoot "Container\Edit-EVWebServerParameter.ps1")

. (Join-Path $PSScriptRoot "Repository\New-Repository.ps1")
. (Join-Path $PSScriptRoot "Repository\Sync-EVRemoteLocalRepository.ps1")

. (Join-Path $PSScriptRoot "Mics\New-EVShortCut.ps1")
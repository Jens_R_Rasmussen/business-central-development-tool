# TODO
function New-EVShortCut {
    [CmdLetBinding(SupportsShouldProcess)]
    Param (
        [Parameter(Mandatory = $true)]
        [string]$Name,
        [Parameter(Mandatory = $true)]
        [string]$TargetPath,
        [string]$WorkingDirectory = "",
        [string]$IconLocation = "",
        [string]$Arguments = "",
        [string]$Folder,
        [switch]$RunAsAdministrator = $isAdministrator
    )
    if (!(Test-Path $Folder -PathType Container)) {
        New-Item $Folder -ItemType Directory | Out-Null
    }

    if ($Folder) {
        Write-Host -ForegroundColor Green "Create Shortcut: '$Name'"
        $filename = Join-Path $Folder "$Name.lnk"
        if (Test-Path -Path $filename) {
            Remove-Item $filename -force
        }
        $Shell = $null
        $Shell = New-object -comobject WScript.Shell
        $Shortcut = $Shell.CreateShortcut($filename)
        $Shortcut.TargetPath = $TargetPath
        if ($WorkingDirectory -eq "") {
            if ($Shortcut.TargetPath -ne "") {
                $WorkingDirectory = (Split-Path $Shortcut.TargetPath)
                if (!(Test-Path $WorkingDirectory -PathType Any -ErrorAction SilentlyContinue)) {
                    $WorkingDirectory = ""
                }
            }
        }
        $Shortcut.WorkingDirectory = $WorkingDirectory
        if ($Arguments) {
            $Shortcut.Arguments = $Arguments
        }
        if ($IconLocation) {
            $IconFile = Get-Item -Path $IconLocation -ErrorAction SilentlyContinue
            if ($IconFile) {
                $NewIconLocation = Join-Path $env:ProgramData 'BcContainerHelper\Icons'
                if (!(Test-Path $NewIconLocation)) {
                    New-Item -Path $NewIconLocation -ItemType Directory -Force -ErrorAction SilentlyContinue | Out-Null
                }
                $NewIconFile = Join-Path $NewIconLocation $IconFile.Name
                if (!(Get-Item -Path $NewIconFile -ErrorAction SilentlyContinue)) {
                    Copy-Item -Path $IconLocation -Destination $NewIconFile -Force -ErrorAction SilentlyContinue
                }
                $Shortcut.IconLocation = $NewIconFile
            }
            else {
                $Shortcut.IconLocation = $IconLocation
            }
        }
        $Shortcut.save()
        if ($RunAsAdministrator) {
            $bytes = [System.IO.File]::ReadAllBytes($filename)
            $bytes[0x15] = $bytes[0x15] -bor 0x20 #set byte 21 (0x15) bit 6 (0x20) ON
            [System.IO.File]::WriteAllBytes($filename, $bytes)
        }
    }
}

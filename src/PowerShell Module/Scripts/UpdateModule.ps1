param (
    [Parameter(Mandatory = $true)]
    [String]$ModuleName,
    [Parameter(Mandatory = $true)]
    $CurrentVersion
)
if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell.exe -Verb runAs -ArgumentList $arguments, $ModuleName, $CurrentVersion
    Break
}
Write-Host -ForegroundColor Green "Running $ModuleName Update"
Update-Module $ModuleName -Force
Get-InstalledModule $ModuleName -RequiredVersion $CurrentVersion | Uninstall-Module
param (
    [Parameter(Mandatory = $true)]
    [String]$ModuleName
)
if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell.exe -Verb runAs -ArgumentList $arguments, $ModuleName
    Break
}
Write-Host -ForegroundColor Green "Installing $ModuleName"
Install-Module -Name $ModuleName -Force

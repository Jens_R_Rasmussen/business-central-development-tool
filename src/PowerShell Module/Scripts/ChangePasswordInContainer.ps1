param (
    [Parameter(Mandatory = $true)]
    $ContainerId
)
if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments, $ContainerId
    Break
}
$Session = New-PSSession -ContainerId $ContainerId -RunAsAdministrator
Invoke-Command -Session $Session -ArgumentList $env:USERNAME -ScriptBlock {
    param($UserName)
    Write-Host "Enter new password: " -NoNewline
    $Password = Read-Host -AsSecureString
    $UserAccount = Get-LocalUser -Name $UserName
    $UserAccount | Set-LocalUser -Password $Password
}
Remove-PSSession $Session
﻿Get-ChildItem -Path $PSScriptRoot -Recurse | ForEach-Object { Unblock-File -Path $_.FullName }

Remove-Module EVBusinessCentralDevelopmentHelper -ErrorAction Ignore
Uninstall-module EVBusinessCentralDevelopmentHelper -ErrorAction Ignore

$modulePath = Join-Path $PSScriptRoot "EVBusinessCentralDevelopmentHelper.psm1"
Import-Module $modulePath -DisableNameChecking

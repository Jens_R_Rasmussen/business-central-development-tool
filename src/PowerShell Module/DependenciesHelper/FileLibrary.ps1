function GetFile {
    param (
        $Path,
        $Destination
    )
    try {
        $file = (Get-Item $Path -ErrorAction Stop) | Sort-Object DirectoryName, LastWriteTime | Select-Object -Last 1 
        Copy-Item -Path $file -Destination $Destination -ErrorAction Stop
    }
    catch {
        Write-Host -ForegroundColor Red "Get File Error: $($_)"
    }
}
function Get-NewTempFolder {
    $tempFolder = "$env:TEMP\Dependencies-$([System.Guid]::NewGuid())"
    if (!(Test-Path -Path $tempFolder)) {
        New-Item -ItemType Directory -Path $tempFolder | Out-Null
    }
    $tempFolder
}

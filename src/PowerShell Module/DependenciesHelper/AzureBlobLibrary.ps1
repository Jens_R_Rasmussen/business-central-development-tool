function DownloadAzureBlob {
    param (
        $Url,
        $Folder,
        $SharedKey = '',
        $Version = '2019-12-12'
    )
    [uri]$uri = $url
    $blobInfo = @{
        filename  = ($uri.LocalPath.Split('/') | Select-Object -Last 1)
        account   = $uri.Host.Split('.')[0]
        path      = $uri.AbsolutePath
        uri       = $uri.AbsoluteUri
        sharedKey = $SharedKey
    }
    $newFile = (Join-Path $Folder "$([System.Guid]::NewGuid())-$($blobInfo.filename)")
    if (($blobInfo.sharedKey -eq '') -or ($null -eq $blobInfo.sharedKey)) {
        Invoke-WebRequest -UseBasicParsing -Uri $blobInfo.uri -OutFile $newFile
    }
    else {
        $date = [datetime]::UtcNow.ToString('R')
        $authString = "GET`n`n`n`n`n`n`n`n`n`n`n`nx-ms-date:$($date)`nx-ms-version:$($Version)`n/$($blobInfo.account)$($blobInfo.path)"
        $hmacsha = New-Object System.Security.Cryptography.HMACSHA256
        $hmacsha.key = [Convert]::FromBase64String($blobInfo.sharedKey)
        # $hmacsha.key = [Convert]::FromBase64String('UpaiJZUolQL7bbR+vuL7Ibt/3X6iujJouwrcJcRw0oWTO1FqfQlqRrJxB2W3iXs4ugEpCjhTBOhaC4pZkmMNig==')
        $signature = $hmacsha.ComputeHash([Text.Encoding]::UTF8.GetBytes($authString))
        $signature = [Convert]::ToBase64String($signature)
        # $signature
        $headers = @{
            Authorization  = "SharedKey $($blobInfo.account):$($signature)";
            "x-ms-date"    = $date;
            "x-ms-version" = $version
        }

        if ($uri.Query -ne '') {
            $blobInfo.uri = $blobInfo.uri.Replace($uri.Query, " ")
        }
        
        Invoke-WebRequest -UseBasicParsing -Uri $blobInfo.uri -Headers $headers -OutFile $newFile
    }
    $newFile
}

function ListBlobs {
    param (
        $url,
        $sharedKey,
        $Version = '2021-04-10'
    )
    if (($sharedKey -eq '') -or ($null -eq $sharedKey)) {
        $response = Invoke-WebRequest -Uri $url -UseBasicParsing
    }
    else {
        [uri]$uri = $url 
        $blobInfo = @{
            filename  = ($uri.LocalPath.Split('/') | Select-Object -Last 1)
            account   = $uri.Host.Split('.')[0]
            path      = $uri.AbsolutePath
            container = $uri.AbsolutePath.Split('/')[1]
            uri       = $uri.AbsoluteUri
            query     = $uri.Query
            sharedKey = $sharedKey
        }
        $date = [datetime]::UtcNow.ToString('R')
        $query = $blobInfo.query.Split('&');
        $prefix = $query.Get($query.length - 1).Split('=')[1]
        $prefix = [System.Web.HttpUtility]::UrlDecode($prefix);
        $authString = "GET`n`n`n`n`n`n`n`n`n`n`n`nx-ms-date:$($date)`nx-ms-version:$($Version)`n/$($blobInfo.account)/$($blobInfo.container)`ncomp:list`nprefix:$prefix`nrestype:container"
        $hmacsha = New-Object System.Security.Cryptography.HMACSHA256
        $hmacsha.key = [Convert]::FromBase64String($blobInfo.sharedKey)
        # $hmacsha.key = [Convert]::FromBase64String('UpaiJZUolQL7bbR+vuL7Ibt/3X6iujJouwrcJcRw0oWTO1FqfQlqRrJxB2W3iXs4ugEpCjhTBOhaC4pZkmMNig==')
        $signature = $hmacsha.ComputeHash([Text.Encoding]::UTF8.GetBytes($authString))
        $signature = [Convert]::ToBase64String($signature)
        # $signature
        $headers = @{
            Authorization  = "SharedKey $($blobInfo.account):$($signature)";
            "x-ms-date"    = $date;
            "x-ms-version" = $version
        }

        if ($uri.Query -ne '') {
            $blobInfo.uri = $blobInfo.uri.Replace($uri.Query, " ")
        }
        
        $response = Invoke-WebRequest -UseBasicParsing -Uri $uri -Headers $headers
    }
    $response
}
function UploadAppToAzureBlob {
    param(
        $appFile,
        [string]$containerUrl,
        $sasToken,
        [switch]$overwrite
    )
    $appFile = Get-Item $appFile
    $module = (Get-Item 'C:\bcartifacts.cache\*\*\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll') | Select-Object -Last 1
    Import-Module $module
    $appInfo = Get-NAVAppInfo -Path $appFile
    $blobInfo = @{
        name      = $appInfo.Name.Replace('/', ' ').TrimEnd('.');
        publisher = $appInfo.Publisher.Replace('/', ' ').TrimEnd('.');
        version   = $appInfo.Version;
        filename  = $appFile.Name
    }
    $prefix = [System.Web.HttpUtility]::UrlEncode("$($blobInfo.publisher)/$($blobInfo.name)/$($blobInfo.version)/$($blobInfo.filename)")
    $url = "$($containerUrl.TrimEnd('/'))$($sasToken)&restype=container&comp=list&prefix=$($prefix)"
    $response = Invoke-WebRequest -Uri $url -UseBasicParsing
    [xml]$blobList = $response.Content.Substring(3)
    if ($blobList.SelectNodes('//Blobs/Blob/Name').'#text') {
        if (!$overwrite) {
            Write-Host -ForegroundColor Green "$($blobInfo.filename) already exist!"
            return
        }
    } 
    
    $url = "$($containerUrl.TrimEnd('/'))/$($blobInfo.publisher)/$($blobInfo.name)/$($blobInfo.version)/$($blobInfo.filename)$($sasToken)"
    $headers = @{
        "x-ms-blob-type" = 'BlockBlob' 
    }
    Write-Host -ForegroundColor Green $blob
    try {
        Write-Host -ForegroundColor Green "Uploading: $($blobInfo.filename)"
        $result = Invoke-WebRequest -Method PUT -Uri $url -InFile $appFile.FullName -Headers $headers 
        if ($result.StatusCode -ne 201) {
            Write-Host -ForegroundColor Red "Error on appfile: $($blobInfo.filename)"
            Write-Host -ForegroundColor Red $result
        }
        else {
            Write-Host -ForegroundColor Green "Done: $($blobInfo.filename)"
        }
    }
    catch {
        Write-Host -ForegroundColor Red "Error on appfile: $($blobInfo.filename)"
        Write-Host -ForegroundColor Red $_
    }
    
}
function installApps {
    param(
        $container,
        $appFiles
    )
    Write-Host -ForegroundColor Green "Sorting all dependencies"
    $apps = Sort-AppFilesByDependencies -appFiles $appFiles -containerName $container
    $installedApps = Get-BcContainerAppInfo -containerName $container
    Write-Host -ForegroundColor Green "Installing all dependencies"
    $apps | ForEach-Object {
        $module = (Get-Item 'C:\bcartifacts.cache\*\*\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll') | Select-Object -Last 1
        Import-Module $module
        $appInfo = Get-NAVAppInfo -Path $_
        $currentInstalledApps = $installedApps | Where-Object AppId -EQ $appInfo.AppId
        if ($currentInstalledApps) {
            $currentInstalledApps | ForEach-Object {
                $currentInstalledApp = $_
                if ($currentInstalledApp.Version -lt $appInfo.Version) {
                    Publish-BcContainerApp -containerName $container -appFile $_ -sync -install -upgrade -tenant default -skipVerification
                    UnPublish-BcContainerApp -containerName $container -name $currentInstalledApp.Name -publisher $currentInstalledApp.Publisher -version $currentInstalledApp.Version -tenant default
                }
                else {
                    if ($currentInstalledApp.Version -eq $appInfo.Version) {
                        Write-Host -ForegroundColor Green "$($appInfo.name) Version: $($appInfo.version) Already Installed"
                    }
                    else {
                        Write-Host -ForegroundColor Green -NoNewline "$($appInfo.name) Version: $($currentInstalledApp.version) Already Installed!"
                        Write-Host -ForegroundColor Red " Can't Install Version: $($appInfo.version)"
                    }
                } 
            }  
        }
        else {
            Publish-BcContainerApp -containerName $container -appFile $_ -sync -install -tenant default -skipVerification
        }
    }
}
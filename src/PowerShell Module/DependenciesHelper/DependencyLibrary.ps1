
function Get-AllFixedDependenciesToInstall {
    param(
        $tempFolder,
        $dependencies
    )
    . ".\FileLibrary.ps1"
    . ".\DevOpsLibrary.ps1"
    . ".\AzureBlobLibrary.ps1"
    if (!$dependencies) {
        return
    }
    $dependencies | Foreach-Object {
        $entry = $_
        switch ($entry.type) {
            'File' {
                GetFile -Path $entry.path -Destination $tempFolder
            }
            'DevOps' {
                GetArtifacts -tempFolder $tempFolder -organization $entry.organization -project $entry.project -definition $entry.definition -build $entry.build -branch $entry.branch -accessToken $entry.accessToken
            }
            'Blob' {
                $file = DownloadAzureBlob -Url $entry.url -Folder $tempFolder -SharedKey $entry.sharedKey
                Write-Host -ForegroundColor Green "File Downloaded: $file"
            }
            Default {
                Write-Host "Default"
            }
        }
    }
}

function Get-Dependencies {
    param(
        $tempFolder,
        $dependencies,
        $fixedDependencies,
        [ValidateSet("current",
            "closest",
            "latest")]
        $target,
        $containerUrl,
        $sasToken,
        $sharedKey,
        $publisherBlobFilter,
        $containerVersion
    )
    . ".\AzureBlobLibrary.ps1"
    $module = (Get-Item 'C:\bcartifacts.cache\*\*\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll') | Select-Object -Last 1
    Import-Module $module
    while ($dependencies | Where-Object downloaded -ne $true) {
        $dependencies | Where-Object downloaded -ne $true | ForEach-Object {
            $dependency = $_
            if ($fixedDependencies) {
                $fixedDependency = $fixedDependencies | Where-Object id -EQ $dependency.id
            }
            if ($fixedDependency) {
                $dependency | Add-Member -NotePropertyName downloaded -NotePropertyValue $true
                $dependency | Add-Member -NotePropertyName fixed -NotePropertyValue $true
            }
            else {
                $blobInfo = @{
                    id        = $dependency.id;
                    name      = $dependency.Name.Replace('/', ' ').TrimEnd('.');
                    publisher = $dependency.Publisher.Replace('/', ' ').TrimEnd('.');
                    version   = [version]$dependency.Version;
                }
                $contains = $publisherBlobFilter | Where-Object publisher -EQ $blobInfo.publisher | Select-Object contains -First 1 
                if ($contains -ne '') {
                    $contains = $contains.contains
                }
                if ($target -eq "current") {
                    $prefix = [System.Web.HttpUtility]::UrlEncode("$($blobInfo.publisher)/$($blobInfo.name)/$($blobInfo.version)")
                    $where = "&where=id %3D '$($blobInfo.id)' AND version %3D '$($blobInfo.version)'"
                }
                else {
                    $prefix = [System.Web.HttpUtility]::UrlEncode("$($blobInfo.publisher)/$($blobInfo.name)")
                    $where = "&where=id %3D '$($blobInfo.id)'"
                    $where = "&where=id %3D '$($blobInfo.id)' AND version >%3D '$($blobInfo.version)'"
                }
                if ($sharedKey) {
                    $url = "$($containerUrl.TrimEnd('/'))?restype=container&comp=list&prefix=$($prefix)"
                    $url = "$($containerUrl.TrimEnd('/'))?restype=container&comp=blobs$($where)"
                }
                else {
                    $url = "$($containerUrl.TrimEnd('/'))$($sasToken)&restype=container&comp=list&prefix=$($prefix)"
                    $url = "$($containerUrl.TrimEnd('/'))$($sasToken)&restype=container&comp=blobs$($where)"
                }
                $response = ListBlobs -url $url -sharedKey $sharedKey
                # $response = Invoke-WebRequest -Uri $url -UseBasicParsing
                [xml]$blobList = $response.Content.Substring(3)
                $blobs = $blobList.SelectNodes('//Blobs/Blob/Name').'#text'
                if ($blobs) {
                    if ($contains -ne '') {
                        $blob = GetBlob -blobs $blobs -version $blobInfo.version -target $target -contains $containerVersion
                    }
                    if (!$blob) {
                        $blob = GetBlob -blobs $blobs -version $blobInfo.version -target $target -contains $contains
                    }
                    if ($blob) {
                        $url = "$($containerUrl.TrimEnd('/'))/$($blob)$sasToken"
                        $file = DownloadAzureBlob -Url $url -Folder $tempFolder -SharedKey $sharedKey
                        Write-Host -ForegroundColor Green "File Downloaded: $file"
                        $dependency | Add-Member -NotePropertyName downloaded -NotePropertyValue $true
                        $dependency | Add-Member -NotePropertyName filepath -NotePropertyValue $file
                        $newDependencies = (Get-NAVAppInfo -Path $file).Dependencies
                        $newDependencies | ForEach-Object {
                            $newDependency = $_
                            $found = $dependencies | Where-Object { $newDependency.Name -eq $_.Name -and $newDependency.Publisher -eq $_.Publisher -and $newDependency.Version -eq $_.Version }
                            if (!$found) {
                                $found = $dependencies | Where-Object { $newDependency.Name -eq $_.Name -and $newDependency.Publisher -eq $_.Publisher -and $newDependency.Version -lt $_.Version }
                                if (!$found) {
                                    $dependencies += New-Object PSObject -Property @{
                                        id        = $newDependency.AppId;
                                        publisher = $newDependency.Publisher;
                                        name      = $newDependency.Name;
                                        version   = $newDependency.Version
                                    }
                                }
                            }
                        }
                    }
                    else {
                        $dependency | Add-Member -NotePropertyName downloaded -NotePropertyValue $true
                        $dependency | Add-Member -NotePropertyName noblob -NotePropertyValue $true
                    }
                }
                else {
                    $dependency | Add-Member -NotePropertyName downloaded -NotePropertyValue $true
                    $dependency | Add-Member -NotePropertyName noblob -NotePropertyValue $true
                }
            }
        }
    }
    ($dependencies | Where-Object noblob -EQ $true) | ForEach-Object {
        Write-Host -ForegroundColor Red "Could not download: $($_.publisher) - $($_.name) version: $($_.version) Blob does not exist!"
    }
    ($dependencies | Where-Object { $_.noblob -NE $true -AND $_.fixed -NE $true }) | Sort-Object name, version -Descending | ForEach-Object {
        if ($id -ne $_.id) {
            $id = $_.id
            $version = $_.version     
        }
        else {
            if ($version -ne $_.version) {
                Write-Host -ForegroundColor Green "Remove: $($_.name) version: $($_.version) file: $($_.filepath)"
                Write-Host -ForegroundColor Green "App: $($_.name) version: $version Already downloaded"
                Remove-Item -Path $_.filepath -Force -ErrorAction SilentlyContinue
            }
        }
    }
}

function GetBlob {
    param (
        $blobs,
        [ValidateSet("current",
            "closest",
            "latest")]
        $target,
        [version]$version,
        $contains = ''
    )
    $bloblist = @()
    $blobs | ForEach-Object {
        $parts = $_.Split('/');
        $blob = New-Object PSObject -Property @{ 
            publisher = $parts[0];
            version   = [version]$parts[2];
            file      = $parts[$parts.Length - 1]
            path      = $_
        }
        if ($contains -eq '') {
            $bloblist += $blob;
        }
        else {
            if ($blob.file.contains($contains)) {
                $bloblist += $blob;
            }
        }
    }
    $bloblist = $bloblist | Where-Object version -GE $version | Sort-Object version
    if ($target -eq "latest") {
        ($bloblist | Select-Object -Last 1).path
    }
    else {
        ($bloblist | Select-Object -First 1).path
    }
}

function Install-Dependencies {
    param (
        $appManifestFile,
        $appCompilerSettingsFile,
        $compilerSettingsName,
        $containerName,
        $containerUrl,
        $sasToken,
        $sharedKey,
        $version
    )
    $ProgressPreference = 'SilentlyContinue'
    . ".\FileLibrary.ps1"
    . ".\AppLibrary.ps1"
    $module = (Get-Item 'C:\bcartifacts.cache\*\*\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll') | Select-Object -Last 1
    Import-Module $module
    $appManifest = Get-Content -Path $appManifestFile | ConvertFrom-Json
    $compilerSettings = Get-Content -Path $appCompilerSettingsFile | ConvertFrom-Json
    $compilerSetting = ($compilerSettings | Where-Object name -EQ $compilerSettingsName)
    $appCompilerSettings = $compilerSetting.app
    $dependencyTarget = if ($null -eq $appCompilerSettings.dependencyTarget) { 'closest' }else { $appCompilerSettings.dependencyTarget }
    if ($appCompilerSettings.replaceDependencies) {
        $dependencies = $appCompilerSettings.dependencies
    }
    else {
        $dependencies = $appManifest.dependencies
        $appCompilerSettings.dependencies | ForEach-Object {
            if ($_ -ne $null) {
                $newDependency = $_
                $found = $dependencies | Where-Object { $newDependency.Name -eq $_.Name -and $newDependency.Publisher -eq $_.Publisher }
                if ($found) {
                    $found = $dependencies | Where-Object { $newDependency.Name -eq $_.Name -and $newDependency.Publisher -eq $_.Publisher -and $newDependency.Version -gt $_.Version }
                    if ($found) {
                        $found.Version = $newDependency.Version
                    }
                }
                else {
                    $dependencies += $_
                }
            }
        }
    }

    $tempFolder = Get-NewTempFolder
    Get-AllFixedDependenciesToInstall -tempFolder $tempFolder -dependencies $appCompilerSettings.fixedDependenciesInstall

    Get-ChildItem -Path $tempFolder -Recurse -Filter '*.app' | ForEach-Object {
        $file = $_.FullName
        $newDependencies = (Get-NAVAppInfo -Path $file).Dependencies
        $newDependencies | ForEach-Object {
            $newDependency = $_
            $found = $dependencies | Where-Object { $newDependency.Name -eq $_.Name -and $newDependency.Publisher -eq $_.Publisher -and $newDependency.Version -eq $_.Version }
            if (!$found) {
                $found = $dependencies | Where-Object { $newDependency.Name -eq $_.Name -and $newDependency.Publisher -eq $_.Publisher -and $newDependency.Version -lt $_.Version }
                if (!$found) {
                    $dependencies += New-Object PSObject -Property @{
                        id        = $newDependency.AppId;
                        publisher = $newDependency.Publisher;
                        name      = $newDependency.Name;
                        version   = $newDependency.Version
                    }
                }
            }
        }
    }

    if ($sharedKey) {
        Get-Dependencies -tempFolder $tempFolder -dependencies $dependencies -fixedDependencies $appCompilerSettings.fixedDependenciesInstall -target $dependencyTarget -containerUrl $containerUrl -sharedKey $sharedKey -publisherBlobFilter $compilerSetting.publisherBlobFilter -containerVersion $version
    }
    if ($sasToken) {
        Get-Dependencies -tempFolder $tempFolder -dependencies $dependencies -fixedDependencies $appCompilerSettings.fixedDependenciesInstall -target $dependencyTarget -containerUrl $containerUrl -sasToken $sasToken -publisherBlobFilter $compilerSetting.publisherBlobFilter -containerVersion $version
    }  
    [array]$filesToInstall = (Get-ChildItem $tempFolder -Recurse -Filter '*.app').FullName
    Write-Host
    Write-Host -ForegroundColor Green "$($filesToInstall.Count) Files to install"
    installApps -container $containerName -appFiles $filesToInstall

    Remove-Item $tempFolder -Recurse -Force
    $ProgressPreference = 'Continue'
}
function Install-AppWithDependencies {
    param(
        $file,
        $containerName,
        $containerUrl,
        $sasToken,
        $sharedKey
    )
    $ProgressPreference = 'SilentlyContinue'
    . ".\FileLibrary.ps1"
    . ".\AppLibrary.ps1"
    $module = (Get-Item 'C:\bcartifacts.cache\*\*\platform\ServiceTier\program files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll') | Select-Object -Last 1
    Import-Module $module
    $tempFolder = Get-NewTempFolder
    Copy-Item -Path $file -Destination $tempFolder -Recurse -Force
    $dependencies = @()
    $newDependencies = (Get-NAVAppInfo -Path $file).Dependencies
    $newDependencies | ForEach-Object {
        $newDependency = $_
        $dependencies += New-Object PSObject -Property @{
            id        = $newDependency.AppId;
            publisher = $newDependency.Publisher;
            name      = $newDependency.Name;
            version   = $newDependency.Version
        }
    }      
    if ($sharedKey) {
        Get-Dependencies -tempFolder $tempFolder -dependencies $dependencies -target 'current' -containerUrl $containerUrl -sharedKey $sharedKey 
    }
    if ($sasToken) {
        Get-Dependencies -tempFolder $tempFolder -dependencies $dependencies -target 'current' -containerUrl $containerUrl -sasToken $sasToken
    }
    installApps -container $containerName -appFiles (Get-ChildItem $tempFolder -Recurse -Filter '*.app').FullName
    Remove-Item $tempFolder -Recurse -Force
    $ProgressPreference = 'Continue'
}
function GetArtifacts {
    param (
        $tempFolder,
        $organization = "eogv",
        $project,
        $definition,
        [string]$build = "latest",
        $branch = "master",
        $accessToken = ""
    )
    if ($build -ne "latest") {
        Write-Host -ForegroundColor Green "Downloading $definition build: $build from DevOps"
    }
    else {
        Write-Host -ForegroundColor green "Downloading $definition latest build from DevOps"
    }
    $user = ''
    $apiVersion = '5.0'
    $apiVersionPreview = '5.0-preview.1'  
    
    $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::UTF8.GetBytes("$($user):$accessToken"))
    $Header = @{
        Authorization = ("Basic {0}" -f $base64AuthInfo)
    }
    try {
        if ($build.ToLower() -eq "latest") {
            $url = "https://dev.azure.com/$organization/$project/_apis/build/latest/$($definition)?branchName=$branch&api-version=$apiVersionPreview"
            $latest = Invoke-WebRequest -UseBasicParsing $url -Headers $header
            $build = ($latest.Content | ConvertFrom-Json).id
        }
        $url = "https://dev.azure.com/$organization/$project/_apis/build/builds/$build/artifacts?api-version=$apiVersion"
        $artifacts = Invoke-WebRequest -UseBasicParsing $url -Headers $header
        $downloadUrl = ($artifacts.Content | ConvertFrom-Json).value.resource.downloadUrl
    }
    catch {
        if ($build -ne "latest") {
            Write-Host -ForegroundColor Red "Error downloading $definition build: $build from DevOps"
        }
        else {
            Write-Host -ForegroundColor Red "Error downloading $definition latest build from DevOps"
        }
        return
    }
    $tempFile = "$([System.IO.Path]::GetTempFileName()).zip"
    Invoke-WebRequest -UseBasicParsing -Uri $downloadUrl -OutFile $tempFile -Headers $header
    Expand-Archive $tempFile -DestinationPath $tempFolder -Force
    Remove-Item $tempFile -Force
    Get-ChildItem (Join-Path $tempFolder $project) -Recurse -Exclude '*APP.app' | ForEach-Object {
        Remove-Item $_.FullName -Force
    }
}
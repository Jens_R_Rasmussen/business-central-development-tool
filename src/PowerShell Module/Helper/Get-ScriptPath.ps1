function Get-ScriptPath {
    [OutputType('System.String')]
    param(
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true)]
        [String]$ScriptName
    )
    $Path = ""
    try {
        $SearchPath = (Get-Item (Get-Module EvBusinessCentralDevelopmentHelper | Select-Object -First 1).path).DirectoryName
        $File = Get-ChildItem -Path $SearchPath -Name $ScriptName -Recurse
        $File = get-item (Join-Path $SearchPath $File)
        $Path = $File.FullName   
    }
    catch {
        try {
            $SearchPath = Join-Path $env:USERPROFILE '.vscode\extensions\elbekvejrup.ev-business-central-dev-tool-*'
            $File = Get-ChildItem -Path $SearchPath -Name $ScriptName -Recurse
            $File = get-item (Join-Path $SearchPath $File)
            $Path = $File.FullName   
        }
        catch {
            $SearchPath = Join-Path $env:USERPROFILE 'Documents\*\ev-business-central-dev-tool'
            $File = Get-ChildItem -Path $SearchPath -Name $ScriptName -Recurse
            $File = get-item (Join-Path $SearchPath $File)
            $Path = $File.FullName   
        }
    }
    return $Path | Select-Object -Last 1
}
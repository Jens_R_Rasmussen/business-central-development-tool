<#
 .Synopsis
  Get License file path
 .Description
  Return latest BC License file path From S: Drive
 .Example
  Get-LicenseFilePath
#>
function Get-EVLicenseFilePath {
    [OutputType([string])]
    [CmdLetBinding()]
    param(
        [Parameter(mandatory = $false)]
        [int]$MajorVersion,
        [Parameter(mandatory = $false)]
        [string]$LicensePath = "\\ev-filsrv01.ev.dk.local\Navision\EV Internal Tools\Powershell\License\"
    )
    $LocalFolder = ''
    $LicenseFile = $null
    switch ($true) {
        !$MajorVersion {
            $LicenseFilePath = Join-Path $LicensePath "*.flf"
            break
        }
        ($MajorVersion -eq 0) {
            $LicenseFilePath = Join-Path $LicensePath "*.flf"
            break
        }
        ($MajorVersion -ne 0) {
            $LocalFolder = [string]$MajorVersion
            if (Test-Path (Join-Path $LicensePath "$MajorVersion") -PathType Any) {
                $LicenseFilePath = Join-Path $LicensePath "$MajorVersion\*.flf"
                break
            }
            if (Test-Path $LicensePath -PathType Any) {
                $LocalFolder = ''
                $LicenseFilePath = Join-Path $LicensePath "*.flf"
                break
            }
        }
    }
    if ($LicenseFilePath) {
        $LicenseFile = Get-Item $LicenseFilePath -ErrorAction SilentlyContinue | Sort-Object LastWriteTime -Descending | Select-Object -First 1
    }
    if ($LicenseFile) {
        if (!(Test-Path "$env:ProgramData\BcContainerHelper\$LocalFolder" -PathType Container)) {
            New-Item "$env:ProgramData\BcContainerHelper\$LocalFolder" -ItemType Directory -Force | Out-Null
        }
        Copy-Item -Path $LicenseFile -Destination (Join-Path "$env:ProgramData\BcContainerHelper\$LocalFolder" "License.flf") -Force -Recurse
    }
    $LicenseFile = Get-Item (Join-Path "$env:ProgramData\BcContainerHelper\$LocalFolder" "License.flf") -ErrorAction SilentlyContinue
    if (!$LicenseFile) {
        Throw "No Licens File Exist! ($(Join-Path "$env:ProgramData\BcContainerHelper\$LocalFolder" "License.flf"))"
    }
    $LicenseFile.FullName
}
Export-ModuleMember Get-EVLicenseFilePath
function Get-AppFolder() {
    [OutputType([string])]
    [CmdLetBinding()]
    param(
        [Parameter(mandatory = $false)]
        [string]$BasePath
    )
    Push-Location -StackName "CurrentRunPath"
    if ($BasePath) {
        Push-Location $BasePath -StackName "Base"
    }
    try {
        $App = $null
        try {
            $App = Get-Item app.json -ErrorAction Stop
        }
        catch {
            $App = Get-Item .\app\app.json -ErrorAction SilentlyContinue
        }
        if ($App) {
            $App.DirectoryName
        }
        else {
            Write-Log -LogType Error -Message "Could not find app.json"
            if ($BasePath) {
                if (!(Test-Path (Join-Path $BasePath "App"))) {
                    $BasePath
                }
                else {
                    Join-Path $BasePath "App"
                }
            }
            else {
                if (!(Test-Path (Join-Path "." "App"))) {
                    "."
                }
                else {
                    Join-Path "." "App"
                }
            }
        }
    }
    catch {
        Pop-Location -StackName "CurrentRunPath"
    }
    Pop-Location -StackName "CurrentRunPath"
}
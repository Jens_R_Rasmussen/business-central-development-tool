﻿function Test-RunAsAdministrator {
    if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
        Write-Info -Text "You need to run as administrator" -Color Red
        return $false
    }
    return $true
}
function Write-Log () {
    param(
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true)]
        $Message,
        [Parameter(Mandatory = $False)]
        [ValidateSet('Info', 'Error')]
        [string]$LogType = 'Info'
    )
    try {
        if ($Message.GetType() -eq [System.Management.Automation.ErrorRecord]) {
            $ScriptName = $Message.InvocationInfo.ScriptName.Split('\') | Select-Object -Last 1
            $Message = "$ScriptName`t$($Message.Exception.Message)"
        }
        $LogPath = '\\ev.dk.local\ev\Tools, media and licenses\EV AL Development\Tools\VS Code Extension for EVBusinessCentralDevelopmentHelper\Log'
        $logTime = "[{0:dd/MM/yyyy} {0:HH:mm:ss}]" -f (Get-Date)
        $FileName = "{0:yyyy-MM-dd} log.txt" -f (Get-Date) 
        try {
            Write-Output "$logTime`t$env:USERNAME`t$env:COMPUTERNAME`t$LogType`t$Message" | Out-File (Join-Path $LogPath $FileName) -Append
        } catch{
            $LogPath = $env:TEMP
            Write-Output "$logTime`t$env:USERNAME`t$env:COMPUTERNAME`t$LogType`t$Message" | Out-File (Join-Path $LogPath "EV-BC-Helper-$FileName") -Append
        }
    }
    catch {
        # Do Nothing
    }
}
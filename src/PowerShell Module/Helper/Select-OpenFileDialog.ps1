function Select-OpenFileDialog {
    param(
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $True,
            ValueFromPipelineByPropertyName = $true)]
        [string]$Title,
        [string]$Directory = '',
        [string]$Filter = "All Files (*.*)|*.*"
    )
    [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
    $OpenfileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenfileDialog.InitialDirectory = $Directory
    $OpenfileDialog.Filter = $Filter
    $OpenfileDialog.Title = $Title
    $Show = $OpenfileDialog.ShowDialog()
    if ($Show -eq "OK") {
        return $OpenfileDialog.FileName
    }
    else {
        throw "Operation cancelled by user."
    }
}
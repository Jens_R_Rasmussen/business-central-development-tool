<#
 .Synopsis
  Install or Update Powershell Module
 .Description
  Install or Update Powershell Module
 .Parameter ModuleName
  Name of the Powershell module
 .Example
  Install-OrUpdateModule -ModuleName BcContainerHelper
#>
function Install-EVOrUpdateModule {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $True,
            ValueFromPipelineByPropertyName = $true)]
        [String]$ModuleName
    )
    #Import-Module -Name EVBusinessCentralDevelopmentHelper -Global | Out-Null
    try {
        $ModuleExist = Get-Module -Name $ModuleName -ListAvailable
        if (!($ModuleExist)) {
            Write-host
            Write-Host -ForegroundColor Green "Install $ModuleName (Y/N)" -NoNewline
            $Install = (Read-host).ToUpper()
            if ($Install -eq 'Y') {
                Write-Host
                Write-Host -ForegroundColor Green "Installing $ModuleName"
                Start-Process powershell.exe -ArgumentList "& '$(Get-ScriptPath "InstallModule.ps1")'", $ModuleName -Wait
                Write-Host
                Write-Host -ForegroundColor Green "Import Module $ModuleName"
                Import-Module -Name $ModuleName -Global -WarningAction SilentlyContinue
            }
        }
        else {
            Write-Host
            Write-Host -ForegroundColor Green "Looking for update to module: $ModuleName"
            $NewVersion = $(Find-Module $ModuleName -ErrorAction SilentlyContinue) | Select-Object Version
            if ($NewVersion) {
                $CurrentVersion = $ModuleExist[0] | Select-Object Version
                if ([System.Version]$NewVersion.Version -gt $CurrentVersion.Version) {
                    Write-Host
                    Write-Host -ForegroundColor Green "Update $ModuleName from: $($CurrentVersion.Version) to: $($NewVersion.Version)"
                    Start-Process powershell.exe -ArgumentList "& '$(Get-ScriptPath "UpdateModule.ps1")'", $ModuleName, $($CurrentVersion.Version) -Wait
                    Remove-Module -Name $ModuleName -Force -ErrorAction SilentlyContinue
                }
            }
            Write-Host
            Write-Host -ForegroundColor Green "Import Module $ModuleName"
            if ($NewVersion) {
                Import-Module -Name $ModuleName -Global -RequiredVersion $NewVersion.Version
            }
            else {
                Import-Module -Name $ModuleName -Global
            }
        }
    }
    catch {
        Write-Host $_
    }
}
Export-ModuleMember Install-EVOrUpdateModule
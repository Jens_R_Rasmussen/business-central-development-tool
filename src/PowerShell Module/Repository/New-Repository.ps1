<#
 .Synopsis
  Clone GIT Repository
 .Description
  Clone GIT Repository to %USERPROFILE%\Documents\AL
 .Parameter GitRepositoryUrl
  URL to git repository
 .Example
  New-Repository -GitRepositoryUrl https://bitbucket.org/Account/MyRepository.git
#>
function New-EVRepository {
    [OutputType('String')]
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true)]
        [string]$GitRepositoryUrl,
        [Parameter(Mandatory = $false)]
        [string]$ContainerName,
        [Parameter(Mandatory = $false)]
        [ValidateSet("Desktop", "MyDocuments", "None")]
        [string]$ShortCutLocation = "Desktop",
        [Parameter(Mandatory = $false)]
        [string]$LocalRepositoryFolder = ""

    )
    Push-Location -StackName "Default"
    if ($LocalRepositoryFolder -eq "") {
        $LocalRepositoryFolder = (Join-Path $([Environment]::GetFolderPath("MyDocuments")) "AL")
    }
    if ((Test-Path $LocalRepositoryFolder) -ne $true) {
        New-Item -Path $LocalRepositoryFolder -ItemType Directory
    }
    if ($ContainerName) {
        $RepositoryFolder = (Join-Path $LocalRepositoryFolder $ContainerName.ToUpper())
    }
    else {
        $RepositoryFolder = (Join-Path $LocalRepositoryFolder $([System.IO.Path]::GetFileNameWithoutExtension($($GitRepositoryUrl.Split('/') | Select-Object -Last 1)))).ToUpper();
    }
    if ((Test-Path $RepositoryFolder) -ne $true) {
        Push-Location $LocalRepositoryFolder -StackName "LocalRepository"
        Write-Host
        Write-Host -ForegroundColor Green "Clone: $GitRepositoryUrl"
        Start-Process Git -ArgumentList "clone $GitRepositoryUrl $ContainerName" -Wait
        Push-Location $RepositoryFolder -StackName "RepositoryFolder"
        Start-Process Git -ArgumentList "checkout -q --track origin/develop" -Wait
        if ($ShortCutLocation -ne "None") {
            if ($ContainerName) {
                if (Test-Path (Join-Path $RepositoryFolder "*.code-workspace")) {
                    $WorkspaceFile = Get-Item (Join-Path $RepositoryFolder "*.code-workspace") | Select-Object -First 1
                    $Argument = $WorkspaceFile.FullName
                }
                else {
                    $Argument = Get-AppFolder -BasePath $RepositoryFolder
                }
                $ShortCutFolder = "$([Environment]::GetFolderPath($ShortCutLocation))\Docker Container Shortcuts\$ContainerName\"
                New-EVShortCut -Name "AL Project" -Folder $ShortCutFolder -TargetPath "Code" -Arguments "$Argument" -IconLocation (Get-ScriptPath 'al.ico')
            }
        }
    }
    else {
        Push-Location $RepositoryFolder -StackName "RepositoryFolder"
        Start-Process Git -ArgumentList "checkout -q --track origin/develop" -Wait
    }
    Pop-Location -StackName "Default"
    $RepositoryFolder
}
Export-ModuleMember New-EVRepository
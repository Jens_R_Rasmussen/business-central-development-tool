<#
 .Synopsis
  Remove deleted remote repositories and delete local copies
 .Description
  Remove deleted remote repositories and delete local copies
  Must be run from git repository folder
 .Example
  Sync-EVRemoteLocalRepository
#>
function Sync-EVRemoteLocalRepository
{
    $ErrorActionPreference = "Continue"
    Clear-Host
    Write-Host
    try {
        $Status = (git status)
        if (!$Status) {
            throw "Error"
        }
    } catch {
        Write-Host -ForegroundColor Red "There is no GIT in " -NoNewline
        Write-Host -ForegroundColor Yellow $((Get-Item .).FullName)
        return
    }
    git fetch --prune
    $OriginBranch = git branch -r --format="%(refname:lstrip=3)"
    $LocalBranch = git branch --format="%(refname:lstrip=2)" | Where-Object {($_ -ne 'master') -and ($_ -ne 'origin')}
    $LocalBranch | ForEach-Object {
        if (!$OriginBranch.Contains($_)) {
            Write-Host -ForegroundColor Green "Delete $_ (Y/N) " -NoNewline
            $Result = (Read-Host).ToUpper() -eq 'Y'
            if ($Result) {
                Write-Host -ForegroundColor Green "Deleting: $_"
                git branch --delete --force $_
            }
        }
    }
    Write-Host -ForegroundColor Green "Done!"
}
Export-ModuleMember -Function Sync-EVRemoteLocalRepository
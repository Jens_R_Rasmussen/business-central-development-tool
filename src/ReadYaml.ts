import { readFileSync } from "fs";
import * as constants from "./constants";
import { workspace } from "vscode";
import { parse } from "yaml";

export function readYamlFile(path: string): ContainerSettings {
    let containerParameter: ContainerSettings = {};
    let config = workspace.getConfiguration(constants.extensionConfig);
    let yamlParameters: YamlFileVariables = getYamlParameters(config.get(constants.yamlFileVariablesSettings));
    try {
        let yamlcontent = readFileSync(path, 'utf-8');
        let azurePipelines = parse(yamlcontent);
        containerParameter.complete = azurePipelines.variables.some((variable: { name: string; value: string; }) => {
            switch (variable.name.toUpperCase()) {
                case yamlParameters.artifactVersion.toUpperCase():
                    containerParameter.artifactVersion = variable.value;
                    break;
                case yamlParameters.artifactCountry.toUpperCase():
                    containerParameter.artifactCountry = variable.value;
                    break;
                case yamlParameters.artifactType.toUpperCase():
                    containerParameter.artifactType = variable.value;
                    break;
                case yamlParameters.license.toUpperCase():
                    containerParameter.license = variable.value.startsWith("$") ? undefined : variable.value;
                    break;
            }
            if ((containerParameter.artifactCountry !== undefined) && (containerParameter.license !== undefined) && (containerParameter.artifactType !== undefined) && (containerParameter.artifactVersion !== undefined)) {
                return true;
            }
        });
        console.log(azurePipelines, containerParameter);
    } catch (e: any) {
        containerParameter.complete = false;
        containerParameter.errormassage = e.message;
        console.log(containerParameter);
    }
    return containerParameter;
}
function getYamlParameters(value: any): YamlFileVariables {
    if (value !== undefined) {
        return value;
    }
    return { artifactVersion: "BC_Version", artifactCountry: "BC_Localization", artifactType: "BC_EnvironmentType", license: "BC_License" };
}
interface ContainerSettings {
    artifactVersion?: string;
    artifactCountry?: string;
    artifactType?: "OnPrem" | "Sandbox" | string;
    artifactStorageAccount?: string;
    artifactSasToken?: string;
    artifactSelect?: "All" | "Closest" | "Latest" | "SecondToLastMajor" | string;
    license?: string;
    complete?: boolean;
    errormassage?: string;
}
interface YamlFileVariables {
    artifactVersion: string;
    artifactCountry: string;
    artifactType: string;
    license: string;
}
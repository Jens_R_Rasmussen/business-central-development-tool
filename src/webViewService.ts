import { ContainerList } from './webViews/containerListWebView';
import { NewContainer } from './webViews/newContainerWebView';
import { getDefaultContainerParameter } from './dockerLibrary';
import * as vscode from 'vscode';
import * as constants from './constants';
import { CompilerSettingsView } from './webViews/compilerSettingsWebView';

export class WebViewService {
    private context: vscode.ExtensionContext;
    private webViews: { [id: string]: any; };
    constructor(context: vscode.ExtensionContext) {
        this.context = context;
        this.webViews = {};
        this.registreCommands();
        this.onStartup();
    }
    registreCommands() {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(vscode.commands.registerCommand(constants.showContainerPanelCommand, () => this.showContainerList()));
        subscriptions.push(vscode.commands.registerCommand(constants.newContainerPanelCommand, () => this.showNewContainer()));
        subscriptions.push(vscode.commands.registerCommand(constants.showCompilerSettingsPanelCommand, () => this.showCompilerSettings()));
    }
    showCompilerSettings() {
        if (this.webViews[constants.compilerSettingsPanelTitle]) {
            this.webViews[constants.compilerSettingsPanelTitle].reveal();
        } else {
            this.webViews[constants.compilerSettingsPanelTitle] = new CompilerSettingsView(this.context, constants.compilerSettingsPanelTitle);
            this.webViews[constants.compilerSettingsPanelTitle].show();
            this.webViews[constants.compilerSettingsPanelTitle].on('dispose', () => {
                this.webViews[constants.compilerSettingsPanelTitle] = undefined;
            });
        }
    }

    showContainerList() {
        if (this.webViews[constants.containerPanelTitle]) {
            this.webViews[constants.containerPanelTitle].reveal();
        } else {
            this.webViews[constants.containerPanelTitle] = new ContainerList(this.context, constants.containerPanelTitle);
            this.webViews[constants.containerPanelTitle].show();
            this.webViews[constants.containerPanelTitle].on('dispose', () => {
                this.webViews[constants.containerPanelTitle] = undefined;
            });
        }
    }
    showNewContainer() {
        if (this.webViews[constants.newContainerPanelTitle]) {
            this.webViews[constants.newContainerPanelTitle].reveal();
        } else {
            this.webViews[constants.newContainerPanelTitle] = new NewContainer(this.context, constants.newContainerPanelTitle);
            this.webViews[constants.newContainerPanelTitle].show();
            this.webViews[constants.newContainerPanelTitle].updateContainerParameter(getDefaultContainerParameter(), '');
            this.webViews[constants.newContainerPanelTitle].on('dispose', () => {
                this.webViews[constants.newContainerPanelTitle] = undefined;
            });
        }
    }
    onStartup() {
        let config = vscode.workspace.getConfiguration(constants.extensionConfig);
        let autoStartContainers = config.get(constants.showContainerWebpanelOnStartupSettings);
        if (autoStartContainers) {
            this.showContainerList();
        }
    }
    dispose() {
        this.webViews = {};
    }
}

import * as vscode from 'vscode';
import * as childProcess from 'child_process';
import * as constants from './constants';
import { getCurrentWorkspaceFolder } from './workspaceFileAndFolderHelper';
import * as https from 'https';
import * as fs from 'fs';
import { tmpdir } from 'os';
import extract from 'extract-zip';
export class Process {
    static exec(command: string, callback: any = undefined) {
        return childProcess.exec(command, { encoding: "utf8" }, callback);
    }
    static execSync(command: string) {
        try {
            return childProcess.execSync(command, { encoding: "utf8" });
        } catch (e) {
            throw e;
        }
    }
    static execPowershellCommand(command: string, callback: any = undefined) {
        try {
            return childProcess.exec(this.createPowerShellCommand(command), { encoding: "utf8" }, callback);
        } catch (e) {
            throw e;
        }
    }
    static execSyncPowershellCommand(command: string) {
        try {
            return childProcess.execSync(this.createPowerShellCommand(command), { encoding: "utf8" });
        } catch (e) {
            throw e;
        }
    }

    private static createPowerShellCommand(command: string) {
        let executeCommand = `Powershell -Command Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process; Import-Module (Join-Path $env:TEMP 'BcContainerHelper/BcContainerHelper.psm1'); `;
        executeCommand += command;
        return executeCommand;
    }
}

export class Terminal {
    static terminal: vscode.Terminal | undefined;
    static executeCmdLet(command: string, title = '', clearHost = true, show = true) {
        this.pushLocation();
        if (this.terminal === undefined) {
            this.terminal = vscode.window.createTerminal(constants.powerShellModuleTerminalName);
            this.loadModule(show);
        }
        if (clearHost) {
            this.terminal.sendText('Clear-Host');
        }
        if (title !== '') {
            this.terminal.show(show);
            this.terminal.sendText(`Clear-Host;Write-Host;Write-Host;Write-Host -ForegroundColor Green "${title}";Write-host;${command};Write-Host;Write-Host;Write-Host -ForegroundColor Green "Done" `);
        }
        else {
            this.terminal.show(show);
            this.terminal.sendText(command);
        }
    }
    private static loadModule(show: boolean, loadInstalledModule = true) {
        if (this.terminal !== undefined) {
            this.terminal.show(show);
            this.terminal.sendText('Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process');
            if (!fs.existsSync(`${tmpdir}\\BcContainerHelper\\BcContainerHelper.psm1`)) {
                this.terminal.sendText(`Import-Module BcContainerHelper`);
            }
            else {
                this.terminal.sendText(`Import-Module (Join-Path $env:TEMP 'BcContainerHelper/BcContainerHelper.psm1')`);
            }
            let path = vscode.extensions.getExtension('ElbekVejrup.ev-business-central-dev-tool')?.extensionPath;
            let importModule = `import-module (join-path '${path}' 'out\\PowerShell Module\\EVBusinessCentralDevelopmentHelper.psd1')`;
            this.terminal.sendText(importModule);
        }
    }
    static downloadBCContainerHelper() {
        console.log(`${this.getTimeStamp()} Download BcContainerHelper`);
        let client = https.get('https://bccontainerhelper.azureedge.net/public/latest.zip', (response) => {
            let tempFolder = tmpdir();
            if (response.statusCode !== 200) {
                console.log(`${this.getTimeStamp()} Download of BcContainerHelper failed with Error Code: ${response.statusCode}, Error Message: ${response.statusMessage}`);
                return;
            }
            let filePath = `${tempFolder}\\latest.zip`;
            let fileStream = fs.createWriteStream(filePath);
            response.pipe(fileStream);
            fileStream.on('finish', () => {
                fileStream.close();
                extract(filePath, { dir: tempFolder }).then(() => {
                    console.log(`${this.getTimeStamp()} Unzip BcContainerHelper Done!`);
                });
                console.log(`${this.getTimeStamp()} BcContainerHelper Downloaded`);
            });
        });
        client.on('error', (err) => {
            console.log(`${this.getTimeStamp()} Download of BcContainerHelper failed with Error Message: ${err.message}`);
        });
    }
    static show(show = true) {
        if (this.terminal !== undefined) {
            this.terminal.show(show);
        }
    }
    static pushLocation() {
        if (this.terminal !== undefined) {
            let workFolder = getCurrentWorkspaceFolder();
            if (workFolder !== undefined) {
                this.terminal.sendText(`Push-Location "${workFolder?.uri.fsPath}"`);
            }
        }
    }
    static onDidCloseTerminal(closedTerminal: vscode.Terminal) {
        if (closedTerminal.name === constants.powerShellModuleTerminalName) {
            delete this.terminal;
        }
    }
    static getTimeStamp() {
        let date = new Date();
        return `[${date.getFullYear()}-${appendLeadingZero(date.getMonth() + 1)}-${appendLeadingZero(date.getDate())} ${appendLeadingZero(date.getHours())}:${appendLeadingZero(date.getMinutes())}:${appendLeadingZero(date.getSeconds())}.${appendLeadingZeroes(date.getMilliseconds())}]`;
        function appendLeadingZero(n: number) {
            if (n <= 9) {
                return "0" + n;
            }
            return n;
        }
        function appendLeadingZeroes(n: number) {
            if (n <= 9) {
                return "00" + n;
            }
            if (n <= 99) {
                return "0" + n;
            }
            return n;
        }
    }
    static dispose(){
        if (this.terminal !== undefined) {
            this.terminal.dispose();
            delete this.terminal;
        }
    }
}
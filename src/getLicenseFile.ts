import { workspace } from 'vscode';
import * as constants from './constants';
import { readdirSync } from "fs";
import { parse } from "path";

export function getLicensePath(majorVersion: number): string {
    let settingsFile = getLicensePathFromSettings(majorVersion);
    if (settingsFile === '') {
        return '';
    }
    return validateLicensePath(settingsFile);
}

export function validateLicensePath(path: string): string {
    if (path.toUpperCase().startsWith('HTTP')) {
        return validateHTTPPath(path);
    }
    return validateFilePath(path);
}

function validateHTTPPath(url: string): string {
    return url;
}

function validateFilePath(filePath: string): string {
    try {
        let files = getFiles(filePath);
        if (files.length !== 0) {
            return filePath;
        }
    } catch (e) {
        console.log(e);
    }
    return '';
}
function getFiles(filePath: string) {
    let filePathInfo = parse(filePath);
    let elements = readdirSync(filePathInfo.dir);
    let files = elements.filter(value => {
        return (value.match(filePathInfo.name) && (value.endsWith(filePathInfo.ext)));
        // return value.match(filePathInfo.name);
    });
    return files;
}

function getLicensePathFromSettings(majorVersion: number): string {
    let paths: { version: number; path: string; }[] | undefined = workspace.getConfiguration(constants.extensionConfig).get(constants.licensePathSettings);
    if (paths === undefined) {
        return '';
    }
    let path: string | undefined;
    let currentVersionExist = paths.some((item: { version: number | number[]; path: string; }) => {
        let versionType = typeof item.version;
        console.log(versionType);
        if (typeof item.version === 'number') {
            if (item.version === majorVersion) {
                path = item.path;
                return true;
            }
        } else {
            if (item.version.includes(majorVersion)) {
                path = item.path;
                path = path?.replace('{{version}}', majorVersion.toString());
                return true;
            }
        }
        if (item.version === 0) {
            path = item.path;
        }
    });
    if (!currentVersionExist) {
        path = path?.replace('{{version}}', majorVersion.toString());
    }
    return path !== undefined ? path : '';
}

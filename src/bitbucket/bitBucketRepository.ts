import { commands, window, workspace } from "vscode";
import { BitBucketAuthorization } from "./bitBucketAuthorization";
import fetch, { Headers } from "node-fetch";
import * as constants from '../constants';
export class BitBucketRepository {
    private bitBucketAuthorization: BitBucketAuthorization;
    constructor(bitBucketAuthorization: BitBucketAuthorization) {
        this.bitBucketAuthorization = bitBucketAuthorization;
    }
    async listRepositories() {
        let bitBucketApiUrl: string;
        let repositories = new Array();
        let config = workspace.getConfiguration(constants.extensionConfig);
        let querystring = config.get(constants.bitbucketApiQuerystringSettings);
        let workspaceId = config.get(constants.bitbucketWorkspaceIdSettings);
        if ((workspaceId === undefined) || (workspaceId === '')) {
            commands.executeCommand("workbench.action.openSettings", "ev-tools.bitbucketWorkspaceId");
            window.showErrorMessage('You must define the BibBucket WorkspaceId');
            return;
        }
        if (querystring !== "") {
            bitBucketApiUrl = `https://api.bitbucket.org/2.0/repositories/${workspaceId}/?pagelen=100&${querystring}`;
        }
        else {
            bitBucketApiUrl = `https://api.bitbucket.org/2.0/repositories/${workspaceId}/?pagelen=100`;
        }
        let headers = await this.bitBucketAuthorization.getDefaultHttpHeaders();
        return new Promise((resolve, rejects) => {
            this.fetchRepositories(repositories, bitBucketApiUrl, headers, resolve, rejects);
        });
    }
    private fetchRepositories(repositories: any, bitBucketApiUrl: string, httpHeaders: Headers, resolve: Function, rejects: Function) {
        fetch(bitBucketApiUrl, { headers: httpHeaders })
            .then(res => res.json())
            .then(json => this.buildReopsitorylist(repositories, json, httpHeaders, resolve, rejects))
            .catch(err => rejects(err));
    }

    private buildReopsitorylist(repositories: any, json: any, httpHeaders: Headers, resolve: Function, rejects: Function) {
        if (json.error !== undefined) {
            console.log(json.error);
            rejects(`BitBucket Error: ${json.error.message}`);
            return;
        }
        json.values.forEach((element: any) => {
            repositories.push(element);
        });
        if (json.next === undefined) {
            resolve(repositories);
        }
        else {
            this.fetchRepositories(repositories, json.next, httpHeaders, resolve, rejects);
        }
    }


    async createRepository(name: string, projectKey: string) {
        let headers = await this.bitBucketAuthorization.getDefaultHttpHeaders();
        return new Promise((resolve, rejects) => {
            let body = {
                type: 'repository',
                name: name,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                is_private: true,
                project: { type: 'project', key: projectKey }
            };
            let repoSlug = name.replace(/[^A-Za-z0-9\-]/gm, '-').toLowerCase();
            let bitBucketWorkspace = workspace.getConfiguration(constants.extensionConfig).get(constants.bitbucketWorkspaceIdSettings);
            let url = `https://api.bitbucket.org/2.0/repositories/${bitBucketWorkspace}/${repoSlug}`;
            fetch(url, { method: 'POST', headers: headers, body: JSON.stringify(body) })
                .then(response => {
                    console.log('Response:', response);
                    response.json()
                        .then(body => {
                            console.log('Body:', body);
                            resolve(body);
                        }).catch(reason => {
                            console.log(reason);
                            rejects({ error: true, errorMessage: 'Invalid login to BitBucket.org' });
                        });
                })
                .catch(err => {
                    console.log(err);
                    rejects({ error: true, errorMessage: 'Invalid login to BitBucket.org', errorObj: err });
                });
        });
    }
    async getProjects(): Promise<Project[]> {
        let headers = await this.bitBucketAuthorization.getDefaultHttpHeaders();
        return new Promise((resolve, rejects) => {
            let workspace = 'ElbekVejrup';
            let url = `https://api.bitbucket.org/2.0/workspaces/${workspace}/projects`;
            let result: Project[] = [];
            fetch(url, { headers: headers })
                .then(response => {
                    console.log('Response:', response);
                    response.json()
                        .then(body => {
                            console.log('Body:', body);
                            body.values.forEach((element: any) => {
                                result.push({ key: element.key, name: element.name });
                            });
                            resolve(result);
                        }).catch(reason => {
                            console.log(reason);
                            rejects({ error: true, errorMessage: 'Invalid login to BitBucket.org' });
                        });
                })
                .catch(err => {
                    console.log(err);
                    rejects({ error: true, errorMessage: 'Invalid login to BitBucket.org', errorObj: err });
                });
        });
    }
}
export type Project = {
    key: string,
    name: string
};
import { commands, ExtensionContext, window, workspace } from "vscode";
import * as constants from '../constants';
import { exec } from "child_process";
import { homedir } from "os";
import { OutputChannel } from '../outputChannel';
import { readYamlFile } from "../ReadYaml";
import { ContainerParameter, getDefaultContainerParameter, convertToBoolean, convertToString } from "../dockerLibrary";
import { NewContainer } from "../webViews/newContainerWebView";
import { validateLicensePath } from "../getLicenseFile";
import { mkdirSync, readFileSync, readdirSync } from 'fs';
import { BitBucketAuthorization } from "./bitBucketAuthorization";
import { BitBucketRepository } from "./bitBucketRepository";

const channalId = 'Git Clone';
export class BitBucket {
    context: ExtensionContext;
    credential: { token: string; refreshToken: string; valid: boolean; dialog: boolean } = { token: '', refreshToken: '', valid: false, dialog: false };
    bitBucketAuthorization: BitBucketAuthorization;
    constructor(context: ExtensionContext, bitBucketAuthorization: BitBucketAuthorization) {
        this.context = context;
        this.bitBucketAuthorization = bitBucketAuthorization;
        this.registreCommands();
    }
    private registreCommands(): void {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(commands.registerCommand(constants.cloneDevenvironmentFromBitBucketCommand, () => this.cloneBitBucketRepository()));
    }

    async cloneBitBucketRepository() {
        let bitBucketRepository = new BitBucketRepository(this.bitBucketAuthorization);
        let rep = await bitBucketRepository.listRepositories();
        this.showRepositoryList(rep);
    }
    private cloneRepository(url: string, name: string): void {
        let config = workspace.getConfiguration(constants.extensionConfig);
        let clonePath = convertToString(config.get(constants.repositoryClonePathSettings));
        if ((clonePath === '')) {
            clonePath = `${homedir}\\Documents\\AL`;
        }
        mkdirSync(clonePath, { recursive: true });
        OutputChannel.createChannal(channalId);
        OutputChannel.appendLine(channalId, `Clone: ${name} from ${url}`);
        OutputChannel.appendLine(channalId, `To: ${clonePath}\\${name}`);
        const process = exec(`git clone ${url} "${name}"`, { cwd: clonePath });
        process.on("error", (err: Error) => {
            OutputChannel.appendLine(channalId, `Error: ${err}`);
        });
        process.on("exit", (code: number) => {
            switch (code) {
                case 0:
                    OutputChannel.appendLine(channalId, `Clone complete`);
                    this.buildContainer(`${clonePath}\\${name}`, name, true);
                    break;
                case 128:
                    OutputChannel.appendLine(channalId, `Repository already exist!`);
                    this.buildContainer(`${clonePath}\\${name}`, name, false);
                    break;
                default:
                    OutputChannel.appendLine(channalId, `Something vend wrong!`);
                    break;
            }
        });
    }

    private showRepositoryList(repositories: any) {
        let url: string;
        let quickPick = window.createQuickPick();
        quickPick.items = repositories.map((repo: any) => ({ label: repo.name, clone: repo.links.clone, detail: `${repo.project.name} ${repo.description}`, fullname: repo.full_name, slug: repo.slug, des: repo.description }));
        quickPick.onDidChangeSelection(([item]) => {
            quickPick.dispose();
            let selectedItem: any = item;
            if (selectedItem.clone.some((clone: any) => {
                if (clone.name === "https") {
                    url = clone.href;
                    return true;
                }
            })) {
                this.cloneRepository(url, item.label);
            }
            else {
                window.showErrorMessage('No Repository url exist');
            }
        });
        quickPick.onDidHide(() => quickPick.dispose());
        quickPick.show();
    }
    buildContainer(localPath: string, name: string, newFolder: boolean): void {
        if (newFolder) {
            OutputChannel.appendLine(channalId, `Checkout develop branch`);
            const process = exec("git checkout -q --track origin/develop", { cwd: localPath });
            process.on("exit", () => {
                this.buildContainer(localPath, name, false);
            });
            return;
        }
        let newContainer = new NewContainer(this.context, `${constants.continueCreationOfContainerToRepositoryPanalTitle}: ${name}`);
        let containerParameter: ContainerParameter | undefined = getWorkspaceSettingsContainerSettings(localPath);
        if (containerParameter !== undefined) {
            newContainer.dontLoadDefaultValues();
        }
        if (containerParameter === undefined) {
            let config = workspace.getConfiguration(constants.extensionConfig);
            let settingsFile = `${localPath}\\${config.get(constants.repositoryYamlPipelineFileNameSettings)}`;
            OutputChannel.appendLine(channalId, `Read settings from: ${settingsFile}`);
            let containerSettings = readYamlFile(settingsFile);
            console.log(containerSettings);
            let country = convertToString(config.get(constants.containerCountrySettings));

            containerParameter = getDefaultContainerParameter();
            containerParameter.name = this.createContainerName(name, containerSettings.artifactVersion);
            containerParameter.type = containerSettings.artifactType === undefined ? 'OnPrem' : containerSettings.artifactType;
            containerParameter.version = containerSettings.artifactVersion;
            containerParameter.country = containerSettings.artifactCountry === undefined ? country : containerSettings.artifactCountry;
            containerParameter.licensefile = validateLicensePath(convertToString(containerSettings.license));
            containerParameter.updateDependencies = convertToBoolean(config.get(constants.updateDependenciesOnNewDevelopmentEnvironmentSettings));
        }
        newContainer.showDevelopmentContainer(containerParameter, localPath);
    }
    createContainerName(name: string, artifactVersion: string | undefined): string {
        let version: string = '';
        if (artifactVersion) {
            version = artifactVersion.replace('.', '');
        }
        name = name.replace(/[^A-Za-z0-9 \-]/gm, '');
        name = name.replace(/[^A-Za-z0-9\-]/gm, '-');
        name = name.substr(0, 15 - version.length);
        return `${name}${version}`;
    }

    dispose() {
        this.bitBucketAuthorization.dispose();
    }
}
interface Authorization {
    scopes: string;
    expiresIn: number;
    tokenType: string;
    state: string;
    expireTime: Date;
}
function getWorkspaceSettingsContainerSettings(path: string): ContainerParameter | undefined {
    let workspaceFiles = readdirSync(path).filter(fn => fn.endsWith('.code-workspace'));
    if (workspaceFiles.length === 1) {
        let content = readFileSync(`${path}\\${workspaceFiles[0]}`);
        try {
            let data = JSON.parse(content.toString());
            if (data.settings === undefined) {
                return undefined;
            } if (data.settings['ev-tools.containerSettings'] === undefined) {
                return undefined;
            }
            return data.settings['ev-tools.containerSettings'];
        } catch {
            // TODO
            return undefined;
        }
    }
    return undefined;
}
import { ExtensionContext, commands, window } from "vscode";
import * as constants from '../constants';
import { randomUUID } from "crypto";
import open from "open";
import { createServer, IncomingMessage, Server, ServerResponse } from 'http';
import { parse } from 'querystring';
import { readFileSync } from "fs";
import fetch, { Headers } from "node-fetch";


export class BitBucketAuthorization {
    private service = 'api.bitbucket.org';
    private state = '';
    private clientId = 'yx4nyqrdVx7Lvbj8zj';
    private clientSecret = 'kHeEkG2T85frFAARML9XGhJf3wvKkwbv';
    private server: Server = new Server();
    context: ExtensionContext;
    private credentials: Credentials = this.initCredentials();
    constructor(context: ExtensionContext) {
        this.context = context;
        this.getCredentials();
        this.registreCommands();
    }

    // git remote add origin https://Jens_R_Rasmussen@bitbucket.org/ElbekVejrup/vscode-test.git
    // git push -u origin master

    private initCredentials(): Credentials {
        return {
            expiresIn: 0,
            expireTime: new Date(),
            refreshToken: '',
            scopes: '',
            state: '',
            token: '',
            tokenType: ''
        };
    }

    private registreCommands(): void {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(commands.registerCommand(constants.getBitBucketAuthorizationCommand, () => this.getDefaultHttpHeaders()));
        subscriptions.push(commands.registerCommand(constants.clearBitBucketCredentialsCommand, () => this.clearCredentials()));
    }
    private getCredentials() {
        let auth: Authorization | undefined = this.context.globalState.get(this.service);
        if (auth) {
            this.credentials = <Credentials>auth;
            this.context.secrets.get(`${this.service}.token`).then((value) => {
                this.credentials.token = value === undefined ? '' : value;
            }, (error) => {
                console.log(error);
            });
            this.context.secrets.get(`${this.service}.refreshToken`).then((value) => {
                this.credentials.refreshToken = value === undefined ? '' : value;
            }, (error) => {
                console.log(error);
            });
        }
    };

    getDefaultHttpHeaders(): Promise<Headers> {
        return new Promise((resolve, rejects) => {
            this.getHttpHeaders()
                .then((element) => {
                    let headers = element;
                    this.validateToken(headers)
                        .then((result) => {
                            if (result.error) {
                                rejects(result);
                            } else {
                                resolve(headers);
                            }
                        })
                        .catch((err) => {
                            rejects(err);
                        });
                })
                .catch((err) => {
                    rejects(err);
                });
        });
    }

    private validateToken(headers: Headers): Promise<ValidateResult> {
        return new Promise((resolve) => {
            let url = 'https://api.bitbucket.org/2.0/user';
            fetch(url, { headers: headers })
                .then(response => {
                    console.log('Response:', response);
                    response.json()
                        .then(body => {
                            console.log('Body:', body);
                            if (body.account_status !== 'active') {
                                resolve({ error: true, errorMessage: `Account: ${body.username} is not valid` });
                            }
                            resolve({ error: false, errorMessage: '' });
                        }).catch(reason => {
                            console.log(reason);
                            resolve({ error: true, errorMessage: 'Invalid login to BitBucket.org', errorObj: reason });
                        });
                })
                .catch(err => {
                    console.log(err);
                    resolve({ error: true, errorMessage: 'Invalid login to BitBucket.org', errorObj: err });
                });
        });
    }

    private async getToken() {
        let requestTime = new Date();
        let authorizationCode = await this.runRedirectServer(9731);
        if (this.state !== authorizationCode.state) {
            window.showErrorMessage('Wrong OAuth Request');
            this.server.close();
            return;
        }
        this.server.close();
        let token = await this.fetchToken(authorizationCode);
        if (token.error !== undefined) {
            window.showErrorMessage(token.error);
            return;
        }
        await this.updateAuth(token.data, requestTime);
    }

    private async getHttpHeaders(): Promise<Headers> {
        return new Promise(async (resolve, rejects) => {
            let requestTime = new Date();
            if (this.credentials.token === '') {
                await this.getToken();
            }
            if (new Date(this.credentials.expireTime) < requestTime) {
                if (this.credentials.refreshToken === '') {
                    await this.getToken();
                } else {
                    let token = await this.fetchRefreshToken();
                    if (token.error !== undefined) {
                        window.showErrorMessage(token.error);
                        rejects(new Headers());
                    }
                    await this.updateAuth(token.data, requestTime);
                }
            }
            const headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('authorization', `Bearer ${this.credentials.token}`);
            resolve(headers);
        });
    }

    private httpOAuthHeaders(): Headers {
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('authorization', `Basic ${Buffer.from(`${this.clientId}:${this.clientSecret}`).toString('base64')}`);
        return headers;
    }

    private updateAuth(tokenResponse: any, requestTime: Date) {
        return new Promise<boolean>((resolve, reject) => {
            let authorization: Authorization = {
                expireTime: new Date(requestTime.getTime() + (tokenResponse.expires_in * 1000)),
                expiresIn: tokenResponse.expires_in,
                scopes: tokenResponse.scopes,
                state: tokenResponse.state,
                tokenType: tokenResponse.token_type
            };
            this.context.secrets.store(`${this.service}.token`, tokenResponse.access_token);
            this.context.secrets.store(`${this.service}.refreshToken`, tokenResponse.refresh_token);
            this.context.globalState.update(this.service, authorization).then(() => {
                this.credentials = <Credentials>authorization;
                this.credentials.token = tokenResponse.access_token;
                this.credentials.refreshToken = tokenResponse.refresh_token;
                resolve(true);
            });
        });
    }

    private async fetchToken(authorizationCode: AuthorizationCode): Promise<any> {
        return new Promise((resolve, reject) => {
            let body = `grant_type=authorization_code&code=${authorizationCode.code}`;
            fetch('https://bitbucket.org/site/oauth2/access_token', { method: 'POST', body: body, headers: this.httpOAuthHeaders() })
                .then(res => res.json())
                .then(result => {
                    resolve({ error: undefined, data: result });
                })
                .catch((err) => {
                    reject({ error: err, data: undefined });
                });
        });
    }
    private async fetchRefreshToken(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.context.secrets.get(`${this.service}.refreshToken`).then((result) => {
                this.credentials.refreshToken = result === undefined ? '' : result;
                let body = `grant_type=refresh_token&refresh_token=${this.credentials.refreshToken}`;
                fetch('https://bitbucket.org/site/oauth2/access_token', { method: 'POST', body: body, headers: this.httpOAuthHeaders() })
                    .then(res => res.json())
                    .then(result => {
                        resolve({ error: undefined, data: result });
                    })
                    .catch((err) => {
                        reject({ error: err, data: undefined });
                    });
            });
        });
    }
    private async runRedirectServer(port: number): Promise<AuthorizationCode> {
        return new Promise((resolve, rejects) => {
            this.state = randomUUID();
            this.server = createServer((req: IncomingMessage, res: ServerResponse) => {
                if (req.url !== undefined) {
                    let query = parse(req.url);
                    if ((query['/?code'] !== undefined) && (query.state !== undefined)) {
                        resolve({ code: <string>query['/?code'], state: <string>query.state, error: undefined });
                    }
                    try {
                        let content = readFileSync(this.context.asAbsolutePath('out/BitBucket/redirectpage.htm'), 'utf8');
                        res.write(content);
                    } catch (e) {
                        console.log(e);
                        rejects({ error: e });
                    }
                    res.end();
                }
            });
            this.server.listen(port).listening;
            open(`https://bitbucket.org/site/oauth2/authorize?client_id=${this.clientId}&response_type=code&state=${this.state}`);
        });
    }
    private clearCredentials() {
        this.credentials = this.initCredentials();
        this.context.globalState.update(this.service, undefined);
        this.context.secrets.delete(`${this.service}.token`);
        this.context.secrets.delete(`${this.service}.refreshToken`);
    }
    dispose() { }
}

type ValidateResult = {
    error: boolean,
    errorMessage: string,
    errorObj?: any
};
type AuthorizationCode = {
    error: any,
    code: string,
    state: string
};
type Authorization = {
    scopes: string,
    expiresIn: number,
    tokenType: string,
    state: string,
    expireTime: Date
};
type Credentials = Authorization & {
    token: string,
    refreshToken: string
};
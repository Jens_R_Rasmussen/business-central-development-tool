import { ExtensionContext, commands, workspace, Uri, window, extensions, QuickPickItem } from 'vscode';
import * as constants from './constants';
import { Process } from './executeLibrary';
import { OutputChannel } from './outputChannel';
import open from 'open';
export function registreCommands(context: ExtensionContext) {
    let subscriptions = context.subscriptions;
    subscriptions.push(commands.registerCommand(constants.gitFetchPruneAndCheckoutCommand, () => gitFetchPruneAndCheckout()));
    subscriptions.push(commands.registerCommand(constants.gitCleanupLocalBranches, () => cleanUpLocalGitBranches()));
    subscriptions.push(commands.registerCommand(constants.openGitRemoteCommand, () => openRemote()));
}
async function openRemote() {
    let repository: Repository | undefined = await getRepository();
    if (repository === undefined) {
        return;
    }
    OutputChannel.createChannal('EV Git');
    OutputChannel.appendLine('EV Git', `Get Remote From Repository: ${repository.label}`);
    let remoteUrl = await getRemoteUrl(repository.path);
    OutputChannel.appendLine('EV Git', `Open Git Remote ${remoteUrl}`);
    open(remoteUrl);
}

async function cleanUpLocalGitBranches() {
    let repository: Repository | undefined = await getRepository();
    if (repository === undefined) {
        return;
    }
    OutputChannel.createChannal('EV Git');
    OutputChannel.appendLine('EV Git', `Repository: ${repository.label}`);
    OutputChannel.appendLine('EV Git', 'git fetch --prune');
    try {
        let gitFetchSuccessed = await gitFetchPrune(repository.path);
        OutputChannel.appendLine('EV Git', `Reading remote branch list`);
        let remote = await getRemoteBranches(repository.path);
        remote.forEach(element => {
            OutputChannel.appendLine('EV Git', `  ${element}`);
        });
        OutputChannel.appendLine('EV Git', `Reading local branch list`);
        let local = await getLocalBranches(repository.path);
        local.forEach(element => {
            OutputChannel.appendLine('EV Git', `  ${element}`);
        });
        console.log('Branch objects', remote, local);
        let repositoryDeleted: boolean = false;
        local = local.filter((item: string) => { return item !== "" && item !== 'master' && item !== 'origin'; });
        while (local.length > 0) {
            let item = local.shift();
            if (item !== undefined) {
                if (!remote.includes(item)) {
                    if (workspace.getConfiguration(constants.extensionConfig).get(constants.cleanupLocalBranchesConfirmDialogSettings) !== true) {
                        try {
                            if (repository !== undefined) {
                                let response = await deleteBranch(repository.path, item, false);
                                repositoryDeleted = true;
                                OutputChannel.appendLine('EV Git', `Delete branch: ${item}`);
                            }
                        } catch (err) {
                            OutputChannel.appendLine('EV Git', `Error: ${err}`);
                        }
                    } else {
                        OutputChannel.appendLine('EV Git', `Branch: ${item} Waiting for response`);
                        await window.showInformationMessage(`Are you sure you want to delete local branch: ${item}`, ...['Yes', 'No']).then(async selection => {
                            if (selection === 'Yes') {
                                try {
                                    if ((repository !== undefined) && (item !== undefined)) {
                                        let response = await deleteBranch(repository.path, item, true);
                                        repositoryDeleted = true;
                                        OutputChannel.appendLine('EV Git', `Delete branch: ${item}`);
                                    }
                                } catch (err) {
                                    OutputChannel.appendLine('EV Git', `Error: ${err}`);
                                }
                            } else {
                                OutputChannel.appendLine('EV Git', `Branch: ${item} not deleted`);
                            }
                        });
                    }
                }
            }
        }
        if (repositoryDeleted === false) {
            OutputChannel.appendLine('EV Git', 'No Repositories is deleted!');
        } else {
            OutputChannel.appendLine('EV Git', 'git fetch --prune');
            try {
                let gitFetchSuccessed = await gitFetchPrune(repository.path);
            } catch (err) {
                OutputChannel.appendLine('EV Git', `Error: ${err}`);
            }
        }
    } catch (e) {
        OutputChannel.appendLine('EV Git', `Error: ${e}`);
    }
    OutputChannel.appendLine('EV Git', 'Done!');
}
async function getRepository() {
    let repository: Repository | undefined;
    let list = getRepositoryList();
    if (list.length === 1) {
        return list[0];
    }
    wait(30);
    repository = await window.showQuickPick(list, { placeHolder: 'Choose a repository', canPickMany: false });
    return repository;
}

async function wait(ms: number) {
    await new Promise(resolve => setTimeout(resolve, ms));
}
function getRepositoryList(): Repository[] {
    // const gitExtension: GitExtension | any = extensions.getExtension('vscode.git')?.exports;
    const gitExtension: any = extensions.getExtension('vscode.git')?.exports;
    const git = gitExtension.getAPI(1);
    let list: Repository[];
    list = git.repositories.map((element: any) => ({ label: element.repository.root.split('\\').slice(-1)[0], description: element.repository.headLabel, path: element.repository.root, branches: element.repository._refs }));
    return list;
}

async function gitFetchPruneAndCheckout() {
    let repository: Repository | undefined = await getRepository();
    if (!repository) {
        return;
    }
    OutputChannel.createChannal('EV Git');
    OutputChannel.appendLine('EV Git', `Repository: ${repository.label}`);
    OutputChannel.appendLine('EV Git', 'git fetch --prune');
    try {
        let gitFetchSuccessed = await gitFetchPrune(repository.path);
        let branchlist = await getAllBranches(repository.path);
        let branch = await window.showQuickPick(branchlist.map((element: any) => ({ label: element.name, description: getCommitDescription(element), type: element.type })), { placeHolder: 'Choose a branch', canPickMany: false });
        if (branch === undefined) {
            OutputChannel.appendLine('EV Git', 'No branch selected');
            OutputChannel.appendLine('EV Git', 'Done!');
            return;
        }
        OutputChannel.appendLine(`EV Git`, `git checkout ${branch.label}`);
        Process.exec(`powershell -Command "&{ push-location '${repository?.path}'; git checkout -q ${getBranchName(branch)};}"`, (error: any, stdout: string, stderr: string) => {
            if (error !== null) {
                OutputChannel.appendLine('EV Git', `Error: ${error.message}`);
                return;
            }
            if (stderr !== '') {
                OutputChannel.appendLine('EV Git', `Error: ${stderr}`);
                return;
            }
            OutputChannel.appendLine('EV Git', 'Done!');
        });
    } catch (err) {
        OutputChannel.appendLine('EV Git', `Error: ${err}`);
    }
}
function getBranchName(branch: { label: string; description: string; type: number; }): string {
    if (branch.type === 0) {
        return branch.label;
    }
    return branch.label.substring(7);
}

// function getCommitDescription(branch: Ref): string {
function getCommitDescription(branch: any): string {
    let commit = `${branch.commit}`.substr(0, 8);
    if (branch.type === 0) {
        return `${commit}`;
    }
    return `Remote branch at ${commit}`;
}
function getRemoteUrl(repositoryPath: string) {
    return new Promise<string>((resolve, reject) => {
        Process.exec(`powershell -Command "&{ push-location '${repositoryPath}'; git config --get remote.origin.url;}"`, (error: any, stdout: string, stderr: string) => {
            if (error !== null) {
                reject(error.message);
            }
            if (stderr.startsWith('error:')) {
                reject(stderr);
            }
            let url = stdout.replace(/\.git$/gmi, '');
            if (url.includes('@')) {
                let parts = url.split('@');
                let protocol = url.split('/')[0];
                url = `${protocol}//${parts[1]}`;
            }
            resolve(url);
        });
    });
}
function gitFetchPrune(repositoryPath: string) {
    return new Promise<boolean>((resolve, reject) => {
        Process.exec(`powershell -Command "&{ push-location '${repositoryPath}'; git fetch --prune; echo done;}"`, (error: any, stdout: string, stderr: string) => {
            if (error !== null) {
                reject(error.message);
            }
            if (stderr.startsWith('error:')) {
                reject(stderr);
            }
            resolve(stdout.trimEnd() === 'done');
        });
    });
}
function getRemoteBranches(repositoryPath: string) {
    return new Promise<string[]>((resolve, reject) => {
        Process.exec(`powershell -Command "&{ push-location '${repositoryPath}'; git branch -r --format='%(refname:lstrip=3)';}"`, (error: any, stdout: string, stderr: string) => {
            if (error !== null) {
                reject(error.message);
            }
            if (stderr !== '') {
                reject(stderr);
            }
            let remote = stdout.trimEnd().split(/[\r\n]+/g);
            resolve(remote.filter((item: string) => { return item !== "HEAD"; }));
        });
    });
}
function getLocalBranches(repositoryPath: string) {
    return new Promise<string[]>((resolve, reject) => {
        Process.exec(`powershell -Command "&{ push-location '${repositoryPath}'; git branch --format='%(refname:lstrip=2)';}"`, (error: any, stdout: string, stderr: string) => {
            if (error !== null) {
                reject(error.message);
            }
            if (stderr !== '') {
                reject(stderr);
            }
            let remote = stdout.trimEnd().split(/[\r\n]+/g);
            resolve(remote.filter((item: string) => { return item !== "HEAD"; }));
        });
    });
}
function getAllBranches(repositoryPath: string) {
    // return new Promise<Ref[]>((resolve, reject) => {
    return new Promise<any[]>((resolve, reject) => {
        Process.exec(`powershell -Command "&{ push-location '${repositoryPath}'; git branch -a --format='%(refname:lstrip=1);%(refname:lstrip=2);%(objectname)';}"`, (error: any, stdout: string, stderr: string) => {
            if (error !== null) {
                reject(error.message);
            }
            if (stderr !== '') {
                reject(stderr);
            }
            let remote = stdout.trimEnd().split(/[\r\n]+/g);
            // let branshes = remote.map<Ref>((item) => {
            let branshes = remote.map<any>((item) => {
                let branch = item.split(';');
                let ref = branch[0].split('/');
                return { type: (ref[0] === 'remotes' ? 1 : 0), name: branch[1], commit: branch[2], remote: (ref[0] === 'remotes' ? ref[1] : '') };
            });
            resolve(branshes);
        });
    });
}
function deleteBranch(repositoryPath: string, branch: string, force: boolean) {
    return new Promise<boolean>((resolve, reject) => {
        let forceParameter = '';
        if (force === true) {
            forceParameter = '--force ';
        }
        Process.exec(`powershell -Command "&{ push-location '${repositoryPath}'; git branch --delete ${forceParameter}${branch};}"`, (error: any, stdout: string, stderr: string) => {
            if (error !== null) {
                reject(error.message);
            }
            if (stderr !== '') {
                reject(stderr);
            }
            resolve(true);
        });
    });
}
export function gitInit(repositoryPath: string) {
    return new Promise<boolean>((resolve, reject) => {
        let process = Process.exec(`powershell -Command "&{ push-location '${repositoryPath}'; git init; git add .; git commit -m Init; write-host done;}"`, (error: any, stdout: string, stderr: string) => {
            if (error !== null) {
                reject(error.message);
            }
            if (stderr.startsWith('error:')) {
                reject(stderr);
            }
        });
        process.stdout?.on('data', (chunck) => {
            if (chunck.trimEnd() === 'done') {
                resolve(true);
            }
        });
        process.on('close', () => {
            if (process.exitCode !== 0) {
                reject(process.exitCode);
            }
            resolve(true);
            process.kill();
        });

    });
}
export function gitAddRemote(repositoryPath: string, remote: string) {
    return new Promise<boolean>((resolve, reject) => {
        let process = Process.exec(`powershell -Command "&{ push-location '${repositoryPath}'; git remote add origin ${remote}; git push -u origin master; write-host done;}"`, (error: any, stdout: string, stderr: string) => {
            if (error !== null) {
                reject(error.message);
            }
            if (stderr.startsWith('error:')) {
                reject(stderr);
            }
        });
        process.stdout?.on('data', (chunck) => {
            if (chunck.trimEnd() === 'done') {
                resolve(true);
            }
        });
        process.on('close', () => {
            if (process.exitCode !== 0) {
                reject(process.exitCode);
            }
            resolve(true);
            process.kill();
        });

    });
}

export function setGitCommitConfiguration() {
    let config = workspace.getConfiguration(constants.extensionConfig);
    let enable = config.get(constants.setMandatortStagingSettings);
    if (enable) {
        try {
            let workspaceFolders = workspace.workspaceFolders;
            if (workspaceFolders === undefined) {
                throw new Error("No Folder Found");
            }
            config = workspace.getConfiguration("git", workspaceFolders[0]);
            config.update('enableSmartCommit', false);
            config.update('suggestSmartCommit', false);
        } catch (e) {
            config = workspace.getConfiguration("git");
            config.update('enableSmartCommit', false);
            config.update('suggestSmartCommit', false);
        }
    }
}
interface Repository {
    label: string;
    description: string;
    path: string;
    // branches: Ref[];
    branches: any[];
}

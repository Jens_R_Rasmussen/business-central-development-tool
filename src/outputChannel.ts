import { window, OutputChannel as OutputChannelObject  } from 'vscode';
export class OutputChannel {
    static outputChannels: { [id: string]: OutputChannelObject; } = {};
    static createChannal(id: string) {
        if (this.outputChannels[id] === undefined) {
            this.outputChannels[id] = window.createOutputChannel(id);
        }
        this.outputChannels[id].clear();
        this.outputChannels[id].show();
    }
    static appendLine(id: string, value: string) {
        this.outputChannels[id].appendLine(value);
        this.outputChannels[id].show();
    }
}

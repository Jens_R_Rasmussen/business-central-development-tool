import { ExtensionContext, commands, workspace, window, Uri } from 'vscode';
import * as constants from './constants';
import { DockerContainer } from './dockerLibrary';
import { Terminal } from './executeLibrary';

export function registerCommands(context: ExtensionContext) {
    let subscriptions = context.subscriptions;
    subscriptions.push(commands.registerCommand(constants.openTerminalWithPowerShellModuleCommand, openCleanTerminal));
    subscriptions.push(commands.registerCommand(constants.updateAddinsCommand, () => executeEVToolsProperty(constants.updateAddinsSettings)));
    subscriptions.push(commands.registerCommand(constants.addFontsToContainerCommand, () => executeEVToolsProperty(constants.addFontsToContainerSettings)));
    subscriptions.push(commands.registerCommand(constants.editServiceParameterCommand, () => executeEVToolsProperty(constants.editServiceParameterSettings)));
    subscriptions.push(commands.registerCommand(constants.editWebserverParameterCommand, () => executeEVToolsProperty(constants.editWebserverParameterSettings)));
    subscriptions.push(commands.registerCommand(constants.getContainerInfoCommand, () => executeEVToolsProperty(constants.getContainerInfoSettings)));
    subscriptions.push(commands.registerCommand(constants.openContainerEventlogCommand, () => executeEVToolsProperty(constants.getContainerEventlogSettings)));
    subscriptions.push(commands.registerCommand(constants.removeContainerCommend, () => executeEVToolsProperty(constants.removeContainerSettings)));
    subscriptions.push(commands.registerCommand(constants.updatePowerShellModuleCommand, () => executeEVToolsProperty(constants.updatePowerShellModuleSettings)));
}
function openCleanTerminal() {
    Terminal.show(false);
    Terminal.executeCmdLet('', '', false, false);
    Terminal.show();
}
function executeEVToolsProperty(settings: string, clearHost = true, show = true) {
    let config = workspace.getConfiguration(constants.extensionConfig);
    let command: string | undefined = config.get(settings);
    if (command === undefined) {
        window.showErrorMessage(`Settings: '${settings}' has no value`);
        return;
    }
    Terminal.executeCmdLet(command, command, clearHost, show);
}
export function executeEVToolsPropertyWithContainerNameParameter(settings: string, containerName: string, clearHost = true, show = true) {
    let config = workspace.getConfiguration(constants.extensionConfig);
    let command: string | undefined = config.get(settings);
    if (command === undefined) {
        window.showErrorMessage(`Settings: '${settings}' has no value`);
        return;
    }
    command += ` ${containerName}`;
    Terminal.executeCmdLet(command, command, clearHost, show);
}

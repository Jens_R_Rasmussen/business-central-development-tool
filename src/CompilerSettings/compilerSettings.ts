export interface CompilerSettings {
    name: string;
    app: App;
    launch: Launch | any;
    appSourceCop?: AppSourceCop;
    publisherBlobFilter?: PublisherBlobFilter[]
    pipeline?: any
}
export interface PublisherBlobFilter {
    publisher: string;
    contains: string;
}
export interface AppSourceCop {
    version?: string;
    baselinePackageCachePath?: string;
}
export interface App {
    replaceDependencies?: boolean;
    dependencies?: Dependencies[];
    application?: string;
    version?: string;
    runtime?: string;
    resourceExposurePolicy?: ResourceExposurePolicy;
    showMyCode?: boolean;
    target?: string;
    preprocessorSymbols?: string[];
    dependencyTarget?: DependencyTarget;
    fixedDependenciesInstall?: FixedDependenciesInstall[];
    applicationInsightsKey?: string;
    applicationInsightsConnectionString?: string;
}
export interface Launch {
    tenant?: string;
    server?: string;
    environmentType?: string;
    environmentName?: string;
    serverInstance?: string;
}
export interface Dependencies {
    id: string;
    publisher?: string;
    name?: string;
    version: string;
}
export interface ResourceExposurePolicy {
    allowDebugging: boolean;
    allowDownloadingSource: boolean;
    includeSourceInSymbolFile: boolean;
}
export enum DependencyTarget {
    current,
    closest,
    latest
}
export interface FixedDependenciesInstall {
    id: string;
    type: InstallSourceType;
    organization?: string;
    project?: string;
    definition?: string;
    build?: string;
    branch?: string;
    url?: string;
    sharedKey?: string;
    path?: string;
}
export enum InstallSourceType {
    devOps,
    blob,
    file
}
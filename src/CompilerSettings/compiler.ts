import { readFileSync, writeFileSync, existsSync, readdirSync, unlinkSync, exists, mkdirSync } from 'fs';
import { join, resolve } from 'path';
import { commands, ExtensionContext, Range, TextDocumentShowOptions, TextEditor, TextLine, Uri, window, workspace, WorkspaceConfiguration, WorkspaceFolder } from 'vscode';
import { AppInfo } from '../AppHandling/AppInfo';
import * as constants from '../constants';
import { getAppManifest, getAppSourceCop, getCurrentWorkspaceFolder, getEVCompilerSettings, getWorkspaceFolderFromPath, setAppManifest, setAppSourceCop, setEVCompilerSettings } from '../workspaceFileAndFolderHelper';
import { App, CompilerSettings, Dependencies, Launch, DependencyTarget, FixedDependenciesInstall, InstallSourceType, AppSourceCop } from './compilerSettings';
import { ConfigurationTarget, extensions } from 'vscode';
import path = require('path');

export class Compiler {
    context: ExtensionContext;
    defaultConfigurations: CompilerSettings = { name: "", launch: { tenant: "default" }, app: {}, appSourceCop: {} };
    initalizeDebugAdapterService: any;
    buildService: any;
    serverProxy: any;
    constructor(context: ExtensionContext) {
        this.context = context;
        this.testActiveCompilerSettings(null);
    }
    registerCommands() {
        let subscriptions = this.context.subscriptions;
        subscriptions.push(commands.registerCommand(constants.debugWithoutPublishingCommand, () => this.debugOnly()));
        subscriptions.push(commands.registerCommand(constants.packageCommand, () => this.compile(false, false, false)));
        subscriptions.push(commands.registerCommand(constants.publishAndOpenInTheDesignerCommand, () => this.openInDesigner()));
        subscriptions.push(commands.registerCommand(constants.publishWithDebuggingCommand, () => this.compile(true, false, true)));
        subscriptions.push(commands.registerCommand(constants.publishWithoutDebuggingCommand, () => this.compile(false, false, true)));
        subscriptions.push(commands.registerCommand(constants.rapidApplicationPublishWithDebuggingCommand, () => this.compile(true, true, true)));
        subscriptions.push(commands.registerCommand(constants.rapidApplicationPublishWithoutDebuggingCommand, () => this.compile(false, true, true)));
        subscriptions.push(commands.registerCommand(constants.updateAppMainfestFromCompilerSettingsCommand, () => this.updateAppFileFromCompilerSettingsInCurrentWorkspace()));
        subscriptions.push(commands.registerCommand(constants.initCompilerSettingsCommand, () => this.initCompilerSettings()));
        subscriptions.push(commands.registerCommand(constants.downloadSymbolsCommand, () => this.downloadSymbols()));
        subscriptions.push(commands.registerCommand(constants.editCompilerSettingsCommand, () => this.editCompilerSettings()));
        subscriptions.push(commands.registerCommand(constants.copyCompilerSettingsCommand, () => this.copyCompilerSettings()));
        subscriptions.push(commands.registerCommand(constants.updateAppCompilerSettingsCommand, () => this.updateAppCompilerSettings()));
        subscriptions.push(commands.registerCommand(constants.updateAppSourceCopCompilerSettingsCommand, () => this.updateAppSourceCopCompilerSettings()));
        subscriptions.push(commands.registerCommand(constants.updateLaunchCompilerSettingsCommand, () => this.updateLaunchCompilerSettings()));
        subscriptions.push(commands.registerCommand(constants.updateAllCompilerSettingsCommand, () => this.updateAllCompilerSettings()));

        subscriptions.push(window.onDidChangeActiveTextEditor((e: any) => this.testActiveCompilerSettings(e)));
    }
    async updateAllCompilerSettings(): Promise<any> {
        await this.selectedCompilerSettingsAndUpdateFiles(true, true, true);
    }

    async updateLaunchCompilerSettings(): Promise<any> {
        await this.selectedCompilerSettingsAndUpdateFiles(false, false, true);
    }
    async updateAppSourceCopCompilerSettings(): Promise<any> {
        await this.selectedCompilerSettingsAndUpdateFiles(false, true, false);
    }
    async updateAppCompilerSettings(): Promise<any> {
        await this.selectedCompilerSettingsAndUpdateFiles(true, false, false);
    }
    async selectedCompilerSettingsAndUpdateFiles(updateAppFile: boolean, updateAppSouceCopFile: boolean, updateLaunch: boolean) {
        let compilerSettingsInfo: any = await this.getCompilerSettingsInformation();
        if (compilerSettingsInfo !== undefined) {
            if (compilerSettingsInfo.selectedCompilerSettings !== undefined) {
                this.updateFilesFromCompilerSettings(compilerSettingsInfo, updateAppFile, updateAppSouceCopFile, updateLaunch);
            }
        }
    }
    async copyCompilerSettings(): Promise<any> {
        if (workspace.workspaceFolders === undefined) {
            return;
        } let compilerSettingsInfo: any = await this.getCompilerSettingsInformation();
        if (compilerSettingsInfo === undefined) {
            return;
        }
        if (compilerSettingsInfo.selectedCompilerSettings === undefined) {
            return;
        }
        let destinationWorkspace: WorkspaceFolder | any;
        if (workspace.workspaceFolders.length === 1) {
            destinationWorkspace = workspace.workspaceFolders.map((item: WorkspaceFolder) => ({ label: item.name, description: item.uri.fsPath }))[0];
        } destinationWorkspace = await window.showQuickPick(workspace.workspaceFolders?.map((item: WorkspaceFolder) => ({ label: item.name, description: item.uri.fsPath })), { placeHolder: 'Select copy to workspace', canPickMany: false });
        if (destinationWorkspace === undefined) {
            return;
        }
        window.showInputBox({ title: 'New Settings Name', value: compilerSettingsInfo.selectedCompilerSettings.name }).then((name: string | any) => {
            if (name !== undefined) {
                compilerSettingsInfo.selectedCompilerSettings.name = name;
                let compilerSettings: CompilerSettings[] = getEVCompilerSettings(destinationWorkspace.description);
                compilerSettings.push(compilerSettingsInfo.selectedCompilerSettings);
                setEVCompilerSettings(destinationWorkspace.description, compilerSettings);
            }
        });
    }

    async updateFilesFromCompilerSettings(compilerSettingsInfo: any, updateAppFile: boolean, updateAppSouceCopFile: boolean, updateLaunch: boolean) {
        let settings = compilerSettingsInfo.allWorkspaceData.filter((element: any) => {
            return element.compilerSettings.some((element: any) => element.name === compilerSettingsInfo.selectedCompilerSettings.name);
        });
        if (settings.length > 1) {
            let response = await window.showInformationMessage(`Setting '${compilerSettingsInfo.selectedCompilerSettings.name}' exist in ${settings.length} workspaces do you want to update all workspaces?`, { modal: true }, { title: 'Yes', isCloseAffordance: false }, { title: 'No', isCloseAffordance: true });
            if (response?.title === 'Yes') {
                settings.forEach((element: any) => {
                    this.updateFilesFromSelectedCompilerSettings({ compilerSettings: element.compilerSettings.find((element: any) => element.name === compilerSettingsInfo.selectedCompilerSettings.name), file: element.compilerSettingsPath }, updateAppFile, updateAppSouceCopFile, updateLaunch);
                });
                return;
            }
        }
        this.updateFilesFromSelectedCompilerSettings({ compilerSettings: compilerSettingsInfo.selectedCompilerSettings, file: compilerSettingsInfo.compilerSettingsPath }, updateAppFile, updateAppSouceCopFile, updateLaunch);
    }
    updateFilesFromSelectedCompilerSettings(data: { compilerSettings: any; file: string; }, updateAppFile: boolean, updateAppSouceCopFile: boolean, updateLaunch: boolean) {
        if (updateAppFile) {
            this.updateAppFile(data.compilerSettings, data.file);
        }
        if (updateAppSouceCopFile) {
            this.updateAppSourceCopFile(data.compilerSettings, data.file);
        }
        if (updateLaunch) {
            this.updateWorkspaceLaunch(<WorkspaceFolder>getWorkspaceFolderFromPath(data.file), data.compilerSettings);
        }
    }
    async editCompilerSettings() {
        let compilerSettingsInfo: any = await this.getCompilerSettingsInformation();
        if (compilerSettingsInfo !== undefined) {
            if (compilerSettingsInfo.selectedCompilerSettings !== undefined) {
                this.openEditCompilerSettings(compilerSettingsInfo.compilerSettingsPath, compilerSettingsInfo.selectedCompilerSettings.name);
            }
        }
    }

    private async getCompilerSettingsInformation() {
        let compilerSettingsInfo: any;
        let workspaceCompilerSettings = getAllCompilerSettingsInWorkspace();
        if (workspaceCompilerSettings.length === 1) {
            compilerSettingsInfo = workspaceCompilerSettings[0];
        } else {
            let result = await window.showQuickPick(workspaceCompilerSettings.map((item: any) => ({ label: item.workspaceName, description: `Path: ${item.compilerSettingsPath}`, item: item })), { placeHolder: 'Select Workspace', canPickMany: false });
            if (result !== undefined) {
                compilerSettingsInfo = result.item;
            }
        }
        if (compilerSettingsInfo === undefined) {
            return undefined;
        }
        compilerSettingsInfo.allWorkspaceData = workspaceCompilerSettings;
        compilerSettingsInfo.selectedCompilerSettings = await this.getCompilerSettings(compilerSettingsInfo.compilerSettings);
        return compilerSettingsInfo;
    }

    openEditCompilerSettings(path: string, name: string) {
        workspace.openTextDocument(Uri.file(path)).then((file) => {
            let line: TextLine;
            let range: Range | undefined = undefined;
            try {
                for (let index = 0; index < file.lineCount; index++) {
                    line = file.lineAt(index);
                    if (line.text.includes(`"name": "${name}"`)) {
                        range = new Range(line.lineNumber, line.firstNonWhitespaceCharacterIndex, line.lineNumber, line.firstNonWhitespaceCharacterIndex);
                        break;
                    }
                }
            } catch (error) {
                console.log(error);
            }
            let option: TextDocumentShowOptions = {
                selection: range
            };
            window.showTextDocument(file, option);
        });
    }

    initCompilerSettings() {
        if (this.testActiveCompilerSettings(null) === true) {
            return;
        }
        let newLaunch: Launch = this.initLaunch();
        let newApp: App = this.initApp();
        let newAppSourceCop = this.initAppSourceCop();
        let pipeline = this.initPipeline(newApp);
        let newCompilerSettings: CompilerSettings = { name: 'Standard', launch: newLaunch, app: newApp, appSourceCop: newAppSourceCop, pipeline: pipeline };
        writeFileSync(getCompilerFilePath(), JSON.stringify([newCompilerSettings], this.replacer, 4));
    }
    initPipeline(newApp: App): any {
        let localization = workspace.getConfiguration(constants.extensionConfig).get(constants.containerCountrySettings);
        let version = constants.initVersion;
        if (newApp.application) {
            version = `${newApp.application?.split('.')[0]}.${newApp.application?.split('.')[1]}`;
        }
        return {
            /* eslint-disable @typescript-eslint/naming-convention */
            FailAppOnWarnings: newApp.target === "Cloud" ? "1" : "0",
            EnvironmentType: newApp.target === "Cloud" ? "SandBox" : "OnPrem",
            Localization: localization,
            Version: version,
            CompilerArgumentForVersionNo: '?.?.?.*'
            /* eslint-enable @typescript-eslint/naming-convention */
        };
    }

    replacer(key: any, value: any): any {
        if (typeof value === 'number') {
            if (key === 'dependencyTarget') {
                return DependencyTarget[value];
            }
            if (key === 'type') {
                return InstallSourceType[value];
            }
        }
        return value;
    }
    initApp(): App {
        let app = getAppManifest();
        return {
            application: app.application,
            applicationInsightsConnectionString: app.applicationInsightsConnectionString,
            applicationInsightsKey: app.applicationInsightsKey,
            dependencies: app.dependencies,
            dependencyTarget: DependencyTarget.current,
            replaceDependencies: false,
            fixedDependenciesInstall: [],
            preprocessorSymbols: app.preprocessorSymbols,
            resourceExposurePolicy: app.resourceExposurePolicy,
            runtime: app.runtime,
            showMyCode: app.showMyCode,
            target: app.target,
            version: app.version,
        };
    }
    initLaunch(): Launch {
        let launch = workspace.getConfiguration('launch', getCurrentWorkspaceFolder());
        let configurations: any = launch.get('configurations');
        let containerConfigs = configurations.filter((item: any) => { return (item.type.toUpperCase() === 'AL') && (item.serverInstance !== undefined) && (item.server !== undefined) && (item.serverInstance.toUpperCase() === 'BC' || item.serverInstance.toUpperCase() === 'NAV'); });
        if (containerConfigs.length === 0) {
            return {
                server: 'http://localhost',
                serverInstance: 'BC',
                tenant: 'Default'
            };
        }
        return {
            server: configurations[0].server,
            serverInstance: configurations[0].serverInstance,
            tenant: configurations[0].tenant,
            environmentName: configurations[0].environmentName,
            environmentType: configurations[0].environmentType
        };

    }
    initAppSourceCop(): AppSourceCop | undefined {
        let appSourceCop = getAppSourceCop();
        if (appSourceCop === undefined) {
            return undefined;
        }
        return {
            version: appSourceCop.version,
            baselinePackageCachePath: appSourceCop.baselinePackageCachePath
        };
    }
    async updateAppFileFromCompilerSettingsInCurrentWorkspace() {
        let compilerSettings = await this.getCompilerSettings();
        console.log(compilerSettings);
        if (compilerSettings === undefined) {
            return;
        }
        this.updateAppFile(compilerSettings);
    }
    testActiveCompilerSettings(e: TextEditor | undefined | null): any {
        if (e === undefined) {
            return;
        }
        let exist = existsSync(getCompilerFilePath());
        commands.executeCommand('setContext', constants.eVCompilerSettingsExist, exist);
        return exist;
    }
    async downloadSymbols() {
        this.getMSBuildService();
        if (this.buildService === undefined) {
            return;
        }
        let launch: WorkspaceConfiguration | undefined;
        let compilerSettings = await this.getCompilerSettings();
        console.log(compilerSettings);
        if (compilerSettings === undefined) {
            return;
        }
        await this.clearAlPackages();
        launch = await this.updateLaunch(compilerSettings);
        // await commands.executeCommand('workbench.action.files.saveFiles');
        // let newLaunch = workspace.getConfiguration('launch', getCurrentWorkspaceFolder());
        // console.log(newLaunch);
        workspace.saveAll(true).then((result) => {
            console.log(result);
            this.buildService.symbolsService.downloadSymbols(false, true).then(() => {
                if (launch !== undefined) {
                    this.restoreLaunch(launch);
                }
            }).catch(() => {
                if (launch !== undefined) {
                    this.restoreLaunch(launch);
                }
            });
        });
    }
    private async clearAlPackages() {
        let mSAlLanguageSettings = workspace.getConfiguration('al');
        let alPackagesPath: string[] = <string[]>mSAlLanguageSettings.get('packageCachePath');
        let currWorkspaceFolder = getCurrentWorkspaceFolder();
        if (currWorkspaceFolder === undefined) {
            return;
        }
        let compAlPackagesPath = join(currWorkspaceFolder.uri.fsPath, <string>alPackagesPath[0]);
        try {
            let files = readdirSync(compAlPackagesPath);
            if (files.length > 0) {
                let result = await window.showInformationMessage(`Do you want to clear '${alPackagesPath}' before download new symbols?`, { modal: true }, 'Yes');
                if (result === 'Yes') {
                    files.forEach(element => {
                        try {
                            unlinkSync(join(compAlPackagesPath, element));
                        } catch (error) {
                            console.log(error);
                        }
                    });
                }
            }
        } catch {
        }
    }

    async compile(debug: boolean, rapid: boolean, publish: boolean) {
        this.getMSBuildService();
        if (this.buildService === undefined) {
            return;
        }
        let launch: WorkspaceConfiguration | undefined | any;
        let compilerSettings = await this.getCompilerSettings();
        console.log(compilerSettings);
        if (compilerSettings === undefined) {
            return;
        }
        launch = this.updateLaunch(compilerSettings);
        this.updateFiles(compilerSettings);
        this.closeAllLaunchAndAppFiles();
        if (publish) {
            this.buildService.publishContainer(!debug, rapid).then((result: any) => {
                if (result) {
                    this.restoreData(launch);
                }
            }).catch(() => {
                this.restoreData(launch);
            });
        } else {
            this.buildService.packageContainer(rapid).then((result: any) => {
                if (result) {
                    this.restoreData(launch);
                }
            }).catch(() => {
                this.restoreData(launch);
            });
        }
    }
    closeAllLaunchAndAppFiles() {
        let tabs = window.tabGroups.all;
        console.log(tabs);
        let filesToClose = workspace.textDocuments.filter((file: any) => {
            console.log(file.fileName);
            return ((file.fileName === 'launch.json') || (file.fileName === 'app.json'));
        });
        console.log(filesToClose);
    }
    getMSBuildService() {
        if (this.buildService !== undefined) {
            return;
        }
        this.buildService = this.getMicrosoftAlExtensionService('BuildService');
        if (this.buildService === undefined) {
            window.showErrorMessage('BuildService could not be loaded from AL Language Extension');
        }
        this.initalizeDebugAdapterService = this.buildService.initalizeDebugAdapterService;
        this.serverProxy = this.buildService.serverProxy;
    }
    private restoreData(launch: WorkspaceConfiguration | undefined) {
        if (launch !== undefined) {
            this.restoreLaunch(launch);
            this.restoreAppFile();
            this.restoreAppSourceCopFile();
        }
    }
    async addFixeddependenciesToEVCompilerSettings(appInfo: AppInfo, fixedDependencies: FixedDependenciesInstall) {
        let compilerSettingsList = getEVCompilerSettingsList();
        let compilerSettings = await this.getCompilerSettings(compilerSettingsList);
        if (compilerSettings === undefined) {
            return;
        }
        console.log(compilerSettings);
        let fixedDependencyIndex = compilerSettings?.app.fixedDependenciesInstall?.findIndex(element => element.id === appInfo.id);
        if ((fixedDependencyIndex !== undefined) && (fixedDependencyIndex !== -1)) {
            await window.showInformationMessage(`Do you want to replace App: ${appInfo.name}\nWith Version: ${appInfo.version} ?`, { modal: true }, 'Yes').then((result: any) => {
                if (result === 'Yes') {
                    if (compilerSettings?.app.fixedDependenciesInstall !== undefined) {
                        if (fixedDependencyIndex !== undefined) {
                            compilerSettings.app.fixedDependenciesInstall[fixedDependencyIndex] = fixedDependencies;
                        }
                    }
                }
            });
        } else {
            if (compilerSettings?.app.fixedDependenciesInstall === undefined) {
                compilerSettings.app.fixedDependenciesInstall = [fixedDependencies];
            } else {
                compilerSettings?.app.fixedDependenciesInstall?.push(fixedDependencies);
            }
        }
        console.log(compilerSettingsList);
        this.setEVCompilerSettingsList(compilerSettingsList);
    }

    async getCompilerSettings(compilerSettingsList: CompilerSettings[] | undefined = undefined): Promise<CompilerSettings | undefined> {
        if (compilerSettingsList === undefined) {
            compilerSettingsList = getEVCompilerSettingsList();
        }
        if (compilerSettingsList.length === 1) {
            return compilerSettingsList[0];
        }
        let result = await window.showQuickPick(compilerSettingsList.map((item: any) => ({ label: item.name, description: `Version: ${item.app !== undefined ? item.app.version : undefined}`, detail: `Server: ${item.launch !== undefined ? item.launch.server : undefined}`, item: item })), { placeHolder: 'Select Compiler Settings', canPickMany: false });
        if (result !== undefined) {
            return result.item;
        }
        return undefined;
    }
    updateFiles(compilerSettings: CompilerSettings) {
        this.updateAppFile(compilerSettings);
        this.updateAppSourceCopFile(compilerSettings);
    }
    updateAppFile(compilerSettings: CompilerSettings, file: string = '') {
        let app = getAppManifest(file);
        console.log(app);
        this.defaultConfigurations.app.application = app.application;
        this.defaultConfigurations.app.replaceDependencies = app.replaceDependencies;
        this.defaultConfigurations.app.dependencies = app.dependencies;
        this.defaultConfigurations.app.version = app.version;
        this.defaultConfigurations.app.runtime = app.runtime;
        this.defaultConfigurations.app.target = app.target;
        this.defaultConfigurations.app.preprocessorSymbols = app.preprocessorSymbols;
        this.defaultConfigurations.app.resourceExposurePolicy = app.resourceExposurePolicy;
        this.defaultConfigurations.app.showMyCode = app.showMyCode;
        this.defaultConfigurations.app.applicationInsightsKey = app.applicationInsightsKey;
        this.defaultConfigurations.app.applicationInsightsConnectionString = app.applicationInsightsConnectionString;
        app.application = compilerSettings.app.application ? compilerSettings.app.application : app.application;
        this.updateAppDependencies(compilerSettings, app);
        app.version = this.updateAppVersion(compilerSettings.app.version ? compilerSettings.app.version : app.version);
        app.runtime = compilerSettings.app.runtime ? compilerSettings.app.runtime : app.runtime;
        app.target = compilerSettings.app.target ? compilerSettings.app.target : app.target;
        app.preprocessorSymbols = compilerSettings.app.preprocessorSymbols;
        if (app.runtime.split('.')[0] >= 8) {
            app.resourceExposurePolicy = compilerSettings.app.resourceExposurePolicy ? compilerSettings.app.resourceExposurePolicy : app.resourceExposurePolicy;
            app.applicationInsightsConnectionString = compilerSettings.app.applicationInsightsConnectionString ? compilerSettings.app.applicationInsightsConnectionString : app.applicationInsightsConnectionString;
            app.applicationInsightsKey = undefined;
            if (app.applicationInsightsConnectionString === undefined) {
                if (compilerSettings.app.applicationInsightsKey !== undefined) {
                    app.applicationInsightsConnectionString = `InstrumentationKey=${compilerSettings.app.applicationInsightsKey}`;
                }
            }
            if (app.resourceExposurePolicy === undefined) {
                app.resourceExposurePolicy = {
                    allowDebugging: false,
                    allowDownloadingSource: false,
                    includeSourceInSymbolFile: false
                };
            }
            app.showMyCode = undefined;
        } else {
            app.applicationInsightsConnectionString = undefined;
            app.applicationInsightsKey = compilerSettings.app.applicationInsightsKey;
            app.resourceExposurePolicy = undefined;
            app.showMyCode = compilerSettings.app.showMyCode ? compilerSettings.app.showMyCode : app.showMyCode;
            if (app.showMyCode === undefined) {
                app.showMyCode = false;
            }
        }
        console.log(app);
        setAppManifest(app, file);
    }
    updateAppVersion(version: string | undefined): any {
        if (version === undefined) {
            return undefined;
        }
        let replace = workspace.getConfiguration(constants.extensionConfig).get(constants.replaceBuildInVersionByDateStampSettings);
        if (!replace) {
            return version;
        }
        let versionparts = version.split('.');
        let date = new Date();
        return `${versionparts[0]}.${versionparts[1]}.${date.getFullYear()}${(date.getMonth() + 1).toString().padStart(2, '0')}${date.getDate().toString().padStart(2, '0')}.${versionparts[3]}`;
    }
    updateAppSourceCopFile(compilerSettings: CompilerSettings, file: string = '') {
        let appSourceCop = getAppSourceCop(file);
        if (appSourceCop !== undefined) {
            if (compilerSettings.appSourceCop !== undefined) {
                if ((appSourceCop.version !== compilerSettings.appSourceCop.version) || (appSourceCop.baselinePackageCachePath !== compilerSettings.appSourceCop.baselinePackageCachePath)) {
                    this.defaultConfigurations.appSourceCop = { version: appSourceCop.version, baselinePackageCachePath: appSourceCop.baselinePackageCachePath };
                    appSourceCop.version = compilerSettings.appSourceCop.version;
                    appSourceCop.version = compilerSettings.appSourceCop.version;
                    setAppSourceCop(appSourceCop, file);
                }
            }
        }
    }
    private updateAppDependencies(compilerSettings: CompilerSettings, app: any) {
        if (compilerSettings.app.replaceDependencies) {
            app.dependencies = compilerSettings.app.dependencies;
        } else {
            if (app.dependencies === undefined) {
                app.dependencies = [];
            }
            compilerSettings.app.dependencies?.forEach((dependency) => {
                let index = app.dependencies.findIndex((value: Dependencies) => {
                    return (value.id === dependency.id);
                });
                if (index !== -1) {
                    app.dependencies[index] = dependency;
                } else {
                    app.dependencies.push(dependency);
                }
            });
        }
    }

    restoreAppFile() {
        let app = getAppManifest();
        console.log(app);
        app.application = this.defaultConfigurations.app.application;
        app.dependencies = this.defaultConfigurations.app.dependencies;
        app.version = this.defaultConfigurations.app.version;
        app.runtime = this.defaultConfigurations.app.runtime;
        app.target = this.defaultConfigurations.app.target;
        app.preprocessorSymbols = this.defaultConfigurations.app.preprocessorSymbols;
        app.resourceExposurePolicy = this.defaultConfigurations.app.resourceExposurePolicy;
        app.showMyCode = this.defaultConfigurations.app.showMyCode;
        app.applicationInsightsKey = this.defaultConfigurations.app.applicationInsightsKey;
        app.applicationInsightsConnectionString = this.defaultConfigurations.app.applicationInsightsConnectionString;
        console.log(app);
        setAppManifest(app);
    }
    restoreAppSourceCopFile() {
        let appSourceCop = getAppSourceCop();
        if (appSourceCop !== undefined) {
            let updated = false;
            if (this.defaultConfigurations.appSourceCop !== undefined) {
                if ((appSourceCop.version !== this.defaultConfigurations.appSourceCop.version) || (appSourceCop.baselinePackageCachePath !== this.defaultConfigurations.appSourceCop.baselinePackageCachePath)) {
                    if (this.defaultConfigurations.appSourceCop.version !== undefined) {
                        updated = true;
                        appSourceCop.version = this.defaultConfigurations.appSourceCop.version;
                    }
                    if (this.defaultConfigurations.appSourceCop.baselinePackageCachePath !== undefined) {
                        updated = true;
                        appSourceCop.baselinePackageCachePath = this.defaultConfigurations.appSourceCop.baselinePackageCachePath;
                    }
                }
            }
            if (updated === true) {
                setAppSourceCop(appSourceCop);
            }
        }
    }
    restoreLaunch(launch: WorkspaceConfiguration) {
        let configurations: any = launch.get('configurations');
        let index = configurations.findIndex((item: any) => {
            return (item.type.toUpperCase() === 'AL') &&
                (item.request.toUpperCase() === 'LAUNCH') &&
                (item.name === this.defaultConfigurations.launch.name);
        });
        if (index === -1) {
            window.showErrorMessage(`Can't restore Launch: ${this.defaultConfigurations.launch.name}`, { modal: true });
            return;
        }
        configurations[index] = this.defaultConfigurations.launch;
        launch.update('configurations', configurations, ConfigurationTarget.WorkspaceFolder);
    }
    updateWorkspaceLaunch(scope: WorkspaceFolder, compilerSettings: CompilerSettings) {
        this.updateLaunch(compilerSettings, scope);
    }
    private async updateLaunch(compilerSettings: CompilerSettings, scope: WorkspaceFolder | undefined = getCurrentWorkspaceFolder()) {
        let launchBackup = workspace.getConfiguration('launch', scope);
        let launch: any = undefined;
        try {
            launch = getLaunch(scope?.uri.fsPath);
        } catch (Error) {
            console.log(Error);
            if (this.buildService === undefined) {
                this.buildService = this.getMicrosoftAlExtensionService('BuildService');
            }
            launch = { version: '0.2.0', configurations: [await this.buildService.initalizeDebugAdapterService.getLocalConfig()] };
        }
        let configurations: any = launch.configurations;
        let containerConfigs = configurations.filter((item: any) => {
            return (item.type.toUpperCase() === 'AL') &&
                (item.request.toUpperCase() === 'LAUNCH');
        })[0];
        if (containerConfigs === undefined) {
            window.showErrorMessage('No AL Launch Exist');
            return;
        }
        this.defaultConfigurations.name = containerConfigs.name;
        this.defaultConfigurations.launch = Object.assign({}, containerConfigs);
        containerConfigs.name = compilerSettings.name;

        Object.entries(compilerSettings.launch).forEach(([key, value]) => {
            console.log(`${key}: ${value}`);
            containerConfigs[key] = value;
        });
        if (compilerSettings.launch.environmentType !== undefined) {
            if ((containerConfigs.environmentType.toUpperCase() === 'SANDBOX') || (containerConfigs.environmentType.toUpperCase() === 'PRODUCTION')) {
                containerConfigs.authentication = undefined;
                containerConfigs.server = undefined;
                containerConfigs.serverInstance = undefined;
            } else {
                if (containerConfigs.authentication === undefined) {
                    containerConfigs.authentication = 'UserPassword';
                }
                containerConfigs.environmentType = compilerSettings.launch.environmentType;
                containerConfigs.environmentName = undefined;
            }
        } else {
            if (containerConfigs.authentication === undefined) {
                containerConfigs.authentication = 'UserPassword';
            }
            containerConfigs.environmentType = compilerSettings.launch.environmentType;
            containerConfigs.environmentName = undefined;
        }
        setLaunchFile(launch, scope?.uri.fsPath);
        return launchBackup;
    }

    async debugOnly() {
        this.getMSBuildService();
        if (this.buildService === undefined) {
            return;
        }
        let launch: WorkspaceConfiguration | undefined | any;
        let compilerSettings = await this.getCompilerSettings();
        console.log(compilerSettings);
        if (compilerSettings === undefined) {
            return;
        }
        launch = this.updateLaunch(compilerSettings);
        this.updateFiles(compilerSettings);
        this.buildService.startDebugging(false, false, true).then((result: any) => {
            if (result) {
                this.restoreData(launch);
            }
        }).catch(() => {
            this.restoreData(launch);
        });
    }
    async openInDesigner() {
        this.getMSBuildService();
        if (this.buildService === undefined) {
            return;
        }
        let launch: WorkspaceConfiguration | undefined | any;
        let openPageDesignerService = this.getMicrosoftAlExtensionService('BuildService');
        if (openPageDesignerService === undefined) {
            window.showErrorMessage('BuildService could not be loaded from AL Language Extension');
        }
        let compilerSettings = await this.getCompilerSettings();
        console.log(compilerSettings);
        if (compilerSettings === undefined) {
            return;
        }
        launch = this.updateLaunch(compilerSettings);
        this.updateFiles(compilerSettings);
        this.openPageDesigner(launch);
        console.log('done');
    }
    getMicrosoftAlExtensionService(serviceName: string) {
        let alLanguageExt = extensions.getExtension('ms-dynamics-smb.al');
        if (alLanguageExt === undefined) {
            return undefined;
        }
        let extensionService = alLanguageExt.exports.services.find((element: any) => (element[serviceName] !== undefined));
        return extensionService[serviceName];
    }
    dispose() {

    }
    openPageDesigner(launch: any) {
        return this.initalizeDebugAdapterService.getDebugAdapterConfiguration().then((c: any) => this.sendOpenPageDesignerRequest(c, launch));
    }
    sendOpenPageDesignerRequest(launchConfig: any, launch: any) {
        const params = this.getAlParams();
        this.buildService.packageContainer()
            .then(() => {
                this.serverProxy.sendRequest(launchConfig, 'al/openPageDesigner', params);
                this.restoreData(launch);
            }).catch(() => {
                this.restoreData(launch);
            });
    }

    getAlParams() {
        const alConfig = workspace.getConfiguration('al');
        const alParams = {
            browserInfo: {
                browser: alConfig.get("browser"),
                incognito: alConfig.get("incognito")
            },
            environmentInfo: {
                env: alConfig.get("env")
            }
        };
        if (alParams.browserInfo.incognito && alParams.browserInfo.browser === "SystemDefault") {
            window.showErrorMessage("The 'al.incognito' setting can only be used if the 'al.browser' setting is set to a value different from 'SystemDefault'");
        }
        return alParams;
    }
    setEVCompilerSettingsList(compilerSettingsList: CompilerSettings[]) {
        writeFileSync(getCompilerFilePath(), JSON.stringify(compilerSettingsList, this.replacer, 4));
    }
}
export function getEVCompilerSettingsList(): CompilerSettings[] {
    let compilerFilePath = getCompilerFilePath();
    if (!existsSync(compilerFilePath)) {
        let compilerSettingsList: CompilerSettings[] = [];
        return compilerSettingsList;
    }
    let data = readFileSync(compilerFilePath);
    let compilerSettingsList: CompilerSettings[] = JSON.parse(data.toString("utf-8"));
    return compilerSettingsList;
}

export function getCompilerFilePath() {
    let appfilePath = getCurrentWorkspaceFolder();
    if (!appfilePath) {
        return constants.compilerFileName;
    }
    return `${appfilePath.uri.fsPath}\\${constants.compilerFileName}`;
}
export function getAllCompilerSettingsInWorkspace(): Array<any> {
    let workspacesCompilerSettings: Array<any> = [];
    workspace.workspaceFolders?.forEach((folder) => {
        let compilerPath = `${folder.uri.fsPath}\\${constants.compilerFileName}`;
        if (existsSync(compilerPath)) {
            let data = readFileSync(compilerPath);
            let compilerSettingsList: CompilerSettings[] = JSON.parse(data.toString("utf-8"));
            workspacesCompilerSettings.push({
                workspaceName: folder.name,
                compilerSettingsPath: compilerPath,
                compilerSettings: compilerSettingsList
            });
        }
    });
    return workspacesCompilerSettings;
}
export function getLaunchFilePath(filePath: string = '') {
    let appfilePath: WorkspaceFolder | undefined = undefined;
    if (filePath === '') {
        appfilePath = getCurrentWorkspaceFolder();
    } else {
        appfilePath = getWorkspaceFolderFromPath(filePath);
    }
    let dotVSCodePath = '.vscode';
    if (appfilePath) {
        dotVSCodePath = `${appfilePath.uri.fsPath}\\.vscode`;
    }
    if (!existsSync(dotVSCodePath)) {
        mkdirSync(dotVSCodePath);
    }
    return `${dotVSCodePath}\\launch.json`;
}
export function getLaunch(filePath: string = ''): WorkspaceConfiguration {
    let data = readFileSync(getLaunchFilePath(filePath));
    let compilerSettingsList: WorkspaceConfiguration = JSON.parse(data.toString("utf-8"));
    return compilerSettingsList;
}
export function setLaunchFile(launch: WorkspaceConfiguration, filePath: string = '') {
    writeFileSync(getLaunchFilePath(filePath), JSON.stringify(launch, undefined, 4));
}
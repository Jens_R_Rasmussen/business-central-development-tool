class EVGoSetup {
    data = undefined;
    updates = [];
    constructor() {
        this.vscode = acquireVsCodeApi();
        var me = this;
        window.addEventListener('message', event => {
            me.onMessage(event.data);
        });

        this.sendMessage({
            command: 'documentLoaded'
        });
    }
    sendMessage(data) {
        this.vscode.postMessage(data);
    }
    onMessage(message) {
        switch (message.command) {
            case 'updateForm':
                this.updateForm(message.data);
                break;
            case 'updateGitProjects':
                this.updateGitProjectValues(message.data);
                break;
            case 'workspaceFilesUpdate':
                this.updateWorkspaceFiles(message.data);
                break;
            case 'addApplication':
                this.data = message.data;
                this.insertSubFolder(message.newApp);
                break;
        }
    }
    addProjectFiles(files) {
        if (files === undefined) {
            return;
        }
        let table = $('#projectInformation');
        table.append(`<tr><td colspan=2><div class="projectFiles" id="projectFiles"></td></tr>`);
        let div = $(`#projectFiles`);
        div.append(`<div><br><span>Project Files:</span><br><br></div>`);
        div.append(`<table class="projectFilesTable" id="projectFilesTable"></table>`);
        let fileTable = $(`#projectFilesTable`);
        fileTable.append('<tr><th>File</th></tr>');
        files.forEach((file) => {
            let path = file.split('?')[0];
            if (path.includes('\\')) {
                path = path.replace('\\', '/');
            }
            let segments = path.split('/');
            fileTable.append(`<tr><td><a href="${file}">${segments.pop()}</a></td></tr>`);
        });
    }
    updateWorkspaceFiles(data) {
        $(`#${data.path}-workspaceFiles`).remove();
        if (data.files.length > 0) {
            let table = $(`#table-workspace-${data.path}`);
            table.after(`<div class="workspaceFiles" id="${data.path}-workspaceFiles">`);
            let div = $(`#${data.path}-workspaceFiles`);
            div.append(`<div><br><span>Workspace Files:</span><br><br></div>`);
            div.append(`<table class="workspaceTable" id="${data.path}-workspaceFilesTable"></table>`);
            let fileTable = $(`#${data.path}-workspaceFilesTable`);
            fileTable.append('<tr><th>Path</th><th>File</th></tr>');
            data.files.forEach((file) => {
                let path = file.path.split('?')[0];
                if (path.includes('\\')) {
                    path = path.replace('\\', '/');
                }
                let segments = path.split('/');
                fileTable.append(`<tr><td>${file.destination}</td><td><a href="${file.path}">${segments.pop()}</a></td></tr>`);
            });
        }
    }
    updateGitProjectValues(data) {
        let gitProject = $('#gitProject');
        gitProject.attr("disabled", false);
        data.forEach((element) => {
            gitProject.append(`<option value="${element.key}" selected>${element.name}</option>`);
        });
    }
    updateForm(data) {
        $(document).on("contextmenu", (e) => {
            return false;
        });
        this.data = data;
        this.updateTextInput('#projectName', data.projectName);
        $('#projectPath').text(data.path);
        $('#projectTarget').text(data.target);
        this.updateCheckboxInput('#createBitBucketRepository', false);
        this.updateTextInput('#gitProject', '');
        this.addProjectFiles(data.files);
        data.subFolders.forEach((subFolder) => {

            this.insertSubFolder(subFolder);
        });
    }
    insertSubFolder(subFolder) {
        this.addTableToApps(subFolder.path);
        this.addTextInputToTable(subFolder.path, 'workspace', 'name', 'Workspace Name');
        this.updateTextInput(`#${subFolder.path}-name`, subFolder.name);
        this.addTextToTable(subFolder.path, 'workspace', 'path', 'Workspace Path');
        this.updateText(`#${subFolder.path}-path`, subFolder.path);
        if (subFolder.testApp !== undefined) {
            this.addTextInputToTable(subFolder.path, 'workspace', 'testApp', 'Test');
            this.updateTextInput(`#${subFolder.path}-testApp`, subFolder.testApp);
        }
        this.addCheckboxInputToTable(subFolder.path, 'workspace', 'includeSourceFolder', 'Include Source Folder');
        this.updateCheckboxInput(`#${subFolder.path}-includeSourceFolder`, subFolder.includeSourceFolder);
        if (subFolder.includeTestDependecies) {
            this.addCheckboxInputToTable(subFolder.path, 'workspace', 'includeTestDependecies', 'Include Test Dependecies');
            this.updateCheckboxInput(`#${subFolder.path}-includeTestDependecies`, subFolder.includeTestDependecies);
        }
        if (subFolder.includeTestInstallCodeunit) {
            this.addCheckboxInputToTable(subFolder.path, 'workspace', 'includeTestInstallCodeunit', 'Include Test Install Codeunit');
            this.updateCheckboxInput(`#${subFolder.path}-includeTestInstallCodeunit`, subFolder.includeTestInstallCodeunit);
        }
        if (subFolder.appType !== undefined) {
            this.addAppTypeInput(subFolder.path, 'appType', 'App Type');
            this.updateAppTypeInput(`#${subFolder.path}-appType`, subFolder.appType);
        }
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-name', 'Name');
        this.updateTextInput(`#${subFolder.path}-appJson-name`, subFolder.appJsonInformation.name);
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-publisher', 'Publisher');
        this.updateTextInput(`#${subFolder.path}-appJson-publisher`, subFolder.appJsonInformation.publisher);
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-version', 'Version');
        this.updateTextInput(`#${subFolder.path}-appJson-version`, subFolder.appJsonInformation.version);
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-brief', 'Brief');
        this.updateTextInput(`#${subFolder.path}-appJson-brief`, subFolder.appJsonInformation.brief);
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-description', 'Description');
        this.updateTextInput(`#${subFolder.path}-appJson-description`, subFolder.appJsonInformation.description);
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-privacyStatement', 'PrivacyStatement');
        this.updateTextInput(`#${subFolder.path}-appJson-privacyStatement`, subFolder.appJsonInformation.privacyStatement);
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-eula', 'Eula');
        this.updateTextInput(`#${subFolder.path}-appJson-eula`, subFolder.appJsonInformation.eula);
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-help', 'Help');
        this.updateTextInput(`#${subFolder.path}-appJson-help`, subFolder.appJsonInformation.help);
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-url', 'Url');
        this.updateTextInput(`#${subFolder.path}-appJson-url`, subFolder.appJsonInformation.url);
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-contextSensitiveHelpUrl', 'ContextSensitiveHelpUrl');
        this.updateTextInput(`#${subFolder.path}-appJson-contextSensitiveHelpUrl`, subFolder.appJsonInformation.contextSensitiveHelpUrl);
        this.addInputElementToTable(subFolder.path, 'manifest', 'appJson-logo', 'file', 'Logo');
        this.updateFileInput(`#${subFolder.path}-appJson-logo`, subFolder.appJsonInformation.logo);
        this.addTextInputToTable(subFolder.path, 'manifest', 'appJson-applicationInsightsKey', 'ApplicationInsightsKey');
        this.updateTextInput(`#${subFolder.path}-appJson-applicationInsightsKey`, subFolder.appJsonInformation.applicationInsightsKey);
        this.addRangeInputElementToTable(subFolder.path, 'Id Range');
        this.updateRangeInput(subFolder.path, subFolder.appJsonInformation.idRange);
        this.addFeaturesInputElementToTable(subFolder.path, 'Features');
        this.updateFeaturesInput(subFolder.path, subFolder.appJsonInformation.features);
        this.addTargetInputElementToTable(subFolder.path, 'appJson-target', 'Target');
        this.updateTargetInput(`#${subFolder.path}-appJson-target`, subFolder.appJsonInformation.target);
        this.workspaceFileUpdate(subFolder);
    }

    workspaceFileUpdate(subFolder) {
        let message = {
            command: 'workspaceFilesUpdate',
            data: subFolder
        };
        this.sendMessage(message);
    }
    addTableToApps(path) {
        let apps = $('#apps');
        let active = '';
        let tabs = $('#apps #tabs');
        if (tabs.length === 0) {
            apps.append('<div id="tabs" class="tabs"></div><br>');
            tabs = $('#apps #tabs');
            active = 'active';
        }
        tabs.append(`<button class="tab ${active}" id="tab-${path}" onclick="showTable('${path}')">${path}</button>`);
        apps.append(`<table id="table-${path}" class="app" style="${active === 'active' ? '' : 'Display:none;'}">`);
        $(`#table-${path}`).append(`<tr>
                                        <td class="appColumn"><center><table class="appTable" id="table-workspace-${path}" ></center>
                                            <tr role="row"><td><span>Workspace</span></td></tr>
                                        </table></td>
                                        <td class="appColumn"><center><table class="appTable" id="table-manifest-${path}" ></center>
                                            <tr role="row"><td><span>App Json</span></td></tr>
                                        </table></td>
                                    </tr>`);
    }
    showTable(path) {
        $('.tab').removeClass('active');
        $(`#tab-${path}`).addClass('active');
        $(`.app`).hide();
        $(`#table-${path}`).show();
    }
    addTextInputToTable(path, area, subId, fieldName) {
        this.addInputElementToTable(path, area, subId, 'text', fieldName);
    }
    addAppTypeInput(path, subId, fieldName) {
        let fieldElement = `<select id="${path}-${subId}">
                                <option value="0" selected>None</option>
                                <option value="1">AppSource</option>
                                <option value="2">PTE</option>
                                <option value="3">Test</option>
                            </select>`;
        this.addElementToTable(path, 'workspace', fieldName, fieldElement);
    }
    addTargetInputElementToTable(path, subId, fieldName) {
        let fieldElement = `<select id="${path}-${subId}">
                                <option value="Cloud" selected>Cloud</option>
                                <option value="OnPrem">OnPrem</option>
                            </select>`;
        this.addElementToTable(path, 'manifest', fieldName, fieldElement);
    }
    addRangeInputElementToTable(path, fieldName) {
        let fieldElement = `<input type="number" id="${path}-appJson-fromId" /><input type="number" id="${path}-appJson-toId" />`;
        this.addElementToTable(path, 'manifest', fieldName, fieldElement);
    }
    addFeaturesInputElementToTable(path, fieldName) {
        let fieldElement = `<input type="checkbox" id="${path}-appJson-features1" name="features1" value="AllTranslationItems" /> <label for="${path}-appJson-features1"> AllTranslationItems</label><br>
                            <input type="checkbox" id="${path}-appJson-features2" name="features2" value="ExcludeGeneratedTranslations" /> <label for="${path}-appJson-features2"> ExcludeGeneratedTranslations</label><br>
                            <input type="checkbox" id="${path}-appJson-features3" name="features3" value="GenerateCaptions" /> <label for="${path}-appJson-features3"> GenerateCaptions</label><br>
                            <input type="checkbox" id="${path}-appJson-features4" name="features4" value="GenerateLockedTranslations" /> <label for="${path}-appJson-features4"> GenerateLockedTranslations</label><br>
                            <input type="checkbox" id="${path}-appJson-features5" name="features5" value="NoImplicitWith" /> <label for="${path}-appJson-features5"> NoImplicitWith</label><br>
                            <input type="checkbox" id="${path}-appJson-features6" name="features6" value="NoPromotedActionProperties" /> <label for="${path}-appJson-features6"> NoPromotedActionProperties</label><br>
                            <input type="checkbox" id="${path}-appJson-features7" name="features7" value="TranslationFile" /> <label for="${path}-appJson-features7"> TranslationFile</label><br>
                            <input type="checkbox" id="${path}-appJson-features8" name="features8" value="UseLegacyAnalyzerStrategy" /> <label for="${path}-appJson-features8"> UseLegacyAnalyzerStrategy</label><br>`;
        this.addElementToTable(path, 'manifest', fieldName, fieldElement);
    }
    addTextToTable(path, area, subId, fieldName) {
        let fieldElement = `<div id="${path}-${subId}" />`;
        this.addElementToTable(path, area, fieldName, fieldElement);
    }

    addCheckboxInputToTable(path, area, subId, fieldName) {
        this.addInputElementToTable(path, area, subId, 'checkbox', fieldName);
    }
    addInputElementToTable(path, area, subId, type, fieldName) {
        let fieldElement = `<input type="${type}" id="${path}-${subId}" />`;
        this.addElementToTable(path, area, fieldName, fieldElement);
    }
    addElementToTable(path, area, fieldName, fieldElement) {
        let element = `<tr role="row">
                        <td>${fieldName}:</td>
                        <td>${fieldElement}</td>
                       </tr>`;
        $(`#table-${area}-${path}`).append(element);
    }
    updateRangeInput(path, idRange) {
        if (idRange) {
            this.updateTextInput(`#${path}-appJson-fromId`, idRange[0]);
            this.updateTextInput(`#${path}-appJson-toId`, idRange[1]);
        }
    }
    updateFeaturesInput(path, features) {
        let cf = [`#${path}-appJson-features1`,
        `#${path}-appJson-features2`,
        `#${path}-appJson-features3`,
        `#${path}-appJson-features4`,
        `#${path}-appJson-features5`,
        `#${path}-appJson-features6`,
        `#${path}-appJson-features7`,
        `#${path}-appJson-features8`
        ];
        cf.forEach((element) => {
            $(element).on('change', {}, (event) => {
                this.updateData(event.target);
            });
        });

        features.forEach((feature) => {
            switch (feature) {
                case 'AllTranslationItems':
                    $(`#${path}-appJson-features1`).prop("checked", true);
                    break;
                case 'ExcludeGeneratedTranslations':
                    $(`#${path}-appJson-features2`).prop("checked", true);
                    break;
                case 'GenerateCaptions':
                    $(`#${path}-appJson-features3`).prop("checked", true);
                    break;
                case 'GenerateLockedTranslations':
                    $(`#${path}-appJson-features4`).prop("checked", true);
                    break;
                case 'NoImplicitWith':
                    $(`#${path}-appJson-features5`).prop("checked", true);
                    break;
                case 'NoPromotedActionProperties':
                    $(`#${path}-appJson-features6`).prop("checked", true);
                    break;
                case 'TranslationFile':
                    $(`#${path}-appJson-features7`).prop("checked", true);
                    break;
                case 'UseLegacyAnalyzerStrategy':
                    $(`#${path}-appJson-features8`).prop("checked", true);
                    break;
            }
        });
    }
    updateTextInput(element, value) {
        $(element).val(value ? value : '').on('change', {}, (event) => {
            this.updateData(event.target);
        });
    }
    updateAppTypeInput(element, value) {
        $(element).val(value ? value : 0).on('change', {}, (event) => {
            this.updateData(event.target);
        });
    }
    updateTargetInput(element, value) {
        $(element).val(value ? value : 'Cloud').on('change', {}, (event) => {
            this.updateData(event.target);
        });
    }
    updateFileInput(element, value) {
        if ((value !== '') && (value !== undefined)) {
            let e = element.substring(1);
            let fileInput = document.getElementById(e);
            let logoFile = new File(['Logo'], value, {
                lastModified: new Date(),
            });
            let dataTransfer = new DataTransfer();
            dataTransfer.items.add(logoFile);
            if (fileInput !== null) {
                fileInput.files = dataTransfer.files;
            }
        }
        $(element).on('change', {}, (event) => {
            this.updateData(event.target);
        });
    }
    updateCheckboxInput(element, value) {
        $(element).prop('checked', value).on('change', {}, (event) => {
            this.updateData(event.target);
        });
    }
    updateText(element, value) {
        $(element).text(value);
    }
    createRepository() {
        let message = {
            command: 'create',
            data: this.data
        };
        this.sendMessage(message);
    }
    addApplication() {
        let message = {
            command: 'addApplication',
            data: this.data
        };
        this.sendMessage(message);
    }
    updateData(element) {
        switch (element.id) {
            case 'projectName':
                this.data.projectName = element.value.trim();
                this.data.mainfolder = this.data.projectName.replace(/\\|\/|:|\*|\?|"|<|>|\|/gm, '');
                this.data.path = this.updatePath();
                $('#projectPath').text(this.data.path);
                $('#app-appJson-name').val(this.data.projectName).trigger('change');
                $('#test-appJson-name').val(`${this.data.projectName} Test`).trigger('change');
                break;
            case 'createBitBucketRepository':
                this.data.createBitBucketRepository = element.checked;
                if (element.checked) {
                    this.updateGitProjects();
                }
                break;
            case 'gitProject':
                this.data.gitProject = element.value;
                break;
            default:
                this.updateSubFolder(element);
                break;
        }
    }
    updateSubFolder(element) {
        let index = this.data.subFolders.findIndex((folder) => (folder.path === element.id.split('-')[0]));
        switch (true) {
            case element.id.endsWith('-name'):
                this.data.subFolders[index].name = element.value;
                break;
            case element.id.endsWith('-testApp'):
                this.data.subFolders[index].testApp = element.value;
                break;
            case element.id.endsWith('-includeSourceFolder'):
                this.data.subFolders[index].includeSourceFolder = element.checked;
                break;
            case element.id.endsWith('-includeTestDependecies'):
                this.data.subFolders[index].includeTestDependecies = element.checked;
                break;
            case element.id.endsWith('-includeTestInstallCodeunit'):
                this.data.subFolders[index].includeTestInstallCodeunit = element.checked;
                break;
            case element.id.endsWith('-appType'):
                this.data.subFolders[index].appType = Number.parseInt(element.value);
                this.workspaceFileUpdate(this.data.subFolders[index]);
                break;
            case element.id.endsWith('-appJson-name'):
                this.data.subFolders[index].appJsonInformation.name = element.value;
                break;
            case element.id.endsWith('-appJson-publisher'):
                this.data.subFolders[index].appJsonInformation.publisher = element.value;
                break;
            case element.id.endsWith('-appJson-version'):
                this.data.subFolders[index].appJsonInformation.version = element.value;
                break;
            case element.id.endsWith('-appJson-brief'):
                this.data.subFolders[index].appJsonInformation.brief = element.value;
                break;
            case element.id.endsWith('-appJson-description'):
                this.data.subFolders[index].appJsonInformation.description = element.value;
                break;
            case element.id.endsWith('-appJson-privacyStatement'):
                this.data.subFolders[index].appJsonInformation.privacyStatement = element.value;
                break;
            case element.id.endsWith('-appJson-eula'):
                this.data.subFolders[index].appJsonInformation.eula = element.value;
                break;
            case element.id.endsWith('-appJson-help'):
                this.data.subFolders[index].appJsonInformation.help = element.value;
                break;
            case element.id.endsWith('-appJson-url'):
                this.data.subFolders[index].appJsonInformation.url = element.value;
                break;
            case element.id.endsWith('-appJson-contextSensitiveHelpUrl'):
                this.data.subFolders[index].appJsonInformation.contextSensitiveHelpUrl = element.value;
                break;
            case element.id.endsWith('-appJson-logo'):
                this.data.subFolders[index].appJsonInformation.logo = element.files[0].path;
                break;
            case element.id.endsWith('-appJson-applicationInsightsKey'):
                this.data.subFolders[index].appJsonInformation.applicationInsightsKey = element.value;
                break;
            case element.id.endsWith('-appJson-target'):
                this.data.subFolders[index].appJsonInformation.target = element.value;
                break;
            case element.id.endsWith('-appJson-fromId'):
                this.data.subFolders[index].appJsonInformation.idRange[0] = element.value;
                break;
            case element.id.endsWith('-appJson-toId'):
                this.data.subFolders[index].appJsonInformation.idRange[1] = Number.parseInt(element.value);
                break;
            case element.id.includes('-appJson-features'):
                if (this.data.subFolders[index].appJsonInformation.features.includes(element.value)) {
                    let featuresIndex = this.data.subFolders[index].appJsonInformation.features.indexOf(element.value);
                    delete this.data.subFolders[index].appJsonInformation.features[featuresIndex];
                } else {
                    this.data.subFolders[index].appJsonInformation.features.push(element.value);
                }
                break;
            default:
                break;
        }

    }
    updatePath() {
        let elements = this.data.path.split('\\');
        elements[elements.length - 1] = this.data.mainfolder;
        return elements.join('\\');
    }

    updateGitProjects() {
        let message = {
            command: 'updateGitProjects'
        };
        this.sendMessage(message);
    }

}

const evGoSetup = new EVGoSetup();

function createRepository() {
    evGoSetup.createRepository();
}
function showTable(path) {
    evGoSetup.showTable(path);
}
function addApplication() {
    evGoSetup.addApplication();
}
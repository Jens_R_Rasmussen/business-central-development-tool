import * as path from "path";
import { WebViewControler } from "./webViewControler";
import { ExtensionContext, window, workspace } from "vscode";
import * as constants from "../constants";
import { DefaultFiles, WorkspaceCreationData, WorkspaceCreator, getDefaultAppFiles } from "../EV.Go/workspaceCreator";
import { BitBucketAuthorization } from "../bitbucket/bitBucketAuthorization";
import { BitBucketRepository } from "../bitbucket/bitBucketRepository";

export class EVGoSetup extends WebViewControler {
    data: WorkspaceCreationData;
    context: ExtensionContext;
    bitBucketAuthorization: BitBucketAuthorization;
    projectsIsUpdated: boolean = false;
    workspaceCreator: WorkspaceCreator;
    bitBucketRepository: BitBucketRepository;
    constructor(context: ExtensionContext, bitBucketAuthorization: BitBucketAuthorization, title: string, data: WorkspaceCreationData) {
        super(context, title);
        this.data = data;
        this.context = context;
        this.bitBucketAuthorization = bitBucketAuthorization;
        this.workspaceCreator = new WorkspaceCreator(this.context, this.bitBucketAuthorization);
        this.bitBucketRepository = new BitBucketRepository(this.bitBucketAuthorization);
    }

    getViewType() {
        return constants.evGoSetupPanelTitle;
    }
    getHtmlContentFile() {
        return path.join('out', 'webViews', 'evGoSetup', 'index.htm');
    }
    onPanelClosed() {
        this.workspaceCreator.dispose();
        this.dispose();
    }
    onDocumentLoaded(): void {
        this.sendMessage({ command: 'updateForm', data: this.data });
    }
    async processWebViewMessage(message: any) {
        if (message) {
            switch (message.command) {
                case 'documentLoaded':
                    this.documentLoaded = true;
                    this.onDocumentLoaded();
                    break;
                case 'create':
                    console.log(message.data);
                    this.workspaceCreator.create(message.data);
                    this.dispose();
                    break;
                case 'updateGitProjects':
                    if (this.projectsIsUpdated) {
                        break;
                    }
                    this.projectsIsUpdated = true;
                    let projects = await this.bitBucketRepository.getProjects();
                    this.sendMessage({ command: 'updateGitProjects', data: projects });
                    break;
                case 'workspaceFilesUpdate':
                    let files: DefaultFiles[] = getDefaultAppFiles(message.data.appType);
                    this.sendMessage({ command: 'workspaceFilesUpdate', data: { path: message.data.path, files: files } });
                    break;
                case 'addApplication':
                    let newApp = await this.workspaceCreator.initNewAppDialog(message.data);
                    if (newApp === undefined) {
                        return;
                    }
                    message.data.subFolders.push(newApp);
                    this.sendMessage({ command: 'addApplication', data: message.data, newApp: newApp });
                    break;
            }
        }
    }
}
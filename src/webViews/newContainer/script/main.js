class NewContainer {
    updateRepository = false;
    repositoryPath = '';
    constructor() {
        this.vscode = acquireVsCodeApi();
        var me = this;
        window.addEventListener('message', event => {
            me.onMessage(event.data);
        });

        this.sendMessage({
            command: 'documentLoaded'
        });
    }
    sendMessage(data) {
        this.vscode.postMessage(data);
    }
    onMessage(message) {
        console.log(message);
        switch (message.command) {
            case 'licensefile':
                $('#licensefile').val(message.path);
                break;
            case 'sqlbackupfile':
                $('#sqlbackupfile').val(message.path);
                break;
            case 'updatedefaultvalues':
                $('title').html(message.title);
                $('.Caption b span').html(message.title);
                if (message.country !== undefined) {
                    $("#country").val(message.country);
                }
                if (message.isolation !== undefined) {
                    $("#isolation").val(message.isolation);
                }
                if (message.createshortcut !== undefined) {
                    $("#createshortcut").val(message.createshortcut);
                }
                break;
            case 'newenvironment':
                //$('.Caption b span').html('New Development Environment');
                $('#name').removeAttr("autofocus");
                $('.newdevelopment').show();
                $('.newdevelopment input').attr("required", "required").attr("autofocus", "autofocus").focus();
                break;
            case 'updateContainerParameter':
                this.updateContainerParameter(message);
                break;
            case 'errorMessage':
                this.showErrorMessage(message);
                break;
        }
    }
    showErrorMessage(message) {
        $('#errorMessage').html(` ${message.errorMessage}`);
        $('#error').show();
        $('#InputForm').hide();
        this.sendMessage({ command: "dispose", timeOut: 5000 });
    }

    updateContainerParameter(message) {
        this.updateRepository = (message.repositoryPath !== '');
        this.repositoryPath = message.repositoryPath;
        let containerParameter = message.parameter;
        if (containerParameter.repository !== undefined) {
            if (containerParameter.repository !== '') {
                $('#name').removeAttr("autofocus");
                $('.newdevelopment').show();
                $('.newdevelopment input').attr("required", "required").attr("autofocus", "autofocus").focus();
                this.setvalue('#gitrepository', containerParameter.repository);
            }
        }
        if (containerParameter.updateDependencies !== undefined) {
            $('.updatedependencies').show();
            this.setvalue('#updatedependencies', containerParameter.updateDependencies);
        }
        this.setvalue('#name', containerParameter.name);
        this.setvalue('#type', containerParameter.type);
        this.setvalue('#version', containerParameter.version);
        this.setvalue('#country', containerParameter.country);
        // containerurl: $('#containerurl').val(),
        // tag: $('#tag').val(),
        this.setvalue('#licensefile', containerParameter.licensefile);
        this.setvalue('#isolation', containerParameter.isolation);
        this.setvalue('#authentication', containerParameter.authentication);
        this.setvalue('#username', containerParameter.username);
        this.setvalue('#password', containerParameter.password);
        this.setvalue('#createshortcut', containerParameter.createshortcut);
        this.setvalue('#ismultitenant', containerParameter.ismultitenant);
        this.setvalue('#customparameter', containerParameter.customparameter);
        // includetesttoolkit: $('#includetesttoolkit').is(":checked"),
        // usessl: $('#usessl').is(":checked"),
        this.setvalue('#includetesttoolkit', containerParameter.includetesttoolkit);
        this.setvalue('#testlibrariesonly', containerParameter.testlibrariesonly);
        this.setvalue('#usessl', containerParameter.usessl);
        this.setvalue('#webclientport', containerParameter.webclientport);
        this.setvalue('#publishedports', containerParameter.publishedports);
        this.setvalue('#sqlbackupfile', containerParameter.sqlbackupfile);
        // useexternaldatabase: $('#useexternaldatabase').is(":checked"),
        this.setvalue('#useexternaldatabase', containerParameter.useexternaldatabase);
        this.setvalue('#databaseserver', containerParameter.databaseserver);
        this.setvalue('#databaseinstance', containerParameter.databaseinstance);
        this.setvalue('#databasename', containerParameter.databasename);
        this.setvalue('#sqlusername', containerParameter.sqlusername);
        this.setvalue('#sqlpassword', containerParameter.sqlpassword);
    }
    setvalue(name, value) {
        if (value !== undefined) {
            if (typeof value === "boolean") {
                console.log(name, value);
                $(name).prop("checked", value);
            }
            else {
                $(name).val(value);
            }
        }
    }
    updateLayout(value) {
        if (value === false) {
            $('.advanced').hide();
        } else {
            $('.advanced').show();
            this.updateLayoutFromUseExternalDatabase($('#useexternaldatabase:checked').val() === 'on');
        }
        this.updateLayoutTestLibrariesOnly($('#includetesttoolkit:checked').val() === 'on');
    }
    updateLayoutTestLibrariesOnly(value) {
        if ((value === false) || ($('#advancedoptions:checked').val() !== 'on')) {
            $('.testtoolkit').hide();
        } else {
            $('.testtoolkit').show();
        }
    }
    updateLayoutFromType(value) {
        if (value === 'Custom') {
            $('.standard').hide();
            $('.custom').show();
            $('.standard input').removeAttr("required");
            $('.custom input').attr("required", "required");
        } else {
            $('.standard').show();
            $('.custom').hide();
            $('.standard input').attr("required", "required");;
            $('.custom input').removeAttr("required");
        }
        if (value === 'Insider') {
            $('#versionRow').hide();
            $('#versionRow input').removeAttr("required");
        }
    }
    updateLayoutFromAuthentication(value) {
        if (value === 'UserPassword') {
            $('.username, .password').show();
        } else {
            $('.username, .password').hide();
        }
    }
    updateLayoutFromUseExternalDatabase(value) {
        if (value === true) {
            $('.externaldatabase').show();
            $('.externaldatabase input').attr("required", "required");
            $('#databaseinstance').removeAttr("required");
        } else {
            $('.externaldatabase').hide();
            $('.externaldatabase input').removeAttr("required");
        }
    }
    createContainer() {
        let command = {
            command: 'createContainer',
            container: {
                name: $('#name').val(),
                type: $('#type').val(),
                version: $('#version').val(),
                country: $('#country').val(),
                containerurl: $('#containerurl').val(),
                tag: $('#tag').val(),
                licensefile: $('#licensefile').val(),
                isolation: $('#isolation').val(),
                authentication: $('#authentication').val(),
                username: $('#username').val(),
                password: $('#password').val(),
                createshortcut: $('#createshortcut').val(),
                ismultitenant: $('#ismultitenant').is(":checked"),
                customparameter: $('#customparameter').val(),
                includetesttoolkit: $('#includetesttoolkit').is(":checked"),
                testlibrariesonly: $('#testlibrariesonly').is(":checked"),
                usessl: $('#usessl').is(":checked"),
                webclientport: $('#webclientport').val(),
                publishedports: $('#publishedports').val(),
                sqlbackupfile: $('#sqlbackupfile').val(),
                useexternaldatabase: $('#useexternaldatabase').is(":checked"),
                databaseserver: $('#databaseserver').val(),
                databaseinstance: $('#databaseinstance').val(),
                databasename: $('#databasename').val(),
                sqlusername: $('#sqlusername').val(),
                sqlpassword: $('#sqlpassword').val(),
                repository: $('#gitrepository').val(),
                updateDependencies: $('#updatedependencies').is(":checked")
            },
            updateRepository: this.updateRepository,
            repositoryPath: this.repositoryPath
        };
        console.log(command);
        this.sendMessage(command);
    }

    selectLicenseFile() {
        let command = {
            command: 'selectlicensefile'
        };
        console.log(command);
        this.sendMessage(command);
    }
    selectSQLBackupFile() {
        let command = {
            command: 'selectsqlbackupfile'
        };
        console.log(command);
        this.sendMessage(command);
    }
    getLicenseFromVersion(versionElement) {
        let currValue = versionElement.value;
        currValue = currValue.replace(',', '.');
        versionElement.value = currValue;
        let licensePath = $('#licensefile').val();
        if (licensePath === '') {
            let command = {
                command: 'getLicensePath',
                version: currValue
            };
            console.log(command);
            this.sendMessage(command);
        }
    }
}
const newContainer = new NewContainer;
$(function () {
    $(document).on('keydown', function (e) {
        if (e === 27) {
            close();
        }
    });
    $('#canclebutton').on('click', () => {
        close();
    });
    $('#okbutton').on('click', () => {
        let notvalid = $('#InputForm input:invalid');
        if (notvalid.length === 0) {
            newContainer.createContainer();
        }
    });
    $('.advanced').hide();

    $('#version').on('focusout', (e) => {
        newContainer.getLicenseFromVersion(e.currentTarget);
    });

    function close() {
        newContainer.sendMessage({ command: "dispose" });
    }
});
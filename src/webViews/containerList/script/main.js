class ContainerList {
    constructor() {
        this.vscode = acquireVsCodeApi();
        var me = this;
        window.addEventListener('message', event => {
            me.onMessage(event.data);
        });

        this.sendMessage({
            command: 'documentLoaded'
        });
    }
    sendMessage(data) {
        this.vscode.postMessage(data);
    }

    onPostLoadingMessage(command, containerName) {
        let buttonText = 'Process';
        switch (command) {
            case 'start':
                buttonText = 'Starting';
                break;
            case 'stop':
                buttonText = 'Stopping';
                break;
            case 'delete':
                buttonText = 'Deleting';
                break;
        }
        $("#button-" + containerName).addClass('loading').text(buttonText);
        $('#dropdown-content-' + containerName).remove();
        this.sendMessage({
            command: command,
            status: $('#Status-' + containerName).text(),
            containerName: containerName
        });
    }

    updateDevLicense(containerName) {
        this.sendMessage({
            command: 'updateDevLicense',
            status: $('#Status-' + containerName).text(),
            version: $(`#ContainerRow-${containerName} #Version`).text(),
            containerName: containerName
        });
    }

    onPostMessage(command, containerName) {
        this.sendMessage({
            command: command,
            status: $('#Status-' + containerName).text(),
            containerName: containerName
        });
    }
    onMessage(message) {
        switch (message.command) {
            case 'updateSetting':
                $('#settingsCheckBox').prop('checked', message.checked);
                break;
            case 'updateStatus':
                $("#button-" + message.data).removeClass('loading').text('Select');
                $('#dropdown-content-' + message.data).remove();
                $('#dropdown-' + message.data).append(message.dropdown);
                let version = $('#Status-' + message.data)[0];
                if (version.innerHTML === 'Running') {
                    version.innerHTML = 'Not Running';
                } else {
                    version.innerHTML = 'Running';
                }
                break;
            case 'delete':
                $("#ContainerRow-" + message.data).remove();
                break;
            case 'dontDelete':
                $("#button-" + message.data).removeClass('loading').text('Select');
                $('#dropdown-content-' + message.data).remove();
                $('#dropdown-' + message.data).append(message.dropdown);
                break;
            case 'tableBody':
                $('#loading-containes').hide();
                $('#tableBody').empty();
                $('#tableBody').append(message.data);
                $('#table').show();
                break;
            case 'showLoadingApps':
                $('#apps').show();
                let backgroundColor = $('body').css('background-color');
                $('.apps-header').css('background-color', backgroundColor);
                $('.apps-body').css('background-color', backgroundColor);
                $('.apps-footer').css('background-color', backgroundColor);
                $('#loading-apps').show();
                $('#installNewAppTable').hide();
                $('#app-table').hide();
                $('#appcontainerheader')[0].innerHTML = `Extension Management on '${message.containerName}'`;
                this.sendMessage({ command: 'readapplist', containerName: message.containerName });
                break;
            case 'appTableBody':
                $('#installNewApp').on('click', () => {
                    this.sendMessage({ command: 'installNewApp', containerName: message.containerName });
                });
                $('#installNewAppForceSync').on('click', () => {
                    this.sendMessage({ command: 'installNewApp', containerName: message.containerName, forceSync: true });
                });
                $('#loading-apps').hide();
                $('#app-tableBody').empty();
                $('#app-tableBody').append(message.data);
                $('#app-table').show();
                $('#installNewAppTable').show();
                break;
            case 'appTableBodyError':
                $('#installNewApp').on('click', () => {
                    this.sendMessage({ command: 'installNewApp', containerName: message.containerName });
                });
                $('#installNewAppForceSync').on('click', () => {
                    this.sendMessage({ command: 'installNewApp', containerName: message.containerName, forceSync: true });
                });
                $('#loading-apps').hide();
                $('#installNewAppTable').show();
                $('#loading-error').show();
                break;
            case 'notInstalled':
                $('#appRow-' + message.data).find('#appbutton').show();
                $('#appRow-' + message.data).find('.dropdown-content').css("display", "");
                $('#appRow-' + message.data).find('#loadingbutton').hide();
                $('#appRow-' + message.data).find('#Installed').text('false');
                $('#appRow-' + message.data).find('#install').show();
                $('#appRow-' + message.data).find('#uninstall').hide();
                break;
            case 'installed':
                $('#appRow-' + message.data).find('#appbutton').show();
                $('#appRow-' + message.data).find('.dropdown-content').css("display", "");
                $('#appRow-' + message.data).find('#loadingbutton').hide();
                $('#appRow-' + message.data).find('#Installed').text('true');
                $('#appRow-' + message.data).find('#install').hide();
                $('#appRow-' + message.data).find('#uninstall').show();
                break;
            case 'unpublished':
                $('#appRow-' + message.data).hide();
                break;
            case 'processapp':
                $('#appRow-' + message.data).find('#appbutton').hide();
                $('#appRow-' + message.data).find('.dropdown-content').hide();
                $('#appRow-' + message.data).find('#loadingbutton').show();
                break;
        }
    }
}
const containerList = new ContainerList();

$(function () {
    window.onclick = function (event) {
        if (event.target === apps) {
            hideApps();
        }
    };
    $(document).on('keydown', function (e) {
        if (e === 27) {
            hideApps();
        }
    });
    $('.apps-close').on('click', () => {
        hideApps();
    });
    $('#settingsCheckBox').on('click', () => {
        containerList.sendMessage({ command: 'updatesetting', checked: $('#settingsCheckBox').prop('checked') });
    });

    function hideApps() {
        $('#apps').hide();
        $('#installNewApp').off('click');
        $('#installNewAppForceSync').off('click');
        $('#loading-error').hide();
    }
});
class CompilerSettings {
    activeMenuItems = [];
    data = [];
    currentTab;
    constructor() {
        this.vscode = acquireVsCodeApi();
        var me = this;
        window.addEventListener('message', event => {
            me.onMessage(event.data);
        });

        this.sendMessage({
            command: 'documentLoaded'
        });
    }
    sendMessage(data) {
        this.vscode.postMessage(data);
    }
    onMessage(message) {
        switch (message.command) {
            case 'initCompilerSettings':
                this.init(message.data);
                break;
            case 'updateCompilerSettings':
                this.update(message.data);
                break;
        }
    }

    init(data) {
        this.data = data;
        data.forEach((element) => {
            {
                this.addCompilerTabs(element);
            }
        });
        $('.tab').first().addClass('active');
        $('.menu').first().css('display', 'block');
        $('.menuitem').first().addClass('active');
        $('.menucontent').first().addClass('active');
    }

    update(data) {
        this.data = data;
        data.forEach((element) => {
            {
                this.addCompilerTabs(element);
            }
        });
        let tabName = this.currentTab;
        $(`#${tabName}`).show();
        let menuItem = this.activeMenuItems.find((menuItem) => menuItem.tab === tabName);
        if ($(`#${menuItem.menuItem}`).length === 0) {
            $(`#${tabName} button:first-child`).addClass('active');
            $(`.${tabName}`).first().addClass('active');
            return;
        }
        $(`#${menuItem.menuItem}`).addClass('active');
        if (menuItem.content !== undefined) {
            $(".menucontent").removeClass('active');
            $(`#${menuItem.content}`).addClass('active');
        } else {
            $(`.${tabName}`).first().addClass('active');
        }
    }

    addCompilerTabs(data) {
        $('#compilerSettingsView').innerHTML = '';
        let menuId = data.workspaceName.replace(/[^a-zA-Z0-9]/g, '');
        if (!this.currentTab) {
            this.currentTab = menuId;
        }
        let tab = this.activeMenuItems.find((i) => i.tab === menuId);
        if (!tab) {
            this.activeMenuItems.push({
                tab: menuId,
                path: data.compilerSettingsPath,
                menuItem: `${menuId}-menuItem0`
            });
        }
        this.addCompilerTab(menuId, data.workspaceName);
        if ($('#menu').length === 0) {
            $('#compilerSettingsView').append(`<div id="menu"></div>`);
        }
        $(`#${menuId}`).remove();
        $('#menu').append(`<div class="menu" id="${menuId}" style="display:none"></div>`);
        data.compilerSettings.sort((a, b) => {
            let nameA = a.name.toUpperCase();
            let nameB = b.name.toUpperCase();
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            return 0;
        }).forEach(element => {
            let content = JSON.stringify(element, undefined, 4);
            let lines = content.split('\n');
            let contentId = element.name.replace(/[^a-zA-Z0-9]/g, '');
            $(`#${menuId}`).append(`<button id="${menuId}-menuItem-${contentId}" class="menuitem" onclick="showSettings(event, '${menuId}-${contentId}')" >${element.name}</button>`);
            $('#compilerSettingsView').append(`<textarea readonly rows=${lines.length + 3} id="${menuId}-${contentId}" class="menucontent ${menuId}">${content}</textarea>`);
        });
    }
    addCompilerTab(menuId, workspaceFolder) {
        if ($('#tabs').length === 0) {
            $('#compilerSettingsView').append(`<div id="tabs" class="tabs"></div>`);
        }
        if ($(`#tab-${menuId}`).length === 0) {
            $('#tabs').append(`<button class="tab" id="tab-${menuId}" onclick="showTab(event, '${menuId}')">${workspaceFolder}</button>`);
        }
    }
    showSettings(evt, settingsName) {
        $(".menucontent").removeClass('active');
        $(".menuitem").removeClass('active');
        $(`#${settingsName}`).addClass('active');
        let menuItemId = evt.currentTarget.id;
        let tabName = menuItemId.split('-')[0];
        let menuItem = this.activeMenuItems.find((menuItem) => menuItem.tab === tabName);
        menuItem.menuItem = menuItemId;
        menuItem.content = settingsName;
        evt.currentTarget.className += " active";
    }
    showTab(evt, tabName) {
        $('.menu').hide();
        $('.menucontent').removeClass('active');
        $('.tab').removeClass('active');
        this.currentTab = tabName;
        $(`#${tabName}`).show();
        evt.currentTarget.className += " active";
        let menuItem = this.activeMenuItems.find((menuItem) => menuItem.tab === tabName);
        if ($(`#${menuItem.menuItem}`).length !== 0) {
            $(`#${menuItem.menuItem}`).addClass('active');
        }
        else {
            $(`#${tabName}`).find('button:first').addClass('active');
        }
        if (menuItem.content !== undefined) {
            $(`#${menuItem.content}`).addClass('active');
        } else {
            $(`.${tabName}`).first().addClass('active');
        }
    }
    hideContextMenu() {
        $('#contextMenu').css('display', 'none');;
    }
    rightClickContent(evt) {
        if (evt.target.classList.contains('menucontent') !== true) {
            evt.preventDefault();
            hideContextMenu();
            return;
        }
        evt.preventDefault();
        if ($('#contextMenu').css('display') === 'block') {
            hideContextMenu();
        }
        $('#contextMenu').css('display', 'block');
        $('#contextMenu').css('left', `${evt.pageX}px`);
        $('#contextMenu').css('top', `${evt.pageY}px`);
    }
    executeContextMenu(type) {
        $('.active').each((index, element) => {
            if (element.classList.contains('menucontent') === true) {
                let activeMenuItem = this.activeMenuItems.find((item) => item.tab === element.id.split('-')[0]);
                let message = {
                    command: type.toLowerCase(),
                    data: {
                        path: activeMenuItem.path,
                        item: JSON.parse(element.value),
                        allData: this.data
                    }
                };
                this.sendMessage(message);
            }
        });
    }
}

const compilerSettings = new CompilerSettings();

function showTab(evt, tabName) {
    compilerSettings.showTab(evt, tabName);
}

function showSettings(evt, settingsName) {
    compilerSettings.showSettings(evt, settingsName);
}

function hideContextMenu() {
    compilerSettings.hideContextMenu();
}
function rightClickContent(evt) {
    compilerSettings.rightClickContent(evt);
}
function executeContextMenu(type) {
    compilerSettings.executeContextMenu(type);
}
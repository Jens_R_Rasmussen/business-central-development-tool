import * as vscode from 'vscode';
import { readFileSync } from 'fs';
import { EventEmitter } from 'events';
export class WebViewControler extends EventEmitter {
    disposables: any[];
    documentLoaded: boolean;
    title: string;
    extensionContext: vscode.ExtensionContext;
    extensionPath: string;
    panel: vscode.WebviewPanel | undefined;
    viewColumn: vscode.ViewColumn;
    html: string | undefined;

    constructor(context: vscode.ExtensionContext, title: string) {
        super();
        this.disposables = [];
        this.documentLoaded = false;
        this.title = title;
        this.extensionContext = context;
        this.extensionPath = context.extensionPath;
        this.panel = undefined;
        this.viewColumn = vscode.ViewColumn.Active;
        this.html = undefined;
    }
    dispose() {
        if (this.panel) {
            this.panel.dispose();
            this.panel = undefined;
        }
        while (this.disposables.length) {
            const item = this.disposables.pop();
            if (item) {
                item.dispose();
            }
        }
        this.emit('dispose');
    }
    show() {
        this.createWebView();
    }
    reveal() {
        if (this.panel) {
            this.panel.reveal();
        }
    }
    getViewType() {
        return "WebViewControler";
    }
    getHtmlContentFile() {
        return "";
    }
    loadHtmlContent() {
        let filePath = this.extensionContext.asAbsolutePath(this.getHtmlContentFile());
        let content = readFileSync(filePath, 'utf8');
        if (this.panel !== undefined) {
            let resourceUri = this.panel.webview.asWebviewUri(vscode.Uri.file(this.extensionPath));
            let t = resourceUri.toString();
            let tt = resourceUri.path;
            let reg = new RegExp('vscode-scheme:', 'g');
            let newContent = content.replace(reg, t);

            return newContent;
            //return content.replace(new RegExp('vscode-scheme:', 'g'), resourceUri. toString());
        }
        return content;
    }

    createWebView() {
        this.panel = vscode.window.createWebviewPanel(this.getViewType(), this.title, this.viewColumn, {
            enableScripts: true,
            retainContextWhenHidden: true
        });
        this.reloadWebViewContent();
        this.panel.onDidDispose(() => this.dispose(), null, this.disposables);
        this.panel.onDidChangeViewState(e => {
            if ((this.panel) && (this.panel.visible)) {
                this.reloadWebViewContent();
            }
        }, null, this.disposables);
        this.panel.webview.onDidReceiveMessage(message => {
            this.processWebViewMessage(message);
        }, null, this.disposables);
        this.panel.onDidDispose(e => {
            this.onPanelClosed();
        });
    }
    onPanelClosed() { }
    reloadWebViewContent() {
        this.documentLoaded = false;
        if (this.panel) {
            if (!this.html) {
                this.html = this.loadHtmlContent();
            }
            this.panel.webview.html = this.html;
        }
    }
    sendMessage(message: any) {
        if (this.panel) {
            this.panel.webview.postMessage(message);
        }
    }
    processWebViewMessage(message: any) {
        if (message) {
            switch (message.command) {
                case 'documentLoaded':
                    this.documentLoaded = true;
                    this.onDocumentLoaded();
                    break;
                case 'showInformation':
                    vscode.window.showInformationMessage(message.message);
                    break;
                case 'showError':
                    vscode.window.showErrorMessage(message.message);
                    break;
            }
        }
    }
    onDocumentLoaded() { }
    close() {
        if (this.panel) {
            this.panel.dispose();
        }
    }
}
import { WebViewControler } from './webViewControler';
import { window, workspace, ExtensionContext } from 'vscode';
import { join } from 'path';
import { Terminal } from '../executeLibrary';
import * as constants from '../constants';
import { ContainerParameter, DockerContainer, updateServerConfig, saveContainerSettings } from '../dockerLibrary';
import { getLicensePath } from '../getLicenseFile';

export class NewContainer extends WebViewControler {
    private dockerContainer: DockerContainer;
    private loadDefaultValues = true;
    constructor(context: ExtensionContext, title: string) {
        super(context, title);
        this.dockerContainer = new DockerContainer(context);
    }
    show(newEnvironment: boolean = false) {
        super.show();
        this.updateNewEnvironmentLayout(newEnvironment);
    }
    showDevelopmentContainer(containerParameter: ContainerParameter, path: string) {
        super.show();
        this.updateContainerParameter(containerParameter, path);
    }
    updateContainerParameter(containerParameter: ContainerParameter, path: string) {
        this.sendMessage({ command: 'updateContainerParameter', parameter: containerParameter, repositoryPath: path });
    }
    updateNewEnvironmentLayout(newEnvironment: boolean) {
        if (!newEnvironment) {
            return;
        }
        let message: any = { command: 'newenvironment' };
        this.sendMessage(message);
    }
    getHtmlContentFile() {
        return join('out', 'webViews', 'newContainer', 'newContainer.htm');
    }
    dontLoadDefaultValues() {
        this.loadDefaultValues = false;
    }
    getViewType() {
        return "New Container";
    }
    processWebViewMessage(message: any) {
        if (message) {
            switch (message.command) {
                case 'documentLoaded':
                    this.documentLoaded = true;
                    this.onDocumentLoaded();
                    break;
                case 'dispose':
                    if (message.timeOut !== undefined) {
                        this.disposeWithTimeOut(message.timeOut);
                    } else {
                        this.dispose();
                    }
                    break;
                case 'createContainer':
                    this.createNewContainer(message.container, message.updateRepository, message.repositoryPath);
                    break;
                case 'selectlicensefile':
                    this.selectLicenseFile();
                    break;
                case 'selectsqlbackupfile':
                    this.selectSQLBackupFile();
                    break;
                case 'getLicensePath':
                    this.getLicensePath(message.version);
                    break;
            }
        }
    }
    disposeWithTimeOut(timeOut: number) {
        if (timeOut === 0) {
            this.dispose();
        } else {
            const timeout = setTimeout(() => this.dispose(), 3000);
        }
    }
    getLicensePath(version: string) {
        let majorVersion: number = parseInt(version.split('.')[0]);
        let path = getLicensePath(majorVersion);
        if (path !== '') {
            this.sendMessage({ command: 'licensefile', path: path });
        }
    }
    selectLicenseFile() {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        window.showOpenDialog({ title: 'Select Licensefile', canSelectFiles: true, filters: { 'License File': ['flf'], 'All files (*.*)': ['*'] }, openLabel: "OK" }).then((file) => {
            if (file === undefined) {
                return;
            }
            this.sendMessage({ command: 'licensefile', path: file[0].fsPath });
        });
    }
    selectSQLBackupFile() {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        window.showOpenDialog({ title: 'Select SQL Backupfile', canSelectFiles: true, filters: { 'Backup File (*.bak)': ['bak'], 'All files (*.*)': ['*'] }, openLabel: "OK" }).then((file) => {
            if (file === undefined) {
                return;
            }
            this.sendMessage({ command: 'sqlbackupfile', path: file[0].fsPath });
        });
    }
    sendErrorMessage(errorMessage: string) {
        this.sendMessage({ command: 'errorMessage', errorMessage: errorMessage });
    }
    createNewContainer(container: ContainerParameter, updateRepository: boolean = false, repositoryPath: string = '') {
        let config = workspace.getConfiguration(constants.extensionConfig);
        let storeContainerSettings = config.get(constants.storeContainerSettings);
        if (storeContainerSettings) {
            saveContainerSettings(container, repositoryPath);
        }
        let command = undefined;
        if (container.type !== 'Custom') {
            if (container.type === 'Insider') {
                let sasToken = config.get(constants.insiderbuildSastokenSettings);
                if (sasToken === '') {
                    this.sendErrorMessage('You need to add Microsoft insiderbuild-sasToken to your settingsfile');
                    window.showErrorMessage('You need to add Microsoft insiderbuild-sasToken to your settingsfile');
                    return;
                }
                command = `$ArtifactUrl = Get-BCArtifactUrl -country ${container.country.toLowerCase()} -select Latest -storageAccount bcinsider -sasToken "${sasToken}"`;
            } else {
                command = `$ArtifactUrl = Get-BCArtifactUrl -country ${container.country.toLowerCase()} -select Latest -type ${container.type} -version ${container.version}`;
            }
            Terminal.executeCmdLet(command, `Create New Container ${container.name}`, false);
        }
        if ((container.type !== 'Insider') && (container.licensefile === undefined || container.licensefile === '')) {
            // eslint-disable-next-line @typescript-eslint/naming-convention
            window.showOpenDialog({ title: 'Select License File', canSelectFiles: true, filters: { LicenseFile: ['flf'] }, openLabel: "OK" }).then((file) => {
                if (file === undefined) {
                    this.sendErrorMessage('No license file selected');
                    return;
                }
                container.licensefile = file[0].fsPath;
                this.createNewContainer(container);
            });
            return;
        }
        if (container.useexternaldatabase) {
            command = `$SecureSQLPassword = ConvertTo-SecureString "${container.sqlpassword}" -AsPlainText -Force;`;
            command += `$SQLCredential = New-Object PSCredential ("${container.sqlusername}", $SecureSQLPassword);`;
            Terminal.executeCmdLet(command, '', false);
        }
        if (container.repository !== '') {
            command = `New-EVDevelopmentEnvironment -ContainerName ${container.name} -GITRepositoryUrl ${container.repository}`;
            if (container.updateDependencies !== undefined) {
                if (container.updateDependencies === true) {
                    command += " -InstallDependencies";
                }
            }
            let mainRepositoryPath = config.get(constants.repositoryClonePathSettings);
            if (mainRepositoryPath !== undefined) {
                command += ` -MainRepositoryFolder "${mainRepositoryPath}"`;
            }
        }
        else {
            command = `New-EVContainer -ContainerName ${container.name}`;
        }
        if (container.type !== 'Custom') {
            command += ' -ArtifactUrl $ArtifactUrl';
        } else {
            command += ` -ImageTag ${container.tag} -DockerImageRepositoryUrl ${container.containerurl}`;
        }
        if ((container.licensefile !== undefined) && (container.licensefile !== '')) {
            command += ` -LicenseFilePath "${container.licensefile}"`;
        }
        command += ` -Isolation ${container.isolation} -Auth ${container.authentication} -ShortCutLocation ${container.createshortcut}`;
        if (container.usessl) {
            command += ' -useSSl';
        }
        if (container.includetesttoolkit) {
            command += ' -IncludeTestToolkit';
            if (container.testlibrariesonly) {
                command += ' -IncludeTestLibrariesOnly';
            }
        }
        if (container.ismultitenant) {
            command += ' -multitenant';
        }
        if (container.webclientport !== '') {
            command += ` -WebClientPort ${container.webclientport}`;
        }
        if (container.publishedports !== '') {
            command += ` -PublishPorts ${container.publishedports}`;
        }
        if (container.customparameter !== '') {
            command += ` -customParameter "${this.formatCustomParameter(container.customparameter)}"`;
        }
        if (container.useexternaldatabase) {
            command += ` -DatabaseServer ${container.databaseserver} -DatabaseName ${container.databasename} -DatabaseCredential $SQLCredential`;
            if (container.databaseinstance !== '') {
                command += ` -DatabaseInstance ${container.databaseinstance}`;
            }
        } else {
            if (container.sqlbackupfile !== '') {
                command += ` -bakFile "${container.sqlbackupfile}"`;
            }
        }
        Terminal.executeCmdLet(command, '', false);
        if (updateRepository && repositoryPath !== '') {
            command = `Update-ContainerFromRepository -ContainerName ${container.name} -RepositoryFolder "${repositoryPath}" -Auth ${container.authentication} -Credential $Credential`;
            if (container.updateDependencies !== undefined) {
                if (container.updateDependencies === true) {
                    command += " -InstallDependencies";
                }
            }
            Terminal.executeCmdLet(command, '', false);
        }
        updateServerConfig(container.name);
        //fs.writeFileSync(`c:\\temp\\${container.name} containerSettings.json`, JSON.stringify({ updateRepository: updateRepository, repositoryPath: repositoryPath, container: container }));
        this.dispose();
    }
    onDocumentLoaded() {
        if (!this.loadDefaultValues) {
            this.loadDefaultValues = true;
            return;
        }
        let config = workspace.getConfiguration(constants.extensionConfig);
        let message = {
            command: 'updatedefaultvalues',
            title: this.title,
            country: config.get(constants.containerCountrySettings),
            isolation: config.get(constants.useIsolationTypeSettings),
            createshortcut: config.get(constants.shortcutLocationSettings)
        };
        this.sendMessage(message);
    }
    formatCustomParameter(value: any) {
        let result = '';
        let arr = value.split('\n');
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].includes('"')) {
                let originalValue = arr[i];
                arr[i] = '';
                for (var x = 0; x < originalValue.length; x++) {
                    arr[i] += originalValue[x];
                    if (originalValue[x] === '"') {
                        arr[i] += '"';
                    }
                }
            }
            result += `${arr[i]},`;
        }
        if (result !== '') {
            return `{${result.substring(0, result.length - 1)}}`;
        }
        return result;
    }
}
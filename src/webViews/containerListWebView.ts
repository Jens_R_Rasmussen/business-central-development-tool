import * as vscode from 'vscode';
import { WebViewControler } from './webViewControler';
import path = require('path');
import { DockerContainer } from '../dockerLibrary';
import { Terminal, Process } from '../executeLibrary';
import * as constants from '../constants';
import { OutputChannel } from '../outputChannel';

export class ContainerList extends WebViewControler {
    private dockerContainer: DockerContainer;
    constructor(context: vscode.ExtensionContext, title: string) {
        super(context, title);
        this.dockerContainer = new DockerContainer(context);
    }
    getHtmlContentFile() {
        return path.join('out', 'webViews', 'containerList', 'containerList.htm');
    }
    getViewType() {
        return constants.containerPanelTitle;
    }
    onDocumentLoaded() {
        this.sendMessage({ command: 'updateSetting', checked: vscode.workspace.getConfiguration(constants.extensionConfig).get(constants.showContainerWebpanelOnStartupSettings) });
        this.updateTableBody();
    }
    reloadWebViewContent() {
        this.documentLoaded = false;
        if (this.panel) {
            if (!this.html) {
                this.html = this.loadHtmlContent();
                this.panel.webview.html = this.html;
            }
            else {
                this.updateTableBody();
            }
        }
    }

    private updateTableBody() {
        let containers = this.dockerContainer.getContainers().filter((item: any) => { return (item.Version !== 'Unknown'); });
        let html = '';
        containers.sort(this.sortByProperty("Names"));
        containers.forEach((item: any) => {
            try {
                let containerConfig = this.dockerContainer.getContainerConfigValues(item.Names);
                let containerType = containerConfig.isSandBox ? 'Sandbox' : 'OnPrem';
                let expired = this.dockerContainer.isContainerExpired(containerConfig);
                let status = item.Status.startsWith('Up') ? 'Running' : 'NotRunning';
                let columnClass = expired ? 'ColumnHeaderLeftExpired' : 'ColumnHeaderLeft';
                html += `<tr id=ContainerRow-${item.Names}>
                        <td class=${columnClass}>
                            <p><span id=Name >${item.Names}</span></p>
                        </td>
                        <td class=${columnClass}>
                            <p><span id=Version >${item.Version}</span></p>
                        </td>
                        <td class=${columnClass}>
                            <p><span id=Version >${getStringDate(item.CreatedAt)}</span></p>
                        </td>
                        <td class=${columnClass}>
                            <p><span id=Version >${containerType}</span></p>
                        </td>
                        <td class=${columnClass}>
                            <p><span id="Status-${item.Names}" >${status === 'NotRunning' ? 'Not Running' : status}</span></p>
                        </td>
                        <td class="button">
                        <div class="dropdown" id="dropdown-${item.Names}">    
                            <button class="dropdownButton" id="button-${item.Names}">Select</button>`;
                html += this.getDropdownHtml(expired, status, item.Names);
                html += `</div>
                    </td>
                </tr>`;
            } catch (e: any) {
                console.log(e);
            }
        });
        this.sendMessage({ command: 'tableBody', data: html });
    }

    sortByProperty(property: string) {
        return function (a: { [x: string]: string; }, b: { [x: string]: string; }) {
            if (a[property].toUpperCase() > b[property].toUpperCase()) {
                return 1;
            }
            else if (a[property].toUpperCase() < b[property].toUpperCase()) {
                return -1;
            }
            return 0;
        };
    }
    getDropdownHtml(expired: boolean, status: string, containerName: string) {
        let html = `<div class="dropdown-content" id="dropdown-content-${containerName}">`;
        if (status === 'NotRunning') {
            html += `<a onClick="containerList.onPostLoadingMessage('start', '${containerName}')">Start</a>`;
        } else {
            html += `<a onClick="containerList.onPostLoadingMessage('stop', '${containerName}')">Stop</a>`;
        }
        if (expired) {
            html += `<a onClick="containerList.onPostMessage('newContainer', '${containerName}')">Create New Container</a>`;
        }
        if (status === 'Running') {
            html += `<a onClick="containerList.onPostMessage('runclient', '${containerName}')">Open Client</a>
                <a onClick="containerList.onPostMessage('backup', '${containerName}')">Backup Database</a>
                <a onClick="containerList.onPostMessage('restore', '${containerName}')">Restore Database</a>
                <a onClick="containerList.onPostMessage('license', '${containerName}')">Update License</a>
                <a onClick="containerList.updateDevLicense('${containerName}')">Update Developer License</a>

                <a onClick="containerList.onPostMessage('addtestusers', '${containerName}')">Add Test Users</a>
                <a onClick="containerList.onPostMessage('addnewuser', '${containerName}')">Add New User</a>
                
                <!-- <a onClick="containerList.onPostMessage('install', '${containerName}')">Install App</a>
                <a onClick="containerList.onPostMessage('uninstall', '${containerName}')">Uninstall App</a>
                <a onClick="containerList.onPostMessage('unpublish', '${containerName}')">Unpublish App</a> -->

                <a onClick="containerList.onPostMessage('loadapplist', '${containerName}')">Extension Management</a>
                <a onClick="containerList.onPostMessage('installaddins', '${containerName}')">Install NST Server Add-ins</a>
                <a onClick="containerList.onPostMessage('editservicesettings', '${containerName}')">Edit NST Settings</a>
                <a onClick="containerList.onPostMessage('editwebclientsettings', '${containerName}')">Edit Web Client Settings</a>
                <a onClick="containerList.onPostMessage('installfonts', '${containerName}')">Install Fonts</a>
                <a onClick="containerList.onPostMessage('openterminalincontainer', '${containerName}')">Open Terminal</a>
                <a onClick="containerList.onPostMessage('opensharedfolder', '${containerName}')">Open Shared Folder</a>`;
        }
        html += `<a onClick="containerList.onPostLoadingMessage('delete', '${containerName}')">Delete</a>
            </div>`;
        return html;

    }
    processWebViewMessage(message: any) {
        if (message) {
            switch (message.command) {
                case 'documentLoaded':
                    this.documentLoaded = true;
                    this.onDocumentLoaded();
                    break;
                case 'start':
                    this.startStopContainer(true, message.containerName);
                    break;
                case 'stop':
                    this.startStopContainer(false, message.containerName);
                    break;
                case 'delete':
                    this.deleteContainer(message);
                    break;
                case 'runclient':
                    this.dockerContainer.runClient(message.containerName);
                    break;
                case 'license':
                    this.dockerContainer.importContainerLicense(message.containerName);
                    break;
                case 'updateDevLicense':
                    let majorVersion = message.version.split('.')[0];
                    this.dockerContainer.updateLicenseFromMajorVersion(majorVersion, message.containerName);
                    break;
                case 'backup':
                    this.dockerContainer.backupContainerDatabase(message.containerName);
                    break;
                case 'restore':
                    this.dockerContainer.restoreContainerDatabase(message.containerName);
                    break;
                case 'installNewApp':
                    this.dockerContainer.installNewAppInContainer(message.containerName, message.forceSync);
                    break;
                case 'loadapplist':
                    this.sendMessage({ command: 'showLoadingApps', containerName: message.containerName });
                    break;
                case 'readapplist':
                    this.getAppList(message.containerName);
                    break;
                case 'uninstall':
                    this.uninstallAppInContainer(message);
                    break;
                case 'install':
                    this.installAppInContainer(message);
                    break;
                case 'unpublish':
                case 'unpublishcleanmode':
                    this.unpublishAppInContainer(message);
                    break;
                case 'updatesetting':
                    let config = vscode.workspace.getConfiguration(constants.extensionConfig);
                    let setting = config.inspect(constants.showContainerWebpanelOnStartupSettings);
                    let target = 1;
                    if (setting !== undefined) {
                        if (setting.workspaceFolderValue !== undefined) {
                            target = 3;
                        } else
                            if (setting.workspaceValue !== undefined) {
                                target = 2;
                            }
                    }
                    config.update(constants.showContainerWebpanelOnStartupSettings, message.checked, target);
                    break;
                case 'installaddins':
                    this.dockerContainer.installAddIns(message.containerName);
                    break;
                case 'installfonts':
                    this.dockerContainer.installFonts(message.containerName);
                    break;
                case 'openterminalincontainer':
                    this.dockerContainer.openTerminalInContainer(message.containerName);
                    break;
                case 'opensharedfolder':
                    this.dockerContainer.openSharedFolder(message.containerName);
                    break;
                case 'editservicesettings':
                    this.dockerContainer.editServiceSettings(message.containerName);
                    break;
                case 'editwebclientsettings':
                    this.dockerContainer.editWebClientSettings(message.containerName);
                    break;
                case 'addtestusers':
                    this.dockerContainer.addContainerTestUsersCommand(message.containerName);
                    break;
                case 'addnewuser':
                    this.dockerContainer.addNewContainerUserCommand(message.containerName);
                    break;
                case 'newContainer':
                    this.newContainerFromExistingContainer(message);
            }
        }
    }
    newContainerFromExistingContainer(message: any) {
        this.dockerContainer.newContainerFromExistingContainer(message.containerName);
    }
    getAppList(containerName: string) {
        let html = '';
        let appList = this.dockerContainer.getAppList(containerName);
        if (appList !== undefined) {
            appList.sort(this.sortByProperty("Name"));
            appList.forEach((item: any) => {
                item.Version = this.getVersionAsString(item.Version);
                html += `<tr id="appRow-${item.Name.replace(/\s|\W/g, '-')}-${item.Version.replace(/\./g, '-')}">
                    <td class=ColumnHeaderLeft>
                        <p><span id=appName >${item.Name}</span></p>
                    </td>
                    <td class=ColumnHeaderLeft>
                        <p><span id=appPublisher >${item.Publisher}</span></p>
                    </td>
                    <td class=ColumnHeaderLeft>
                        <p><span id=appVersion >${item.Version}</span></p>
                    </td>
                    <td class=ColumnHeaderLeft>
                        <p><span id="Installed" >${item.IsInstalled}</span></p>
                    </td>
                    <td class="button">
                    <div class="dropdown" id="appdropdown-${item.Name}">    
                        <button class="dropdownButton" id="appbutton">Select</button>
                        <button style="display: none;" class="dropdownButton loading" id="loadingbutton">Processing</button>`;
                html += this.getAppDropdownHtml(item, containerName);
                html += `</div>
                    </td>
                    </tr>`;
            });
            this.sendMessage({ command: 'appTableBody', data: html, containerName: containerName });
        } else {
            this.sendMessage({ command: 'appTableBodyError', containerName: containerName });
        }
    }
    getVersionAsString(version: any): string {
        let type = typeof version;
        if (type === 'string') {
            return version;
        }
        return `${version.Major}.${version.Minor}.${version.Build}.${version.Revision}`;
    }
    getAppDropdownHtml(item: any, containerName: string) {
        let html = `<div class="dropdown-content" id="dropdown-content-${item.AppId}">`;
        if (item.IsInstalled) {
            html += `<a id="uninstall" style="display: block;" onClick="containerList.sendMessage({ command: 'uninstall', id: '${item.AppId}', name: '${item.Name}', version: '${item.Version}', containerName: '${containerName}' })">Uninstall App</a>`;
            html += `<a id="install" style="display: none;" onClick="containerList.sendMessage({ command: 'install', id: '${item.AppId}', name: '${item.Name}', version: '${item.Version}', containerName: '${containerName}' })">Install App</a>`;
        } else {
            html += `<a id="uninstall" style="display: none;" onClick="containerList.sendMessage({ command: 'uninstall', id: '${item.AppId}', name: '${item.Name}', version: '${item.Version}', containerName: '${containerName}' })">Uninstall App</a>`;
            html += `<a id="install" style="display: block;" onClick="containerList.sendMessage({ command: 'install', id: '${item.AppId}', name: '${item.Name}', version: '${item.Version}', containerName: '${containerName}' })">Install App</a>`;
        }
        html += `<a onClick="containerList.sendMessage({ command: 'unpublish', id: '${item.AppId}', name: '${item.Name}', version: '${item.Version}', containerName: '${containerName}' })">Unpublish App</a>
                <a onClick="containerList.sendMessage({ command: 'unpublishcleanmode', id: '${item.AppId}', name: '${item.Name}', version: '${item.Version}', containerName: '${containerName}' })">Unpublish App (Remove Data)</a>
            </div>`;
        return html;

    }
    startStopContainer(start: boolean, containerName: string) {
        OutputChannel.createChannal(containerName);
        let result: any;
        let command = `Start-BcContainer -containerName ${containerName}`;
        if (start) {
            OutputChannel.appendLine(containerName, `Execute command: ${command}`);
            // result = Process.execSyncPowershellCommand(`Import-Module (Join-Path $env:TEMP 'BcContainerHelper/BcContainerHelper.psm1'); ${command}; Clear-Host; Write-Host 'response:${containerName}';`);
            result = Process.execPowershellCommand(`${command}; Clear-Host; Write-Host 'response:${containerName}';`);
        } else {
            command = `Stop-BcContainer -containerName ${containerName}`;
            OutputChannel.appendLine(containerName, `Execute command: ${command}`);
            // result = Process.execSyncPowershellCommand(`Import-Module (Join-Path $env:TEMP 'BcContainerHelper/BcContainerHelper.psm1'); ${command}; Clear-Host; Write-Host 'response:${containerName}';`);
            result = Process.execPowershellCommand(`${command}; Clear-Host; Write-Host 'response:${containerName}';`);
        }
        result.stdout.on('data', (data: any, ttt: any) => {
            data = data.trim();
            if (data.startsWith('response:')) {
                let info = `Container '${data.split(':')[1]}' is now stopped`;
                if (start) {
                    info = `Container '${data.split(':')[1]}' is now running`;
                }
                OutputChannel.appendLine(containerName, info);
                vscode.window.showInformationMessage(info);
                let status = start ? 'Running' : 'NotRunning';
                let isExpired = this.dockerContainer.isContainerExpired(this.dockerContainer.getContainerConfigValues(containerName));
                this.sendMessage({ command: 'updateStatus', data: data.split(':')[1], dropdown: this.getDropdownHtml(isExpired, status, containerName) });
            } else {
                if ((!data.startsWith('WARNING:')) && (!data.startsWith('BcContainerHelper')) && (data !== '')) {
                    OutputChannel.appendLine(containerName, data);
                }
            }
        });
        result.stderr.on('data', (data: any) => {
            if (!data.startsWith('Import-Module')) {
                vscode.window.showErrorMessage(data);
            }
        });
        result.on('close', () => {
            result.kill();
        });
    }
    deleteContainer(message: any) {
        vscode.window.showInformationMessage(`Are you sure you want to delete container: ${message.containerName}`, ...['Yes', 'No']).then(selection => {
            if (selection === 'Yes') {
                let command = `Remove-BcContainer -containerName ${message.containerName}`;
                OutputChannel.createChannal(message.containerName);
                OutputChannel.appendLine(message.containerName, `Execute command: ${command}`);
                let result: any = Process.execPowershellCommand(`${command}; Clear-Host; Write-Host 'response:${message.containerName}';`);
                result.stdout.on('data', (data: any) => {
                    data = data.trim();
                    if (data.startsWith('response:')) {
                        let info = `Container '${data.split(':')[1]}' is now deleted`;
                        OutputChannel.appendLine(message.containerName, info);
                        vscode.window.showInformationMessage(info);
                        this.sendMessage({ command: 'delete', data: data.split(':')[1] });
                        this.removeContainerFolder(data.split(':')[1]);
                    } else {
                        if ((!data.startsWith('WARNING:')) && (!data.startsWith('BcContainerHelper')) && (data !== '')) {
                            OutputChannel.appendLine(message.containerName, data);
                        }
                    }
                });
                result.stderr.on('data', (data: any) => {
                    if (!data.startsWith('Import-Module')) {
                        vscode.window.showErrorMessage(data);
                    }
                });
                result.on('close', () => {
                    result.kill();
                });
            } else {
                let status = message.status === 'Running' ? 'Running' : 'NotRunning';
                let isExpired = this.dockerContainer.isContainerExpired(this.dockerContainer.getContainerConfigValues(message.containerName));
                this.sendMessage({ command: 'dontDelete', data: message.containerName, dropdown: this.getDropdownHtml(isExpired, status, message.containerName) });
            }
        });

    }
    removeContainerFolder(containerName: string) {
        let command = `$ShortCutFolder = "$([Environment]::GetFolderPath("Desktop"))\\Docker Container Shortcuts\\${containerName}\\"
        if (Test-Path $ShortCutFolder -ErrorAction SilentlyContinue) {
            Remove-Item -Path $ShortCutFolder -Recurse -Force -ErrorAction SilentlyContinue
        }
        $ShortCutFolder = "$([Environment]::GetFolderPath("MyDocuments"))\\Docker Container Shortcuts\\${containerName}\\"
        if (Test-Path $ShortCutFolder -ErrorAction SilentlyContinue) {
            Remove-Item -Path $ShortCutFolder -Recurse -Force -ErrorAction SilentlyContinue
        }`;
        Terminal.executeCmdLet(command);
    }

    onPanelClosed() {
        this.dispose();
    }
    uninstallAppInContainer(item: any) {
        let errorOccurred = false;
        this.sendMessage({ command: 'processapp', data: `${item.name.replace(/\s|\W/g, '-')}-${item.version.replace(/\./g, '-')}` });
        let command = `UnInstall-BcContainerApp -containerName ${item.containerName} -appName "'${item.name}'" -appVersion ${item.version}`;
        OutputChannel.createChannal(item.containerName);
        OutputChannel.appendLine(item.containerName, `Execute command: ${command}`);
        let result: any = Process.execPowershellCommand(`${command}; Clear-Host; Write-Host "'{Done:${item.name}},{Version:${item.version}}'";`);
        result.stdout.on('data', (data: any) => {
            let doneResponse = data.match('{Done:(.+)},');
            if ((doneResponse !== undefined) && (doneResponse !== null) && (errorOccurred === false)) {
                let versionResponse = data.match(',{Version:(.+)}');
                this.sendMessage({ command: 'notInstalled', data: `${doneResponse[1].replace(/\s|\W/g, '-')}-${versionResponse[1].replace(/\./g, '-')}` });
                OutputChannel.appendLine(item.containerName, `Done Uninstall: '${item.name}' Version: ${item.version}`);
            } else {
                if ((!data.startsWith('WARNING:')) && (!data.startsWith('BcContainerHelper')) && (data !== '')) {
                    OutputChannel.appendLine(item.containerName, data);
                }
            }
        });
        result.stderr.on('data', (data: any) => {
            if (!data.startsWith('Import-Module')) {
                errorOccurred = true;
                vscode.window.showErrorMessage(data, item);
                this.sendMessage({ command: 'installed', data: `${item.name.replace(/\s|\W/g, '-')}-${item.version.replace(/\./g, '-')}` });
            }
        });
        result.on('close', () => {
            result.kill();
        });
    }
    installAppInContainer(item: any) {
        let errorOccurred = false;
        this.sendMessage({ command: 'processapp', data: `${item.name.replace(/\s|\W/g, '-')}-${item.version.replace(/\./g, '-')}` });
        let command = `Install-BcContainerApp -containerName ${item.containerName} -appName '${item.name}' -appVersion ${item.version}`;
        OutputChannel.createChannal(item.containerName);
        OutputChannel.appendLine(item.containerName, `Execute command: ${command}`);
        let result: any = Process.execPowershellCommand(`${command}; Clear-Host; Write-Host "'{Done:${item.name}},{Version:${item.version}}'";`);
        result.stdout.on('data', (data: any) => {
            let doneResponse = data.match('{Done:(.+)},');
            if ((doneResponse !== undefined) && (doneResponse !== null) && (errorOccurred === false)) {
                let versionResponse = data.match(',{Version:(.+)}');
                this.sendMessage({ command: 'installed', data: `${doneResponse[1].replace(/\s|\W/g, '-')}-${versionResponse[1].replace(/\./g, '-')}` });
                OutputChannel.appendLine(item.containerName, `Done Install: '${item.name}' Version: ${item.version}`);
            } else {
                if ((!data.startsWith('WARNING:')) && (!data.startsWith('BcContainerHelper')) && (data !== '')) {
                    OutputChannel.appendLine(item.containerName, data);
                }
            }
        });
        result.stderr.on('data', (data: any) => {
            if (!data.startsWith('Import-Module')) {
                errorOccurred = true;
                vscode.window.showErrorMessage(data, item);
                this.sendMessage({ command: 'notInstalled', data: `${item.name.replace(/\s|\W/g, '-')}-${item.version.replace(/\./g, '-')}` });
            }
        });
        result.on('close', () => {
            result.kill();
        });
    }
    unpublishAppInContainer(item: any) {
        this.sendMessage({ command: 'processapp', data: `${item.name.replace(/\s|\W/g, '-')}-${item.version.replace(/\./g, '-')}` });
        let command = undefined;
        if (item.command === 'unpublish') {
            command = `UnPublish-BcContainerApp -containerName ${item.containerName} -appName "'${item.name}'" -version ${item.version} -unInstall`;
        } else {
            command = `UnPublish-BcContainerApp -containerName ${item.containerName} -appName "'${item.name}'" -version ${item.version} -unInstall -doNotSaveData`;
        }
        OutputChannel.createChannal(item.containerName);
        OutputChannel.appendLine(item.containerName, `Execute command: ${command}`);
        let result: any = Process.execPowershellCommand(`${command}; Clear-Host; Write-Host "'{Done:${item.name}},{Version:${item.version}}'";`);
        result.stdout.on('data', (data: any) => {
            let doneResponse = data.match('{Done:(.+)},');
            if (doneResponse !== undefined) {
                let versionResponse = data.match(',{Version:(.+)}');
                this.sendMessage({ command: 'unpublished', data: `${doneResponse[1].replace(/\s|\W/g, '-')}-${versionResponse[1].replace(/\./g, '-')}` });
                OutputChannel.appendLine(item.containerName, `Done Unpublish: '${item.name}' Version: ${item.version}`);
            } else {
                if ((!data.startsWith('WARNING:')) && (!data.startsWith('BcContainerHelper')) && (data !== '')) {
                    OutputChannel.appendLine(item.containerName, data);
                }
            }
        });
        result.stderr.on('data', (data: any) => {
            if (!data.startsWith('Import-Module')) {
                vscode.window.showErrorMessage(data);
            }
        });
        result.on('close', () => {
            result.kill();
        });
    }
}
function getStringDate(value: string): string {
    // "2022-04-27 10:12:03 +0200 CEST"
    let result = new Date(`${value.substring(0, 10)}T${value.substring(11, 19)}${value.substring(20, 25)}`);
    return `${result.getFullYear()}-${getMissingSero(result.getMonth() + 1)}-${getMissingSero(result.getDate())} ${getMissingSero(result.getHours())}:${getMissingSero(result.getMinutes())}:${getMissingSero(result.getSeconds())}`;
}
function getMissingSero(value: number): string {
    if (value <= 9) {
        return `0${value}`;
    }
    return `${value}`;
}


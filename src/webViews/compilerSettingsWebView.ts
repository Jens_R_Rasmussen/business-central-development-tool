import * as constants from '../constants';
import path = require("path");
import { WebViewControler } from "./webViewControler";
import { Compiler, getAllCompilerSettingsInWorkspace } from '../CompilerSettings/compiler';
import { ExtensionContext, window, workspace, WorkspaceFolder } from 'vscode';
import { CompilerSettings } from '../CompilerSettings/compilerSettings';
import { existsSync, readFileSync, writeFileSync } from 'fs';
import { getWorkspaceFolderFromPath } from '../workspaceFileAndFolderHelper';
export class CompilerSettingsView extends WebViewControler {
    compiler: Compiler;
    constructor(context: ExtensionContext, title: string) {
        super(context, title);
        this.compiler = new Compiler(context);
    }

    getViewType() {
        return constants.compilerSettingsPanelTitle;
    }
    getHtmlContentFile() {
        return path.join('out', 'webViews', 'compilerSettings', 'index.htm');
    }
    onPanelClosed() {
        this.dispose();
    }
    onDocumentLoaded(): void {
        let data = getAllCompilerSettingsInWorkspace();
        this.sendMessage({ command: 'initCompilerSettings', data: data });
    }
    reloadWebViewContent() {
        this.documentLoaded = false;
        if (this.panel) {
            if (!this.html) {
                this.html = this.loadHtmlContent();
                this.panel.webview.html = this.html;
            }
            else {
                let newdata = getAllCompilerSettingsInWorkspace();
                this.sendMessage({ command: 'updateCompilerSettings', data: newdata });
            }
        }
    }
    async processWebViewMessage(message: any) {
        if (message) {
            switch (message.command) {
                case 'documentLoaded':
                    this.documentLoaded = true;
                    this.onDocumentLoaded();
                    break;
                case 'updateapp':
                    this.updateFilesFromData(message.data, true, false);
                    break;
                case 'updatelaunch':
                    this.updateFilesFromData(message.data, false, true);
                    break;
                case 'updateboth':
                    this.updateFilesFromData(message.data, true, true);
                    break;
                case 'edit':
                    this.openEditCompilerSettings(message);
                    break;
                case 'copy':
                    this.copyCompilerSettings(message.data.item);
                    break;
            }
        }
    }
    async updateFilesFromData(data: any, updateAppFile: boolean, updateLaunch: boolean) {
        let settings = data.allData.filter((element: any) => {
            return element.compilerSettings.some((element: any) => element.name === data.item.name);
        });
        if (settings.length > 1) {
            let response = await window.showInformationMessage(`Setting '${data.item.name}' exist on ${settings.length} folders do you want to update all folders?`, { modal: true }, { title: 'Yes', isCloseAffordance: false }, { title: 'No', isCloseAffordance: true });
            if (response?.title === 'Yes') {
                settings.forEach((element: any) => {
                    this.updateFiles(updateAppFile, { item: element.compilerSettings.find((element: any) => element.name === data.item.name), path: element.compilerSettingsPath }, updateLaunch);
                });
                return;
            }
        }
        this.updateFiles(updateAppFile, data, updateLaunch);

    }
    private updateFiles(updateAppFile: boolean, data: any, updateLaunch: boolean) {
        if (updateAppFile) {
            this.updateAppFile(data.item, data.path);
        }
        if (updateLaunch) {
            this.updateLaunch(data.item, data.path);
        }
    }

    async copyCompilerSettings(data: any) {
        if (workspace.workspaceFolders === undefined) {
            return;
        }
        let result: WorkspaceFolder | any;
        if (workspace.workspaceFolders.length === 1) {
            result = workspace.workspaceFolders.map((item: WorkspaceFolder) => ({ label: item.name, description: item.uri.fsPath }))[0];
        }
        result = await window.showQuickPick(workspace.workspaceFolders?.map((item: WorkspaceFolder) => ({ label: item.name, description: item.uri.fsPath })), { placeHolder: 'Select to workspace', canPickMany: false });
        if (result === undefined) {
            return;
        }
        window.showInputBox({ title: 'New Settings Name', value: data.name }).then((name: string | any) => {
            if (name !== undefined) {
                data.name = name;
                let compilerSettings: CompilerSettings[] = this.getEVCompilerSettingsList(result.description);
                compilerSettings.push(data);
                this.setEVCompilerSettingsList(result.description, compilerSettings);
                let newdata = getAllCompilerSettingsInWorkspace();
                this.sendMessage({ command: 'updateCompilerSettings', data: newdata });
            }
        });
    }
    updateAppFile(item: CompilerSettings, path: any) {
        this.compiler.updateAppFile(item, path);
    }
    updateLaunch(item: CompilerSettings, path: any) {
        this.compiler.updateWorkspaceLaunch(<WorkspaceFolder>getWorkspaceFolderFromPath(path), item);
    }

    private openEditCompilerSettings(message: any) {
        this.compiler.openEditCompilerSettings(message.data.path, message.data.item.name);
    }
    private setEVCompilerSettingsList(filePath: string, compilerSettingsList: CompilerSettings[]) {
        writeFileSync(this.getCompilerFilePath(filePath), JSON.stringify(compilerSettingsList, this.compiler.replacer, 4));
    }

    private getEVCompilerSettingsList(filePath: string): CompilerSettings[] {
        let compilerFilePath = this.getCompilerFilePath(filePath);
        if (!existsSync(compilerFilePath)) {
            let compilerSettingsList: CompilerSettings[] = [];
            return compilerSettingsList;
        }
        let data = readFileSync(compilerFilePath);
        let compilerSettingsList: CompilerSettings[] = JSON.parse(data.toString("utf-8"));
        return compilerSettingsList;
    }

    private getCompilerFilePath(filePath: string) {
        let appfilePath = getWorkspaceFolderFromPath(filePath);
        if (!appfilePath) {
            return constants.compilerFileName;
        }
        return `${appfilePath.uri.fsPath}\\${constants.compilerFileName}`;
    }
}